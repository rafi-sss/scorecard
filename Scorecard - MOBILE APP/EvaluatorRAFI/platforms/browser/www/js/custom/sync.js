var db = window.openDatabase("Evaluator Database", "1.0", "Evaluator RAFI Database", 1000000);
var kra_name, tk_weight, detail_name, ti_weight, lop_code, lop_letter, lop_value, lop_description, executeQuery;

$(document).ready(function(){
	var ipConfigs = new ipConfig();
	function getParameterByName(name, url) {
    if (!url) {
      url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
    }	

    function syncAvailable(){
    	db.transaction(function(tx){
		tx.executeSql("SELECT * FROM scorecard",[],function(tx,result){
		   var len = result.rows.length;
		   if(len == 0){
		   	$(".sync-status").html("Syncing Available Scorecard...");
		   	syncScorecards();
		   }else{
		   	$(".sync-status").html("Redirecting to dashboard, please wait");
		   	window.location.replace("dashboard.html");
		   }
		});
		});
    }

    syncAvailable();

	function syncScorecards(){
		var sh = getParameterByName("sh");
		$.ajax({
			type: "GET",
			url: "http://"+ipConfigs.ip+"/phpFile/actions/sync.php",
			data: "sh="+sh,
			success: function(data){
				var obj = $.parseJSON(data);
				var count = obj.length;

				for(var i=0; i<count; i++){
					kra_name = obj[i].kra_name; tk_weight = obj[i].tk_weight; detail_name = obj[i].detail_name; ti_weight = obj[i].ti_weight; lop_code = obj[i].lop_code;lop_letter = obj[i].lop_letter;lop_value = obj[i].lop_value;lop_description = obj[i].lop_description;

					(function(i){
						var executeQuery = "INSERT INTO scorecard (kra_name, tk_weight, detail_name, ti_weight, lop_code, lop_letter, lop_value, lop_description) VALUES ('"+kra_name+"',"+tk_weight+",'"+detail_name+"',"+ti_weight+",'"+lop_code+"','"+lop_letter+"',"+lop_value+",'"+lop_description+"')";
						db.transaction(function(tx){
							tx.executeSql(executeQuery, [], successCB, errorCB);
						}, errorCB);
					})(i);
				}

				$(".sync-status").html("Syncing Success.. redirecting to dashboard, please wait");
				// window.location.replace("evaluationpage.html");
			}
		});
	}

	function errorCB(err) {
	 alert("Error processing SQLAFDSDFSDFSDFSDF: "+err.code);
	}

	function successCB() {
	 console.log("success");
	}
});