var db = window.openDatabase("Evaluator Database", "1.0", "Evaluator RAFI Database", 1000000), res = 0, totalSC = 0, ratedSC = 0, kracount = 0, dd = 0, ctr = 0, minCtr = 0, secCtrDefault = 59, hrCtr = 0, currentSec = 0, diffH = 0, diffM = 0, diffSec = 0;

$(document).ready(function(){
    var ipConfigs = new ipConfig();
    //get param
	function getParameterByName(name, url) {
    if (!url) {
      url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    $('#btd').on('click', function(){
        db.transaction(function(tx){
            tx.executeSql('INSERT INTO epiryDate (hours, minutes, seconds) VALUES(?,?,?)', [hrCtr, minCtr, secCtrDefault], function(){
                    window.location.replace("dashboard.html");
            });
        });
    });


    //timer
    var remTime = setInterval(function(){
        ctr++;
        //use default value if currentSec is = 0
        if(currentSec == 0){
            currentSec = 1;
        }
        secCtrDefault = currentSec;
        secCtrDefault-=ctr;
        if(secCtrDefault == 0){
            setTimeout(function(){
                // minCtr-=1;
                    if(minCtr > 0){
                        minCtr-=1;
                    }else{
                        minCtr=60;
                        minCtr-=1;
                        hrCtr-=1;
                    }
                secCtrDefault = 60;
                currentSec = secCtrDefault;
            },500);
            ctr = 0;
        }
        //stop timer
        if(hrCtr == 0 && minCtr == 0 &&secCtrDefault == 0){
            console.log("stop");
            updateProjStat();
            $('#timex').modal('show');
            hrCtr = 0;
            minCtr = 0;
            secCtrDefault = 0;
            clearInterval(remTime);
        }
        //add 0 if length is 1
        if(secCtrDefault.toString().length == 1)
            secCtrDefault = '0' + secCtrDefault;
        if(minCtr.toString().length == 1)
            minCtr = '0' + minCtr;
        if(hrCtr.toString().length == 1)
            hrCtr = '0' + hrCtr;

        $('.timearea').html('<b class="label label-danger" style="color: #ffffff;font-weight: 600;font-size: 15px;margin: 5px;">Time left '+hrCtr+':'+minCtr+':'+secCtrDefault+'</b>'); //append the display

        $('#loader').modal('hide');
    }, 1000);

    //update projstatus
    function updateProjStat(){
        console.log('udpatedz');
        db.transaction(function(tx){
            tx.executeSql('UPDATE project_name SET status = 2', [], function(){
                queryAllKRA();
            });
        });
    }

    //get time difference
    function difference(){
        db.transaction(function(tx){
            tx.executeSql('SELECT * FROM loginTime ORDER BY rowid DESC LIMIT 2', [], function(tx, result){
                if(result.rows.length > 1){
                    var date = new Date();
                    var logHrOld = Number(result.rows.item(1).hoursLog);
                    var logHrLatest = Number(result.rows.item(0).hoursLog);
                    var logTimeH = logHrLatest - logHrOld; // 12

                    var logMinOld = Number(result.rows.item(1).minutesLog);
                    var logMinLatest = Number(result.rows.item(0).minutesLog);
                    var logTimeM = logMinLatest - logMinOld;

                    if(logTimeH < 0){
                        logTimeH*=-1;
                    }

                    if(logTimeM < 0){
                        logTimeM*=-1;
                    }

                    diffH = logTimeH; // 12
                    diffM = logTimeM;

                    console.log('sds'+diffM);
                    timeRem();
                }else{
                    diffH = 0;
                    diffM = 0;
                    timeRem();
                }
            });
        });
    }    

    difference();

    //get time remaining
    function timeRem(){
        $('#loader').modal('show');
        db.transaction(function(tx){
            tx.executeSql('SELECT * FROM epiryDate ORDER BY rowid DESC LIMIT 1', [], function(tx, result){

                    hrCtr = Number(result.rows.item(0).hours) - diffH // 12;
                    minCtr = result.rows.item(0).minutes;
                    currentSec = result.rows.item(0).seconds;
                    console.log(result.rows.item(0).hours);
                    console.log(hrCtr);
                    if(hrCtr < 0){
                        hrCtr = 0;
                    }

                    if(hrCtr == 0){
                        minCtr = minCtr - diffM;
                        console.log('dfdf'+minCtr);
                    }

                    if(minCtr < 0){
                        minCtr = 0;
                    }

                    loadKRA();
            });
        });
    }

    //save time left
    function saveTimeLeft(kra_id, total){
        db.transaction(function(tx){
            tx.executeSql('INSERT INTO epiryDate (hours, minutes, seconds) VALUES(?,?,?)', [hrCtr, minCtr, secCtrDefault], function(){
                    window.location.replace("indicators.html?kra_id="+kra_id+"&sc_id="+getParameterByName("sc_id")+"&round_number="+getParameterByName("round_number")+"&evalid="+getParameterByName("evalid")+"&sctype="+getParameterByName("sctype")+"&total="+total);
            });
        });
    }

	function loadKRA(){
		db.transaction(function(tx){
		tx.executeSql("SELECT * FROM kra",[],function(tx,result){
		   var len = result.rows.length;
           kracount = len;
		   var a = 0;
		   for(var i=0;i<len; i++){
		   	var kra_id = result.rows.item(i).kra_id;
		   	a++;

		   	$(".kra-list").append('<li class="contain"><div class="row kra-list-cont" id="chooseKRA" data-kra-id='+result.rows.item(i).kra_id+'><div class="col-xs-3 col"><p class="inline-block rel"><i class="fa fa-exclamation-circle stats" aria-hidden="true" id="staticon'+kra_id+'"></i><span class="" id="status'+kra_id+'" data-complete="incomplete"></span></p></div><div class="col-xs-9 col">'+result.rows.item(i).kra_name+'</div></div></li>');

		   	getNumberPerKRA(kra_id);
		   }

		});
		});
	}

	function getNumberPerKRA(kra_id){
		db.transaction(function(tx){
		tx.executeSql("SELECT COUNT(indicator.parent_id) as cc, COUNT(CASE WHEN indicator.status = 1 THEN indicator.status END) as ss ,indicator.kra_id, indicator.status FROM indicator LEFT JOIN kra ON indicator.kra_id = kra.kra_id  WHERE kra.kra_id = ? AND indicator.has_child = 0 ",[kra_id],function(tx,result){
			totalSC+=result.rows.item(0).cc;
			ratedSC+=result.rows.item(0).ss;
            dd++;
            changeIcon(dd);
			console.log(totalSC);
			// alert(result.rows.item(0).cc +" = "+result.rows.item(0).ss);
			if(result.rows.item(0).ss != result.rows.item(0).cc){
			// $("span#status"+kra_id+"").addClass("label-danger");
			}else if(result.rows.item(0).ss == result.rows.item(0).cc){
			$("#staticon"+kra_id+"").removeClass("fa-exclamation-circle").addClass("fa-check-circle");
			$("span#status"+kra_id+"").attr("data-complete", "complete");
            updateProgressStat(kra_id);
			}

		   $("#status"+kra_id+"").html(result.rows.item(0).ss+" of "+result.rows.item(0).cc);
		   // alert(result.rows.item(0).cc +" : "+result.rows.item(0).kra_id);
		});
		});
	}

    function updateProgressStat(kraid){
        db.transaction(function(tx){
            tx.executeSql('UPDATE progress SET status = 1 WHERE kra_id = ?', [kraid], function(){
              console.log('updated');  
            });
        });
    }

    function changeIcon(dd){
        if(dd == kracount){
            if(ratedSC == totalSC){
                $("#sdsicon").removeClass("fa-exclamation-circle").addClass("fa-check-circle");
            }else{
                $("#sdsicon").addClass("fa-exclamation-circle");
            }
        }
    }
	$(document).on("click", "#chooseKRA", function(){
        var total = $(this).find('div p span').text().split(" ")[0];
		var kra_id = $(this).attr("data-kra-id");
		var stat = $(this).find("span#status"+kra_id+"").attr("data-complete");
        var evalted = $(this).find("span").text().split(" ")[0];
        db.transaction(function(tx){
            tx.executeSql("SELECT * FROM scorecard_ledger_detail WHERE kra_id = ? AND status = 'SUBMITTED'", [kra_id], function(tx, res){
                var len = res.rows.length;
                if(len == 0){
                saveTimeLeft(kra_id, total);
                }else{
                alert("You already submitted a rating to your School District Supervisor(SDS)."); 
                }
            });
        });
		
	});

	$("#sendData").on("click", function(){
		if(ratedSC == totalSC){
			var status = "PENDING";
            db.transaction(function(tx){
                tx.executeSql("SELECT * FROM scorecard_ledger_detail WHERE status = ?", [status], function(tx, result){
                    var len = result.rows.length;
                    if(len == 0){
                        alert("You already submitted a rating to the server."); 
                    }else{
                        if(confirm('Do you really want to upload this rating to the server? \n\nNOTE: This can not be undone!')){
                            $('#loader').modal('show');
                            $('.capt').html('sending your ratings.');
                            getEqui();
                        }
                    }
                });
            });
		}else{
			alert("Please rate all KRA first!");
		}
    });

    function getEqui(){
        var evaluator_id = getParameterByName("evalid");
            db.transaction(function(tx){
                tx.executeSql("SELECT SUM(equivalent) as gt FROM scorecard_ledger_detail", [], function(tx, result){
                    var gt = result.rows.item(0).gt;
                    getHeader(gt, evaluator_id);
                });
        });
    }

    function getHeader(gt, evaluator_id){
        db.transaction(function(tx){
        tx.executeSql("SELECT * FROM scorecard_ledger_header WHERE evaluator_id = ?",[evaluator_id],function(tx,result){

            var scorecard_header_id = result.rows.item(0).scorecard_header_id;
            var assessment_round_id = result.rows.item(0).assessment_round_id;
            var evaluator_id = result.rows.item(0).evaluator_id;

            insertHeader(scorecard_header_id, assessment_round_id, evaluator_id, gt);
        });
        });
    }

    function insertHeader(scorecard_header_id, assessment_round_id, evaluator_id, gt){
        var inserts = "scorecard_header_id="+scorecard_header_id+"&assessment_round_id="+assessment_round_id+"&evaluator_id="+evaluator_id+"&gt="+gt;
        $.ajax({
            type: "POST",
            url: "http://"+ipConfigs.ip+"/phpFile/actions/insertHeader.php",
            data: inserts,
            success: function(id){
                var status = "PENDING";
                getDetails(id,status);
            }
        });
    }

    function getDetails(id,status){
        db.transaction(function(tx){
        tx.executeSql("SELECT * FROM scorecard_ledger_detail WHERE status = ?",[status],function(tx,result){
           var len = result.rows.length;

           for(var i=0;i<len; i++){
            var kra_id = result.rows.item(i).kra_id;
            var parent_id = result.rows.item(i).parent_id;
            var indicator_id = result.rows.item(i).indicator_id;
            var lop_letter = result.rows.item(i).lop_letter;
            var lop_value = result.rows.item(i).lop_value;
            var equivalents = result.rows.item(i).equivalent;

            sendToAdmin(id, kra_id, parent_id, indicator_id,lop_letter, lop_value, len, equivalents);
           }
        });
        });
    }

    function sendToAdmin(id, kra_id, parent_id, indicator_id,lop_letter, lop_value, len, equivalents){
        var dataS = "id="+id+"&kra_id="+kra_id+"&parent_id="+parent_id+"&indicator_id="+indicator_id+"&lop_letter="+lop_letter+"&lop_value="+lop_value+"&equivalents="+equivalents;
        $.ajax({
            type: "POST",
            url: "http://"+ipConfigs.ip+"/phpFile/actions/sendScores.php",
            data: dataS,
            success: function(response){
                res++;
                if(res == len){
                    updateSD();
                }
            }
        });

    }

    function updateSD(){
        var status = "SUBMITTED";
        var executeQuery = "UPDATE scorecard_ledger_detail SET status = '"+status+"'";  
            db.transaction(function(tx){
                tx.executeSql(executeQuery, [], function(tx, res){
                	getFindingsRecommendations();
                    // window.location.replace("kra.html?sc_id="+scorecard_header_id+"&round_number="+assessment_round_id+"&evalid="+evaluator_id);
                }, errorCB);
        }, errorCB);
    }

    function getFindingsRecommendations(){
        db.transaction(function(tx){
            tx.executeSql('SELECT * FROM reports', [], function(tx,result){
                var count = result.rows.length;
                if(count > 0){
                    for(var a=0; a<count; a++){
                        var kra_id = result.rows.item(a).kra_id;
                        var evalid = result.rows.item(a).evaluator_id;
                        var findings = result.rows.item(a).findings;
                        var recommendations = result.rows.item(a).recommendations;
                        var subindicatoridd = result.rows.item(a).sub_indicators_id
                        sendFindingsRecommendations(a,kra_id,evalid,findings,recommendations, subindicatoridd);
                    }
                }else{
                    // alert("Success");
                    updateSendStatus();
                }
            });
        });
    }

    var da = 0;
    function sendFindingsRecommendations(a,kra_id,evalid,findings,recommendations, subindicatoridd){
        $.ajax({
            type: 'POST',
            url: 'http://'+ipConfigs.ip+'/phpFile/actions/findrec.php',
            data: {kra_id:kra_id, evalid:evalid, findings: findings, recommendations:recommendations, subindicatoridd: subindicatoridd},
            success: function(response){  
                if(a == da){
                    updateSendStatus();
                }
                
                da++;
            }
        });
    }

    function updateSendStatus(){
        db.transaction(function(tx){
            tx.executeSql('UPDATE project_name SET sendStatus = 1', [], function(){
                // window.location.replace('dashboard.html');
                updateEvalStatus();
            });
        });
    }

    function updateEvalStatus(){
        var evalid1 = getParameterByName("evalid");
        loginstatus = 0;
        $.ajax({
            type: "GET",
            url: "http://"+ipConfigs.ip+"/phpFile/actions/updateEvalStat.php",
            data: {evaluator_id: evalid1, loginstatus: loginstatus},
            success: function(response){
                alert("Success");
                window.location.replace("dashboard.html");
            }
        });
    }

    //callbacks
    function errorCB(err) {
     alert("Error processing SQLSDSDD: "+err.code);
    }

    function successCB() {
    }

    function success1(tx, res){
        // alert(res.insertId);
    }

    //autofill if time is up

    function queryAllKRA(){
        db.transaction(function(tx){
            tx.executeSql('SELECT * FROM kra', [], function(tx, res){
                for(var a=0;a<res.rows.length; a++){
                    checkRates(res.rows.item(a).kra_id);
                }
            });
        });
    }

    function checkRates(kraid){
    db.transaction(function(tx){
        tx.executeSql('SELECT COUNT(indicator.parent_id) as cc, COUNT(CASE WHEN indicator.status = 1 THEN indicator.status END) as ss, indicator.kra_id, indicator.indicators_id, indicator.parent_id FROM indicator LEFT JOIN kra ON indicator.kra_id = kra.kra_id WHERE kra.kra_id = ? AND indicator.has_child = 0', [kraid], function(tx, result){
            var kulang = Number(result.rows.item(0).cc) - Number(result.rows.item(0).ss);
            var kraid = result.rows.item(0).kra_id;
            var indicators_id = result.rows.item(0).indicators_id;
            if(kulang > 0){
                console.log(kulang +'='+kraid);
                autoFillData(kraid, indicators_id, parent_id);
            }
        });
    }); 
    }

    function autoFillData(kraid, indicators_id, prntid){
        db.transaction(function(tx){
            tx.executeSql('INSERT INTO scorecard_ledger_detail (ledger_id, kra_id, parent_id, indicator_id, lop_letter, lop_value, status, equivalent) VALUES (0, '+kraid+', '+prntid+', '+indicators_id+', "0","0", "PENDING", "0")', [], function(){
                console.log('sdsd');
            });
        });
    }

    //send to admin finish or unfinish
    $('#sendData1').on('click', function(){
        getEqui1();
    });

    //if evaluator reaches expiry date
    function getEqui1(){
            var evaluator_id = getParameterByName("evalid");
                db.transaction(function(tx){
                    tx.executeSql("SELECT SUM(equivalent) as gt FROM scorecard_ledger_detail", [], function(tx, result){
                        var gt = result.rows.item(0).gt;
                        getHeader1(gt, evaluator_id);
                    });
            });
        }

        function getHeader1(gt, evaluator_id){
            db.transaction(function(tx){
            tx.executeSql("SELECT * FROM scorecard_ledger_header WHERE evaluator_id = ?",[evaluator_id],function(tx,result){

                var scorecard_header_id = result.rows.item(0).scorecard_header_id;
                var assessment_round_id = result.rows.item(0).assessment_round_id;
                var evaluator_id = result.rows.item(0).evaluator_id;

                insertHeader1(scorecard_header_id, assessment_round_id, evaluator_id, gt);
            });
            });
        }

        function insertHeader1(scorecard_header_id, assessment_round_id, evaluator_id, gt){
            var inserts = "scorecard_header_id="+scorecard_header_id+"&assessment_round_id="+assessment_round_id+"&evaluator_id="+evaluator_id+"&gt="+gt;
            $.ajax({
                type: "POST",
                url: "http://"+ipConfigs.ip+"/phpFile/actions/insertHeader.php",
                data: inserts,
                success: function(id){
                    var status = "PENDING";
                    getDetails1(id,status);
                }
            });
        }

        function getDetails1(id,status){
        db.transaction(function(tx){
        tx.executeSql("SELECT * FROM scorecard_ledger_detail WHERE status = ?",[status],function(tx,result){
           var len = result.rows.length;

           for(var i=0;i<len; i++){
            var kra_id = result.rows.item(i).kra_id;
            var parent_id = result.rows.item(i).parent_id;
            var indicator_id = result.rows.item(i).indicator_id;
            var lop_letter = result.rows.item(i).lop_letter;
            var lop_value = result.rows.item(i).lop_value;
            var equivalents = result.rows.item(i).equivalent;

            sendToAdmin1(id, kra_id, parent_id, indicator_id,lop_letter, lop_value, len, equivalents);
           }
            });
            });
        }

        var res1 = 0;
        function sendToAdmin1(id, kra_id, parent_id, indicator_id,lop_letter, lop_value, len, equivalents){
            var dataS = "id="+id+"&kra_id="+kra_id+"&parent_id="+parent_id+"&indicator_id="+indicator_id+"&lop_letter="+lop_letter+"&lop_value="+lop_value+"&equivalents="+equivalents;
            $.ajax({
                type: "POST",
                url: "http://"+ipConfigs.ip+"/phpFile/actions/sendScores.php",
                data: dataS,
                success: function(response){
                    res1++;
                    console.log(res1);
                    if(res1 == 1){
                        window.open('http://'+ipConfigs.ip+'/blog/public/timeout', '_blank');
                    }
                }
            });

        }
});