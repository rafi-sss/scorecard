
//get param
function getParameterByName(name, url) {
if (!url) {
  url = window.location.href;
}
name = name.replace(/[\[\]]/g, "\\$&");
var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
    results = regex.exec(url);
if (!results) return null;
if (!results[2]) return '';
return decodeURIComponent(results[2].replace(/\+/g, " "));
}

//variables
var db = window.openDatabase("Evaluator Database", "1.0", "Evaluator RAFI Database", 1000000),counter = 0, subcount = 0, indicators_id, finind, k = 1, scores =[], count, value, letter, saveCounter = 0, lineCount = 0, iterationCount = 0, finparent, sub_id, o = 0, p= 0, ps, ti_weight = 0, ps = 0, equivalent = 0, identifier, res = 0, btnback = "", subindTotal = 0, z = 0, array = [], hasChild = [], ctr = 0, minCtr = 0, secCtrDefault = 59, hrCtr = 0, currentSec = 0, prognum = 0, countprog = 0;

$(document).ready(function(){
    var ipConfigs = new ipConfig();
    var rtcount = 0;
    var sctype = getParameterByName('sctype');
    var totalscrated = Number(getParameterByName("total"));

    if(sctype == 2){
    //START FOR OCP
        $('#yf').hide();
        $(".back-button").on("click", function(){
        btnback = "back";
        // var ct = result.rows.length;
        // result.rows.item(0).progress_number;

        var kd = getParameterByName("kra_id");
        db.transaction(function(tx){
            tx.executeSql('SELECT * FROM rate_tracker WHERE change_status = 1', [], function(tx, res){
                var len = res.rows.length;
                rtcount = len;
                if(len > 0){
                    $("#loader").modal("show");
                    $(".caption").html("Saving progress, please wait.");
                    for(var i=0;i<len;i++){
                        var round_number = res.rows.item(i).round_number;
                        var sc_id = res.rows.item(i).sc_id;
                        var evalid = res.rows.item(i).eval_id;
                        var kra_id = res.rows.item(i).kra_id;
                        var Indicator = res.rows.item(i).indicator_id;
                        var sub_indi = res.rows.item(i).sub_indicator_id;
                        var Lop_Letter = res.rows.item(i).lop_letter;
                        var Lop_Value = res.rows.item(i).lop_value;
                        var equivalents = res.rows.item(i).equivalent;
                        checkIfExist(i, round_number, sc_id, evalid, kra_id, Indicator, sub_indi, Lop_Letter, Lop_Value, equivalents);
                        updateSCStatus(sub_indi, Lop_Letter);
                    }
                }else{
                    saveTimeLeft();
                }
            });
        });

    });

    function saveTimeLeft(){
        db.transaction(function(tx){
            tx.executeSql('INSERT INTO epiryDate (hours, minutes, seconds) VALUES(?,?,?)', [hrCtr, minCtr, secCtrDefault], function(){
                    db.transaction(function(tx1){
                        tx1.executeSql("UPDATE rate_tracker SET change_status = 0", [], function(){
                                window.location.replace("kra.html?sc_id="+getParameterByName("sc_id")+"&round_number="+getParameterByName("round_number")+"&evalid="+getParameterByName("evalid")+"&sctype="+getParameterByName('sctype'));
                                // saveTimeLeft();
                        });
                    });
            });
        });
    }

    //timer
    var remTime = setInterval(function(){
        ctr++;
        //use default value if currentSec is = 0
        if(currentSec == 0){
            currentSec = 1;
        }
        secCtrDefault = currentSec;
        secCtrDefault-=ctr;
        if(secCtrDefault == 0){
            setTimeout(function(){
                // minCtr-=1;
                    if(minCtr > 0){
                        minCtr-=1;
                    }else{
                        minCtr=60;
                        minCtr-=1;
                        hrCtr-=1;
                    }
                secCtrDefault = 60;
                currentSec = secCtrDefault;
            },500);
            ctr = 0;
        }
        //stop timer
        if(hrCtr == 0 && minCtr == 0 &&secCtrDefault == 0){
            console.log("stop");
            updateProjStat();
            $('#timex').modal('show');
            clearInterval(remTime);
        }
        //add 0 if length is 1
        if(secCtrDefault.toString().length == 1)
            secCtrDefault = '0' + secCtrDefault;
        if(minCtr.toString().length == 1)
            minCtr = '0' + minCtr;
        if(hrCtr.toString().length == 1)
            hrCtr = '0' + hrCtr;

        $('#kra').html('<b class="badge-lg">Time Remaining ('+hrCtr+':'+minCtr+':'+secCtrDefault+')</b>'); //append the display
    }, 1000);

    //update projstat
    function updateProjStat(){
        console.log('udpatedz');
        db.transaction(function(tx){
            tx.executeSql('UPDATE project_name SET status = 2', [], function(){
                queryAllKRA();
            });
        });
    }

    //time remaining
    function timeRem(){
        db.transaction(function(tx){
            tx.executeSql('SELECT * FROM epiryDate ORDER BY rowid DESC LIMIT 1', [], function(tx, result){
                    hrCtr = result.rows.item(0).hours;
                    minCtr = result.rows.item(0).minutes;
                    currentSec = result.rows.item(0).seconds;
            });
        });
    }

    function indicatorsL(){
        timeRem();
        var kra_id = getParameterByName("kra_id");

        //last rate
        db.transaction(function(tx){
            tx.executeSql('SELECT rowid, progress_number FROM progress WHERE kra_id = ? AND status = 0 ORDER BY rowid DESC LIMIT 1',[kra_id], function(tx, result){
                var ct = result.rows.length;
                if(ct != 0){
                    prognum = result.rows.item(0).progress_number;
                    // $("#myCarousel").carousel(result.rows.item(0).progress_number);
                }
            });
        });

        db.transaction(function(tx){
            tx.executeSql("SELECT indicator.rowid, kra.kra_name, indicator.detail_name, indicator.parent_id, indicator.indicators_id, indicator.status, indicator.has_child FROM kra LEFT JOIN indicator ON kra.kra_id = indicator.kra_id WHERE kra.kra_id = ? ",[kra_id], function(tx, result){
                count = result.rows.length;
                var status = "", icon = "", color = "";
                var slide = 0;

                //add 0 if length is 1
                if(secCtrDefault.toString().length == 1)
                    secCtrDefault = '0' + secCtrDefault;
                if(minCtr.toString().length == 1)
                    minCtr = '0' + minCtr;
                if(hrCtr.toString().length == 1)
                    hrCtr = '0' + hrCtr;

                $('#kra').html('<b class="badge-lg">Time Remaining ('+hrCtr+':'+minCtr+':'+currentSec+')</b>');
                for(var i=0;i<count;i++){
                    if(result.rows.item(i).parent_id == 0){
                        if(result.rows.item(i).has_child == 1){
                            finind = result.rows.item(i).indicators_id;
                            if(i == 0){
                                status1 = "active";
                            }
                            else{
                                status1 = "";
                            }

                            $("#indicator").html('<span id='+result.rows.item(0).indicators_id+'>'+result.rows.item(0).detail_name+'</span><span class="timearea"><b class="badge badge-lg"></b></span>');

                            $(".inddata").append('<div class="subindwrap" data-id3='+finind+' id="siw'+finind+'"><h5 class="parent-indicator" style="color: #333; text-align: left">'+result.rows.item(i).detail_name+'</h5><hr><div class="row"><table class="table table-striped"><thead><th>Indicator Name</th><th>Rating</th></thead><tbody id="data'+finind+'" style="text-align: left"></tbody></table></div></div>');

                            for(var a=0;a<count; a++){

                                var dd = result.rows.item(a).parent_id;
                                var statind = result.rows.item(a).status;

                                if(finind == dd){
                                    // console.log(finind);
                                    //icon
                                    if(statind == 1){
                                    icon = "fa fa-check-circle";
                                    color = "color: #03ce68; font-size: 20px; float: right;";
                                    }
                                    else{
                                    icon = "";
                                    color = "";
                                    }
                                    //end icon
                                    counter++;
                                    if(counter == 1){
                                        status = "active";
                                    }else{
                                        status = "";
                                    }
                                     sub_id = result.rows.item(a).indicators_id;

                                    $("#data"+dd+"").append("<tr><td>"+result.rows.item(a).detail_name+"</td><td><button class='btn btn-proceed' data-target='#myCarousel' data-id2="+finind+" data-slide-to="+slide+" id='proceed' data-sb="+sub_id+" data-num="+counter+">Proceed</button></td></tr>");

                                    $("#carousel-sc").append('<div class="item '+status+'" id="id'+counter+'" data-id='+finind+' data-subid='+sub_id+'><p class="h4 title indi-cat-title">'+result.rows.item(a).detail_name+' <i class="'+icon+'" style="'+color+'"></i></p><div class="container"><div class="row" id="lops'+sub_id+'"></div></div><div>');
                                    $('#bar').append('<div id="snackbar" class="page'+sub_id+'">Page '+counter+' of <span id="total'+sub_id+'"></span></div>');

                                    loadLops(sub_id, counter);
                                    slide++;
                                }

                            }
                        
                        }else{
                            var ffd = result.rows.item(i).indicators_id;
                            counter++;
                            $("#carousel-sc").append('<div class="item" id="id'+counter+'" data-id='+ffd+' data-subid='+ffd+'><div class="container"><div class="row" id="lops'+ffd+'"></div></div><div>');
                            $('#bar').append('<div id="snackbar" class="page'+ffd+'">Page '+counter+' of <span id="total'+ffd+'"></span></div>');
                            loadLops(ffd, counter);
                        }
                    }
                }

                
                //condition last rated if progress number has a value
                if(totalscrated != 0){
                    countprog = counter;
                    countprog-=totalscrated;
                    // counter-=prognum;
                    console.log(countprog);
                    $("#myCarousel").carousel(totalscrated);
                    var progs = totalscrated + 1;
                    var indiid = 0, order;
                    k=progs;
                    indiid = $("#id"+progs).attr("data-id");
                    var subd = $("#id"+progs).attr("data-subid");
                    $("#total"+subd).html(counter);
                    $(".page"+subd).addClass("show");
                    setTimeout(function(){$(".page"+subd).removeClass("show");},1000);

                    for(var i=0;i<count;i++){
                        if(result.rows.item(i).indicators_id == indiid){
                                $("#indicator").html('<span id='+result.rows.item(i).indicators_id+'>'+result.rows.item(i).detail_name+'</span><span class="timearea"></span>');
                        }
                    }
                }

                //events
                $(document).on("click", "#proceed",function(){
                var indiid2 = $(this).attr("data-id2");
                var subd = $(this).attr("data-sb");
                var num = $(this).attr("data-num");
                //change indicator name
                for(var i=0;i<count;i++){
                    if(result.rows.item(i).indicators_id == indiid2){
                        $("#indicator").html('<span id='+result.rows.item(i).indicators_id+'>'+result.rows.item(i).detail_name+'</span>');
                    }
                }
                //snackbar
                k = num;
                $("#parentModal").modal("hide");
                $("#total"+subd).html(counter);
                $(".page"+subd).addClass("show");
                setTimeout(function(){$(".page"+subd).removeClass("show");},1000);
                });

                 var df = 0;
                 df = totalscrated;
                 $("#btn-continue").on("click", function(){
                    $("#findingsOCPForm")[0].reset();
                    $('#findingsOCP').modal('show');
                    var indiid = 0, order;
                    var slen = scores.length;

                    if(slen < counter){

                        if(countprog == slen && countprog != 0){
                            $("#loader").modal("show");
                            $("#btn-continue").attr("disabled", true);
                            db.transaction(function(tx){
                                tx.executeSql('SELECT * FROM rate_tracker WHERE change_status = 1', [], function(tx, res){
                                    var len = res.rows.length;
                                    if(len > 0){
                                        for(var i=0;i<len;i++){
                                            var round_number = res.rows.item(i).round_number;
                                            var sc_id = res.rows.item(i).sc_id;
                                            var evalid = res.rows.item(i).eval_id;
                                            var kra_id = res.rows.item(i).kra_id;
                                            var Indicator = res.rows.item(i).indicator_id;
                                            var sub_indi = res.rows.item(i).sub_indicator_id;
                                            var Lop_Letter = res.rows.item(i).lop_letter;
                                            var Lop_Value = res.rows.item(i).lop_value;
                                            var equivalents = res.rows.item(i).equivalent;
                                            checkIfExist(i, round_number, sc_id, evalid, kra_id, Indicator, sub_indi, Lop_Letter, Lop_Value, equivalents);
                                        }
                                    }else{
                                        saveTimeLeft();
                                    }
                                });
                            });
                        }else{

                            if(df < counter){
                                df++;
                            }

                            k++;
                            var indname = $('#id'+df).find('p.h4.title.indi-cat-title').text();
                            var iddddd = $('#id'+df).attr('data-subid');

                            db.transaction(function(tx){
                                    tx.executeSql("SELECT * FROM reports WHERE sub_indicators_id = ?", [iddddd], function(tx, result){
                                        var len = result.rows.length;
                                        if(len > 0){
                                            $('#sidOCP').val(result.rows.item(0).sub_indicators_id);
                                            $('#krdOCP').val(result.rows.item(0).kra_id);
                                            $('[name="findingsnameOCP"]').val(result.rows.item(0).findings);
                                            $('[name="recommendationsname"]').val(result.rows.item(0).recommendations);
                                        }
                                    });
                            });

                            if(indname == ""){
                                var spansss = $('span#'+iddddd).text();
                                $('#sidOCP').val(iddddd);
                                $('#krdOCP').val(getParameterByName('kra_id'));
                                $('#indicatorname').html(spansss);
                            }else{
                                $('#sidOCP').val(iddddd);
                                $('#krdOCP').val(getParameterByName('kra_id'));
                                $('#indicatorname').html(indname);
                            }
                            p = k;
                            if( k <= counter){
                                indiid = $("#id"+k).attr("data-id");
                                var subd = $("#id"+k).attr("data-subid");
                                // alert(subd);
                                $("#total"+subd).html(counter);
                                $(".page"+subd).addClass("show");
                                setTimeout(function(){$(".page"+subd).removeClass("show");},1000);
                            }else if(k > counter){
                                k = counter;
                            }

                            for(var i=0;i<count;i++){
                                if(result.rows.item(i).indicators_id == indiid){
                                        $("#indicator").html('<span id='+result.rows.item(i).indicators_id+'>'+result.rows.item(i).detail_name+'</span><span class="timearea"></span>');
                                }
                            }
                        }

                    }else if(slen == counter){
                        $("#loader").modal("show");
                        $("#btn-continue").attr("disabled", true);
                        db.transaction(function(tx){
                                tx.executeSql('SELECT * FROM rate_tracker WHERE change_status = 1', [], function(tx, res){
                                    var len = res.rows.length;
                                    if(len > 0){
                                        for(var i=0;i<len;i++){
                                            var round_number = res.rows.item(i).round_number;
                                            var sc_id = res.rows.item(i).sc_id;
                                            var evalid = res.rows.item(i).eval_id;
                                            var kra_id = res.rows.item(i).kra_id;
                                            var Indicator = res.rows.item(i).indicator_id;
                                            var sub_indi = res.rows.item(i).sub_indicator_id;
                                            var Lop_Letter = res.rows.item(i).lop_letter;
                                            var Lop_Value = res.rows.item(i).lop_value;
                                            var equivalents = res.rows.item(i).equivalent;
                                            checkIfExist(i, round_number, sc_id, evalid, kra_id, Indicator, sub_indi, Lop_Letter, Lop_Value, equivalents);
                                        }
                                    }else{
                                        saveTimeLeft();
                                    }
                                });
                            });
                    }


                });

                $("#btn-prev").on("click", function(){
                    var x = document.getElementById("snackbar");
                    var prev;
                    var indiid = 0
                    if($("#myCarousel").hasClass("carousel")){
                        o++;
                        k -= 1;

                        if(df > 0){
                            df -= 1;
                        }

                        if(k > 0){
                            var slen = k - o;
                            indiid = $("#id"+k).attr("data-id");
                            var subd = $("#id"+k).attr("data-subid");
                            $("#total"+subd).html(counter);
                            $(".page"+subd).addClass("show");
                            setTimeout(function(){$(".page"+subd).removeClass("show");},1000);

                            if(slen != 0 || slen == 0){
                            prev = k;
                            }
                        }else if(k == 0 || k < 0){
                            k = 1;
                        }

                        for(var i=0;i<count;i++){
                            if(result.rows.item(i).indicators_id == indiid){
                                    $("#indicator").html('<span id='+result.rows.item(i).indicators_id+'>'+result.rows.item(i).detail_name+'</span><span class="timearea"></span>');
                            }
                        }
                    }
                });
            });
        });
    }

    indicatorsL();

    $('#findingsOCPForm').on('submit', function(e){
        e.preventDefault();
        var dataaaaaa = $(this).serializeArray();
        var dataaaaaaObj = {};
        $(dataaaaaa).each(function(i, field){
            dataaaaaaObj[field.name] = field.value;
        });
        var subid = dataaaaaaObj['subindicatoridOCP'];
        var krdOCP = dataaaaaaObj['kraidOCP'];
        var findingsnameOCP = dataaaaaaObj['findingsnameOCP'];
        var recommendationsname = dataaaaaaObj['recommendationsname'];
        var evaluatid = getParameterByName('evalid');
        db.transaction(function(tx){
            tx.executeSql('SELECT * FROM reports WHERE kra_id = ? AND sub_indicators_id = ?', [krdOCP, subid], function(tx, result){
                var len = result.rows.length;
                if(len > 0){
                    db.transaction(function(tx1){
                        tx1.executeSql('UPDATE reports SET findings = ?, recommendations = ? WHERE sub_indicators_id = ?', [findingsnameOCP, recommendationsname, subid], function(){
                            alert('Findings and recommendations updated!');
                            $('#findingsOCP').modal('hide');
                        });
                    });
                }else{
                    db.transaction(function(tx1){
                        tx1.executeSql('INSERT INTO reports (kra_id, evaluator_id, findings, recommendations, sub_indicators_id) VALUES (?,?,?,?,?)', [krdOCP, evaluatid, findingsnameOCP, recommendationsname, subid], function(){
                                alert('Findings and Recommendations added!');
                                $('#findingsOCP').modal('hide');
                        });
                    });
                }
            });
        });
    });

    function checkIfExist(i, round_number, sc_id, evalid, kra_id, Indicator, sub_indi, Lop_Letter, Lop_Value, equivalents){
        db.transaction(function(tx){
            tx.executeSql("SELECT rowid FROM scorecard_ledger_header WHERE scorecard_header_id = "+sc_id+" LIMIT 1", [], function(tx, result){
                var len = result.rows.length;
                // alert(len);
                if(len == 0){
                    saveScores(i, round_number, sc_id, evalid, kra_id, Indicator, sub_indi, Lop_Letter, Lop_Value, equivalents);
                }else{
                    var ledger_id = result.rows.item(0).rowid;
                    // alert(ledger_id);
                    saveDetails(i,ledger_id,Lop_Letter,kra_id, Indicator, sub_indi, Lop_Letter, Lop_Value, equivalents);
                }
            });
        });
    }
    var prevRate = [];
    function loadLops(sub_indicator_id, counter){
        // alert(sub_indicator_id);
        db.transaction(function(tx){
        tx.executeSql("SELECT * FROM scorecard WHERE sub_indicators_id = ? ORDER BY lop_letter DESC",[sub_indicator_id],function(tx,result){
           var len = result.rows.length;
           var g, val, classstat, ratedStat;
           for(var i=0;i<len; i++){
            var lopstatus = result.rows.item(i).status;

            //get highest score
            for(var a= 0; a<len; a++){
                g = result.rows.item(a).lop_letter;
                if(g.trim() === "A"){
                    val = result.rows.item(a).lop_value;
                }
            }
            //display check icon if rated
            if(lopstatus == 1){
                classstat = "display: block";
                prevRate.push(result.rows.item(i).lop_letter);
                ratedStat = "rated";
            }else{
                ratedStat = "";
                classstat = "display: none";
            }

            $('#lops'+sub_indicator_id).append('<div class="indi-cont-in" id="checkVals"><input type="checkbox" class="input'+i+'" id="checkbox" value='+result.rows.item(i).lop_value+' data-row-id='+result.rows.item(i).lop_id+' data-ps='+val+' data-weight='+result.rows.item(i).ti_weight+' data-letter='+result.rows.item(i).lop_letter+' data-parent='+result.rows.item(i).parent_id+' data-indicator='+result.rows.item(i).sub_indicators_id+' data-counter='+counter+' data-rated='+ratedStat+'><div class="clearfix"><div class="col-xs-9 col-xs-offset-3 rel lop-cont"><p>'+result.rows.item(i).lop_description+'</p><div class="flip-cont"><div class="flip"><p>'+result.rows.item(i).lop_letter+'</p><span style="visibility:hidden">'+result.rows.item(i).lop_letter+'</span></div></div></div></div><div class="v-align v'+result.rows.item(i).sub_indicators_id+'" id="bg" style="'+classstat+'"><div class="d-table"><div class="table-cell"><div class="check_mark"><div class="sa-icon sa-success animate"><span class="sa-line sa-tip animateSuccessTip"></span><span class="sa-line sa-long animateSuccessLong"></span><div class="sa-placeholder"></div><div class="sa-fix"></div></div></div></div></div></div></div>');
           }
        });
        });
    }

    var existInd = [];
    
    $(document).on("click", "#checkVals", function(){
        value = $(this).find("#checkbox").val();
        letter = $(this).find("#checkbox").attr("data-letter");
        indicatorid = $(this).find("#checkbox").attr("data-parent");
        sub_indicator_id = $(this).find("#checkbox").attr("data-indicator");
        ti_weight = $(this).find("#checkbox").attr("data-weight");
        ps = $(this).find("#checkbox").attr("data-ps");
        identifier = $(this).find("#checkbox").attr("data-counter");
        var lop_id = $(this).find("#checkbox").attr("data-row-id");

        updateSC(sub_indicator_id, value, letter, indicatorid, ti_weight, ps, identifier, lop_id);
        $(".v-align.v"+sub_indicator_id+"").css("display","none");
        $(this).find(".v-align.v"+sub_indicator_id+"").css("display","block");

    });

    function updateSC(sub_indicator_id, value, letter, indicatorid, ti_weight, ps, identifier, lop_id){
        equivalent = (Number(value) / Number(ps)) * Number(ti_weight);

        var round_number = getParameterByName("round_number"),
        sc_id = getParameterByName("sc_id"),
        eval_id = getParameterByName("evalid"),
        kra_id = getParameterByName("kra_id");

        db.transaction(function(tx){
            tx.executeSql('SELECT * FROM rate_tracker WHERE sub_indicator_id = ?', [sub_indicator_id], function(tx, res){
                var len = res.rows.length;
                if(len > 0){
                    db.transaction(function(tx1){
                        tx1.executeSql('UPDATE rate_tracker SET lop_value = ?, lop_letter = ?, equivalent = ?, change_status = 1 WHERE sub_indicator_id = ?', [value, letter, equivalent, sub_indicator_id], function(){
                            console.log('Updated');
                        });
                    });
                }else{
                    db.transaction(function(tx1){
                        tx1.executeSql('INSERT INTO rate_tracker (sub_indicator_id, lop_value, lop_letter, indicator_id, round_number, sc_id, eval_id, kra_id, equivalent, change_status) VALUES (?,?,?,?,?,?,?,?,?, 1)', [sub_indicator_id, value, letter, indicatorid, round_number, sc_id, eval_id, kra_id, equivalent], function(){
                            scores.push('scores');
                            updateInd(sub_indicator_id, indicatorid);
                        });
                    });
                }
            });
        });
    }

/*    function querySCStatus(sub_indicator_id, letter){
        db.transaction(function(tx){
            tx.executeSql("SELECT * FROM scorecard WHERE sub_indicators_id = ? AND status = 1", [sub_indicator_id], function(tx, result){
                var len = result.rows.length;
                // alert(len);
                if(len > 0){
                    db.transaction(function(tx1){
                        tx1.executeSql("UPDATE scorecard SET status = 0 WHERE sub_indicators_id = ?", [sub_indicator_id], function(tx1, result1){
                            db.transaction(function(tx2){
                                tx2.executeSql("UPDATE scorecard SET status = 1 WHERE sub_indicators_id = ? AND lop_letter = ?", [sub_indicator_id, letter], function(tx4, res4){
                                    console.log("fdgdfg");
                                });
                            });
                        });
                    });
                }else{
                // alert(letter);
                    db.transaction(function(tx3){
                            tx3.executeSql("UPDATE scorecard SET status = 1 WHERE sub_indicators_id = ? AND lop_letter = ?", [sub_indicator_id, letter], function(tx3, res3){
                                console.log("saved");
                            });
                        });
                }
            });
        });
    }*/

    function updateInd(sub_indicator_id, indicatorid){
        db.transaction(function(tx){
            tx.executeSql("UPDATE indicator SET status = 1 WHERE indicators_id = ?", [sub_indicator_id],function (tx, res){
                console.log(indicatorid);
                countSubind(indicatorid);
            });
        });
    }

    function countSubind(indicatorid){
        // alert(indicatorid);
        db.transaction(function(tx){
            tx.executeSql("SELECT COUNT(parent_id) as icount FROM indicator WHERE parent_id = ?", [indicatorid], function(tx, result){
                subindTotal = result.rows.item(0).icount;
                checkSubCount(indicatorid);
                // alert(result.rows.item(0).icount;);
            });
        });
    }

    function checkSubCount(indicatorid){
        db.transaction(function(tx){
            tx.executeSql("SELECT COUNT(parent_id) as iicount, parent_id FROM indicator WHERE parent_id = ? AND status = 1", [indicatorid], function(tx, res){
                if(subindTotal == res.rows.item(0).iicount){
                    updateIndStat(res.rows.item(0).parent_id);
                }
                // alert(res.rows.item(0).iicount);
            });
        });
    }

    function updateIndStat(prntid){
        db.transaction(function(tx){
            tx.executeSql("UPDATE indicator SET indicator_status = 1 WHERE indicators_id = ?", [prntid], function(tx, res){
                console.log("updated");
            });
        });
    }

    function saveScores(i, round_number, sc_id, evalid, kra_id, Indicator, sub_indi, Lop_Letter, Lop_Value,equivalents){
            (function(i){
                var executeQuery = "INSERT INTO scorecard_ledger_header (scorecard_header_id, assessment_round_id, evaluator_id) SELECT "+sc_id+", "+round_number+", "+evalid+" WHERE NOT EXISTS (SELECT 1 FROM scorecard_ledger_header WHERE scorecard_header_id = "+sc_id+" AND assessment_round_id = "+round_number+" AND evaluator_id = "+evalid+")";
                db.transaction(function(tx){
                    var hello = "hello";
                    tx.executeSql(executeQuery, [], function(tx, res){
                        var ledger_id = res.insertId;
                        saveDetails(i,ledger_id,Lop_Letter,kra_id, Indicator, sub_indi, Lop_Letter, Lop_Value, equivalents);
                    }, errorCB);
                }, errorCB);
            })(i);
    }

    function errorCB(err) {
     alert("Error processing SQLSDSDD: "+err.code);
    }

    function successCB() {
    }

    function success1(tx, res){
        // alert(res.insertId);
    }

    function saveDetails(i,ledger_id,Lop_Letter,kra_id, Indicator, sub_indi, Lop_Letter, Lop_Value, equivalents){
        var status = "PENDING";
        db.transaction(function(tx){
            tx.executeSql('SELECT * FROM scorecard_ledger_detail WHERE kra_id = ? AND indicator_id = ?', [kra_id,sub_indi], function(tx, res){
                var len = res.rows.length;
                    if(len > 0){
                        db.transaction(function(tx1){
                            tx1.executeSql('UPDATE scorecard_ledger_detail SET lop_letter = ?, lop_value = ?, equivalent = ? WHERE kra_id = ? AND indicator_id = ?', [Lop_Letter, Lop_Value, equivalents, kra_id, sub_indi], function(){
                                    if(i==scores.length-1 || i==rtcount-1){
                                        if(btnback == "back"){
                                            db.transaction(function(tx1){
                                                tx1.executeSql("UPDATE rate_tracker SET change_status = 0", [], function(){
                                                        saveTimeLeft();
                                                });
                                            });
                                        }else{
                                            $("#myModal").modal("show");
                                            getKRA(i);
                                        }
                                    }
                            });
                        });
                    }else{
                        db.transaction(function(tx1){
                            tx1.executeSql("INSERT INTO scorecard_ledger_detail (ledger_id, kra_id, parent_id, indicator_id, lop_letter, lop_value, status, equivalent) VALUES ("+ledger_id+", "+kra_id+", "+Indicator+", "+sub_indi+",'"+Lop_Letter+"', "+Lop_Value+", '"+status+"', '"+equivalents+"')", [], function(){
                                    if(i==scores.length-1 || i==rtcount-1){
                                        if(btnback == "back"){
                                            db.transaction(function(tx1){
                                                tx1.executeSql("UPDATE rate_tracker SET change_status = 0", [], function(){
                                                        saveTimeLeft();
                                                });
                                            });
                                        }else{
                                        $("#myModal").modal("show");
                                        getKRA(i);
                                        }
                                    }
                            });
                        });
                    }
            });
        });
    }

    function getKRA(i){
        $("#loader").modal("hide");
            var kra_id = getParameterByName("kra_id");
            db.transaction(function(tx){
            tx.executeSql("SELECT * FROM indicator LEFT JOIN kra on indicator.kra_id = kra.kra_id WHERE indicator.kra_id = ? AND indicator.parent_id = 0 AND indicator.indicator_status = 1",[kra_id],function(tx,result){
               var len = result.rows.length;
               var ff = 0;
               for(var i=0;i<len;i++){
                ff++;
                    $(".kra-name").html(result.rows.item(i).kra_name);
                        $(".ind-wrap").append('<hr><h5 class="parent-indicator" style="color: #333; text-align: left">'+result.rows.item(i).detail_name+'</h5><hr><div class="row"><table class="table table-striped"><thead><th>Indicator Name</th><th>Rating</th></thead><tbody id="data'+ff+'" style="text-align: left"></tbody></table></div>');
                    sub_id = result.rows.item(i).indicators_id;
                    getData(sub_id, ff);
               }
            });
            });
    }

    function getData(sub_id, ff){
        var kra_id = getParameterByName("kra_id");
        db.transaction(function(tx){
            tx.executeSql("SELECT scorecard_ledger_detail.indicator_id,scorecard_ledger_detail.lop_letter,scorecard_ledger_detail.kra_id, scorecard_ledger_detail.lop_value, kra.kra_name, in1.detail_name as detail_name1, in2.detail_name as detail_name2, in1.parent_id as parent1, in2.parent_id as parent2 FROM scorecard_ledger_detail LEFT JOIN kra ON scorecard_ledger_detail.kra_id = kra.kra_id LEFT JOIN indicator as in1 ON scorecard_ledger_detail.parent_id = in1.indicators_id LEFT JOIN indicator as in2 ON scorecard_ledger_detail.indicator_id = in2.indicators_id WHERE scorecard_ledger_detail.ledger_id <> 0 AND scorecard_ledger_detail.kra_id = "+kra_id+"",[],function(tx,result){
               var len = result.rows.length;
               for(var i=0;i<len; i++){
                var parent_id = result.rows.item(i).parent2;
                if(sub_id == parent_id){
                  var ind = result.rows.item(i).indicator_id;
                  var ll = result.rows.item(i).lop_letter;
                  updateSCStatus(ind, ll);
                  $("#data"+ff+"").append('<tr><td>'+result.rows.item(i).detail_name2+'</td><td>'+result.rows.item(i).lop_letter+'</td></tr>');
                }
                }
            });
            });
    }

    function updateSCStatus(ind, ll){
      db.transaction(function(tx){
        tx.executeSql('SELECT lop_letter, status FROM scorecard WHERE sub_indicators_id = ? AND status = 1', [ind], function(tx, res){
            var len = res.rows.length;
                 if(len > 0){
                    console.log(true);
                    db.transaction(function(tx1){
                        tx1.executeSql('UPDATE scorecard SET status = 0 WHERE sub_indicators_id = ?', [ind], function(){
                            db.transaction(function(tx2){
                                tx2.executeSql("UPDATE scorecard SET status = 1 WHERE sub_indicators_id = ? AND LIKE ('%"+ll+"%', lop_letter) = 1", [ind], function(){
                                    console.log('updated');
                                });
                            });
                        });
                    });
                }else{
                    db.transaction(function(tx2){
                                tx2.executeSql("UPDATE scorecard SET status = 1 WHERE sub_indicators_id = ? AND LIKE ('%"+ll+"%', lop_letter) = 1", [ind], function(){
                                    console.log('updated');
                                });
                            });
                }  
        });
      });
    }

    $("#indicator").on("click", function(){
        $(".subindwrap").addClass("invisible");
        var thisid = $(this).find("span").attr("id");
        var fg = document.getElementById("siw"+thisid+"");
        var getAttr = fg.getAttribute("data-id3");
        if(thisid == getAttr){
            $("#siw"+thisid+"").removeClass("invisible").addClass("visible");
        }
            $("#parentModal").modal("show");
    });


    $("#cancel").click(function(){
        scores = [];
        saveTimeLeft();
    });

    $(document).on('click', '#yf', function(){
        var data_rate = $(this).attr('data-rate');
        var data_ind = $(this).attr('data-ind');
        var data_kraid = $(this).attr('data-kra');
        $('#sid').val(data_ind);
        $('#ll').val(data_rate);
        $('#krd').val(data_kraid);
        console.log(data_rate + data_ind);
        $('#findings').modal('show');
    });

    $('#findingsForm').on('submit', function(e){
        e.preventDefault();
        var eid = getParameterByName('evalid');
        var kraid = getParameterByName('kra_id');
        var findingsName = $('#findName').val();
        var recommendationsname = $('#recomName').val();
        db.transaction(function(tx){
            tx.executeSql('INSERT INTO reports (kra_id, evaluator_id,findings, recommendations) VALUES (?,?,?,?)', [kraid, eid,findingsName, recommendationsname], function(tx, result){
                alert("Findings and Recommendations added!");
                saveTimeLeft();
            });
        });
    });

    //autofill if times up

    function queryAllKRA(){
        db.transaction(function(tx){
            tx.executeSql('SELECT * FROM kra', [], function(tx, res){
                for(var a=0;a<res.rows.length; a++){
                    checkRates(res.rows.item(a).kra_id);
                }
            });
        });
    }

    function checkRates(kraid){
    db.transaction(function(tx){
        tx.executeSql('SELECT COUNT(indicator.parent_id) as cc, COUNT(CASE WHEN indicator.status = 1 THEN indicator.status END) as ss, indicator.kra_id, indicator.indicators_id, indicator.parent_id FROM indicator LEFT JOIN kra ON indicator.kra_id = kra.kra_id WHERE kra.kra_id = ? AND indicator.has_child = 0', [kraid], function(tx, result){
            var kulang = Number(result.rows.item(0).cc) - Number(result.rows.item(0).ss);
            var kraid = result.rows.item(0).kra_id;
            var indicators_id = result.rows.item(0).indicators_id;
            if(kulang > 0){
                console.log(kulang +'='+kraid);
                autoFillData(kraid, indicators_id, parent_id);
            }
        });
    }); 
    }

    function autoFillData(kraid, indicators_id, prntid){
        db.transaction(function(tx){
            tx.executeSql('INSERT INTO scorecard_ledger_detail (ledger_id, kra_id, parent_id, indicator_id, lop_letter, lop_value, status, equivalent) VALUES (0, '+kraid+', '+prntid+', '+indicators_id+', "0","0", "PENDING", "0")', [], function(){
                console.log('sdsd');
            });
        });
    }
    
    //send to admin finish or unfinish
    $('#sendData').on('click', function(){
            getEqui();  
        });

        function getEqui(){
            var evaluator_id = getParameterByName("evalid");
                db.transaction(function(tx){
                    tx.executeSql("SELECT SUM(equivalent) as gt FROM scorecard_ledger_detail", [], function(tx, result){
                        var gt = result.rows.item(0).gt;
                        getHeader(gt, evaluator_id);
                    });
            });
        }

        function getHeader(gt, evaluator_id){
            db.transaction(function(tx){
            tx.executeSql("SELECT * FROM scorecard_ledger_header WHERE evaluator_id = ?",[evaluator_id],function(tx,result){

                var scorecard_header_id = result.rows.item(0).scorecard_header_id;
                var assessment_round_id = result.rows.item(0).assessment_round_id;
                var evaluator_id = result.rows.item(0).evaluator_id;

                insertHeader(scorecard_header_id, assessment_round_id, evaluator_id, gt);
            });
            });
        }

        function insertHeader(scorecard_header_id, assessment_round_id, evaluator_id, gt){
            var inserts = "scorecard_header_id="+scorecard_header_id+"&assessment_round_id="+assessment_round_id+"&evaluator_id="+evaluator_id+"&gt="+gt;
            $.ajax({
                type: "POST",
                url: "http://"+ipConfigs.ip+"/phpFile/actions/insertHeader.php",
                data: inserts,
                success: function(id){
                    var status = "PENDING";
                    getDetails(id,status);
                }
            });
        }

        function getDetails(id,status){
        db.transaction(function(tx){
        tx.executeSql("SELECT * FROM scorecard_ledger_detail WHERE status = ?",[status],function(tx,result){
           var len = result.rows.length;

           for(var i=0;i<len; i++){
            var kra_id = result.rows.item(i).kra_id;
            var parent_id = result.rows.item(i).parent_id;
            var indicator_id = result.rows.item(i).indicator_id;
            var lop_letter = result.rows.item(i).lop_letter;
            var lop_value = result.rows.item(i).lop_value;
            var equivalents = result.rows.item(i).equivalent;

            sendToAdmin(id, kra_id, parent_id, indicator_id,lop_letter, lop_value, len, equivalents);
           }
            });
            });
        }

        function sendToAdmin(id, kra_id, parent_id, indicator_id,lop_letter, lop_value, len, equivalents){
            var dataS = "id="+id+"&kra_id="+kra_id+"&parent_id="+parent_id+"&indicator_id="+indicator_id+"&lop_letter="+lop_letter+"&lop_value="+lop_value+"&equivalents="+equivalents;
            $.ajax({
                type: "POST",
                url: "http://"+ipConfigs.ip+"/phpFile/actions/sendScores.php",
                data: dataS,
                success: function(response){
                    res++;
                    if(res == 1){
                        window.open('http://'+ipConfigs.ip+'/blog/public/timeout', '_blank');
                    }
                }
            });

        }

    //END FOR OCP    
    }else{
    //START FOR SEED

        $(".back-button").on("click", function(){
        btnback = "back";
        // var ct = result.rows.length;
        // result.rows.item(0).progress_number;

        var kd = getParameterByName("kra_id");
        db.transaction(function(tx){
            tx.executeSql('SELECT * FROM rate_tracker WHERE change_status = 1', [], function(tx, res){
                var len = res.rows.length;
                rtcount = len;
                if(len > 0){
                    $("#loader").modal("show");
                    $(".caption").html("Saving progress, please wait.");
                    for(var i=0;i<len;i++){
                        var round_number = res.rows.item(i).round_number;
                        var sc_id = res.rows.item(i).sc_id;
                        var evalid = res.rows.item(i).eval_id;
                        var kra_id = res.rows.item(i).kra_id;
                        var Indicator = res.rows.item(i).indicator_id;
                        var sub_indi = res.rows.item(i).sub_indicator_id;
                        var Lop_Letter = res.rows.item(i).lop_letter;
                        var Lop_Value = res.rows.item(i).lop_value;
                        var equivalents = res.rows.item(i).equivalent;
                        checkIfExist(i, round_number, sc_id, evalid, kra_id, Indicator, sub_indi, Lop_Letter, Lop_Value, equivalents);
                        updateSCStatus(sub_indi, Lop_Letter);
                    }
                }else{
                    saveTimeLeft();
                }
            });
        });

        // if(scores.length != 0){
        //     $("#loader").modal("show");
        //     $(".caption").html("Saving progress, please wait.");
        //     // var prog = scores.length + prognum;
        //     for(var i=0; i<scores.length; i++){
        //         var round_number = scores[i].round_number;
        //         var sc_id = scores[i].sc_id;
        //         var evalid = scores[i].eval_id;
        //         var kra_id = scores[i].kra_id;
        //         var Indicator = scores[i].Indicator;
        //         var sub_indi = scores[i].sub_indi;
        //         var Lop_Letter = scores[i].Lop_Letter;
        //         var Lop_Value = scores[i].Lop_Value;
        //         var equivalents = scores[i].equivalent;
        //         checkIfExist(i, round_number, sc_id, evalid, kra_id, Indicator, sub_indi, Lop_Letter, Lop_Value, equivalents);
        //         updateSCStatus(sub_indi, Lop_Letter);
        //     }
        //     // db.transaction(function(tx){
        //     //     tx.executeSql('INSERT INTO progress (kra_id,progress_number,status) VALUES (?,?,0)', [kd,prog], function(){
        //     //         for(var i=0; i<scores.length; i++){
        //     //             var round_number = scores[i].round_number;
        //     //             var sc_id = scores[i].sc_id;
        //     //             var evalid = scores[i].eval_id;
        //     //             var kra_id = scores[i].kra_id;
        //     //             var Indicator = scores[i].Indicator;
        //     //             var sub_indi = scores[i].sub_indi;
        //     //             var Lop_Letter = scores[i].Lop_Letter;
        //     //             var Lop_Value = scores[i].Lop_Value;
        //     //             var equivalents = scores[i].equivalent;
        //     //             checkIfExist(i, round_number, sc_id, evalid, kra_id, Indicator, sub_indi, Lop_Letter, Lop_Value, equivalents);
        //     //             updateSCStatus(sub_indi, Lop_Letter);
        //     //         }
        //     //     });
        //     // });
        // }else{
        //     saveTimeLeft();
        // }
    });

    function saveTimeLeft(){
        db.transaction(function(tx){
            tx.executeSql('INSERT INTO epiryDate (hours, minutes, seconds) VALUES(?,?,?)', [hrCtr, minCtr, secCtrDefault], function(){
                    db.transaction(function(tx1){
                        tx1.executeSql("UPDATE rate_tracker SET change_status = 0", [], function(){
                                // window.location.replace("kra.html?sc_id="+getParameterByName("sc_id")+"&round_number="+getParameterByName("round_number")+"&evalid="+getParameterByName("evalid"));
                                window.location.replace("kra.html?sc_id="+getParameterByName("sc_id")+"&round_number="+getParameterByName("round_number")+"&evalid="+getParameterByName("evalid")+"&sctype="+getParameterByName('sctype'));
                                // saveTimeLeft();
                        });
                    });
                    // window.location.replace("kra.html?sc_id="+getParameterByName("sc_id")+"&round_number="+getParameterByName("round_number")+"&evalid="+getParameterByName("evalid")+"&sctype="+getParameterByName('sctype'));
            });
        });
    }

    //timer
    var remTime = setInterval(function(){
        ctr++;
        //use default value if currentSec is = 0
        if(currentSec == 0){
            currentSec = 1;
        }
        secCtrDefault = currentSec;
        secCtrDefault-=ctr;
        if(secCtrDefault == 0){
            setTimeout(function(){
                // minCtr-=1;
                    if(minCtr > 0){
                        minCtr-=1;
                    }else{
                        minCtr=60;
                        minCtr-=1;
                        hrCtr-=1;
                    }
                secCtrDefault = 60;
                currentSec = secCtrDefault;
            },500);
            ctr = 0;
        }
        //stop timer
        if(hrCtr == 0 && minCtr == 0 &&secCtrDefault == 0){
            console.log("stop");
            updateProjStat();
            $('#timex').modal('show');
            clearInterval(remTime);
        }
        //add 0 if length is 1
        if(secCtrDefault.toString().length == 1)
            secCtrDefault = '0' + secCtrDefault;
        if(minCtr.toString().length == 1)
            minCtr = '0' + minCtr;
        if(hrCtr.toString().length == 1)
            hrCtr = '0' + hrCtr;

        $('#kra').html('<b class="badge-lg">Time Remaining ('+hrCtr+':'+minCtr+':'+secCtrDefault+')</b>'); //append the display
    }, 1000);

    //update projstat
    function updateProjStat(){
        db.transaction(function(tx){
            tx.executeSql('UPDATE project_name SET status = 2', [], function(){
                queryAllKRA();
            });
        });
    }

    //time remaining
    function timeRem(){
        db.transaction(function(tx){
            tx.executeSql('SELECT * FROM epiryDate ORDER BY rowid DESC LIMIT 1', [], function(tx, result){
                    hrCtr = result.rows.item(0).hours;
                    minCtr = result.rows.item(0).minutes;
                    currentSec = result.rows.item(0).seconds;
            });
        });
    }

    function indicatorsL(){
        timeRem();
        var kra_id = getParameterByName("kra_id");

        //last rate
        db.transaction(function(tx){
            tx.executeSql('SELECT rowid, progress_number FROM progress WHERE kra_id = ? AND status = 0 ORDER BY rowid DESC LIMIT 1',[kra_id], function(tx, result){
                var ct = result.rows.length;
                if(ct != 0){
                    prognum = result.rows.item(0).progress_number;
                    // $("#myCarousel").carousel(result.rows.item(0).progress_number);
                }
            });
        });

        db.transaction(function(tx){
            tx.executeSql("SELECT indicator.rowid, kra.kra_name, indicator.detail_name, indicator.parent_id, indicator.indicators_id, indicator.status, indicator.has_child FROM kra LEFT JOIN indicator ON kra.kra_id = indicator.kra_id WHERE kra.kra_id = ? ",[kra_id], function(tx, result){
                count = result.rows.length;
                var status = "", icon = "", color = "";
                var slide = 0;

                //add 0 if length is 1
                if(secCtrDefault.toString().length == 1)
                    secCtrDefault = '0' + secCtrDefault;
                if(minCtr.toString().length == 1)
                    minCtr = '0' + minCtr;
                if(hrCtr.toString().length == 1)
                    hrCtr = '0' + hrCtr;

                $('#kra').html('<b class="badge-lg">Time Remaining ('+hrCtr+':'+minCtr+':'+currentSec+')</b>');
                for(var i=0;i<count;i++){
                    if(result.rows.item(i).parent_id == 0){
                        if(result.rows.item(i).has_child == 1){
                            finind = result.rows.item(i).indicators_id;
                            if(i == 0){
                                status1 = "active";
                            }
                            else{
                                status1 = "";
                            }

                            $("#indicator").html('<span id='+result.rows.item(0).indicators_id+'>'+result.rows.item(0).detail_name+'</span><span class="timearea"><b class="badge badge-lg"></b></span>');

                            $(".inddata").append('<div class="subindwrap" data-id3='+finind+' id="siw'+finind+'"><h5 class="parent-indicator" style="color: #333; text-align: left">'+result.rows.item(i).detail_name+'</h5><hr><div class="row"><table class="table table-striped"><thead><th>Indicator Name</th><th>Rating</th></thead><tbody id="data'+finind+'" style="text-align: left"></tbody></table></div></div>');

                            for(var a=0;a<count; a++){

                                var dd = result.rows.item(a).parent_id;
                                var statind = result.rows.item(a).status;

                                if(finind == dd){
                                    // console.log(finind);
                                    //icon
                                    if(statind == 1){
                                    icon = "fa fa-check-circle";
                                    color = "color: #03ce68; font-size: 20px; float: right;";
                                    }
                                    else{
                                    icon = "";
                                    color = "";
                                    }
                                    //end icon
                                    counter++;
                                    if(counter == 1){
                                        status = "active";
                                    }else{
                                        status = "";
                                    }
                                     sub_id = result.rows.item(a).indicators_id;

                                    $("#data"+dd+"").append("<tr><td>"+result.rows.item(a).detail_name+"</td><td><button class='btn btn-proceed' data-target='#myCarousel' data-id2="+finind+" data-slide-to="+slide+" id='proceed' data-sb="+sub_id+" data-num="+counter+">Proceed</button></td></tr>");

                                    $("#carousel-sc").append('<div class="item '+status+'" id="id'+counter+'" data-id='+finind+' data-subid='+sub_id+'><p class="h4 title indi-cat-title">'+result.rows.item(a).detail_name+' <i class="'+icon+'" style="'+color+'"></i></p><div class="container"><div class="row" id="lops'+sub_id+'"></div></div><div>');
                                    $('#bar').append('<div id="snackbar" class="page'+sub_id+'">Page '+counter+' of <span id="total'+sub_id+'"></span></div>');

                                    loadLops(sub_id, counter);
                                    slide++;
                                }

                            }
                        
                        }else{
                            var ffd = result.rows.item(i).indicators_id;
                            counter++;
                            $("#carousel-sc").append('<div class="item" id="id'+counter+'" data-id='+ffd+' data-subid='+ffd+'><div class="container"><div class="row" id="lops'+ffd+'"></div></div><div>');
                            $('#bar').append('<div id="snackbar" class="page'+ffd+'">Page '+counter+' of <span id="total'+ffd+'"></span></div>');
                            loadLops(ffd, counter);
                        }
                    }
                }

                //condition last rated if progress number has a value
                if(totalscrated != 0){
                    countprog = counter;
                    countprog-=totalscrated;
                    // counter-=prognum;
                    console.log(countprog);
                    $("#myCarousel").carousel(totalscrated);
                    var progs = totalscrated + 1;
                    var indiid = 0, order;
                    k=progs;
                    indiid = $("#id"+progs).attr("data-id");
                    var subd = $("#id"+progs).attr("data-subid");
                    $("#total"+subd).html(counter);
                    $(".page"+subd).addClass("show");
                    setTimeout(function(){$(".page"+subd).removeClass("show");},1000);

                    for(var i=0;i<count;i++){
                        if(result.rows.item(i).indicators_id == indiid){
                                $("#indicator").html('<span id='+result.rows.item(i).indicators_id+'>'+result.rows.item(i).detail_name+'</span><span class="timearea"></span>');
                        }
                    }
                }

                //events
                $(document).on("click", "#proceed",function(){
                var indiid2 = $(this).attr("data-id2");
                var subd = $(this).attr("data-sb");
                var num = $(this).attr("data-num");
                //change indicator name
                for(var i=0;i<count;i++){
                    if(result.rows.item(i).indicators_id == indiid2){
                        $("#indicator").html('<span id='+result.rows.item(i).indicators_id+'>'+result.rows.item(i).detail_name+'</span>');
                    }
                }
                //snackbar
                k = num;
                $("#parentModal").modal("hide");
                $("#total"+subd).html(counter);
                $(".page"+subd).addClass("show");
                setTimeout(function(){$(".page"+subd).removeClass("show");},1000);
                });

                 $("#btn-continue").on("click", function(){
                    var indiid = 0, order;
                    var slen = scores.length;

                    if(slen < counter){
                        if(countprog == slen && countprog != 0){
                            $("#loader").modal("show");
                            $("#btn-continue").attr("disabled", true);

                            db.transaction(function(tx){
                                tx.executeSql('SELECT * FROM rate_tracker WHERE change_status = 1', [], function(tx, res){
                                    var len = res.rows.length;
                                    if(len > 0){
                                        for(var i=0;i<len;i++){
                                            var round_number = res.rows.item(i).round_number;
                                            var sc_id = res.rows.item(i).sc_id;
                                            var evalid = res.rows.item(i).eval_id;
                                            var kra_id = res.rows.item(i).kra_id;
                                            var Indicator = res.rows.item(i).indicator_id;
                                            var sub_indi = res.rows.item(i).sub_indicator_id;
                                            var Lop_Letter = res.rows.item(i).lop_letter;
                                            var Lop_Value = res.rows.item(i).lop_value;
                                            var equivalents = res.rows.item(i).equivalent;
                                            checkIfExist(i, round_number, sc_id, evalid, kra_id, Indicator, sub_indi, Lop_Letter, Lop_Value, equivalents);
                                        }
                                    }else{
                                        saveTimeLeft();
                                    }
                                });
                            });

                            // for(var i=0; i<scores.length; i++){
                            //     var round_number = scores[i].round_number;
                            //     var sc_id = scores[i].sc_id;
                            //     var evalid = scores[i].eval_id;
                            //     var kra_id = scores[i].kra_id;
                            //     var Indicator = scores[i].Indicator;
                            //     var sub_indi = scores[i].sub_indi;
                            //     var Lop_Letter = scores[i].Lop_Letter;
                            //     var Lop_Value = scores[i].Lop_Value;
                            //     var equivalents = scores[i].equivalent;
                            //     // alert("Round "+round_number +" Scorecard ID "+ sc_id +" Evalid "+ evalid +" KRA Id "+ kra_id +" Indicator "+ Indicator +" Sub Indi "+ sub_indi +" Lop Let "+ Lop_Letter +" Lop val "+ Lop_Value +"  Eq "+ equivalents);
                            //     checkIfExist(i, round_number, sc_id, evalid, kra_id, Indicator, sub_indi, Lop_Letter, Lop_Value, equivalents);
                            //     //saveScores(i, round_number, sc_id, evalid, kra_id, Indicator, sub_indi, Lop_Letter, Lop_Value);
                            // }
                        }else{
                            k++;
                            p = k;
                            if( k <= counter){
                                indiid = $("#id"+k).attr("data-id");
                                var subd = $("#id"+k).attr("data-subid");
                                $("#total"+subd).html(counter);
                                $(".page"+subd).addClass("show");
                                setTimeout(function(){$(".page"+subd).removeClass("show");},1000);
                            }else if(k > counter){
                                k = counter;
                            }

                            for(var i=0;i<count;i++){
                                if(result.rows.item(i).indicators_id == indiid){
                                        $("#indicator").html('<span id='+result.rows.item(i).indicators_id+'>'+result.rows.item(i).detail_name+'</span><span class="timearea"></span>');
                                }
                            }
                        }

                    }else if(slen == counter){
                        $("#loader").modal("show");
                        $("#btn-continue").attr("disabled", true);
                        db.transaction(function(tx){
                                tx.executeSql('SELECT * FROM rate_tracker WHERE change_status = 1', [], function(tx, res){
                                    var len = res.rows.length;
                                    if(len > 0){
                                        for(var i=0;i<len;i++){
                                            var round_number = res.rows.item(i).round_number;
                                            var sc_id = res.rows.item(i).sc_id;
                                            var evalid = res.rows.item(i).eval_id;
                                            var kra_id = res.rows.item(i).kra_id;
                                            var Indicator = res.rows.item(i).indicator_id;
                                            var sub_indi = res.rows.item(i).sub_indicator_id;
                                            var Lop_Letter = res.rows.item(i).lop_letter;
                                            var Lop_Value = res.rows.item(i).lop_value;
                                            var equivalents = res.rows.item(i).equivalent;
                                            checkIfExist(i, round_number, sc_id, evalid, kra_id, Indicator, sub_indi, Lop_Letter, Lop_Value, equivalents);
                                        }
                                    }else{
                                        saveTimeLeft();
                                    }
                                });
                            });
                        // for(var i=0; i<scores.length; i++){
                        //     var round_number = scores[i].round_number;
                        //     var sc_id = scores[i].sc_id;
                        //     var evalid = scores[i].eval_id;
                        //     var kra_id = scores[i].kra_id;
                        //     var Indicator = scores[i].Indicator;
                        //     var sub_indi = scores[i].sub_indi;
                        //     var Lop_Letter = scores[i].Lop_Letter;
                        //     var Lop_Value = scores[i].Lop_Value;
                        //     var equivalents = scores[i].equivalent;
                        //     // alert("Round "+round_number +" Scorecard ID "+ sc_id +" Evalid "+ evalid +" KRA Id "+ kra_id +" Indicator "+ Indicator +" Sub Indi "+ sub_indi +" Lop Let "+ Lop_Letter +" Lop val "+ Lop_Value +"  Eq "+ equivalents);
                        //     checkIfExist(i, round_number, sc_id, evalid, kra_id, Indicator, sub_indi, Lop_Letter, Lop_Value, equivalents);
                        //     //saveScores(i, round_number, sc_id, evalid, kra_id, Indicator, sub_indi, Lop_Letter, Lop_Value);
                        // }
                    }


                });

                $("#btn-prev").on("click", function(){
                    var x = document.getElementById("snackbar");
                    var prev;
                    var indiid = 0
                    if($("#myCarousel").hasClass("carousel")){
                        o++;
                        k -= 1;
                        if(k > 0){
                            var slen = k - o;
                            indiid = $("#id"+k).attr("data-id");
                            var subd = $("#id"+k).attr("data-subid");
                            $("#total"+subd).html(counter);
                            $(".page"+subd).addClass("show");
                            setTimeout(function(){$(".page"+subd).removeClass("show");},1000);

                            if(slen != 0 || slen == 0){
                            prev = k;
                            }
                        }else if(k == 0 || k < 0){
                            k = 1;
                        }

                        for(var i=0;i<count;i++){
                            if(result.rows.item(i).indicators_id == indiid){
                                    $("#indicator").html('<span id='+result.rows.item(i).indicators_id+'>'+result.rows.item(i).detail_name+'</span><span class="timearea"></span>');
                            }
                        }
                    }
                });
            });
        });
    }

    indicatorsL();

    function checkIfExist(i, round_number, sc_id, evalid, kra_id, Indicator, sub_indi, Lop_Letter, Lop_Value, equivalents){
        db.transaction(function(tx){
            tx.executeSql("SELECT rowid FROM scorecard_ledger_header WHERE scorecard_header_id = "+sc_id+" LIMIT 1", [], function(tx, result){
                var len = result.rows.length;
                // alert(len);
                if(len == 0){
                    saveScores(i, round_number, sc_id, evalid, kra_id, Indicator, sub_indi, Lop_Letter, Lop_Value, equivalents);
                }else{
                    var ledger_id = result.rows.item(0).rowid;
                    // alert(ledger_id);
                    saveDetails(i,ledger_id,Lop_Letter,kra_id, Indicator, sub_indi, Lop_Letter, Lop_Value, equivalents);
                }
            });
        });
    }
    var prevRate = [];
    function loadLops(sub_indicator_id, counter){
        // alert(sub_indicator_id);
        db.transaction(function(tx){
        tx.executeSql("SELECT * FROM scorecard WHERE sub_indicators_id = ? ORDER BY lop_letter DESC",[sub_indicator_id],function(tx,result){
           var len = result.rows.length;
           var g, val, classstat, ratedStat;
           for(var i=0;i<len; i++){
            var lopstatus = result.rows.item(i).status;

            //get highest score
            for(var a= 0; a<len; a++){
                g = result.rows.item(a).lop_letter;
                if(g.trim() === "A"){
                    val = result.rows.item(a).lop_value;
                }
            }
            //display check icon if rated
            if(lopstatus == 1){
                classstat = "display: block";
                prevRate.push(result.rows.item(i).lop_letter);
                ratedStat = "rated";
            }else{
                ratedStat = "";
                classstat = "display: none";
            }

            $('#lops'+sub_indicator_id).append('<div class="indi-cont-in" id="checkVals"><input type="checkbox" class="input'+i+'" id="checkbox" value='+result.rows.item(i).lop_value+' data-row-id='+result.rows.item(i).lop_id+' data-ps='+val+' data-weight='+result.rows.item(i).ti_weight+' data-letter='+result.rows.item(i).lop_letter+' data-parent='+result.rows.item(i).parent_id+' data-indicator='+result.rows.item(i).sub_indicators_id+' data-counter='+counter+' data-rated='+ratedStat+'><div class="clearfix"><div class="col-xs-9 col-xs-offset-3 rel lop-cont"><p>'+result.rows.item(i).lop_description+'</p><div class="flip-cont"><div class="flip"><p>'+result.rows.item(i).lop_letter+'</p><span style="visibility:hidden">'+result.rows.item(i).lop_letter+'</span></div></div></div></div><div class="v-align v'+result.rows.item(i).sub_indicators_id+'" id="bg" style="'+classstat+'"><div class="d-table"><div class="table-cell"><div class="check_mark"><div class="sa-icon sa-success animate"><span class="sa-line sa-tip animateSuccessTip"></span><span class="sa-line sa-long animateSuccessLong"></span><div class="sa-placeholder"></div><div class="sa-fix"></div></div></div></div></div></div></div>');
           }
        });
        });
    }

    var existInd = [];

    $(document).on("click", "#checkVals", function(){
        value = $(this).find("#checkbox").val();
        letter = $(this).find("#checkbox").attr("data-letter");
        indicatorid = $(this).find("#checkbox").attr("data-parent");
        sub_indicator_id = $(this).find("#checkbox").attr("data-indicator");
        ti_weight = $(this).find("#checkbox").attr("data-weight");
        ps = $(this).find("#checkbox").attr("data-ps");
        identifier = $(this).find("#checkbox").attr("data-counter");
        var lop_id = $(this).find("#checkbox").attr("data-row-id");
        // console.log(identifier);
        updateSC(sub_indicator_id, value, letter, indicatorid, ti_weight, ps, identifier, lop_id);
        $(".v-align.v"+sub_indicator_id+"").css("display","none");
        $(this).find(".v-align.v"+sub_indicator_id+"").css("display","block");

    });

    function updateSC(sub_indicator_id, value, letter, indicatorid, ti_weight, ps, identifier, lop_id){
        equivalent = (Number(value) / Number(ps)) * Number(ti_weight);

        var round_number = getParameterByName("round_number"),
        sc_id = getParameterByName("sc_id"),
        eval_id = getParameterByName("evalid"),
        kra_id = getParameterByName("kra_id");

        db.transaction(function(tx){
            tx.executeSql('SELECT * FROM rate_tracker WHERE sub_indicator_id = ?', [sub_indicator_id], function(tx, res){
                var len = res.rows.length;
                if(len > 0){
                    db.transaction(function(tx1){
                        tx1.executeSql('UPDATE rate_tracker SET lop_value = ?, lop_letter = ?, equivalent = ?, change_status = 1 WHERE sub_indicator_id = ?', [value, letter, equivalent, sub_indicator_id], function(){
                            console.log('Updated');
                        });
                    });
                }else{
                    db.transaction(function(tx1){
                        tx1.executeSql('INSERT INTO rate_tracker (sub_indicator_id, lop_value, lop_letter, indicator_id, round_number, sc_id, eval_id, kra_id, equivalent, change_status) VALUES (?,?,?,?,?,?,?,?,?, 1)', [sub_indicator_id, value, letter, indicatorid, round_number, sc_id, eval_id, kra_id, equivalent], function(){
                            scores.push('scores');
                            updateInd(sub_indicator_id, indicatorid);
                        });
                    });
                }
            });
        });
            // if(existInd.indexOf(sub_indicator_id) == -1){
            //     existInd.push(sub_indicator_id);
            //     scores.push({
            //         "round_number": getParameterByName("round_number"),
            //         "sc_id": getParameterByName("sc_id"),
            //         "eval_id": getParameterByName("evalid"),
            //         "kra_id": getParameterByName("kra_id"),
            //         "Indicator": indicatorid,
            //         "sub_indi": sub_indicator_id,
            //         "Lop_Letter": letter,
            //         "Lop_Value": value,
            //         "equivalent": equivalent
            //     });

            //     updateInd(sub_indicator_id, indicatorid);
            //     // querySCStatus(sub_indicator_id, letter);
            // }else{
            //     console.log(getParameterByName("round_number"));
            //     var arr = identifier-1;
            //     console.log(arr);
            //     scores[arr].round_number = getParameterByName("round_number");
            //     scores[arr].sc_id = getParameterByName("sc_id");
            //     scores[arr].eval_id = getParameterByName("evalid");
            //     scores[arr].kra_id = getParameterByName("kra_id");
            //     scores[arr].Indicator = indicatorid;
            //     scores[arr].sub_indi = sub_indicator_id;
            //     scores[arr].Lop_Letter = letter;
            //     scores[arr].Lop_Value = value;
            //     scores[arr].equivalent = equivalent;

            //     // querySCStatus(sub_indicator_id, letter);
            // }

            // countSubind(indicatorid);
    }

/*    function querySCStatus(sub_indicator_id, letter){
        db.transaction(function(tx){
            tx.executeSql("SELECT * FROM scorecard WHERE sub_indicators_id = ? AND status = 1", [sub_indicator_id], function(tx, result){
                var len = result.rows.length;
                // alert(len);
                if(len > 0){
                    db.transaction(function(tx1){
                        tx1.executeSql("UPDATE scorecard SET status = 0 WHERE sub_indicators_id = ?", [sub_indicator_id], function(tx1, result1){
                            db.transaction(function(tx2){
                                tx2.executeSql("UPDATE scorecard SET status = 1 WHERE sub_indicators_id = ? AND lop_letter = ?", [sub_indicator_id, letter], function(tx4, res4){
                                    console.log("fdgdfg");
                                });
                            });
                        });
                    });
                }else{
                // alert(letter);
                    db.transaction(function(tx3){
                            tx3.executeSql("UPDATE scorecard SET status = 1 WHERE sub_indicators_id = ? AND lop_letter = ?", [sub_indicator_id, letter], function(tx3, res3){
                                console.log("saved");
                            });
                        });
                }
            });
        });
    }*/

    function updateInd(sub_indicator_id, indicatorid){
        db.transaction(function(tx){
            tx.executeSql("UPDATE indicator SET status = 1 WHERE indicators_id = ?", [sub_indicator_id],function (tx, res){
                console.log('updateInd');
                countSubind(indicatorid);
            });
        });
    }

    function countSubind(indicatorid){
        // alert(indicatorid);
        db.transaction(function(tx){
            tx.executeSql("SELECT COUNT(parent_id) as icount FROM indicator WHERE parent_id = ?", [indicatorid], function(tx, result){
                subindTotal = result.rows.item(0).icount;
                checkSubCount(indicatorid);
                // alert(result.rows.item(0).icount;);
            });
        });
    }

    function checkSubCount(indicatorid){
        db.transaction(function(tx){
            tx.executeSql("SELECT COUNT(parent_id) as iicount, parent_id FROM indicator WHERE parent_id = ? AND status = 1", [indicatorid], function(tx, res){
                if(subindTotal == res.rows.item(0).iicount){
                    updateIndStat(res.rows.item(0).parent_id);
                }
                // alert(res.rows.item(0).iicount);
            });
        });
    }

    function updateIndStat(prntid){
        db.transaction(function(tx){
            tx.executeSql("UPDATE indicator SET indicator_status = 1 WHERE indicators_id = ?", [prntid], function(tx, res){
                console.log("updated");
            });
        });
    }

    function saveScores(i, round_number, sc_id, evalid, kra_id, Indicator, sub_indi, Lop_Letter, Lop_Value,equivalents){
            (function(i){
                var executeQuery = "INSERT INTO scorecard_ledger_header (scorecard_header_id, assessment_round_id, evaluator_id) SELECT "+sc_id+", "+round_number+", "+evalid+" WHERE NOT EXISTS (SELECT 1 FROM scorecard_ledger_header WHERE scorecard_header_id = "+sc_id+" AND assessment_round_id = "+round_number+" AND evaluator_id = "+evalid+")";
                db.transaction(function(tx){
                    var hello = "hello";
                    tx.executeSql(executeQuery, [], function(tx, res){
                        var ledger_id = res.insertId;
                        saveDetails(i,ledger_id,Lop_Letter,kra_id, Indicator, sub_indi, Lop_Letter, Lop_Value, equivalents);
                    }, errorCB);
                }, errorCB);
            })(i);
    }

    function errorCB(err) {
     alert("Error processing SQLSDSDD: "+err.code);
    }

    function successCB() {
    }

    function success1(tx, res){
        // alert(res.insertId);
    }

    function saveDetails(i,ledger_id,Lop_Letter,kra_id, Indicator, sub_indi, Lop_Letter, Lop_Value, equivalents){
        var status = "PENDING";
        db.transaction(function(tx){
            tx.executeSql('SELECT * FROM scorecard_ledger_detail WHERE kra_id = ? AND indicator_id = ?', [kra_id,sub_indi], function(tx, res){
                var len = res.rows.length;
                    if(len > 0){
                        db.transaction(function(tx1){
                            tx1.executeSql('UPDATE scorecard_ledger_detail SET lop_letter = ?, lop_value = ?, equivalent = ? WHERE kra_id = ? AND indicator_id = ?', [Lop_Letter, Lop_Value, equivalents, kra_id, sub_indi], function(){
                                    if(i==scores.length-1 || i==rtcount-1){
                                        if(btnback == "back"){
                                            db.transaction(function(tx1){
                                                tx1.executeSql("UPDATE rate_tracker SET change_status = 0", [], function(){
                                                        // window.location.replace("kra.html?sc_id="+getParameterByName("sc_id")+"&round_number="+getParameterByName("round_number")+"&evalid="+getParameterByName("evalid")+"&sctype="+getParameterByName('sctype'));
                                                        saveTimeLeft();
                                                });
                                            });
                                        }else{
                                            $("#myModal").modal("show");
                                            getKRA(i);
                                        }
                                    }
                            });
                        });
                    }else{
                        db.transaction(function(tx1){
                            tx1.executeSql("INSERT INTO scorecard_ledger_detail (ledger_id, kra_id, parent_id, indicator_id, lop_letter, lop_value, status, equivalent) VALUES ("+ledger_id+", "+kra_id+", "+Indicator+", "+sub_indi+",'"+Lop_Letter+"', "+Lop_Value+", '"+status+"', '"+equivalents+"')", [], function(){
                                    if(i==scores.length-1 || i==rtcount-1){
                                        if(btnback == "back"){
                                            db.transaction(function(tx1){
                                                tx1.executeSql("UPDATE rate_tracker SET change_status = 0", [], function(){
                                                        // window.location.replace("kra.html?sc_id="+getParameterByName("sc_id")+"&round_number="+getParameterByName("round_number")+"&evalid="+getParameterByName("evalid"));
                                                        saveTimeLeft();
                                                });
                                            });
                                        }else{
                                        $("#myModal").modal("show");
                                        getKRA(i);
                                        }
                                    }
                            });
                        });
                    }
            });
        });
    }

    function getKRA(i){
        $("#loader").modal("hide");
            var kra_id = getParameterByName("kra_id");
            db.transaction(function(tx){
            tx.executeSql("SELECT * FROM indicator LEFT JOIN kra on indicator.kra_id = kra.kra_id WHERE indicator.kra_id = ? AND indicator.parent_id = 0 AND indicator.indicator_status = 1",[kra_id],function(tx,result){
               var len = result.rows.length;
               var ff = 0;
               for(var i=0;i<len;i++){
                ff++;
                    $(".kra-name").html(result.rows.item(i).kra_name);
                        $(".ind-wrap").append('<hr><h5 class="parent-indicator" style="color: #333; text-align: left">'+result.rows.item(i).detail_name+'</h5><hr><div class="row"><table class="table table-striped"><thead><th>Indicator Name</th><th>Rating</th></thead><tbody id="data'+ff+'" style="text-align: left"></tbody></table></div>');
                    sub_id = result.rows.item(i).indicators_id;
                    getData(sub_id, ff);
               }
            });
            });
    }

    function getData(sub_id, ff){
        var kra_id = getParameterByName("kra_id");
        db.transaction(function(tx){
            tx.executeSql("SELECT scorecard_ledger_detail.indicator_id,scorecard_ledger_detail.lop_letter,scorecard_ledger_detail.kra_id, scorecard_ledger_detail.lop_value, kra.kra_name, in1.detail_name as detail_name1, in2.detail_name as detail_name2, in1.parent_id as parent1, in2.parent_id as parent2 FROM scorecard_ledger_detail LEFT JOIN kra ON scorecard_ledger_detail.kra_id = kra.kra_id LEFT JOIN indicator as in1 ON scorecard_ledger_detail.parent_id = in1.indicators_id LEFT JOIN indicator as in2 ON scorecard_ledger_detail.indicator_id = in2.indicators_id WHERE scorecard_ledger_detail.ledger_id <> 0 AND scorecard_ledger_detail.kra_id = "+kra_id+"",[],function(tx,result){
               var len = result.rows.length;
               for(var i=0;i<len; i++){
                var parent_id = result.rows.item(i).parent2;
                if(sub_id == parent_id){
                  var ind = result.rows.item(i).indicator_id;
                  var ll = result.rows.item(i).lop_letter;
                  updateSCStatus(ind, ll);
                  $("#data"+ff+"").append('<tr><td>'+result.rows.item(i).detail_name2+'</td><td>'+result.rows.item(i).lop_letter+'</td></tr>');
                }
                }
            });
            });
    }

    function updateSCStatus(ind, ll){
      db.transaction(function(tx){
        tx.executeSql('SELECT lop_letter, status FROM scorecard WHERE sub_indicators_id = ? AND status = 1', [ind], function(tx, res){
            var len = res.rows.length;
                 if(len > 0){
                    console.log(true);
                    db.transaction(function(tx1){
                        tx1.executeSql('UPDATE scorecard SET status = 0 WHERE sub_indicators_id = ?', [ind], function(){
                            db.transaction(function(tx2){
                                tx2.executeSql("UPDATE scorecard SET status = 1 WHERE sub_indicators_id = ? AND LIKE ('%"+ll+"%', lop_letter) = 1", [ind], function(){
                                    console.log('updated');
                                });
                            });
                        });
                    });
                }else{
                    db.transaction(function(tx2){
                                tx2.executeSql("UPDATE scorecard SET status = 1 WHERE sub_indicators_id = ? AND LIKE ('%"+ll+"%', lop_letter) = 1", [ind], function(){
                                    console.log('updated');
                                });
                            });
                }  
        });
      });
    }

    $("#indicator").on("click", function(){
        $(".subindwrap").addClass("invisible");
        var thisid = $(this).find("span").attr("id");
        var fg = document.getElementById("siw"+thisid+"");
        var getAttr = fg.getAttribute("data-id3");
        if(thisid == getAttr){
            $("#siw"+thisid+"").removeClass("invisible").addClass("visible");
        }
            $("#parentModal").modal("show");
    });


    $("#cancel").click(function(){
        scores = [];
        saveTimeLeft();
    });

    $(document).on('click', '#yf', function(){
        var data_rate = $(this).attr('data-rate');
        var data_ind = $(this).attr('data-ind');
        var data_kraid = $(this).attr('data-kra');
        $('#sid').val(data_ind);
        $('#ll').val(data_rate);
        $('#krd').val(data_kraid);
        console.log(data_rate + data_ind);
        $('#findings').modal('show');
    });

    $('#findingsForm').on('submit', function(e){
        e.preventDefault();
        var eid = getParameterByName('evalid');
        var kraid = getParameterByName('kra_id');
        var findingsName = $('#findName').val();
        var recommendationsname = $('#recomName').val();
        db.transaction(function(tx){
            tx.executeSql('INSERT INTO reports (kra_id, evaluator_id,findings, recommendations) VALUES (?,?,?,?)', [kraid, eid,findingsName, recommendationsname], function(tx, result){
                alert("Findings and Recommendations added!");
                saveTimeLeft();
            });
        });
    });

    //autofill if times up

    function queryAllKRA(){
        db.transaction(function(tx){
            tx.executeSql('SELECT * FROM kra', [], function(tx, res){
                for(var a=0;a<res.rows.length; a++){
                    checkRates(res.rows.item(a).kra_id);
                }
            });
        });
    }

    function checkRates(kraid){
    db.transaction(function(tx){
        tx.executeSql('SELECT COUNT(indicator.parent_id) as cc, COUNT(CASE WHEN indicator.status = 1 THEN indicator.status END) as ss, indicator.kra_id, indicator.indicators_id, indicator.parent_id FROM indicator LEFT JOIN kra ON indicator.kra_id = kra.kra_id WHERE kra.kra_id = ? AND indicator.has_child = 0', [kraid], function(tx, result){
            var kulang = Number(result.rows.item(0).cc) - Number(result.rows.item(0).ss);
            var kraid = result.rows.item(0).kra_id;
            var indicators_id = result.rows.item(0).indicators_id;
            if(kulang > 0){
                console.log(kulang +'='+kraid);
                autoFillData(kraid, indicators_id, parent_id);
            }
        });
    }); 
    }

    function autoFillData(kraid, indicators_id, prntid){
        db.transaction(function(tx){
            tx.executeSql('INSERT INTO scorecard_ledger_detail (ledger_id, kra_id, parent_id, indicator_id, lop_letter, lop_value, status, equivalent) VALUES (0, '+kraid+', '+prntid+', '+indicators_id+', "0","0", "PENDING", "0")', [], function(){
                console.log('sdsd');
            });
        });
    }
    
    //send to admin finish or unfinish
    $('#sendData').on('click', function(){
            getEqui();  
        });

        function getEqui(){
            var evaluator_id = getParameterByName("evalid");
                db.transaction(function(tx){
                    tx.executeSql("SELECT SUM(equivalent) as gt FROM scorecard_ledger_detail", [], function(tx, result){
                        var gt = result.rows.item(0).gt;
                        getHeader(gt, evaluator_id);
                    });
            });
        }

        function getHeader(gt, evaluator_id){
            db.transaction(function(tx){
            tx.executeSql("SELECT * FROM scorecard_ledger_header WHERE evaluator_id = ?",[evaluator_id],function(tx,result){

                var scorecard_header_id = result.rows.item(0).scorecard_header_id;
                var assessment_round_id = result.rows.item(0).assessment_round_id;
                var evaluator_id = result.rows.item(0).evaluator_id;

                insertHeader(scorecard_header_id, assessment_round_id, evaluator_id, gt);
            });
            });
        }

        function insertHeader(scorecard_header_id, assessment_round_id, evaluator_id, gt){
            var inserts = "scorecard_header_id="+scorecard_header_id+"&assessment_round_id="+assessment_round_id+"&evaluator_id="+evaluator_id+"&gt="+gt;
            $.ajax({
                type: "POST",
                url: "http://"+ipConfigs.ip+"/phpFile/actions/insertHeader.php",
                data: inserts,
                success: function(id){
                    var status = "PENDING";
                    getDetails(id,status);
                }
            });
        }

        function getDetails(id,status){
        db.transaction(function(tx){
        tx.executeSql("SELECT * FROM scorecard_ledger_detail WHERE status = ?",[status],function(tx,result){
           var len = result.rows.length;

           for(var i=0;i<len; i++){
            var kra_id = result.rows.item(i).kra_id;
            var parent_id = result.rows.item(i).parent_id;
            var indicator_id = result.rows.item(i).indicator_id;
            var lop_letter = result.rows.item(i).lop_letter;
            var lop_value = result.rows.item(i).lop_value;
            var equivalents = result.rows.item(i).equivalent;

            sendToAdmin(id, kra_id, parent_id, indicator_id,lop_letter, lop_value, len, equivalents);
           }
            });
            });
        }

        function sendToAdmin(id, kra_id, parent_id, indicator_id,lop_letter, lop_value, len, equivalents){
            var dataS = "id="+id+"&kra_id="+kra_id+"&parent_id="+parent_id+"&indicator_id="+indicator_id+"&lop_letter="+lop_letter+"&lop_value="+lop_value+"&equivalents="+equivalents;
            $.ajax({
                type: "POST",
                url: "http://"+ipConfigs.ip+"/phpFile/actions/sendScores.php",
                data: dataS,
                success: function(response){
                    res++;
                    if(res == 1){
                        window.open('http://'+ipConfigs.ip+'/blog/public/timeout', '_blank');
                    }
                }
            });

        }

    //END FOR SEED
    }
});
