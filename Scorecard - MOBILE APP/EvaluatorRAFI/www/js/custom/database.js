var db = window.openDatabase("Evaluator Database", "1.0", "Evaluator RAFI Database", 1000000);
db.transaction(populateDB, errorCB, successCB);

//callbacks
function populateDB(tx){
 tx.executeSql('CREATE TABLE IF NOT EXISTS scorecard (lop_id INTEGER NOT NULL,sub_indicators_id INTEGER NOT NULL,parent_id INTEGER NOT NULL,kra_id INTEGER NOT NULL,kra_name TEXT NOT NULL, tk_weight REAL NOT NULL, detail_name TEXT NOT NULL,ti_weight REAL NOT NULL,lop_code TEXT NOT NULL,lop_letter TEXT NOT NULL,lop_value REAL NOT NULL,lop_description TEXT NOT NULL, status INTEGER)');
 tx.executeSql('CREATE TABLE IF NOT EXISTS project_name (evaluator_id INTEGER NOT NULL,round_num INTEGER NOT NULL,scorecard_id INTEGER NOT NULL,project_name TEXT NOT NULL, start_date TEXT NOT NULL, end_date TEXT NOT NULL, division_name TEXT NOT NULL, district_name TEXT NOT NULL, school_name TEXT NOT NULL, status INTEGER NOT NULL, sendStatus INTEGER NOT NULL, round_name INTEGER NOT NULL, evaluator_weight INTEGER, scorecard_type INTEGER NOT NULL)');
 tx.executeSql('CREATE TABLE IF NOT EXISTS kra(kra_id INTEGER NOT NULL, kra_name TEXT NOT NULL, weight REAL NOT NULL)');
 tx.executeSql('CREATE TABLE IF NOT EXISTS indicator(indicators_id INTEGER NOT NULL,kra_id INTEGER NOT NULL, detail_name TEXT NOT NULL, weight REAL NOT NULL, parent_id INTEGER NOT NULL, status INTEGER, indicator_status INTEGER, has_child INTEGER DEFAULT 0)');
 tx.executeSql('CREATE TABLE IF NOT EXISTS scorecard_ledger_header(scorecard_header_id INTEGER NOT NULL, assessment_round_id INTEGER NOT NULL, evaluator_id INTEGER NOT NULL)');
 tx.executeSql('CREATE TABLE IF NOT EXISTS scorecard_ledger_detail(ledger_id INTEGER NOT NULL, kra_id INTEGER NOT NULL, parent_id INTEGER NOT NULL ,indicator_id INTEGER NOT NULL, lop_letter TEXT NOT NULL, lop_value TEXT NOT NULL, status TEXT NOT NULL, equivalent TEXT)');
 tx.executeSql('CREATE TABLE IF NOT EXISTS reports(kra_id INTEGER NOT NULL, evaluator_id INTEGER NOT NULL, findings TEXT NOT NULL, recommendations TEXT NOT NULL, sub_indicators_id INTEGER)');
 tx.executeSql('CREATE TABLE IF NOT EXISTS epiryDate(hours INTEGER NOT NULL, minutes INTEGER NOT NULL, seconds INTEGER NOT NULL)');
 tx.executeSql('CREATE TABLE IF NOT EXISTS loginTime(hoursLog INTEGER NOT NULL, minutesLog INTEGER NOT NULL, secondsLog INTEGER NOT NULL)');
  tx.executeSql('CREATE TABLE IF NOT EXISTS progress(kra_id INTEGER NOT NULL ,progress_number INTEGER NOT NULL, status INTEGER NOT NULL)');
  tx.executeSql('CREATE TABLE IF NOT EXISTS rate_tracker(sub_indicator_id INTEGER NOT NULL ,lop_value INTEGER NOT NULL, lop_letter TEXT NOT NULL, indicator_id INTEGER NOT NULL, round_number INTEGER NOT NULL, sc_id INTEGER NOT NULL, eval_id INTEGER NOT NULL, kra_id INTEGER NOT NULL, equivalent TEXT NOT NULL, change_status INTEGER NOT NULL)');
}

function errorCB(err) {
 alert("Error processing SQL: "+err.code);
}

function successCB() {
 // alert("success!");
}