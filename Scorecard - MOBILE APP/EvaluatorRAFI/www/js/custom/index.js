var db = window.openDatabase("Evaluator Database", "1.0", "Evaluator RAFI Database", 1000000);
var kra_name, tk_weight, detail_name, ti_weight, lop_code, lop_letter, lop_value, lop_description, kra_id, parent_id, sub_indicators_id, lop_id, evalid1;


// alert(ipConfigs.ip);
$(document).ready(function(){
var ipConfigs = new ipConfig();
	$("#loginBtn").on("submit", function(e){
		e.preventDefault();
		var data = $(this).serialize();
		var url = "http://"+ipConfigs.ip+"/phpFile/actions/login.php";
		// alert(url);
		var method = $(this).attr("method");
		var i = 0;
		$.ajax({
			type: method,
			url: url,
			data: data,
			beforeSend: function(){
			$("#loginloader").modal("show");
			},
			success: function(response){
				// alert(response);
				if(response != 0 && response != 2 && response != 3 ){
				var obj = $.parseJSON(response);
				var sc_type = obj.scorecard_type;
				
				if(sc_type == 2){
					var division_name = obj.municipality_name;
					var district_name = obj.municipality_name;
					var school_name = obj.municipality_name;
				}else{
					var division_name = obj.division_name;
					var district_name = obj.district_name;
					var school_name = obj.school_name;
				}

				var eval_id = obj.evalid;
				var round_num = obj.roundnumber;
				var project_name = obj.project_name;
				var start_date = obj.start_date;
				var end_date = obj.end_date;
				var scorecard_id = obj.id;
				var round_name = obj.round_name;
				var evalweight = null;

				if(round_name == 1){
					evalweight = obj.evaluator_weight;
				}else{
					evalweight = null;
				}

				evalid1 = eval_id;
				var executeQuery = "INSERT INTO project_name (evaluator_id, round_num,scorecard_id,project_name, start_date, end_date, division_name, district_name, school_name, status, sendStatus, round_name, evaluator_weight, scorecard_type) VALUES ("+eval_id+","+round_num+","+scorecard_id+",'"+project_name+"','"+start_date+"','"+end_date+"','"+division_name+"','"+district_name+"', '"+school_name+"', 0, 0, "+round_name+", "+evalweight+", "+sc_type+")";
				db.transaction(function(tx){
					tx.executeSql(executeQuery, [], successCB, errorCB);
				}, errorCB);

				saveEvalAccesses(data, obj.id);

				}else if(response == 3){
					alert("Account already rated, You can't rate twice!");
					$("#loginloader").modal("hide");	
				}else if(response == 2){
					$("#loginloader").modal("hide");
					alert("This account has been logged in on another device.");
				}else if(response == 0){
					$("#loginloader").modal("hide");
					alert("Username or password incorrect!");
				}
			},
			error: function(){
				$("#loginloader").modal("hide");
				alert("Cant connect to Server!");
			}
		});
	});

	function saveEvalAccesses(dataS, shid){
		$.ajax({
			type: "GET",
			url: "http://"+ipConfigs.ip+"/phpFile/actions/getEvalAccess.php",
			data: dataS,
			success: function(response){
				var obj = $.parseJSON(response);
				var count = obj.length;

				for(var i=0; i<count;i++){
					var kra_id = obj[i].id;
					kra_name = obj[i].kra_name;
					var weight = obj[i].weight;
					(function(i){
						var executeQuery = "INSERT INTO kra (kra_id, kra_name, weight) VALUES ("+kra_id+",'"+kra_name+"',"+weight+")";
						db.transaction(function(tx){
							tx.executeSql(executeQuery, [], successCB, errorCB);
						}, errorCB);
					})(i);
				}

				syncScorecards(dataS,shid);

			}
		});
	}

	function syncScorecards(datas,shids){
		var sh = shids;
		$.ajax({
			type: "GET",
			url: "http://"+ipConfigs.ip+"/phpFile/actions/sync.php",
			data: "sh="+sh,
			beforeSend: function(){
				$("#loginloader").modal("hide");
				$("#loader").modal("show");
			},
			success: function(data){
				var obj = $.parseJSON(data);
				var count = obj.length;
				$("#total").html(count);
				for(var i=0; i<count; i++){

					sub_indicators_id = obj[i].ti_ind ;parent_id = obj[i].parent_id;kra_id = obj[i].kra_id ;kra_name = obj[i].kra_name; tk_weight = obj[i].tk_weight; detail_name = obj[i].detail_name; ti_weight = obj[i].ti_weight; lop_code = obj[i].lop_code;lop_letter = obj[i].lop_letter;lop_value = obj[i].lop_value;lop_description = obj[i].lop_description; lop_id = obj[i].lop_id;

						var fnlopd = lop_description.replace(/\'/g,"''");

					(function(i){
						var executeQuery = "INSERT INTO scorecard (lop_id,sub_indicators_id,parent_id,kra_id,kra_name, tk_weight, detail_name, ti_weight, lop_code, lop_letter, lop_value, lop_description, status) VALUES ("+lop_id+","+sub_indicators_id+","+parent_id+","+kra_id+",'"+kra_name+"',"+tk_weight+",'"+detail_name+"',"+ti_weight+",'"+lop_code+"','"+lop_letter+"',"+lop_value+",'"+fnlopd+"', 0)";
						db.transaction(function(tx){
						// alert(i);
						$("#progress").html(i+=1);
						// console.log(i++);
							tx.executeSql(executeQuery, [], successCB, errorCB);
						}, errorCB1);
					})(i);
				}

				getIndis(datas);
				// window.location.replace("dashboard.html");
			}
		});
	}

	function getIndis(datas){
		$.ajax({
			type: "get",
			url: "http://"+ipConfigs.ip+"/phpFile/actions/indicator.php",
			data: datas,
			success: function(response){
				// alert(response);
				var obj = $.parseJSON(response);
				var count = obj.length;

				for(var i=0; i<count; i++){
					// console.log(obj[i].has_child +" : "+obj[i].indicators_id);
					var indicators_id = obj[i].indicators_id;
					var kra_id = obj[i].kra_id;
					var detail_name = obj[i].detail_name;
					var weight = obj[i].weight;
					var parent_id = obj[i].parent_id;
					var has_child = obj[i].has_child;
					var defval = 0;
					console.log(has_child);
					if(has_child == null){
					defval = 0;
					console.log(defval)
					}else{
					defval = has_child
					}

					(function(i){
						var executeQuery = "INSERT INTO indicator (indicators_id, kra_id,detail_name, weight, parent_id, status, indicator_status, has_child) VALUES ("+indicators_id+","+kra_id+",'"+detail_name+"',"+weight+","+parent_id+", 0, 0, "+defval+")";
						db.transaction(function(tx){
						// alert(i);
							tx.executeSql(executeQuery, [], successCB1(i, count), errorCB);
						}, errorCB1);
					})(i);
				}
			}
		});
	}

	function errorCB(err) {
	 // alert("Error processing SQLSDSDD: "+err.code);
	}

	function errorCB1(err) {
	 // alert("Error processing SQLITE: "+err.code);
	}

	function successCB() {
	 // alert("success!");
	}

	function successCB1(i, count) {
		if(i == count-1){
			updateEvalStatus();
		}
	 // alert("success!");
	}

	function updateEvalStatus(){
		loginstatus = 1;
		$.ajax({
			type: "GET",
			url: "http://"+ipConfigs.ip+"/phpFile/actions/updateEvalStat.php",
			data: {evaluator_id: evalid1, loginstatus: loginstatus},
			success: function(response){
				window.location.replace("dashboard.html");
			}
		});
			// window.location.replace("dashboard.html");
	}
});