var db = window.openDatabase("Evaluator Database", "1.0", "Evaluator RAFI Database", 1000000), totalSC = 0, evalid1;
$(document).ready(function(){
	var ipConfigs = new ipConfig();
	function checkStat(){
	db.transaction(function(tx){
	tx.executeSql('SELECT sendStatus, status FROM project_name', [], function(tx, result){
		if(result.rows.item(0).status == 1){
			if(result.rows.item(0).sendStatus == 1){
		 		$('#startEval').html('Already Rated');
		 	}else{
				$('#startEval').html('Continue Evaluation');
		 	}
		}
	});
	});
	getProject();
	}

	checkStat();

	function getProject(){
		db.transaction(function(tx){
		tx.executeSql("SELECT * FROM project_name",[],function(tx,result){
		 evalid1 = result.rows.item(0).evaluator_id;
		 if(result.rows.item(0).scorecard_type == 2){
		 	$('#seed').attr('src', 'img/logo-ocp.png');
		 	$('.sc-area').html("Municipality:");
		 	// $('.user').html('OCP');
		 }else{
		 	$('#seed').attr('src', 'img/logo-s1.png');
		 	$('.seed-only').show();
		 	// $('.user').html('SEED');
		 }
		 $(".proj-name").html(result.rows.item(0).project_name);
		 $(".division-name").html(result.rows.item(0).division_name);
		 $(".district-name").html(result.rows.item(0).district_name);
		 $(".school-name").html(result.rows.item(0).school_name);
		 $(".start-date").html(result.rows.item(0).start_date.split(" ")[0]);
		 $(".end-date").html(result.rows.item(0).end_date.split(" ")[0]);
		});
		});
	}

	$("#startEval, .proj-cont").on("click", function(){
		var sc_id;
		db.transaction(function(tx){
		tx.executeSql("SELECT * FROM project_name",[],function(tx,result){
		 sc_id = result.rows.item(0).scorecard_id;
		 round_number = result.rows.item(0).round_num;
		 evalid = result.rows.item(0).evaluator_id;
		 var sctype = result.rows.item(0).scorecard_type;

		 console.log(result.rows.item(0).sendStatus);
		 if(result.rows.item(0).status == 1){
		 	 if(result.rows.item(0).sendStatus == 1){
		 		alert("You already uploaded a rating to the server."); 
		 	}else{
				var date = new Date();
				var logTimeH = date.getHours();
				var logTimeM = date.getMinutes();
				var logTimeS = date.getSeconds();
			 	saveLoginTime(logTimeH,logTimeM,logTimeS,sc_id,round_number,evalid, sctype)
		 	}
		 }else if(result.rows.item(0).status == 2){
		 	$('#timex').modal('show');
		 	queryAllKRA();
		 }else{
		 	 var hrs = 48;
		     var mins = 0;
		     var secs = 0;
		     updateEvalStat(sc_id,round_number,evalid,hrs,mins,secs, sctype);
		 }
	     
		});
		});
	});

	function updateEvalStat(sc_id,round_number,evalid,hrs,mins,secs, sctype){
	 if(confirm('You only have 48 hours to complete the evaluation, Proceed?')){
		 db.transaction(function(tx){
		 	tx.executeSql('UPDATE project_name SET status = 1', [], function(){
	     			saveExpiry(sc_id,round_number,evalid,hrs,mins,secs, sctype);
		 	});
		 });
	 }
	}

	function saveExpiry(sc_id,round_number,evalid,hrs,mins,secs, sctype){
		db.transaction(function(tx){
			tx.executeSql('INSERT INTO epiryDate (hours, minutes, seconds) SELECT ?, ?, ? WHERE NOT EXISTS (SELECT 1 FROM epiryDate WHERE hours = ? AND minutes = ? AND seconds = ?)', [hrs, mins, secs, hrs, mins, secs], function(){
				window.location.replace("kra.html?sc_id="+sc_id+"&round_number="+round_number+"&evalid="+evalid+"&sctype="+sctype);
			});
		});
	}

	function saveLoginTime(logTimeH,logTimeM,logTimeS,sc_id,round_number,evalid, sctype){
		db.transaction(function(tx){
			tx.executeSql('INSERT INTO loginTime (hoursLog, minutesLog, secondsLog) VALUES(?,?,?)', [logTimeH, logTimeM, logTimeS], function(){
					window.location.replace("kra.html?sc_id="+sc_id+"&round_number="+round_number+"&evalid="+evalid+"&sctype="+sctype);	
			});
		});
	}

	//autofill if time is up

	function queryAllKRA(){
		db.transaction(function(tx){
			tx.executeSql('SELECT * FROM kra', [], function(tx, res){
				for(var a=0;a<res.rows.length; a++){
					checkRates(res.rows.item(a).kra_id);
				}
			});
		});
	}

	function checkRates(kraid){
	db.transaction(function(tx){
		tx.executeSql('SELECT COUNT(indicator.parent_id) as cc, COUNT(CASE WHEN indicator.status = 1 THEN indicator.status END) as ss, indicator.kra_id, indicator.indicators_id, indicator.parent_id FROM indicator LEFT JOIN kra ON indicator.kra_id = kra.kra_id WHERE kra.kra_id = ? AND indicator.has_child = 0', [kraid], function(tx, result){
			var kulang = Number(result.rows.item(0).cc) - Number(result.rows.item(0).ss);
			var kraid = result.rows.item(0).kra_id;
			var indicators_id = result.rows.item(0).indicators_id;
			if(kulang > 0){
				console.log(kulang +'='+kraid);
				autoFillData(kraid, indicators_id, parent_id);
			}
		});
	});	
	}

	function autoFillData(kraid, indicators_id, prntid){
		db.transaction(function(tx){
			tx.executeSql('INSERT INTO scorecard_ledger_detail (ledger_id, kra_id, parent_id, indicator_id, lop_letter, lop_value, status, equivalent) VALUES (0, '+kraid+', '+prntid+', '+indicators_id+', "0","0", "PENDING", "0")', [], function(){
				console.log('sdsd');
			});
		});
	}

	//send to Admin finish or unfinish
	$('#sendData').on('click', function(){
			console.log(evalid);
			getEqui();	
		});

	    function getEqui(){
	        var evaluator_id = evalid;
	            db.transaction(function(tx){
	                tx.executeSql("SELECT SUM(equivalent) as gt FROM scorecard_ledger_detail", [], function(tx, result){
	                    var gt = result.rows.item(0).gt;
	                    getHeader(gt, evaluator_id);
	                });
	        });
	    }

	    function getHeader(gt, evaluator_id){
	        db.transaction(function(tx){
	        tx.executeSql("SELECT * FROM scorecard_ledger_header WHERE evaluator_id = ?",[evaluator_id],function(tx,result){

	            var scorecard_header_id = result.rows.item(0).scorecard_header_id;
	            var assessment_round_id = result.rows.item(0).assessment_round_id;
	            var evaluator_id = result.rows.item(0).evaluator_id;

	            insertHeader(scorecard_header_id, assessment_round_id, evaluator_id, gt);
	        });
	        });
	    }

	    function insertHeader(scorecard_header_id, assessment_round_id, evaluator_id, gt){
	        var inserts = "scorecard_header_id="+scorecard_header_id+"&assessment_round_id="+assessment_round_id+"&evaluator_id="+evaluator_id+"&gt="+gt;
	        $.ajax({
	            type: "POST",
	            url: "http://"+ipConfigs.ip+"/phpFile/actions/insertHeader.php",
	            data: inserts,
	            success: function(id){
	                var status = "PENDING";
	                getDetails(id,status);
	            }
	        });
	    }

	    function getDetails(id,status){
        db.transaction(function(tx){
        tx.executeSql("SELECT * FROM scorecard_ledger_detail WHERE status = ?",[status],function(tx,result){
           var len = result.rows.length;

           for(var i=0;i<len; i++){
            var kra_id = result.rows.item(i).kra_id;
            var parent_id = result.rows.item(i).parent_id;
            var indicator_id = result.rows.item(i).indicator_id;
            var lop_letter = result.rows.item(i).lop_letter;
            var lop_value = result.rows.item(i).lop_value;
            var equivalents = result.rows.item(i).equivalent;

            sendToAdmin(id, kra_id, parent_id, indicator_id,lop_letter, lop_value, len, equivalents);
           }
        });
        });
    	}

    	var res = 0;
	    function sendToAdmin(id, kra_id, parent_id, indicator_id,lop_letter, lop_value, len, equivalents){
	        var dataS = "id="+id+"&kra_id="+kra_id+"&parent_id="+parent_id+"&indicator_id="+indicator_id+"&lop_letter="+lop_letter+"&lop_value="+lop_value+"&equivalents="+equivalents;
	        $.ajax({
	            type: "POST",
	            url: "http://"+ipConfigs.ip+"/phpFile/actions/sendScores.php",
	            data: dataS,
	            success: function(response){
	                res++;
	                if(res == 1){
	                    window.open('http://'+ipConfigs.ip+'/blog/public/timeout', '_blank');
	                }
	            }
	        });

	    }

	    var loginstatus = 0;
	    $('#logout').click(function(){
	    	if(confirm('Are you sure you want to logout?')){

	    		db.transaction(function(tx){
	    			tx.executeSql('SELECT sendStatus FROM project_name', [], function(tx, result){
	    				if(result.rows.item(0).sendStatus == 1){
	    					loginstatus = 0;
	    					clearDB();
	    				}else if(result.rows.item(0).sendStatus == 0){
	    					loginstatus = 2;
	    					clearDB();
	    				}
	    			});
	    		});

	    	}
	    });

	    function clearDB(){
	    	//delete tables
		    var db = window.openDatabase("Evaluator Database", "1.0", "Evaluator RAFI Database", 1000000);
			db.transaction(populateDB, errorCB, successCB);
	    }

	    //callbacks
		function populateDB(tx){
			 tx.executeSql('DELETE FROM scorecard');
			 tx.executeSql('DELETE FROM project_name');
			 tx.executeSql('DELETE FROM kra');
			 tx.executeSql('DELETE FROM indicator');
			 tx.executeSql('DELETE FROM scorecard_ledger_header');
			 tx.executeSql('DELETE FROM scorecard_ledger_detail');
			 tx.executeSql('DELETE FROM reports');
			 tx.executeSql('DELETE FROM epiryDate');
			 tx.executeSql('DELETE FROM loginTime');
			 tx.executeSql('DELETE FROM progress');
			 tx.executeSql('DELETE FROM rate_tracker');
		}

		function errorCB(err) {
		 	alert("Error processing SQL: "+err.code);
		}

		function successCB() {
			updateEvalStatus();
		 	// alert("success!");
		}

	    function updateEvalStatus(){
			$.ajax({
				type: "GET",
				url: "http://"+ipConfigs.ip+"/phpFile/actions/updateEvalStat.php",
				data: {evaluator_id: evalid1, loginstatus: loginstatus},
				success: function(response){
					window.location.replace("login.html");
				}
			});
		}
});