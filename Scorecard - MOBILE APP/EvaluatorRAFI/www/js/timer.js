    //timer
    var remTime = setInterval(function(){
        ctr++;
        //use default value if currentSec is = 0
        if(currentSec == 0){
            currentSec = 1;
        }
        secCtrDefault = currentSec;
        secCtrDefault-=ctr;
        if(secCtrDefault == 0){
            setTimeout(function(){
                // minCtr-=1;
                    if(minCtr > 0){
                        minCtr-=1;
                    }else{
                        minCtr=60;
                        minCtr-=1;
                        hrCtr-=1;
                    }
                secCtrDefault = 60;
                currentSec = secCtrDefault;
            },500);
            ctr = 0;
        }
        //stop timer
        if(hrCtr == 0 && minCtr == 0 &&secCtrDefault == 0){
            console.log("stop");
            clearInterval(remTime);
        }
        //add 0 if length is 1
        if(secCtrDefault.toString().length == 1)
            secCtrDefault = '0' + secCtrDefault;
        if(minCtr.toString().length == 1)
            minCtr = '0' + minCtr;
        if(hrCtr.toString().length == 1)
            hrCtr = '0' + hrCtr;

        $('#kra').html('<b class="badge-lg">Time Remaining ('+hrCtr+':'+minCtr+':'+secCtrDefault+')</b>'); //append the display
    }, 1000);