<?php
include '../includes/dbconfig.php';
header('Access-Control-Allow-Origin: *');
$username = $_REQUEST["username"];
$password = $_REQUEST["password"];
// echo $password;
// $decryptPass = password_verify($password,);
$getUser = "SELECT * FROM scorecard_evaluators WHERE username = '$username'";
$getUser = $conn->prepare($getUser, array(PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL));
$getUser->execute();
$count = $getUser->rowCount();

if($count > 0){
$row= $getUser->fetch(PDO::FETCH_ASSOC);
$hashedPass = $row["password"];
if(password_verify($password, $hashedPass)){
	// $shid = $row["scorecard_header_id"];
	if($row['status'] == 1){
		$evalid = $row["id"];
		$sh = $row["scorecard_header_id"];

		$getsctype = "SELECT scorecard_type FROM scorecard__headers WHERE id = $sh";
		$getsctype = $conn->prepare($getsctype, array(PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL));
		$getsctype->execute();
		$scrow = $getsctype->fetch(PDO::FETCH_ASSOC);
		$sctype = $scrow['scorecard_type'];
		if($sctype == 2){
			$getProjname = "SELECT scorecard_assessment_rounds.round_num as roundnumber, scorecard_assessment_rounds.round_name as round_name, scorecard_evaluators.id as evalid, scorecard_evaluators.evaluator_weight, scorecard_evaluators.assessment_round_id,scorecard__headers.id ,scorecard__headers.project_name, scorecard__headers.start_date, scorecard__headers.end_date, scorecard__headers.scorecard_type, mst_municipalities.municipality_name FROM scorecard_evaluators LEFT JOIN scorecard_assessment_rounds ON scorecard_evaluators.assessment_round_id = scorecard_assessment_rounds.id LEFT JOIN scorecard__headers ON scorecard_assessment_rounds.scorecard_header_id = scorecard__headers.id LEFT JOIN mst_municipalities ON scorecard__headers.school_id = mst_municipalities.id WHERE scorecard_evaluators.id = $evalid";
			$getProjname = $conn->prepare($getProjname, array(PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL));
			$getProjname->execute();
			$res = $getProjname->fetch(PDO::FETCH_ASSOC);

			echo json_encode($res);
		}else{
			$getProjname = "SELECT scorecard_assessment_rounds.round_num as roundnumber, scorecard_assessment_rounds.round_name as round_name, scorecard_evaluators.id as evalid, scorecard_evaluators.evaluator_weight, scorecard_evaluators.assessment_round_id,scorecard__headers.id ,scorecard__headers.project_name, scorecard__headers.start_date, scorecard__headers.end_date, scorecard__headers.scorecard_type, mst_divisions.division_name, mst_districts.district_name, mst_schools.school_name FROM scorecard_evaluators LEFT JOIN scorecard_assessment_rounds ON scorecard_evaluators.assessment_round_id = scorecard_assessment_rounds.id LEFT JOIN scorecard__headers ON scorecard_assessment_rounds.scorecard_header_id = scorecard__headers.id LEFT JOIN mst_schools ON scorecard__headers.school_id = mst_schools.id LEFT JOIN mst_divisions ON mst_schools.division_id = mst_divisions.id LEFT JOIN mst_districts ON mst_schools.district_id = mst_districts.id WHERE scorecard_evaluators.id = $evalid";
			$getProjname = $conn->prepare($getProjname, array(PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL));
			$getProjname->execute();
			$res = $getProjname->fetch(PDO::FETCH_ASSOC);

			echo json_encode($res);
		}
		
	}else if($row['status'] == 2){
		echo 2;
	}else{
		echo 3;
	}
}else{
	echo 0;
}
}else{
	echo 0;
}