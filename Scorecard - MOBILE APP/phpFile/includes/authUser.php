<?php 
include 'dbconfig.php';
header('Access-Control-Allow-Origin: *');
session_start();

$username = $_REQUEST['username'];
$password = $_REQUEST['password'];

$query = "SELECT * FROM sec_users WHERE username = '$username'";
$query = $conn->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL));
$query->execute();
$count = $query->rowCount();

if($count > 0){
	$row = $query->fetch( PDO::FETCH_ASSOC );
	$hashedPass = $row['password'];

	if (password_verify($password, $hashedPass)){
		$_SESSION['userid'] = $row['id'];
		echo '{"status": 1}';

	}else{
		echo '{"status": 2}';
	}

}else{
	echo '{"status": 0}';
}
?>
