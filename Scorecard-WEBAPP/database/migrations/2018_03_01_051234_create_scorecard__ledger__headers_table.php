<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScorecardLedgerHeadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scorecard_ledger_headers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('scorecard_header_id');
            $table->integer('assessment_round_id');
            $table->integer('evaluator_id');
            $table->decimal('total_score', 5, 2);
            $table->dateTime('dateTime_submitted');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scorecard_ledger_headers');
    }
}
