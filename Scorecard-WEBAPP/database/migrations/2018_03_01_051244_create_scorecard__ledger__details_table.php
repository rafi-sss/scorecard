<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScorecardLedgerDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scorecard_ledger_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ledger_id');
            $table->integer('kra_id');
            $table->integer('indicator_id');
            $table->char('lop_letter', 4);
            $table->decimal('lop_value', 5, 2);
            $table->decimal('equivalent', 5,2);
            $table->integer('parent_indicator_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scorecard_ledger_details');
    }
}
