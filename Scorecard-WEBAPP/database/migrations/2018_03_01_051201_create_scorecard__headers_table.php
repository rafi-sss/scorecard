<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScorecardHeadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scorecard__headers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('project_name');
            $table->integer('no_assessment');
            $table->integer('school_id');
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->integer('level');
            $table->integer('template_id');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scorecard__headers');
    }
}
