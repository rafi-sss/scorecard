<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RecommendedSchools extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recommended_schools', function(Blueprint $table){
            $table->increments('id');
            $table->integer('scorecard_id');
            $table->integer('school_id');
            $table->integer('district_id');
            $table->integer('template_id');
            $table->string('project_name');
            $table->integer('recommend_by');
            $table->integer('tops');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recommended_schools');
    }
}
