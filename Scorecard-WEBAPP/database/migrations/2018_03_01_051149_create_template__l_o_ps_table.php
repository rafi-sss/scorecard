<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemplateLOPsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('template_lops', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('indicator_id');
            $table->string('lop_code');
            $table->char('lop_letter', 4);
            $table->decimal('lop_value', 5, 2);
            $table->text('lop_description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('template_lops');
    }
}
