<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemplateIndicatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('template_indicators', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kra_id');
            $table->string('detail_name');
            $table->decimal('weight', 5, 2);
            $table->integer('parent_id');
            $table->string('mov')->nullable();
            $table->integer('has_child');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('template_indicators');
    }
}
