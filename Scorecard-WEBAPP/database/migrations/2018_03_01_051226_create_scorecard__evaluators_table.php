<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScorecardEvaluatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scorecard_evaluators', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('scorecard_header_id');
            $table->integer('assessment_round_id');
            $table->string('username');
            $table->string('password');
            $table->string('fname');
            $table->string('mname');
            $table->string('lname');
            $table->string('mobile_no');
            $table->string('email');
            $table->string('status');
            $table->dateTime('expiry_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scorecard_evaluators');
    }
}
