<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemplateHeadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('template_headers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('template_code');
            $table->string('template_name');
            $table->integer('template_type');
            $table->boolean('default');
            $table->integer('level');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('template_headers');
    }
}
