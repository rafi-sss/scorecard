<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMstSchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_schools', function (Blueprint $table) {
            $table->increments('id');
            $table->string('school_code');
            $table->string('school_name');
            $table->integer('district_id');
            $table->integer('division_id');
            $table->integer('municipality_id');
            $table->integer('province_id');
            $table->string('address');
            $table->integer('region_id');
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_schools');
    }
}
