<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMstSdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_sds', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname');
            $table->string('mi');
            $table->string('lastname');
            $table->string('title');
            $table->integer('mobile_number');
            $table->integer('office_number');
            $table->string('email_address');
            $table->integer('district_id');
            $table->integer('division_id');
            $table->integer('municipality_id');
            $table->integer('province_id');
            $table->integer('region_id');
            $table->string('username');
            $table->string('password');
            $table->integer('createdby');
            $table->integer('updatedby');
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_sds');
    }
}
