<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScorecardAssessmentRoundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scorecard_assessment_rounds', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('scorecard_header_id');
            $table->integer('round_num');
            $table->string('round_name');
            $table->decimal('weight', 5, 2);
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->boolean('status');
            $table->integer('no_of_evaluators');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scorecard_assessment_rounds');
    }
}
