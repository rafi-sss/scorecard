<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(UsersTableSeeder::class);
        DB::table('user_type')->insert([
            'type' => 'Super Admin',
        ]);
        DB::table('user_type')->insert([
            'type' => 'Scorecard Admin',
        ]);
        DB::table('user_type')->insert([
            'type' => 'SDS',
        ]);
        DB::table('modules')->insert([
            'module_name' => 'Scorecard',
            'module_type' => 'Navigation',
            'hasSubmodule' => 0,
            'route' => 'scorecard'
        ]);
        DB::table('modules')->insert([
            'module_name' => 'Reports',
            'module_type' => 'Navigation',
            'hasSubmodule' => 0,
            'route' => 'reports'
        ]);
        DB::table('modules')->insert([
            'module_name' => 'Settings',
            'module_type' => 'Navigation',
            'hasSubmodule' => 2,
            'route' => ' '
        ]);
        DB::table('modules')->insert([
            'module_name' => 'Accounts',
            'module_type' => 'Panel',
            'hasSubmodule' => 0,
            'route' => ''
        ]);
         DB::table('sub_modules')->insert([
            'module_id' => '3',
            'parent_id' => 0,
            'hasChild' => 0,
            'sub_module_name' => 'User Management',
            'route' => 'usermanagement'
        ]);
        DB::table('sub_modules')->insert([
            'module_id' => '3',
            'parent_id' => 0,
            'hasChild' => 0,
            'sub_module_name' => 'Access Control',
            'route' => 'usercontrol'
        ]);
        DB::table('sub_modules')->insert([
            'module_id' => '3',
            'parent_id' => 0,
            'hasChild' => 1,
            'sub_module_name' => 'Template',
            'route' => ''
        ]);
        DB::table('sub_modules')->insert([
            'module_id' => '3',
            'parent_id' => 0,
            'hasChild' => 1,
            'sub_module_name' => 'Location',
            'route' => ''
        ]);
        DB::table('sub_modules')->insert([
            'module_id' => '3',
            'parent_id' => 0,
            'hasChild' => 0,
            'sub_module_name' => 'Schools',
            'route' => 'schools'
        ]);
        DB::table('sub_modules')->insert([
            'module_id' => '3',
            'parent_id' => 4,
            'hasChild' => 0,
            'sub_module_name' => 'Region',
            'route' => 'region'
        ]);
        DB::table('sub_modules')->insert([
            'module_id' => '3',
            'parent_id' => 4,
            'hasChild' => 0,
            'sub_module_name' => 'Province',
            'route' => 'province'
        ]);
        DB::table('sub_modules')->insert([
            'module_id' => '3',
            'parent_id' => 4,
            'hasChild' => 0,
            'sub_module_name' => 'Municipality',
            'route' => 'municipality'
        ]);
        DB::table('sub_modules')->insert([
            'module_id' => '3',
            'parent_id' => 4,
            'hasChild' => 0,
            'sub_module_name' => 'Division',
            'route' => 'area'
        ]);
        DB::table('sub_modules')->insert([
            'module_id' => '3',
            'parent_id' => 4,
            'hasChild' => 0,
            'sub_module_name' => 'District',
            'route' => 'district'
        ]);
         DB::table('sub_modules')->insert([
            'module_id' => '3',
            'parent_id' => 3,
            'hasChild' => 0,
            'sub_module_name' => 'Round Names',
            'route' => 'roundnames'
        ]);
        DB::table('sub_modules')->insert([
            'module_id' => '3',
            'parent_id' => 3,
            'hasChild' => 0,
            'sub_module_name' => 'Create Template',
            'route' => 'template'
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '1',
            'category' => 'module',
            'category_id' => '1',
            'grant' => 1
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '1',
            'category' => 'module',
            'category_id' => '2',
            'grant' => 1
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '1',
            'category' => 'module',
            'category_id' => '3',
            'grant' => 1
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '1',
            'category' => 'module',
            'category_id' => '4',
            'grant' => 1
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '3',
            'category' => 'module',
            'category_id' => '1',
            'grant' => 1
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '3',
            'category' => 'module',
            'category_id' => '2',
            'grant' => 1
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '3',
            'category' => 'module',
            'category_id' => '3',
            'grant' => 0
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '3',
            'category' => 'module',
            'category_id' => '4',
            'grant' => 1
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '2',
            'category' => 'module',
            'category_id' => '1',
            'grant' => 1
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '2',
            'category' => 'module',
            'category_id' => '2',
            'grant' => 1
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '2',
            'category' => 'module',
            'category_id' => '3',
            'grant' => 1
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '2',
            'category' => 'module',
            'category_id' => '4',
            'grant' => 1
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '1',
            'category' => 'submodule',
            'category_id' => '1',
            'grant' => 1
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '1',
            'category' => 'submodule',
            'category_id' => '2',
            'grant' => 1
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '1',
            'category' => 'submodule',
            'category_id' => '3',
            'grant' => 1
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '1',
            'category' => 'submodule',
            'category_id' => '4',
            'grant' => 1
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '1',
            'category' => 'submodule',
            'category_id' => '5',
            'grant' => 1
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '1',
            'category' => 'submodule',
            'category_id' => '6',
            'grant' => 1
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '1',
            'category' => 'submodule',
            'category_id' => '7',
            'grant' => 1
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '1',
            'category' => 'submodule',
            'category_id' => '8',
            'grant' => 1
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '1',
            'category' => 'submodule',
            'category_id' => '9',
            'grant' => 1
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '1',
            'category' => 'submodule',
            'category_id' => '10',
            'grant' => 1
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '1',
            'category' => 'submodule',
            'category_id' => '11',
            'grant' => 1
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '1',
            'category' => 'submodule',
            'category_id' => '12',
            'grant' => 1
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '3',
            'category' => 'submodule',
            'category_id' => '1',
            'grant' => 0
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '3',
            'category' => 'submodule',
            'category_id' => '2',
            'grant' => 0
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '3',
            'category' => 'submodule',
            'category_id' => '3',
            'grant' => 0
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '3',
            'category' => 'submodule',
            'category_id' => '4',
            'grant' => 0
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '3',
            'category' => 'submodule',
            'category_id' => '5',
            'grant' => 0
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '3',
            'category' => 'submodule',
            'category_id' => '6',
            'grant' => 0
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '3',
            'category' => 'submodule',
            'category_id' => '7',
            'grant' => 0
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '3',
            'category' => 'submodule',
            'category_id' => '8',
            'grant' => 0
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '3',
            'category' => 'submodule',
            'category_id' => '9',
            'grant' => 0
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '3',
            'category' => 'submodule',
            'category_id' => '10',
            'grant' => 0
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '3',
            'category' => 'submodule',
            'category_id' => '11',
            'grant' => 0
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '3',
            'category' => 'submodule',
            'category_id' => '12',
            'grant' => 0
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '2',
            'category' => 'submodule',
            'category_id' => '1',
            'grant' => 1
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '2',
            'category' => 'submodule',
            'category_id' => '2',
            'grant' => 1
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '2',
            'category' => 'submodule',
            'category_id' => '3',
            'grant' => 1
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '2',
            'category' => 'submodule',
            'category_id' => '4',
            'grant' => 1
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '2',
            'category' => 'submodule',
            'category_id' => '5',
            'grant' => 1
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '2',
            'category' => 'submodule',
            'category_id' => '6',
            'grant' => 1
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '2',
            'category' => 'submodule',
            'category_id' => '7',
            'grant' => 1
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '2',
            'category' => 'submodule',
            'category_id' => '8',
            'grant' => 1
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '2',
            'category' => 'submodule',
            'category_id' => '9',
            'grant' => 1
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '2',
            'category' => 'submodule',
            'category_id' => '10',
            'grant' => 1
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '2',
            'category' => 'submodule',
            'category_id' => '11',
            'grant' => 1
        ]);
        DB::table('user_accesses')->insert([
            'user_type_id' => '2',
            'category' => 'submodule',
            'category_id' => '12',
            'grant' => 1
        ]);
        DB::table('users')->insert([
            'fname' => 'Css',
            'lname' => 'Dev',
            'mname' => 'E',
            'user_type' => 1,
            'scorecard_type' => 0,
            'district_id' => 0,
            'username' => 'cssdev',
            'email' => 'cssdev@gmail.com',
            'password' => bcrypt('123123')
        ]);
        DB::table('users')->insert([
            'fname' => 'Admin',
            'lname' => 'Min',
            'mname' => 'Ad',
            'user_type' => 2,
            'scorecard_type' => 1,
            'district_id' => 0,
            'username' => 'admin',
            'email' => 'admin1@gmail.com',
            'password' => bcrypt('123123')
        ]);
        DB::table('users')->insert([
            'fname' => 'SDS',
            'lname' => 'Sample',
            'mname' => 'V',
            'user_type' => 3,
            'scorecard_type' => 0,
            'district_id' => 1,
            'username' => 'sdsadmin',
            'email' => 'sdsadmin@gmail.com',
            'password' => bcrypt('123123')
        ]);
        DB::table('template_types')->insert([
            'template_name' => 'SEED'
        ]);
    }
}
