<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mst_municipality extends Model
{
    protected $table = 'mst_municipalities';
    protected $fillable = [
        'municipality_code', 'municipality_name', 'province_id', 'region_id'
    ];
}
