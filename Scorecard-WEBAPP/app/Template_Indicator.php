<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Template_Indicator extends Model
{
    protected $table = 'template_indicators';
    protected $fillable = [
        'kra_id', 'detail_name', 'weight', 'parent_id', 'mov', 'has_child'
    ];
}
