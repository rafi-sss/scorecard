<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Template_KRA extends Model
{
    protected $table = 'template_kra';
    protected $fillable = [
        'template_header_id', 'kra_name', 'weight'
    ];
}
