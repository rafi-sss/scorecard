<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class recommend_school extends Model
{
    protected $table = 'recommended_schools';
    protected $fillable = [
        'scorecard_id', 'school_id', 'district_id', 'template_id', 'project_name', 'recommend_by', 'tops' 
    ];
}
