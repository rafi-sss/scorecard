<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Template_Header extends Model
{
    protected $table = 'template_headers';
    protected $fillable = [
        'template_code', 'template_name', 'template_type', 'default', 'created_by', 'updated_by', 'status'
    ];
}
