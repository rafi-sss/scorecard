<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mst_division extends Model
{
    protected $table = 'mst_divisions';
    protected $fillable = [
        'division_code', 'division_name', 'municipality_id', 'province_id', 'region_id'
    ];
}
