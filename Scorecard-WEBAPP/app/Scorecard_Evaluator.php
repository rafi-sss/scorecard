<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Scorecard_Evaluator extends Model
{
    protected $table = 'scorecard_evaluators';
    protected $fillable = [
        'scorecard_header_id', 'assessment_round_id', 'username', 'password', 'fname', 'mname', 'lname', 'mobile_no', 'email', 'status', 'expiry_date', 'evaluator_weight'
    ];
}
