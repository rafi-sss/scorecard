<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class modules extends Model
{
    protected $table = 'modules';
    protected $fillable = [
        'module_name', 'module_type', 'route', 'hasSubmodule'
    ];
}
