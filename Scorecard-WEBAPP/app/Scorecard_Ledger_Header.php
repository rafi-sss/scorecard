<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Scorecard_Ledger_Header extends Model
{
    protected $table = 'scorecard_ledger_headers';
    protected $fillable = [
        'scorecard_header_id', 'assessment_round_id', 'evaluator_id', 'dateTime_submitted', 'total_score'
    ];
}
