<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Template_type extends Model
{
    protected $table = 'template_types';
    protected $fillable = [
        'template_name'
    ];
}
