<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mst_district extends Model
{
    protected $table = 'mst_districts';
    protected $fillable = [
        'district_code', 'district_name', 'division_id', 'municipality_id', 'province_id', 'region_id'
    ];
}
