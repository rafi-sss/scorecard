<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mst_sds extends Model
{
    protected $table = 'mst_sds';
    protected $fillable = [
        'firstname', 'mi', 'lastname', 'title', 'mobile_number', 'office_number', 'email_address', 'district_id', 'division_id', 'municipality_id', 'province_id', 'region_id', 'username', 'password', 'createdby', 'updatedby', 'status'
    ];
}
