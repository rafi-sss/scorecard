<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ocp_evaluator extends Model
{
    protected $table = 'ocp_evaluator';
    protected $fillable = [
        'fname', 'mname', 'lname', 'municipality', 'contact_number', 'alternative_id', 'email_address', 'status', 'position'
    ];

    public $timestamps = false;
}
