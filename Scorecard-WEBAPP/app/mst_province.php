<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mst_province extends Model
{
    protected $table = 'mst_provinces';
    protected $fillable = [
        'province_code', 'province_name', 'region_id'
    ];
}
