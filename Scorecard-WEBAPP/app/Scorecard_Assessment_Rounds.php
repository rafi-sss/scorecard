<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Scorecard_Assessment_Rounds extends Model
{
    protected $table = 'scorecard_assessment_rounds';
    protected $fillable = [
        'scorecard_header_id', 'round_num', 'round_name', 'weight', 'start_date', 'end_date', 'status', 'no_of_evaluators'
    ];
}
