<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mst_region extends Model
{
    protected $table = 'mst_regions';
    protected $fillable = [
        'region_code', 'region_name'
    ];
}
