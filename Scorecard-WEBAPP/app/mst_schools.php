<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mst_schools extends Model
{
    protected $table = 'mst_schools';
    protected $fillable = [
        'school_code', 'school_name', 'district_id', 'division_id', 'municipality_id', 'province_id', 'region_id', 'status'
    ];
}
