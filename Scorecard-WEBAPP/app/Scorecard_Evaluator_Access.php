<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Scorecard_Evaluator_Access extends Model
{
    protected $table = 'scorecard_evaluator_accesses';
    protected $fillable = [
        'evaulator_id', 'kra_id'
    ];
}
