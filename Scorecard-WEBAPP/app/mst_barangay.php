<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mst_barangay extends Model
{
    protected $table = 'mst_barangay';
    protected $fillable = [
        'municipality_id', 'b_code', 'b_name'
    ];

    public $timestamps = false;
}
