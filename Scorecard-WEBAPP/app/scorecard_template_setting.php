<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class scorecard_template_setting extends Model
{
    protected $table = 'scorecard_template_settings';
    protected $fillable = [
        'template_header_id', 'num_of_rounds', 'num_of_evaluators'
    ];
}
