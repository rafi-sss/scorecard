<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class scorecard_template_round_names extends Model
{
    protected $table = 'scorecard_template_round_names';
    protected $fillable = [
         'weight', 'round_type', 'round_name'
    ];
}
