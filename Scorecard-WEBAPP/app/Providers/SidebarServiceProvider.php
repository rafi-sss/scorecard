<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Template_type;
use App\mst_schools;
use App\mst_municipality;
use App\mst_district;

class SidebarServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(Request $request)
    {
        view()->composer('layouts.sidebar', function($view) {
               $usertype = Auth::user()->user_type;
               $scorecardType = Auth::user()->scorecard_type;

        if($usertype == 3){

            $cond = 0;

            // if($scorecardType != 0){
            //     $templateHeader = Template_type::find($scorecardType);
            //     $tempId = $templateHeader->id;
            // }else{
            //     $tempId = 1;
            // }
            $mst_schools = mst_schools::all();
            $mst_municipality = mst_municipality::all();

            if($scorecardType == 2){
                $cond = 2;
                $districtid = Auth::user()->district_id;
                $getMid = mst_district::find($districtid);
                $mid = $getMid->municipality_id;

                $scorecards = DB::table('scorecard__headers as sh')
                ->select([
                    'sh.id',
                    'sh.project_name',
                    'th.template_name',
                    'sh.no_assessment',
                    'sh.status',
                    'sh.school_id',
                    'th.template_type'
                    ])
                ->leftJoin('template_headers as th', 'sh.template_id', '=', 'th.id')
                ->leftJoin('users as u', 'u.id', '=', 'sh.created_by')
                ->orderByRaw('sh.id DESC')
                ->where('th.template_type', $cond)
                ->where('sh.school_id', $mid)
                ->where('level', 1)
                ->take(5)
                ->get();
            }else{
                $cond = 1;
                $districtid = Auth::user()->district_id;

                $scorecards = DB::table('scorecard__headers as sh')
                ->select([
                    'sh.id',
                    'sh.project_name',
                    'th.template_name',
                    'sh.no_assessment',
                    'sh.status',
                    'sh.school_id',
                    'th.template_type'
                    ])
                ->leftJoin('template_headers as th', 'sh.template_id', '=', 'th.id')
                ->leftJoin('users as u', 'u.id', '=', 'sh.created_by')
                ->leftJoin('mst_schools as ms', 'sh.school_id', '=', 'ms.id')
                ->orderByRaw('sh.id DESC')
                ->where('th.template_type', $cond)
                ->where('ms.district_id', $districtid)
                ->where('level', 1)
                ->take(5)
                ->get();
            }               
          
        }else if($usertype == 2){

            $cond = 0;
            $lev = 1;
            if($scorecardType == 2){
                $cond = 2;
            }else{
                $cond = 1;
            }

            $mst_schools = mst_schools::all();
            $mst_municipality = mst_municipality::all();

            $scorecards = DB::table('scorecard__headers as sh')
                ->select([
                    'sh.id',
                    'sh.project_name',
                    'th.template_name',
                    'sh.no_assessment',
                    'sh.status',
                    'sh.school_id',
                    'th.template_type'
                    ])
                ->leftJoin('template_headers as th', 'sh.template_id', '=', 'th.id')
                ->leftJoin('users as u', 'u.id', '=', 'sh.created_by')
                ->where('th.template_type', $cond)
                ->orderByRaw('sh.id DESC')
                ->take(5)
                ->get();
        }else{

             $mst_schools = mst_schools::all();
             $mst_municipality = mst_municipality::all();

             $scorecards = DB::table('scorecard__headers as sh')
                ->select([
                    'sh.id',
                    'sh.project_name',
                    'th.template_name',
                    'sh.no_assessment',
                    'sh.status',
                    'sh.school_id',
                    'th.template_type'
                    ])
                ->leftJoin('template_headers as th', 'sh.template_id', '=', 'th.id')
                ->leftJoin('users as u', 'u.id', '=', 'sh.created_by')
                ->orderByRaw('sh.id DESC')
                ->take(5)
                ->get();
          
        }

         $count = count($scorecards);
                //dd($scorecards->project_name);
                $view->with('scorecards', $scorecards)->with('count', $count)->with('mst_schools', $mst_schools)->with('mst_municipality', $mst_municipality);
 
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
