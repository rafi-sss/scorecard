<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class AccountActivationServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(Request $request)
    {
        view()->composer('layouts.sidebar', function($view) {
               
                $scorecards = DB::table('scorecard__headers as sh')
                ->where('status', 'P')
                ->orderByRaw('id DESC')
                ->take(5)
                ->get();
                $view->with('scorecards', $scorecards);
          
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
