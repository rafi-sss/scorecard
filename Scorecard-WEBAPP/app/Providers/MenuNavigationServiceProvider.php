<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;

class MenuNavigationServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(Request $request)
    {

       

        view()->composer('layouts.app', function($view) {
            $user = Auth::id();
            $getName = User::find($user);
            $modules = DB::table('user_accesses as a')->leftjoin('modules as m', 'a.category_id', '=', 'm.id')->where('user_type_id', '=', Auth::user()->user_type)->where('category', '=', 'module')->where('m.module_type', '=', 'Navigation')->get();
            $submodules = DB::table('user_accesses as a')->leftjoin('sub_modules as m', 'a.category_id', '=', 'm.id')->where('user_type_id', '=', Auth::user()->user_type)->where('category', '=', 'submodule')->where('parent_id', 0)->get();
            $sbchild = DB::table('user_accesses as a')->leftjoin('sub_modules as m', 'a.category_id', '=', 'm.id')->where('user_type_id', '=', Auth::user()->user_type)->where('category', '=', 'submodule')->where('parent_id', '>', 0)->get();
            // $modules = DB::table('modules')->where('module_type', '=', 'Navigation')->get();
            // $submodules = DB::table('sub_modules')->get();
            $view->with('modules', $modules)->with('submodules', $submodules)->with('sbchild', $sbchild)->with('getName', $getName);
          
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
