<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Scorecard_Header extends Model
{
    protected $table = 'scorecard__headers';
    protected $fillable = [
        'project_name', 'no_assessment', 'school_id', 'start_date', 'end_date', 'template_id', 'level', 'grand_total', 'created_by', 'updated_by', 'status', 'scorecard_type'
    ];
}
