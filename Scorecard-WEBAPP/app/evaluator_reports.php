<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class evaluator_reports extends Model
{
    protected $table = 'evaluator_reports';
    protected $fillable = [
        'kra_id', 'evaluator_id', 'findings', 'recommendations'
    ];
}
