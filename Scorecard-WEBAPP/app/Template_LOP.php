<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Template_LOP extends Model
{
    protected $table = 'template_lops';
    protected $fillable = [
        'indicator_id', 'lop_code', 'lop_letter', 'lop_value', 'lop_description'
    ];
}
