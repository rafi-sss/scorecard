<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Scorecard_Ledger_Detail extends Model
{
    protected $table = 'scorecard_ledger_details';
    protected $fillable = [
        'ledger_id', 'kra_id', 'indicator_id', 'lop_letter', 'lop_value', 'parent_indicator_id'
    ];
}
