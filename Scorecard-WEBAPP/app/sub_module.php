<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sub_module extends Model
{
    protected $table = 'sub_modules';
    protected $fillable = [
        'module_id', 'parent_id', 'hasChild', 'sub_module_name', 'route'
    ];
}
