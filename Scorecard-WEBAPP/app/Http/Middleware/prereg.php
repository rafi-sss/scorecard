<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use DB;
class prereg
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $usertype = Auth::user()->user_type;
        $status = Auth::user()->status;

        if($usertype > 1){
            if($status == 'pending'){
                abort(411, 'Pending for Approval'); 
            }else if($status == 'deactivate'){
                abort(411, 'Account Deactivated');
            }
        }
        return $next($request);
    }
}
