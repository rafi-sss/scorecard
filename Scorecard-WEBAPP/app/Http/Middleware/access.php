<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use DB;

class access
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $usertype = Auth::user()->user_type;
        $routeName = explode("/", $request->route()->uri);
        $routeName = $routeName[0];
        $access = DB::table('user_accesses as ua')->leftJoin('sub_modules as sm', 'ua.category_id', '=', 'sm.id')->where('ua.user_type_id', $usertype)->where('ua.category', '=', 'submodule')->where('sm.route', $routeName)->first();
        if(!$access){
            $access = DB::table('user_accesses as ua')->leftJoin('modules as sm', 'ua.category_id', '=', 'sm.id')->where('ua.user_type_id', $usertype)->where('ua.category', '=', 'module')->where('sm.route', $routeName)->first();
            if(!$access){
                 abort(409, 'Module not registerd.');
            }
        }
        if($access->grant == 0){
            abort(401, 'Access denied'); 
        }
        
        return $next($request);
    }
}
