<?php

namespace App\Http\Middleware;
use App\User;
use Closure, Exception, Auth;

class AuthAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function checkAccess($request, Closure $next)
    {
        $id = Auth::id();
        $user = User::select('user_type')->where('id', $id)->first();
        $access = User_Access::select()->where('user_type_id', $user->user_type);
        if ($user->user_type == 1) {
            # code...
        }
       
    }
}
