<?php

namespace App\Http\Controllers;

use App\User;
use App\mst_schools;
use App\mst_region;
use App\mst_province;
use App\mst_division;
use App\mst_district;
use App\mst_municipality;
use App\ocp_evaluator;
use Illuminate\Pagination\Paginator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use DB;

class OtherMaintenanceController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    //school
    public function schools(){
    	$schools = mst_schools::orderBy('id', 'desc')->paginate(10);
        $districts = mst_district::all();

    	return view('maintenance.schools', compact('schools', 'districts'));
    }

    public function addschool(Request $request){

        $this->validate($request, [
            'districtid' => 'required',
            'schoolcode' => 'required',
            'schoolname' => 'required',
            'schooladdress' => 'required',
        ]);

        $districtID = $request->districtid;
        $districtDetails = mst_district::find($districtID);
        $divisionID = $districtDetails->division_id;
        $divisionDetails = mst_division::find($divisionID);
        $municipalityID = $divisionDetails->municipality_id;
        $municipalityDetails = mst_municipality::find($municipalityID);
        $provinceID = $municipalityDetails->province_id;
        $provinceDetails = mst_province::find($provinceID);
        $regionID = $provinceDetails->region_id;

    	$school = new mst_schools;
        $school->district_id = $districtID;
        $school->division_id = $divisionID;
        $school->municipality_id = $municipalityID;
        $school->province_id = $provinceID;
        $school->region_id = $regionID;
        $school->school_code = $request->schoolcode;
    	$school->school_name = $request->schoolname;
        $school->address = $request->schooladdress;
        $school->status = 1;
    	$school->save();
    	return redirect('schools');
    }

    public function editSchool(Request $request){

        $this->validate($request, [
            'udistrictid' => 'required',
            'uschoolcode' => 'required',
            'uschoolname' => 'required',
        ]);

        $districtID = $request->udistrictid;
        $districtDetails = mst_district::find($districtID);
        $divisionID = $districtDetails->division_id;
        $divisionDetails = mst_division::find($divisionID);
        $municipalityID = $divisionDetails->municipality_id;
        $municipalityDetails = mst_municipality::find($municipalityID);
        $provinceID = $municipalityDetails->province_id;
        $provinceDetails = mst_province::find($provinceID);
        $regionID = $provinceDetails->region_id;

        $id = $request->uschoolid;
        $school = mst_schools::find($id);
        $school->district_id = $districtID;
        $school->division_id = $divisionID;
        $school->municipality_id = $municipalityID;
        $school->province_id = $provinceID;
        $school->region_id = $regionID;
        $school->school_code = $request->uschoolcode;
        $school->school_name = $request->uschoolname;
        $school->status = 1;
        $school->save();
        return redirect('schools');
    }

    //region
    public function region(){
        $regions = mst_region::paginate(10);

        return view('maintenance.region', compact('regions'));
    }

    public function addRegion(Request $request){

        $this->validate($request, [
            'regioncode' => 'required',
            'regionname' => 'required',
        ]);

        $region = new mst_region;
        $region->region_code = $request->regioncode;
        $region->region_name = $request->regionname;
        $region->save();
        return redirect('region');
    }

    public function editRegion(Request $request){

        $this->validate($request, [
            'uregioncode' => 'required',
            'uregionname' => 'required',
        ]);

        $id = $request->uregionid;
        $region = mst_region::find($id);
        $region->region_code = $request->uregioncode;
        $region->region_name = $request->uregionname;
        $region->save();
        return redirect('region');
    }

    //province
    public function province(){
        $regions = mst_region::all();
        $provinces = mst_province::paginate(10);

        return view('maintenance.province', compact('regions', 'provinces'));
    }

    public function addProvince(Request $request){

        $this->validate($request, [
            'regionid' => 'required',
            'provincecode' => 'required',
            'provincename' => 'required',
        ]);

        $province = new mst_province;
        $province->region_id = $request->regionid;
        $province->province_code = $request->provincecode;
        $province->province_name = $request->provincename;
        $province->save();
        return redirect('province');
    }

    public function editProvince(Request $request){

        $this->validate($request, [
            'uregionid' => 'required',
            'uprovincecode' => 'required',
            'uprovincename' => 'required',
        ]);

        $id = $request->uprovinceid;
        $province = mst_province::find($id);
        $province->region_id = $request->uregionid;
        $province->province_code = $request->uprovincecode;
        $province->province_name = $request->uprovincename;
        $province->save();
        return redirect('province');
    }

    //municipality
    public function municipality(){
        $provinces = mst_province::all();
        $municipalities = mst_municipality::paginate(10);

        return view('maintenance.municipality', compact('provinces', 'municipalities'));
    }

    public function addMunicipality(Request $request){

        $this->validate($request, [
            'provinceid' => 'required',
            'municipalitycode' => 'required',
            'municipalityname' => 'required',
        ]);

        $provinceID = $request->provinceid;
        $provinceDetails = mst_province::find($provinceID);
        $regionID = $provinceDetails->region_id;

        $municipality = new mst_municipality;
        $municipality->region_id = $regionID;
        $municipality->province_id = $provinceID;
        $municipality->municipality_code = $request->municipalitycode;
        $municipality->municipality_name = $request->municipalityname;
        $municipality->save();
        return redirect('municipality');
    }

    public function editMunicipality(Request $request){

        $this->validate($request, [
            'uprovinceid' => 'required',
            'umunicipalitycode' => 'required',
            'umunicipalityname' => 'required',
        ]);

        $provinceID = $request->uprovinceid;
        $provinceDetails = mst_province::find($provinceID);
        $regionID = $provinceDetails->region_id;

        $id = $request->umunicipalityid;
        $municipality = mst_municipality::find($id);
        $municipality->region_id = $regionID;
        $municipality->province_id = $provinceID;
        $municipality->municipality_code = $request->umunicipalitycode;
        $municipality->municipality_name = $request->umunicipalityname;
        $municipality->save();
        return redirect('municipality');
    }

    //division
    public function areas(){
        $areas = mst_division::paginate(10);
        $municipalities = mst_municipality::all();

        return view('maintenance.area', compact('areas', 'municipalities'));
    }

    public function addarea(Request $request){

        $this->validate($request, [
            'municipalityid' => 'required',
            'areacode' => 'required',
            'areaname' => 'required',
        ]);

        $municipalityID = $request->municipalityid;
        $municipalityDetails = mst_municipality::find($municipalityID);
        $provinceID = $municipalityDetails->province_id;
        $provinceDetails = mst_province::find($provinceID);
        $regionID = $provinceDetails->region_id;

        $division = new mst_division;
        $division->municipality_id = $municipalityID;
        $division->province_id = $provinceID;
        $division->region_id = $regionID;
        $division->division_code = $request->areacode;
        $division->division_name = $request->areaname;
        $division->save();
        return redirect('area');
    }

    public function editArea(Request $request){

        $this->validate($request, [
            'umunicipalityid' => 'required',
            'uareacode' => 'required',
            'uareaname' => 'required',
        ]);

        $municipalityID = $request->umunicipalityid;
        $municipalityDetails = mst_municipality::find($municipalityID);
        $provinceID = $municipalityDetails->province_id;
        $provinceDetails = mst_province::find($provinceID);
        $regionID = $provinceDetails->region_id;

        $id = $request->uareaid;
        $division = mst_division::find($id);
        $division->municipality_id = $municipalityID;
        $division->province_id = $provinceID;
        $division->region_id = $regionID;
        $division->division_code = $request->uareacode;
        $division->division_name = $request->uareaname;
        $division->save();
        return redirect('area');
    }

    public function district(){
        $district = mst_district::paginate(10);
        $dists = mst_municipality::all();
        $areas = mst_division::all();

        return view('maintenance.district', compact('district', 'areas', 'dists'));
    }

    public function addDistrict(Request $request){

        $this->validate($request, [
            'divisionid' => 'required',
            'districtcode' => 'required',
            'districtname' => 'required',
        ]);

        $divisionID = $request->divisionid;
        $divisionDetails = mst_division::find($divisionID);
        $municipalityID = $divisionDetails->municipality_id;
        $municipalityDetails = mst_municipality::find($municipalityID);
        $provinceID = $municipalityDetails->province_id;
        $provinceDetails = mst_province::find($provinceID);
        $regionID = $provinceDetails->region_id;

        $district = new mst_district;
        $district->division_id = $divisionID;
        $district->municipality_id = $municipalityID;
        $district->province_id = $provinceID;
        $district->region_id = $regionID;
        $district->district_code = $request->districtcode;
        $district->district_name = $request->districtname;
        $district->save();
        return redirect('district');
    }

    public function editDistrict(Request $request){

        $this->validate($request, [
            'udivisionid' => 'required',
            'udistrictcode' => 'required',
            'udistrictname' => 'required',
        ]);
        
        $divisionID = $request->udivisionid;
        $divisionDetails = mst_division::find($divisionID);
        $municipalityID = $divisionDetails->municipality_id;
        $municipalityDetails = mst_municipality::find($municipalityID);
        $provinceID = $municipalityDetails->province_id;
        $provinceDetails = mst_province::find($provinceID);
        $regionID = $provinceDetails->region_id;

        $id = $request->udistrictid;
        $district = mst_district::find($id);
        $district->division_id = $divisionID;
        $district->municipality_id = $municipalityID;
        $district->province_id = $provinceID;
        $district->region_id = $regionID;
        $district->district_code = $request->udistrictcode;
        $district->district_name = $request->udistrictname;
        $district->save();
        return redirect('district');
    }

    public function liveSearchSchool(Request $request){
        $distname = $request->sname;

        $schools = DB::table('mst_districts as md')
        ->select([
            'ms.id',
            'ms.school_code',
            'ms.district_id',
            'ms.school_name',
            'md.district_name'
        ])
        ->leftJoin('mst_schools as ms', 'md.id', '=', 'ms.district_id')
        ->where('md.district_name','LIKE',$distname.'%')
        ->get();        

        // $districts = mst_district::where('district_name', $distname)->get();
        // $distid = $districts->id;
        // $schools = mst_schools::where('district_id', $distid)->get();
        $html = '';

        if(count($schools) > 0){
          foreach($schools as $school){
            $html .= '<tr>
              <td>'. $school->id .'</td>
              <td>'. $school->school_code .'</td>
              <td>'. $school->school_name .'</td>
              <td>'. $school->district_name .'</td>
              <input type="hidden" name="disID" id="disID" value="'. $school->district_id .'">
              <td><button class="btn btn-proceed es" style="margin-right: 5px;" data-toggle="modal" data-target="#updateschool"><span class="fa fa-edit"></span> Edit</button></td>
            </tr>';
        }
    }
        else{
        $html .= '<tr class="text-center">
          <td colspan="5"> <strong>No school record.</strong></td>
        </tr>';
        }

        return $html;
    }

    public function liveSearchDistrict(Request $request){
        $distname = $request->sname;

        $districts = DB::table('mst_districts as md')
        ->select([
            'md.id',
            'md.district_code',
            'md.district_name',
            'div.division_name',
            'md.division_id'
        ])
        ->leftJoin('mst_divisions as div', 'md.division_id', '=', 'div.id')
        ->where('md.district_name','LIKE',$distname.'%')
        ->get();        

        $html = '';

        if(count($districts) > 0){
          foreach($districts as $dist){
            $html .= '<tr>
              <td>'. $dist->id .'</td>
              <td>'. $dist->district_code .'</td>
              <td>'. $dist->district_name .'</td>
              <td>'. $dist->division_name .'</td>
              <input type="hidden" name="did" id="did" value='. $dist->division_id .'>
              <td><button class="btn btn-proceed ed" style="margin-right: 5px;" data-toggle="modal" data-target="#updatedistrict"><span class="fa fa-edit"></span> Edit</button></td>
            </tr>';
        }
    }
        else{
        $html .= '<tr class="text-center">
          <td colspan="5"> <strong>No district record.</strong></td>
        </tr>';
        }

        return $html;
    }

    public function evaluatorMaintenance(){
        $usertype = Auth::user()->user_type;
        $sc_type = Auth::user()->scorecard_type;
        $distid = Auth::user()->district_id;

        if($usertype == 2 && $sc_type == 2 || $usertype == 3 && $sc_type == 2 || $usertype == 1){

            if($usertype == 3){
                $municipalities = mst_municipality::all();

                $ocp_evaluators = DB::table('ocp_evaluator as oe')
                ->select([
                    'oe.id',
                    'oe.fname',
                    'oe.mname',
                    'oe.lname',
                    'oe.municipality',
                    'oe.email_address',
                    'oe.contact_number',
                    'oe.alternative_id',
                    'mm.id as mid',
                    'mm.municipality_name'
                ])
                ->leftJoin('mst_municipalities as mm', 'oe.municipality', '=', 'mm.id')
                ->leftJoin('mst_districts as md', 'mm.id', '=', 'md.municipality_id')
                ->where('oe.alternative_id', '0')
                ->where('md.id', $distid)
                ->paginate(10);

            }else{
                $municipalities = mst_municipality::all();

                $ocp_evaluators = DB::table('ocp_evaluator as oe')
                ->select([
                    'oe.id',
                    'oe.fname',
                    'oe.mname',
                    'oe.lname',
                    'oe.municipality',
                    'oe.email_address',
                    'oe.contact_number',
                    'oe.alternative_id',
                    'mm.id as mid',
                    'mm.municipality_name'
                ])
                ->leftJoin('mst_municipalities as mm', 'oe.municipality', '=', 'mm.id')
                ->where('oe.alternative_id', '0')
                ->paginate(10);
            }

            return view('maintenance.evaluator', compact('municipalities', 'ocp_evaluators', 'usertype'));
        }else{
            abort(401, 'Access denied'); 
        }
    }


    public function addOCPEval(Request $request){
        if($request->mainEvalVal === '0'){
            $altID = 0;
            $stat = 1;
        }else{
            $altID = $request->altEvalVal;
            $stat = 0;
        }

        $ocp_evaluators = new ocp_evaluator;
        $ocp_evaluators->fname = $request->fname;
        $ocp_evaluators->mname = $request->mname;
        $ocp_evaluators->lname = $request->lname;
        $ocp_evaluators->email_address = $request->eadd;
        $ocp_evaluators->municipality = $request->municipality;
        $ocp_evaluators->contact_number = $request->cnumber;
        $ocp_evaluators->alternative_id = $altID;
        $ocp_evaluators->status = $stat;
        $ocp_evaluators->position = $request->position;
        $ocp_evaluators->save();

        return $ocp_evaluators->id;
    }

    public function viewOCPAltEval($id){
        $usertype = Auth::user()->user_type;
        $ocp_evaluators = DB::table('ocp_evaluator as oe')
        ->select([
            'oe.id',
            'oe.fname',
            'oe.mname',
            'oe.lname',
            'oe.municipality',
            'oe.email_address',
            'oe.contact_number',
            'oe.alternative_id',
            'mm.id as mid',
            'mm.municipality_name'
        ])
        ->leftJoin('mst_municipalities as mm', 'oe.municipality', '=', 'mm.id')
        ->where('oe.municipality', $id)
        ->where('oe.alternative_id', '>', 0)
        ->get();

        // $ocp_evaluators = ocp_evaluator::where('alternative_id', $id)->get();

        $html = '';

        foreach ($ocp_evaluators as $oe) {
            $html .= '<tr>
                        <td><span class="sfname">'.$oe->fname.'</span>, <span class="smname">'.$oe->mname.'</span>, <span class="slname">'.$oe->lname.'</span></td>
                        <td>'.$oe->email_address.'</td>
                        <td>'.$oe->contact_number.'</td>
                        <td><span id='.$oe->mid.'></span>'.$oe->municipality_name.'</td>
                        <td><button type="button" data-id='.$oe->id.' class="btn btn-proceed va"><span class="fa fa-reply"></span> Transfer</button></td>';
                        
            if($usertype != 3){
                $html .= '<td><button class="btn btn-danger" data-id='.$oe->id.' style="margin-right: 5px;" data-toggle="modal" data-target="#updatearea"><span class="fa fa-edit"></span> Edit </button></td>
                    </tr>';
            }
            
        }

        return $html;
    }

    public function dccenter(){
        $schools = mst_schools::orderBy('id', 'desc')->paginate(10);
        $municipalities = mst_municipality::all();
        // dd($municipalities);
        return view('maintenance.dccenter', compact('schools', 'municipalities'));
    }

    public function adddccenter(){
        return 'add showing';
    }

    public function barangay(){
        $schools = mst_schools::orderBy('id', 'desc')->paginate(10);
        $municipalities = mst_municipality::all();
        // dd($municipalities);
        return view('maintenance.barangay', compact('schools', 'municipalities'));
    }

    public function addbarangay(){
        return 'add barangay';
    }
}
