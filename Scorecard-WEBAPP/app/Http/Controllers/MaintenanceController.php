<?php

namespace App\Http\Controllers;

use App\User;
use App\User_Type;
use App\modules;
use App\User_Access;
use App\sub_module;
use App\Template_type;
use App\mst_district;
use App\mst_province;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use DB;

class MaintenanceController extends Controller
{
    public function index(){
        $users = User_Type::select([
            'id',
            'type'
        ])->get();

        $userTypesControl = User_Type::all();
        
        
        return view('maintenance.user_control', compact('userTypesControl'));
    	// return view('maintenance.user_control');
    }

    public function addType(Request $request){
        $newType = new User_Type;
        $newType->type = $request->typename;
        $newType->save();
        $typeID = $newType->id;

        $modules = modules::all();
        $submodules = sub_module::all();

        foreach ($modules as $module) {
            $newAccess = new User_Access;
            $newAccess->user_type_id = $typeID;
            $newAccess->category = 'module';
            $newAccess->category_id = $module->id;
            $newAccess->grant = 0;
            $newAccess->save();
        }

        foreach ($submodules as $submodule) {
            $newAccess = new User_Access;
            $newAccess->user_type_id = $typeID;
            $newAccess->category = 'submodule';
            $newAccess->category_id = $submodule->id;
            $newAccess->grant = 0;
            $newAccess->save();
        }

        return redirect('usercontrol');
    }

    public function getModules(){
        $modules = modules::all();
        return $modules;
    }

    public function getAccess($id){
        $modules = modules::all();
        $typeAccess = DB::table('user_accesses as u')
            ->select([
                'm.module_name',
                'm.hasSubmodule',
                'u.user_type_id',
                'u.category',
                'u.category_id',
                'u.grant'
                ])
            ->leftJoin('modules as m', 'u.category_id', '=', 'm.id')
            ->where('category', '=', 'module')->where('user_type_id', $id)->get();

        $subModules = DB::table("user_accesses as u")
        ->select([
            'sm.sub_module_name',
            'sm.id as smId',
            'u.grant'
        ])
        ->leftJoin('sub_modules as sm', 'u.category_id', '=', 'sm.id')
        ->leftJoin('modules as m', 'sm.module_id', '=', 'm.id' )
        ->where('category', '=', 'submodule')
        ->where('user_type_id', $id)
        ->get();

        $result = '';
        $optionalItem = '';
            foreach ($typeAccess as $taccess) {

                if($taccess->hasSubmodule != 0){
                    $hasSubM = "hasSub";
                    $optionalItem .= '<button class="fa fa-caret-down" type="button" style="margin-left: -5px ;background: none; border: none; outline: none;color: gray; border-radius: 2px; line-height: 15px" name='.$id.' id="button">
                        </button>
                        <ul class="main-ul" id="main-ul">                        
                        ';

                    /*----------------------------------------------sub modules-----------------------------------------*/

                    foreach($subModules as $smodules){

                        if($smodules->grant == 1){
                            $checked = "checked";
                        }else{
                            $checked = "";
                        }

                        $optionalItem .= '
                                <li>
                                    <label>
                                        <input class="ul-sub-menu" value='.$smodules->smId.' type="checkbox"  '.$checked.' />'. $smodules->sub_module_name.'
                                    </label>
                                </li>
                                ';                           
                    }

                    $optionalItem .= '</ul><script>$("#main-ul").hide();</script>';
                }else{
                    $hasSubM = 'accessVal';
                    $optionalItem = '';
                }

                /*---------------------------------------acess granted------------------------------------------------*/

                if($taccess->grant == 1){
                    $checkedAccess = "checked";
                }else{
                    $checkedAccess = "";
                }

                $result .= '<input class="input-wrapper" name="'.$hasSubM.'" type="checkbox" id="input-wrapper" '.$checkedAccess.' value='.$taccess->category_id.' />
                    <label class="label-wrapper" id="hasSub">                        
                       '.$taccess->module_name.'
                       '.$optionalItem.'                      
                    </label>';
            }

        return $result;
    }

    public function updateUserAccess(Request $request, $id){
        $df = $request->inputData;
        $checked = $request->checked;
        $cls = $request->cls;

        if($cls == "ul-sub-menu"){
            $useraccess = User_Access::where('category','=', 'submodule')->get();
            $catName = "submodule";
        }else if($cls == "input-wrapper"){
            $useraccess = User_Access::where('category','=', 'module')->get();
            $catName = "module";
        }

        foreach($useraccess as $ua){
            if($ua->category_id == $df){
                if($checked == "true"){
                $updateAccess = User_Access::where('user_type_id', $id)
                ->where('category_id', $df)
                ->where('category', $catName)
                ->update([
                    'grant'=>1
                ]);
                }else{
                $updateAccess = User_Access::where('user_type_id', $id)
                ->where('category_id', $df)
                ->where('category', $catName)
                ->update([
                'grant'=>0
                ]);
                }
            }
        }

        
        return $df."\n".$checked;
        
    }

    public function users(){
    	$users = DB::table('users as u')->select([
            'u.id',
    		'u.fname',
    		'u.lname',
            'u.mname',
            'u.email',
    		'u.username',
            'u.status',
    		'ut.type',
            'dist.district_name',
            'temp.template_name'
    		])
        ->leftJoin('user_type as ut', 'u.user_type', '=', 'ut.id')
        ->leftJoin('mst_districts as dist', 'u.district_id', '=', 'dist.id')
        ->leftJoin('template_types as temp', 'u.scorecard_type', '=', 'temp.id')
        ->get();

        $userTypes = User_Type::all();
        $tempTypes = Template_type::all();
        $districts = mst_district::orderBy('district_name','ASC')->get();
        $mst_province = mst_province::all();

    	return view('maintenance.user_management', compact('users', 'userTypes', 'tempTypes', 'districts', 'mst_province'));
    }

    public function addum(Request $request){

        $this->validate($request, [
                'fname' => 'required',
                'lname' => 'required',
                'email' => 'required',
                'password' => 'required'
            ]);

    $utype = $request->utype;
        // dd($utype);
        if($utype < 3){
            $sctype = $request->scorecardtype;
            $district = 0;
        }else if($utype == 3){
            $sctype = $request->scorecardtype;
            $district = $request->district;
        }else if($utype == 16){
            $sctype = 2;
            $district = 0;
        }else if($utype == 22){
            $sctype = $request->scorecardtype;
            $district = $request->district;
        }

        $user = new User;
        $user->fname = $request->fname;
        $user->lname = $request->lname;
        $user->mname = $request->mname;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->user_type = $request->utype;
        $user->scorecard_type = $sctype;
        $user->district_id = $district;
        $user->status = 'pending';
        $user->password = bcrypt($request->password);
        $user->save();

        return redirect('usermanagement');
    }
    public function updateumstatus(Request $request){
        $id = $request->id;
        $stat = $request->status;
        $user = User::find($id);
        $user->status = $stat;
        $user->save();
        return 'success';
    }
    public function updateum(Request $request){

        $this->validate($request, [
            'fname' => 'required',
            'lname' => 'required',
            'mname' => 'required',
            'email' => 'required',
            'password' => 'required'
        ]);

        $id = $request->trid;
        $usertype = $request->usertype;

        $user = User::find($id);
        $user->user_type = $usertype;
        $user->save();

        return redirect()->route('usermanagement');
    }

    public function changePass(Request $request){
        $click = User::find($request->id);
        $dbpass = $click->password;
        $oldpass = $request->oldpass;
        $newpass = $request->newpass;
        $alert = 0;
        if(Hash::check($oldpass, $dbpass)){
            $updatePass = User::find($request->id);
            $updatePass->password = Hash::make($newpass);
            $updatePass->save();
            $alert = 1;
        }else{
            $alert = 0;
        }
        // $alert = '';
        // if($oldpass == $hashNewpass){
        //     $alert = 'Correct';
        // }else{
        //      $alert = 'Wrong';
        // }
        return $alert;
    }
}
