<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Template_type;
use App\mst_district;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        if ($data['user_type'] == 2) {
             return Validator::make($data, [
                'fname' => 'required|string|max:255',
                'lname' => 'required|string|max:255',
                'mname' => 'required|string|max:255',
                'username' => 'required|string|max:50|unique:users',
                'template_type' =>'required',
                'mobile_number' => 'required|string|max:11',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:6|confirmed',
            ]);
        }else{
            return Validator::make($data, [
                'fname' => 'required|string|max:255',
                'lname' => 'required|string|max:255',
                'mname' => 'required|string|max:255',
                'username' => 'required|string|max:50|unique:users',
                'district' => 'required',
                'mobile_number' => 'required|string|max:11',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:6|confirmed',
            ]);
        }
        
       
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $template_type = 0;
        $district = 0;
        if ($data['user_type'] == 2) {
            $template_type = $data['template_type'];
        }else{
            $district = $data['district'];
        }

        return User::create([
            'fname' => $data['fname'],
            'lname' => $data['lname'],
            'mname' => $data['mname'],
            'user_type' => $data['user_type'],
            'mobile_number' => $data['mobile_number'],
            'scorecard_type' => $template_type,
            'username' => $data['username'],
            'district_id' => $district,
            'email' => $data['email'],
            'status' => 'approved',
            'password' => bcrypt($data['password']),
        ]);
        
    }
    public function showRegistrationForm()
    {
        abort(404, 'Page not found');   
        // $templates = Template_type::all();
        // $districts = mst_district::all();
        // return view('auth.register', compact('templates', 'districts'));
    }
}
