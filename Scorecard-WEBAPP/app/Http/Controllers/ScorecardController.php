<?php
namespace App\Http\Controllers;

use App\Template_Header;
use App\Template_type;
use App\Scorecard_Header;
use App\area;
use App\mst_district;
use App\mst_schools;
use App\mst_municipality;
use App\Scorecard_Assessment_Rounds;	
use App\scorecard_template_round_names;
use App\Scorecard_Evaluator;
use App\Scorecard_Evaluator_Access;
use App\Template_KRA;
use App\scorecard_template_setting;
use App\Scorecard_Ledger_Header;
use App\Scorecard_ledger_Detail;
use App\Template_Indicator;
use App\ocp_evaluator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use DB;

class ScorecardController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
	
	public function updateEvaluators(Request $request){
		$evalid = $request->evalId;
		$lname = $request->lname;
		$mname = $request->mname;
		$fname = $request->fname;
		$email = $request->ea;
		$mobnum = $request->mn;

		if($request->strid == 1){
			$evalweight = $request->ew;
		}else{
			$evalweight = NULL;
		}

		$eval = Scorecard_Evaluator::find($evalid);
		$eval->fname = $fname;
		$eval->mname = $mname;
		$eval->lname = $lname;
		$eval->mobile_no = $mobnum;
		$eval->email = $email;
		$eval->evaluator_weight = $evalweight;
		$eval->save();
		return "Success";
	}

	public function modifyEval($id){
		$evals = Scorecard_Evaluator::find($id);
		return $evals;
	}

	public function createChosenScorecard(Request $request){
		$user = Auth::id();
		$proj_name = $request->proj_name;
		$temp_id = $request->temp_id;
		$level = $request->level;
		$start_date = $request->start_date;
		$end_date = $request->end_date;
		$checkboxChecked = $request->checkboxChecked;

		$getTempType = Template_Header::find($temp_id);
		$scorecardtype = $getTempType->template_type;

		if($scorecardtype == 2){
			// for($b=1;$b<=$checkboxChecked;$b++){
			$schoolid = $request->schoolval;
			$schools = mst_municipality::where("id", $schoolid)->get();
			$scorecard_template_setting = scorecard_template_setting::where('template_header_id', $temp_id)->first();
			$num_of_rounds = $scorecard_template_setting->num_of_rounds;
			$no_of_evaluators = $scorecard_template_setting->num_of_evaluators;

			foreach ($schools as $school) {
				$saveScorecard = new Scorecard_Header;
				$saveScorecard->project_name = $proj_name;
				$saveScorecard->no_assessment = $num_of_rounds;
				$saveScorecard->school_id = $school->id;
				$saveScorecard->level = $level;
				$saveScorecard->start_date = $start_date;
				$saveScorecard->end_date = $end_date;
				$saveScorecard->template_id = $temp_id;
				$saveScorecard->created_by = $user;
				$saveScorecard->updated_by = '';
				$saveScorecard->status = 1;
				$saveScorecard->scorecard_type = $scorecardtype;
				$saveScorecard->save();

				for($i=1;$i<=$num_of_rounds;$i++){
					$scorecard_assessment_rounds = new Scorecard_Assessment_Rounds;
					$scorecard_assessment_rounds->scorecard_header_id = $saveScorecard->id;
					$scorecard_assessment_rounds->round_num = $i;
					$scorecard_assessment_rounds->weight = $request->{"weight".$i};
					$scorecard_assessment_rounds->round_name = $request->{"rname".$i};
					$scorecard_assessment_rounds->start_date = $start_date;
					$scorecard_assessment_rounds->end_date = $end_date;	
					$scorecard_assessment_rounds->status = 1;
					$scorecard_assessment_rounds->no_of_evaluators = $no_of_evaluators;
					$scorecard_assessment_rounds->save();	

					for($a=1;$a<=$no_of_evaluators;$a++){
						$scorecard_evaluator = new Scorecard_Evaluator;
						$scorecard_evaluator->scorecard_header_id = $saveScorecard->id;
						$scorecard_evaluator->assessment_round_id = $scorecard_assessment_rounds->id;
						$scorecard_evaluator->fname = '';
						$scorecard_evaluator->mname = '';
						$scorecard_evaluator->lname = '';
						$scorecard_evaluator->mobile_no = '';
						$scorecard_evaluator->email = '';
						$scorecard_evaluator->status = '1';
						$scorecard_evaluator->username = "RAFI-".$saveScorecard->id.mt_rand(100000,999999);
						$scorecard_evaluator->password = Hash::make("RAFI-".$a);
						$scorecard_evaluator->expiry_date = $end_date;
						$scorecard_evaluator->save();

						$template_kra = Template_KRA::where('template_header_id', $temp_id)->get();

						foreach ($template_kra as $tk) {
							$scorecard_evaluator_access = new Scorecard_Evaluator_Access;
							$scorecard_evaluator_access->evaulator_id = $scorecard_evaluator->id;
							$scorecard_evaluator_access->kra_id = $tk->id;
							$scorecard_evaluator_access->save();
						}	
					}
				}
			}
		//}

		return "Success";
		}else{
			// for($b=1;$b<=$checkboxChecked;$b++){
			$schoolid = $request->schoolval;
			$schools = mst_schools::where("id", $schoolid)->get();
			$scorecard_template_setting = scorecard_template_setting::where('template_header_id', $temp_id)->first();
			$num_of_rounds = $scorecard_template_setting->num_of_rounds;
			$no_of_evaluators = $scorecard_template_setting->num_of_evaluators;

			foreach ($schools as $school) {
				$saveScorecard = new Scorecard_Header;
				$saveScorecard->project_name = $proj_name;
				$saveScorecard->no_assessment = $num_of_rounds;
				$saveScorecard->school_id = $school->id;
				$saveScorecard->level = $level;
				$saveScorecard->start_date = $start_date;
				$saveScorecard->end_date = $end_date;
				$saveScorecard->template_id = $temp_id;
				$saveScorecard->created_by = $user;
				$saveScorecard->updated_by = '';
				$saveScorecard->status = 1;
				$saveScorecard->scorecard_type = $scorecardtype;
				$saveScorecard->save();

				for($i=1;$i<=$num_of_rounds;$i++){
					$scorecard_assessment_rounds = new Scorecard_Assessment_Rounds;
					$scorecard_assessment_rounds->scorecard_header_id = $saveScorecard->id;
					$scorecard_assessment_rounds->round_num = $i;
					$scorecard_assessment_rounds->weight = $request->{"weight".$i};
					$scorecard_assessment_rounds->round_name = $request->{"rname".$i};
					$scorecard_assessment_rounds->start_date = $start_date;
					$scorecard_assessment_rounds->end_date = $end_date;	
					$scorecard_assessment_rounds->status = 1;
					$scorecard_assessment_rounds->no_of_evaluators = $no_of_evaluators;
					$scorecard_assessment_rounds->save();	

					for($a=1;$a<=$no_of_evaluators;$a++){
						$scorecard_evaluator = new Scorecard_Evaluator;
						$scorecard_evaluator->scorecard_header_id = $saveScorecard->id;
						$scorecard_evaluator->assessment_round_id = $scorecard_assessment_rounds->id;
						$scorecard_evaluator->fname = '';
						$scorecard_evaluator->mname = '';
						$scorecard_evaluator->lname = '';
						$scorecard_evaluator->mobile_no = '';
						$scorecard_evaluator->email = '';
						$scorecard_evaluator->status = '1';
						$scorecard_evaluator->username = "RAFI-".$saveScorecard->id.mt_rand(100000,999999);
						$scorecard_evaluator->password = Hash::make("RAFI-".$a);
						$scorecard_evaluator->expiry_date = $end_date;
						$scorecard_evaluator->save();

						$template_kra = Template_KRA::where('template_header_id', $temp_id)->get();

						foreach ($template_kra as $tk) {
							$scorecard_evaluator_access = new Scorecard_Evaluator_Access;
							$scorecard_evaluator_access->evaulator_id = $scorecard_evaluator->id;
							$scorecard_evaluator_access->kra_id = $tk->id;
							$scorecard_evaluator_access->save();
						}	
					}
				}
			}
		//}

		return "Success";
		}
	}

	public function createChosenScorecard2(Request $request){
		$user = Auth::id();
		$proj_name = $request->proj_name1;
		$temp_id = $request->temp_id;
		$level = $request->level;
		$start_date = $request->start_date;
		$end_date = $request->end_date;
		$checkboxChecked = $request->checkboxChecked;

		$getTempType = Template_Header::find($temp_id);
		$scorecardtype = $getTempType->template_type;
		// for($b=1;$b<=$checkboxChecked;$b++){
			$schoolid = $request->schoolval;
			$schools = mst_schools::where("id", $schoolid)->get();
			$scorecard_template_setting = scorecard_template_setting::where('template_header_id', $temp_id)->first();
			$num_of_rounds = $scorecard_template_setting->num_of_rounds;
			$no_of_evaluators = $scorecard_template_setting->num_of_evaluators;

			foreach ($schools as $school) {
				$saveScorecard = new Scorecard_Header;
				$saveScorecard->project_name = $proj_name;
				$saveScorecard->no_assessment = $num_of_rounds;
				$saveScorecard->school_id = $school->id;
				$saveScorecard->level = $level;
				$saveScorecard->start_date = $start_date;
				$saveScorecard->end_date = $end_date;
				$saveScorecard->template_id = $temp_id;
				$saveScorecard->created_by = $user;
				$saveScorecard->updated_by = '';
				$saveScorecard->status = 1;
				$saveScorecard->scorecard_type = $scorecardtype;
				$saveScorecard->save();

				for($i=1;$i<=$num_of_rounds;$i++){
					$scorecard_assessment_rounds = new Scorecard_Assessment_Rounds;
					$scorecard_assessment_rounds->scorecard_header_id = $saveScorecard->id;
					$scorecard_assessment_rounds->round_num = $i;
					$scorecard_assessment_rounds->weight = $request->{"weight".$i};
					$scorecard_assessment_rounds->round_name = $request->{"rname".$i};
					$scorecard_assessment_rounds->start_date = $start_date;
					$scorecard_assessment_rounds->end_date = $end_date;	
					$scorecard_assessment_rounds->status = 1;
					$scorecard_assessment_rounds->no_of_evaluators = $no_of_evaluators;
					$scorecard_assessment_rounds->save();	
					if($i == 1){
						for($a=1;$a<=$no_of_evaluators;$a++){
						$scorecard_evaluator = new Scorecard_Evaluator;
						$scorecard_evaluator->scorecard_header_id = $saveScorecard->id;
						$scorecard_evaluator->assessment_round_id = $scorecard_assessment_rounds->id;
						$scorecard_evaluator->fname = '';
						$scorecard_evaluator->mname = '';
						$scorecard_evaluator->lname = '';
						$scorecard_evaluator->mobile_no = '';
						$scorecard_evaluator->email = '';
						$scorecard_evaluator->status = '1';
						$scorecard_evaluator->username = "RAFI-".$saveScorecard->id.mt_rand(100000,999999);
						$scorecard_evaluator->password = Hash::make("RAFI-".$a);
						$scorecard_evaluator->expiry_date = $end_date;
						$scorecard_evaluator->save();

						$template_kra = Template_KRA::where('template_header_id', $temp_id)->get();

						foreach ($template_kra as $tk) {
							$scorecard_evaluator_access = new Scorecard_Evaluator_Access;
							$scorecard_evaluator_access->evaulator_id = $scorecard_evaluator->id;
							$scorecard_evaluator_access->kra_id = $tk->id;
							$scorecard_evaluator_access->save();
						}	
					}
					}
				}
			}
		//}

		return "Success";
	}

	public function createBulkScorecard(Request $request){

		$user = Auth::id();
		$proj_name = $request->proj_name;
		$temp_id = $request->temp_id;
		$level = $request->level;	
		$start_date = $request->start_date;
		$end_date = $request->end_date;
		$scorecard_template_setting = scorecard_template_setting::where('template_header_id', $temp_id)->first();
		$num_of_rounds = $scorecard_template_setting->num_of_rounds;
		$no_of_evaluators = $scorecard_template_setting->num_of_evaluators;
		$getTempType = Template_Header::find($temp_id);
		$scorecardtype = $getTempType->template_type;

		if($scorecardtype == 2){
		$schools = mst_municipality::all();
		$qwe = 0;
		foreach ($schools as $school) {
			$saveScorecard = new Scorecard_Header;
			$saveScorecard->project_name = $proj_name;
			$saveScorecard->no_assessment = $num_of_rounds;
			$saveScorecard->school_id = $school->id;
			$saveScorecard->level = $level;
			$saveScorecard->start_date = $start_date;
			$saveScorecard->end_date = $end_date;
			$saveScorecard->template_id = $temp_id;
			$saveScorecard->created_by = $user;
			$saveScorecard->updated_by = '';
			$saveScorecard->status = 1;
			$saveScorecard->scorecard_type = $scorecardtype;
			$saveScorecard->save();

			for($i=1;$i<=$num_of_rounds;$i++){
				$scorecard_assessment_rounds = new Scorecard_Assessment_Rounds;
				$scorecard_assessment_rounds->scorecard_header_id = $saveScorecard->id;
				$scorecard_assessment_rounds->round_num = $i;
				$scorecard_assessment_rounds->weight = $request->{"weight".$i};
				$scorecard_assessment_rounds->round_name = $request->{"rname".$i};
				$scorecard_assessment_rounds->start_date = $start_date;
				$scorecard_assessment_rounds->end_date = $end_date;	
				$scorecard_assessment_rounds->status = 1;
				$scorecard_assessment_rounds->no_of_evaluators = $no_of_evaluators;
				$scorecard_assessment_rounds->save();	

				for($a=1;$a<=$no_of_evaluators;$a++){
					$scorecard_evaluator = new Scorecard_Evaluator;
					$scorecard_evaluator->scorecard_header_id = $saveScorecard->id;
					$scorecard_evaluator->assessment_round_id = $scorecard_assessment_rounds->id;
					$scorecard_evaluator->fname = '';
					$scorecard_evaluator->mname = '';
					$scorecard_evaluator->lname = '';
					$scorecard_evaluator->mobile_no = '';
					$scorecard_evaluator->email = '';
					$scorecard_evaluator->status = '1';
					$scorecard_evaluator->username = "RAFI-".$saveScorecard->id.mt_rand(100000,999999);
					$scorecard_evaluator->password = Hash::make("RAFI-".$a);
					$scorecard_evaluator->expiry_date = $end_date;
					$scorecard_evaluator->save();

					$template_kra = Template_KRA::where('template_header_id', $temp_id)->get();

					foreach ($template_kra as $tk) {
						$scorecard_evaluator_access = new Scorecard_Evaluator_Access;
						$scorecard_evaluator_access->evaulator_id = $scorecard_evaluator->id;
						$scorecard_evaluator_access->kra_id = $tk->id;
						$scorecard_evaluator_access->save();
					}	
				}
			}
		}

		return "Success";
		}else{
		$schools = mst_schools::all();
		$qwe = 0;
		foreach ($schools as $school) {
			$saveScorecard = new Scorecard_Header;
			$saveScorecard->project_name = $proj_name;
			$saveScorecard->no_assessment = $num_of_rounds;
			$saveScorecard->school_id = $school->id;
			$saveScorecard->level = $level;
			$saveScorecard->start_date = $start_date;
			$saveScorecard->end_date = $end_date;
			$saveScorecard->template_id = $temp_id;
			$saveScorecard->created_by = $user;
			$saveScorecard->updated_by = '';
			$saveScorecard->status = 1;
			$saveScorecard->scorecard_type = $scorecardtype;
			$saveScorecard->save();

			for($i=1;$i<=$num_of_rounds;$i++){
				$scorecard_assessment_rounds = new Scorecard_Assessment_Rounds;
				$scorecard_assessment_rounds->scorecard_header_id = $saveScorecard->id;
				$scorecard_assessment_rounds->round_num = $i;
				$scorecard_assessment_rounds->weight = $request->{"weight".$i};
				$scorecard_assessment_rounds->round_name = $request->{"rname".$i};
				$scorecard_assessment_rounds->start_date = $start_date;
				$scorecard_assessment_rounds->end_date = $end_date;	
				$scorecard_assessment_rounds->status = 1;
				$scorecard_assessment_rounds->no_of_evaluators = $no_of_evaluators;
				$scorecard_assessment_rounds->save();	

				for($a=1;$a<=$no_of_evaluators;$a++){
					$scorecard_evaluator = new Scorecard_Evaluator;
					$scorecard_evaluator->scorecard_header_id = $saveScorecard->id;
					$scorecard_evaluator->assessment_round_id = $scorecard_assessment_rounds->id;
					$scorecard_evaluator->fname = '';
					$scorecard_evaluator->mname = '';
					$scorecard_evaluator->lname = '';
					$scorecard_evaluator->mobile_no = '';
					$scorecard_evaluator->email = '';
					$scorecard_evaluator->status = '1';
					$scorecard_evaluator->username = "RAFI-".$saveScorecard->id.mt_rand(100000,999999);
					$scorecard_evaluator->password = Hash::make("RAFI-".$a);
					$scorecard_evaluator->expiry_date = $end_date;
					$scorecard_evaluator->save();

					$template_kra = Template_KRA::where('template_header_id', $temp_id)->get();

					foreach ($template_kra as $tk) {
						$scorecard_evaluator_access = new Scorecard_Evaluator_Access;
						$scorecard_evaluator_access->evaulator_id = $scorecard_evaluator->id;
						$scorecard_evaluator_access->kra_id = $tk->id;
						$scorecard_evaluator_access->save();
					}	
				}
			}
		}

		return "Success";
		}
	}

	public function createBulkScorecard2(Request $request){
		$user = Auth::id();
		$proj_name = $request->proj_name;
		$temp_id = $request->temp_id;
		$level = $request->level;	
		$start_date = $request->start_date;
		$end_date = $request->end_date;
		$pname = $request->baseProj;

		$schools = DB::table('recommended_schools as r')
						->select('s.id','s.school_code', 's.school_name',
								 'd.id as district_id', 'd.district_name')
						->leftJoin('mst_schools as s', 's.id', '=', 'r.school_id')
						->leftJoin('mst_districts as d', 's.district_id', '=', 'd.id')
						->where('project_name', $pname)
						->get();

		$scorecard_template_setting = scorecard_template_setting::where('template_header_id', $temp_id)->first();
		$num_of_rounds = $scorecard_template_setting->num_of_rounds;
		$no_of_evaluators = $scorecard_template_setting->num_of_evaluators;
		$getTempType = Template_Header::find($temp_id);
		$scorecardtype = $getTempType->template_type;

		foreach ($schools as $school) {
			$saveScorecard = new Scorecard_Header;
			$saveScorecard->project_name = $proj_name;
			$saveScorecard->no_assessment = $num_of_rounds;
			$saveScorecard->school_id = $school->id;
			$saveScorecard->level = $level;
			$saveScorecard->start_date = $start_date;
			$saveScorecard->end_date = $end_date;
			$saveScorecard->template_id = $temp_id;
			$saveScorecard->created_by = $user;
			$saveScorecard->updated_by = '';
			$saveScorecard->status = 1;
			$saveScorecard->scorecard_type = $scorecardtype;
			$saveScorecard->save();

			for($i=1;$i<=$num_of_rounds;$i++){
				$scorecard_assessment_rounds = new Scorecard_Assessment_Rounds;
				$scorecard_assessment_rounds->scorecard_header_id = $saveScorecard->id;
				$scorecard_assessment_rounds->round_num = $i;
				$scorecard_assessment_rounds->weight = $request->{"weight".$i};
				$scorecard_assessment_rounds->round_name = $request->{"rname".$i};
				$scorecard_assessment_rounds->start_date = $start_date;
				$scorecard_assessment_rounds->end_date = $end_date;	
				$scorecard_assessment_rounds->status = 1;
				$scorecard_assessment_rounds->no_of_evaluators = $no_of_evaluators;
				$scorecard_assessment_rounds->save();	

				if($i == 1){
					for($a=1;$a<=$no_of_evaluators;$a++){
						$scorecard_evaluator = new Scorecard_Evaluator;
						$scorecard_evaluator->scorecard_header_id = $saveScorecard->id;
						$scorecard_evaluator->assessment_round_id = $scorecard_assessment_rounds->id;
						$scorecard_evaluator->fname = '';
						$scorecard_evaluator->mname = '';
						$scorecard_evaluator->lname = '';
						$scorecard_evaluator->mobile_no = '';
						$scorecard_evaluator->email = '';
						$scorecard_evaluator->status = '1';
						$scorecard_evaluator->username = "RAFI-".$saveScorecard->id.mt_rand(100000,999999);
						$scorecard_evaluator->password = Hash::make("RAFI-".$a);
						$scorecard_evaluator->expiry_date = $end_date;
						$scorecard_evaluator->save();

						$template_kra = Template_KRA::where('template_header_id', $temp_id)->get();

						foreach ($template_kra as $tk) {
							$scorecard_evaluator_access = new Scorecard_Evaluator_Access;
							$scorecard_evaluator_access->evaulator_id = $scorecard_evaluator->id;
							$scorecard_evaluator_access->kra_id = $tk->id;
							$scorecard_evaluator_access->save();
						}	
					}
				}
			}
		}

		return "Success";
	}

	public function getRounds($id){
		$scorecard_template_setting = scorecard_template_setting::where('template_header_id', $id)->first();
		$num_of_rounds = $scorecard_template_setting->num_of_rounds;
		$roundNames = scorecard_template_round_names::all();
		return [$roundNames, $num_of_rounds];
	}

	public function recoSchools($pname){

		$schools = '';
		$recoSchools = DB::table('recommended_schools as r')
						->select('s.id','s.school_code', 's.school_name',
								 'd.id as district_id', 'd.district_name')
						->leftJoin('mst_schools as s', 's.id', '=', 'r.school_id')
						->leftJoin('mst_districts as d', 's.district_id', '=', 'd.id')
						->where('project_name', $pname)
						->get();

		if(count($recoSchools) > 0){
			foreach($recoSchools as $school){
             	$schools .= '<tr>
	              <td>'. $school->school_code .'</td>
	              <td>'. $school->school_name .'</td>
	              <td>'. $school->district_name .'</td>
	              <input type="hidden" id="disID" value="'.$school->district_id .'">
			      <td><input type="checkbox" name="schoolid" value="'.$school->id.'"></td>';
        	}
		}else{
			$schools .= '<tr class="text-center">
				          <td colspan="5"> <strong>No school record.</strong></td>
				        </tr>';
		}

		return $schools;
	}

	public function chooseSchools(Request $request){

		if($request->aad == 2){
			$munic = mst_municipality::all();
	        $municcount = count($munic);
	        $allschool = '';
	        $counter = 1;
	        if(count($munic) > 0){
		        foreach($munic as $mun){
		        	 $allschool .= '<tr>
		              <td>'. $mun->municipality_code .'</td>
		              <td>'. $mun->municipality_name .'</td>
		              <td><input type="checkbox" name="schoolid" value="'.$mun->id.'"></td></tr>';
	               }
	         //  foreach($schools as $school){
	         //     $allschool .= '<tr>
	         //      <td>'. $school->school_code .'</td>
	         //      <td>'. $school->school_name .'</td>';

	         //      foreach($districts as $district){
		        //       if($school->district_id == $district->id){
			       //      $allschool .= '<td>'.$district->district_name .'</td>
			       //        <input type="hidden" id="disID" value="'.$school->district_id .'">
			       //        <td><input type="checkbox" name="schoolid" value="'.$school->id.'"></td>';
			       //        break;
			       //       $allschool .= '</tr>';
		        // 		}
	         //       }
	        	// }
	        }else{
	         $allschool .= '<tr class="text-center">
	          <td colspan="5"> <strong>No school record.</strong></td>
	        </tr>';
	    	}

	    	return $allschool;
		}else{
			$schools = mst_schools::all();
	        $districts = mst_district::all();
	        $schoolcount = count($schools);
	        $allschool = '';
	        $counter = 1;
	        if(count($schools) > 0){
	          foreach($schools as $school){
	             $allschool .= '<tr>
	              <td>'. $school->school_code .'</td>
	              <td>'. $school->school_name .'</td>';

	              foreach($districts as $district){
		              if($school->district_id == $district->id){
			            $allschool .= '<td>'.$district->district_name .'</td>
			              <input type="hidden" id="disID" value="'.$school->district_id .'">
			              <td><input type="checkbox" name="schoolid" value="'.$school->id.'"></td>';
			              break;
			             $allschool .= '</tr>';
		        		}
	               }
	        	}
	        }else{
	         $allschool .= '<tr class="text-center">
	          <td colspan="5"> <strong>No school record.</strong></td>
	        </tr>';
	    	}

	    	return $allschool;
		}

    	// return view('scorecard.options.chooseschools', compact('schools', 'districts'));
    }

	public function view($id){

		$scorecard = Scorecard_Header::find($id);
		$templateID = $scorecard->template_id;
		$templates = Template_Header::where('id', $templateID)->first();

		if($templates->template_type == 2){
			//OCP
			$schoolid = $scorecard->school_id;
			$schools = mst_municipality::find($schoolid);
		}else{
			//SEED
			$schoolid = $scorecard->school_id;
			$schools = mst_schools::find($schoolid);
			$distID = $schools->district_id;
			$district = mst_district::find($distID);
		}
		
		// $scorecard_assessment_rounds = Scorecard_Assessment_Rounds::where('scorecard_header_id', $id)->get();
		$scorecard_assessment_rounds = DB::table('scorecard_assessment_rounds as sar')
		->select([
			'sar.round_name',
			'sar.round_num',
			'sar.weight',
			'sar.start_date',
			'sar.end_date',
			'sar.scorecard_header_id',
			'sar.no_of_evaluators',
			'strn.round_name as strname',
			'strn.id as strid'
		])
		->leftJoin('scorecard_template_round_names as strn', 'sar.round_name', '=', 'strn.id')
		->where('sar.scorecard_header_id', $id)
		->get();

		$sar = DB::table('scorecard_assessment_rounds as sar')
		->select([
			'sar.scorecard_header_id',
			'sar.round_num',
			'sar.no_of_evaluators',
			'se.id',
			'se.username',
			'se.fname',
			'se.mname',
			'se.mname',
			'se.lname',
			'se.mobile_no',
			'se.email',
			'se.expiry_date',
			'se.evaluator_weight',
			'sar.round_name'
		])
		->leftJoin('scorecard_evaluators as se', 'sar.id', '=', 'se.assessment_round_id')
		->where('sar.status', 1)
		->where('sar.scorecard_header_id', $id)
		->get();
		// average grade
		$scorecard_evaluator = Scorecard_Evaluator::where('scorecard_header_id', $id)->count();
		$evaluator_submit = Scorecard_Ledger_Header::where('scorecard_header_id', $id)->count();
		$score = '';
			
		if ($scorecard_evaluator == $evaluator_submit) {
			$scores = DB::table('scorecard_ledger_headers as headers')
			->select( DB::raw('AVG(headers.total_score) as val'))
			->groupby('headers.scorecard_header_id')
			->where('headers.scorecard_header_id', $id)
			->first();
			// dd($scores);
			$score = number_format($scores->val, 2, '.', ',').'%';
		}
		
		$submit_evaluator = Scorecard_Ledger_Header::all();
		
		
		
		return view('scorecard.actions.view', compact('templates', 'schools', 'district', 'scorecard', 'sar', 'scorecard_assessment_rounds', 'score', 'submit_evaluator'));
	}
	
	public function edit($id){
		$scorecard = Scorecard_Header::find($id);
		$templateID = $scorecard->template_id;
		$area_id = $scorecard->area_id;
		$district_id = $scorecard->district_id;

		$templates = Template_Header::where('id', $templateID)->first();
		$areaSelected = area::where('id', $area_id)->first();
		$districtSelected = district::where('id', $district_id)->first();
		$area = area::all();
		$district = district::all();

		return view('scorecard.edit', compact('templates', 'areaSelected', 'districtSelected', 'area', 'district' ,'scorecard'));
	}

	public function delete($id){
		
	}

	public function assignEvaluator(Request $request){
		$kraid = $request->kraid;
		$evalid = $request->evalid;

		$scorecard_evaluator_accesses = new Scorecard_Evaluator_Access;
		$scorecard_evaluator_accesses->evaulator_id = $evalid;
		$scorecard_evaluator_accesses->kra_id = $kraid;
		$scorecard_evaluator_accesses->save();

		return "Evaluator Assigned!";
		// return redirect('scorecard/edit/'.$id);
	}

	public function updateScorecard(Request $request, $id){
		$allInputs = $request->all();
		$user = Auth::id();
		$updateSc = Scorecard_Header::where('id', $id)
		->update([
			'project_name'=>$allInputs['project_name'],
			'no_assessment'=>$allInputs['noa'],
			'area_id'=>$allInputs['area'],
			'district_id'=>$allInputs['district'],
			'start_date'=>$allInputs['start_date'],
			'end_date'=>$allInputs['end_date'],
			'updated_by'=>$user,
		]);

		return redirect('/scorecard/edit/'.$id);
	}

	public function viewAllPublish(){
		$usertype = Auth::user()->user_type;
		$sctype = Auth::user()->scorecard_type;

		//Condition for SDS / LGU users
		if($usertype == 3){
			$mst_schools = mst_schools::all();
            $mst_municipality = mst_municipality::all();
			//Condition for OCP Scorecard
			if($sctype == 2){
				$districtid = Auth::user()->district_id;
			    $getMid = mst_district::find($districtid);
			    $mid = $getMid->municipality_id;

				$projectlist = DB::table('scorecard__headers as sh')
					->distinct()
					->leftJoin('template_headers as th', 'sh.template_id', '=', 'th.id')
					->where('th.template_type', $sctype)
					->get([
						'sh.scorecard_type',
						'project_name'
					]);

				$scorecards = DB::table('scorecard__headers as sh')
				->select([
					'sh.id',
					'sh.project_name',
					'th.template_name',
					'sh.no_assessment',
					'sh.status',
					'sh.school_id',
					'th.template_type'
					])
				->leftJoin('template_headers as th', 'sh.template_id', '=', 'th.id')
				->leftJoin('users as u', 'u.id', '=', 'sh.created_by')
				->orderByRaw('sh.id DESC')
				->where('th.template_type', $sctype)
				->where('sh.school_id', $mid)
				->paginate(10);

				// dd('dfdf');
			}
			//Condition for SEED Scorecard
			else{
				// dd('dfdf');
				$districtid = Auth::user()->district_id;
				$projectlist = DB::table('scorecard__headers')
					->distinct()
					->get([
						'scorecard_type',
						'project_name'
					]);
				$scorecards = DB::table('scorecard__headers as sh')
		        ->select([
		            'sh.id',
		            'sh.project_name',
		            'th.template_name',
		            'sh.no_assessment',
		            'sh.status',
					'sh.school_id',
					'th.template_type'
		            ])
		        ->leftJoin('template_headers as th', 'sh.template_id', '=', 'th.id')
		        ->leftJoin('users as u', 'u.id', '=', 'sh.created_by')
		        ->leftJoin('mst_schools as ms', 'sh.school_id', '=', 'ms.id')
		        ->orderByRaw('sh.id DESC')
		        ->where('ms.district_id', $districtid)
				->where('level', 1)
				->paginate(10);
			}
		}
		//Condition for Scorecard Admin users
		else if($usertype == 2){
			$mst_schools = mst_schools::all();
            $mst_municipality = mst_municipality::all();
			//Condition for OCP Scorecard
			if($sctype == 2){
				$projectlist = DB::table('scorecard__headers as sh')
					->distinct()
					->leftJoin('template_headers as th', 'sh.template_id', '=', 'th.id')
					->where('th.template_type', $sctype)
					->get([
						'sh.scorecard_type',
						'project_name'
					]);
				$scorecards = DB::table('scorecard__headers as sh')
				->select([
					'sh.id',
					'sh.project_name',
					'th.template_name',
					'sh.no_assessment',
					'sh.status',
					'sh.school_id',
					'th.template_type'
					])
				->leftJoin('template_headers as th', 'sh.template_id', '=', 'th.id')
				->leftJoin('users as u', 'u.id', '=', 'sh.created_by')
				->orderByRaw('sh.id DESC')
				->where('th.template_type', $sctype)
				->paginate(10);
			}
			//Condition for SEED Scorecard
			else{
				$projectlist = DB::table('scorecard__headers as sh')
					->distinct()
					->leftJoin('template_headers as th', 'sh.template_id', '=', 'th.id')
					->where('th.template_type', 1)
					->get([
						'sh.scorecard_type',
						'sh.project_name'
					]);
				$scorecards = DB::table('scorecard__headers as sh')
				->select([
					'sh.id',
					'sh.project_name',
					'th.template_name',
					'sh.no_assessment',
					'sh.status',
					'sh.school_id',
					'th.template_type'
					])
				->leftJoin('template_headers as th', 'sh.template_id', '=', 'th.id')
				->leftJoin('users as u', 'u.id', '=', 'sh.created_by')
				->orderByRaw('sh.id DESC')
				->where('th.template_type', $sctype)
				->paginate(10);
			}
		}
		//Condition for Superadmin users
		else{
			$mst_schools = mst_schools::all();
            $mst_municipality = mst_municipality::all();

			$projectlist = DB::table('scorecard__headers')
			->distinct()
			->get([
				'scorecard_type',
				'project_name'
			]);

			$scorecards = DB::table('scorecard__headers as sh')
	        ->select([
	            'sh.id',
	            'sh.project_name',
	            'th.template_name',
	            'sh.no_assessment',
	            'sh.status',
				'sh.school_id',
				'th.template_type'
	            ])
	        ->leftJoin('template_headers as th', 'sh.template_id', '=', 'th.id')
	        ->leftJoin('users as u', 'u.id', '=', 'sh.created_by')
	        ->orderByRaw('sh.id DESC')
			->paginate(10);	
		}

		$count = count($scorecards);
		return view('scorecard.actions.viewall', compact('scorecards', 'count', 'usertype' ,'projectlist','mst_municipality', 'mst_schools'));
	}

	public function index(){
		$usertype = Auth::user()->user_type;
		$sctype = Auth::user()->scorecard_type;

		//Condition for SDS / LGU users
		if($usertype == 3){
			$mst_schools = mst_schools::all();
            $mst_municipality = mst_municipality::all();

			//Condition for OCP Scorecard
			if($sctype == 2){
				$districtid = Auth::user()->district_id;
			    $getMid = mst_district::find($districtid);
			    $mid = $getMid->municipality_id;

				$projectlist = DB::table('scorecard__headers as sh')
					->distinct()
					->leftJoin('template_headers as th', 'sh.template_id', '=', 'th.id')
					->where('th.template_type', $sctype)
					->get([
						'scorecard_type',
						'project_name'
					]);
				$scorecards = DB::table('scorecard__headers as sh')
				->select([
					'sh.id',
					'sh.project_name',
					'th.template_name',
					'sh.no_assessment',
					'sh.status',
					'sh.school_id',
					'th.template_type'
					])
				->leftJoin('template_headers as th', 'sh.template_id', '=', 'th.id')
				->leftJoin('users as u', 'u.id', '=', 'sh.created_by')
				->orderByRaw('sh.id DESC')
				->where('th.template_type', $sctype)
				->where('sh.school_id', $mid)
				->paginate(10);
			}
			//Condition for SEED Scorecard
			else{
				$districtid = Auth::user()->district_id;
				// dd($districtid);
				$projectlist = DB::table('scorecard__headers as sh')
					->distinct()
					->leftJoin('template_headers as th', 'sh.template_id', '=', 'th.id')
					->where('th.template_type', $sctype)
					->get([
						'scorecard_type',
						'project_name'
					]);
				$scorecards = DB::table('scorecard__headers as sh')
				->select([
					'sh.id',
					'sh.project_name',
					'th.template_name',
					'sh.no_assessment',
					'sh.status',
					'sh.school_id',
					'th.template_type'
					])
				->leftJoin('template_headers as th', 'sh.template_id', '=', 'th.id')
				->leftJoin('users as u', 'u.id', '=', 'sh.created_by')
				->leftJoin('mst_schools as ms', 'sh.school_id', '=', 'ms.id')
				->orderByRaw('sh.id DESC')
				->where('ms.district_id', $districtid)
				->where('th.template_type', $sctype)
				->where('level', 1)
				->paginate(10);
			}
		}
		//Condition for Scorecard Admin users
		else if($usertype == 2){
			$mst_schools = mst_schools::all();
            $mst_municipality = mst_municipality::all();
			//Condition for OCP Scorecard
			if($sctype == 2){
				$projectlist = DB::table('scorecard__headers as sh')
					->distinct()
					->leftJoin('template_headers as th', 'sh.template_id', '=', 'th.id')
					->where('th.template_type', $sctype)
					->get([
						'scorecard_type',
						'project_name'
					]);
				$scorecards = DB::table('scorecard__headers as sh')
				->select([
					'sh.id',
					'sh.project_name',
					'th.template_name',
					'sh.no_assessment',
					'sh.status',
					'sh.school_id',
					'th.template_type'
					])
				->leftJoin('template_headers as th', 'sh.template_id', '=', 'th.id')
				->leftJoin('users as u', 'u.id', '=', 'sh.created_by')
				->orderByRaw('sh.id DESC')
				->where('th.template_type', $sctype)
				->paginate(10);
			}
			//Condition for SEED Scorecard
			else{
				$projectlist = DB::table('scorecard__headers as sh')
					->distinct()
					->leftJoin('template_headers as th', 'sh.template_id', '=', 'th.id')
					->where('th.template_type', 1)
					->get([
						'sh.scorecard_type',
						'sh.project_name'
					]);
				$scorecards = DB::table('scorecard__headers as sh')
				->select([
					'sh.id',
					'sh.project_name',
					'th.template_name',
					'sh.no_assessment',
					'sh.status',
					'sh.school_id',
					'th.template_type'
					])
				->leftJoin('template_headers as th', 'sh.template_id', '=', 'th.id')
				->leftJoin('users as u', 'u.id', '=', 'sh.created_by')
				->orderByRaw('sh.id DESC')
				->where('th.template_type', $sctype)
				->paginate(10);
			}
		}
		//Condition for Superadmin users
		else{
			$projectlist = DB::table('scorecard__headers')
			->distinct()
			->get([
				'scorecard_type',
				'project_name'
			]);

			$mst_schools = mst_schools::all();
            $mst_municipality = mst_municipality::all();

			$scorecards = DB::table('scorecard__headers as sh')
			->select([
				'sh.id',
				'sh.project_name',
				'th.template_name',
				'sh.no_assessment',
				'sh.status',
				'sh.school_id',
				'th.template_type'
				])
			->leftJoin('template_headers as th', 'sh.template_id', '=', 'th.id')
			->leftJoin('users as u', 'u.id', '=', 'sh.created_by')
			->orderByRaw('sh.id DESC')
			->paginate(10);
		}		

		$count = count($scorecards);

		return view('scorecard.index', compact('scorecards', 'count', 'usertype', 'projectlist', 'mst_municipality', 'mst_schools'));
	}

	public function add($id, $level){

		$usertype = Auth::user()->user_type;
		// dd($usertype);
		if($usertype == 3){
			abort(401, 'Access denied');
		}
		
		$templates = Template_Header::where('id', $id)->first();
		// dd($templates->template_type);
		$projects = DB::table('scorecard__headers')
						->select('project_name', 'template_id')
						->where('status', 1)
						->groupBy('project_name', 'template_id')
						->distinct()
						->get();
		$schools = mst_schools::all();
		if($level == 1){
			return view('scorecard.create', compact('templates', 'level', 'schools'));
		}else if($level == 2){

			$schools1 = DB::table('recommended_schools as r')
			->select('s.id','s.school_code', 's.school_name',
					 'd.id as district_id', 'd.district_name')
			->leftJoin('mst_schools as s', 's.id', '=', 'r.school_id')
			->leftJoin('mst_districts as d', 's.district_id', '=', 'd.id')
			->where('project_name', $projects[0]->project_name)
			->get();

			$countPrj = count($schools1);
			return view('scorecard.create2', compact('templates', 'level', 'projects', 'countPrj'));
		}
		
	}

	public function add_scorecard(Request $request){
		$user = Auth::id();
		$allInputs = $request->all();

			$saveScorecard = new Scorecard_Header;
			$saveScorecard->project_name = $allInputs["project_name"];
			$saveScorecard->no_assessment = $allInputs["no_of_assessment"];
			$saveScorecard->area_id = $allInputs["area"];
			$saveScorecard->district_id = $allInputs["district"];
			$saveScorecard->start_date = $allInputs["start_date"];
			$saveScorecard->end_date = $allInputs["end_date"];
			$saveScorecard->template_id = $allInputs["template"];
			$saveScorecard->created_by = $user;
			$saveScorecard->updated_by = $user;
			$saveScorecard->status = '0';
			$saveScorecard->save();


			return redirect('/scorecard/edit/'.$saveScorecard->id);

	}

	public function getEvaluators(Request $request){

		$scorecard_assessment_rounds = Scorecard_Assessment_Rounds::updateOrCreate([
			'scorecard_header_id'=>$request->scorecardid,
			'round_num'=>$request->roundnumber,
			'weight'=>$request->weight,
			'start_date'=>$request->start_date,
			'end_date'=>$request->end_date,
			'status'=>0,
			'no_of_evaluators'=>$request->no_of_evaluators
		]);

		$evaluatorss = DB::table("scorecard_evaluators")
		->where('scorecard_header_id', $request->scorecardid)
		->where('assessment_round_id', $scorecard_assessment_rounds->id)
		->select([
			'scorecard_header_id'
		])
		->get()
		->count();

		$evalCount = $request->no_of_evaluators - $evaluatorss;
		if($evalCount != 0){
			for($i=0;$i<$evalCount; $i++){
				$saveUsername = new Scorecard_Evaluator;
				$saveUsername->scorecard_header_id = $request->scorecardid;
				$saveUsername->assessment_round_id = $scorecard_assessment_rounds->id;
				$saveUsername->fname = '';
				$saveUsername->mname = '';
				$saveUsername->lname = '';
				$saveUsername->mobile_no = '';
				$saveUsername->email = '';
				$saveUsername->status = '0';
				$saveUsername->username = "RAFI-".rand();
				$saveUsername->password = Hash::make("RAFI-".$i);
				$saveUsername->expiry_date = '2018-03-15';
				$saveUsername->save();	
			}
		}

		$kraname = Template_KRA::all();
		$evaluators = Scorecard_Evaluator::where('scorecard_header_id', $request->scorecardid)
		->where('assessment_round_id', $scorecard_assessment_rounds->id)
		->get();

		$evals = "";

		foreach($evaluators as $eval){
			$evals .= '
			<tr>
				<td>'.$eval->username.'</td>
				<td>
					<div class="kname-wrapper">
			';
			foreach($kraname as $kname){
				$evals .= '
						<div>'.$kname->kra_name.'</div>
						<div class="input-wrapper"><input type="checkbox" id='.$eval->id.' value='.$kname->id.' class="kname-input"><span class="kme-weight"> - '.$kname->weight.'</span></div>
				';
			}
			$evals .= '</div></td></tr>';
		}

		return $evals;
	}

	public function publishScorecard(Request $request){
		$scorecard_header_id = $request->scorecard_header_id;
		$checker = '';

		$scorecard_header = DB::table('scorecard__headers')
		->where('id', $scorecard_header_id)
		->select([
			'no_assessment'
		])
		->first();

		$no_of_assessment = $scorecard_header->no_assessment;

		$scorecard_assessment_rounds = DB::table('scorecard_assessment_rounds')
		->where('scorecard_header_id', $scorecard_header_id)
		->get()
		->count();

		if($no_of_assessment == $scorecard_assessment_rounds){

			$updateSH = Scorecard_Header::where('id', $scorecard_header_id)
			->update([
				'status'=>'1'
			]);

			$updateAR = Scorecard_Assessment_Rounds::where('scorecard_header_id', $scorecard_header_id)
			->update([
				'status'=>'1'
			]);

			$updateSE = Scorecard_Evaluator::where('scorecard_header_id', $scorecard_header_id)
			->update([
				'status'=>'1'
			]);

			$checker = '1';
		}else{
			$checker = 'Error, Please setup all Assessment Rounds!';
		}

		return $checker;
		// return $scorecard_header->no_assessment;
	}

	public function level($id){
		$usertype = Auth::user()->user_type;
		// dd($usertype);
		if($usertype == 3){
			abort(401, 'Access denied');
		}

		$templates = Template_Header::where('id', $id)->first();

		return view('scorecard.level', compact('templates'));
	}

	public function tempType(){

		$usertype = Auth::user()->user_type;
    	$sctype = Auth::user()->scorecard_type;
    	if($usertype == 2){
    		$templateType = DB::table('template_headers as th')
			->select([
				'th.id',
				'tt.template_name as ttn',
				'th.template_name as thn',
				'th.template_type',
				'tt.id as ttid'
			])
			->leftJoin('template_types as tt', 'th.template_type', '=', 'tt.id')
			->where('th.template_type', $sctype)
			->get();
			return view('scorecard.template_type', compact('templateType'));
    	}else if($usertype == 3){
    		abort(401, 'Access denied'); 
    	}else{
    		$templateType = DB::table('template_headers as th')
			->select([
				'th.id',
				'tt.template_name as ttn',
				'th.template_name as thn',
				'th.template_type',
				'tt.id as ttid'
			])
			->leftJoin('template_types as tt', 'th.template_type', '=', 'tt.id')
			->get();
			return view('scorecard.template_type', compact('templateType'));
    	}
    	// dd($usertype);
	}

	public function getsc(){
		$sc = Scorecard_Header::all();
		return $sc;
	}

	public function liveSearch(Request $req){
		$keyword = $req->keyword;
		$chosenProj = $req->chosenProj;
		$sctype = $req->sctype;
		$view = $req->view;
		$searchResults = '';
		$pgs = '';

		if($sctype == 2){
			$scorecards = DB::table('scorecard__headers as sh')
			->select([
				'sh.id',
				'sh.project_name',
				'th.template_name',
				'sh.no_assessment',
				'sh.status',
				'sh.school_id',
				'th.template_type'
				])
			->leftJoin('template_headers as th', 'sh.template_id', '=', 'th.id')
			->leftJoin('users as u', 'u.id', '=', 'sh.created_by')
			->leftJoin('mst_municipalities as mm', 'sh.school_id', '=', 'mm.id')
			->where('sh.project_name', $chosenProj)
			->where('mm.municipality_name', 'LIKE', $keyword.'%')
			->where('th.template_type', $sctype)
			->orderByRaw('sh.id DESC')
			->paginate(10);
		}else{
			$scorecards = DB::table('scorecard__headers as sh')
			->select([
				'sh.id',
				'sh.project_name',
				'th.template_name',
				'sh.no_assessment',
				'sh.status',
				'sh.school_id',
				'th.template_type'
				])
			->leftJoin('template_headers as th', 'sh.template_id', '=', 'th.id')
			->leftJoin('users as u', 'u.id', '=', 'sh.created_by')
			->leftJoin('mst_schools as ms', 'sh.school_id', '=', 'ms.id')
			->where('sh.project_name', $chosenProj)
			->where('ms.school_name', 'LIKE', $keyword.'%')
			->where('th.template_type', $sctype)
			->orderByRaw('sh.id DESC')
			->paginate(10);
		}

		$count = count($scorecards);

		if($count == 0){
			$searchResults .= '
				<tr>
	          		<td colspan="4" style="text-align: center; color: red; font-weight: bold;">No scorecards to show!</td>
	          	</tr>
	              	';
	        $pgs .= '';
		}else{
			$mst_schools = mst_schools::all();
            $mst_municipality = mst_municipality::all();
			foreach ($scorecards as $sc) {
				if($view == 2){
					$dds = "view/";
				}else{
					$dds = url()->previous()."/view/";
				}

				if($sc->status == '1'){
              		$pathTo = '#';
              		$textValue = '<span style="color:#1688b7; font-weight: bold;">Published</span>';
	            }
	            else{
              		$pathTo = url()->previous()."/edit/".$scorecard->id;
              		$textValue = '<span style="color: green; font-weight: bold;">Draft</span>'; 
              	}


				$searchResults .= '
					<tr>
	                  <td><div class="clip" ><strong> '.$sc->project_name.' </strong> <br>';

	                 if($sc->template_type == 2){
	                  		foreach($mst_municipality as $mm){
	                  				if($mm->id == $sc->school_id){
	                  					$searchResults .= '<i style="font-size: 13px;">('.$mm->municipality_name.')</i>';
	                  				}
	                  		}
	                  	}
	                  	else{
	                  		foreach($mst_schools as $sch){
	                  			if($sch->id == $sc->school_id){
	                  				$searchResults .= '<i style="font-size: 13px;">('.$sch->school_name.')</i>';
	                  				
	                  			}
	                        }
	                  	}

	            $searchResults .= '</div></td>
	                  <td><div class="clip"> '.$sc->template_name.' </div></td>
	                  <td>'.$textValue.'</td>
	                  <td class="btn-area"><a  class="btn btn-create view" href="'.$dds.''.$sc->id.'"><span class="fa fa-eye"></span></a><a class="btn btn-proceed edit" data-toggle="modal" data-target="#editUser" href=" $pathTo "><span class="fa fa-edit"></span></a><button class="btn btn-cancel delete" type="button"><span class="fa fa-trash"></span> </button></td>
	                </tr>
				';
			}

			$pgs .= $scorecards->links();
		}
		return [$searchResults, $pgs];
	}

	public function rafiLevelScore(Request $request){
		$user = Auth::id();

		$scorecard_header_id = $request->scid;
		$assessment_round_id = $request->rn;
		$rate = $request->rate;
		$dateSubmit = date('Y-m-d H:i:s', strtotime('+8 hours'));

		$scorecard_ledger_header = new Scorecard_Ledger_Header;
		$scorecard_ledger_header->evaluator_id = 0;
		$scorecard_ledger_header->scorecard_header_id = $scorecard_header_id;
		$scorecard_ledger_header->assessment_round_id = $assessment_round_id;
		$scorecard_ledger_header->dateTime_submitted = $dateSubmit;
		$scorecard_ledger_header->total_score = $rate;
		$scorecard_ledger_header->save();

		return $rate;
	}

	public function filterEvaluator(Request $request){
		$nameSearch = $request->name;
		// dd($nameSearch);
		$html = '';
		// where('ms.school_name', 'LIKE', $keyword.'%')
		$ocp_evaluator = ocp_evaluator::where('fname', 'LIKE', $nameSearch.'%')
		->orWhere('lname', 'LIKE', $nameSearch.'%')
		->orWhere('mname', 'LIKE', $nameSearch.'%')
		->get();
		$count = count($ocp_evaluator);
		if($count > 0){
			foreach($ocp_evaluator as $oe){
				$html .= '
				<a href="#" class="resultLink"><li><span class="fname">'.$oe->fname.'</span> <span class="mname">'.$oe->mname.'</span> <span class="lname">'.$oe->lname.'</span><input type="hidden" class="eAdd" value="'.$oe->email_address.'"><input type="hidden" class="mnum" value="'.$oe->contact_number.'"></li><a>
				';
			}
		}else{
			$html .= '
				<span style="color: red">No results found for Evaluator Named <b>"'.$nameSearch.'!"</b></span>
				';
		}

		return $html;

	}
}

