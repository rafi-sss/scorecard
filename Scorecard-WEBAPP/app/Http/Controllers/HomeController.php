<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\modules;
use App\sub_module;
use App\Scorecard_Header;
use App\Scorecard_Evaluator;
use App\Scorecard_Ledger_Header;
use App\mst_district;
use App\mst_schools;
use App\mst_municipality;
use Illuminate\Support\Facades\Auth;
use App\Template_type;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function layout(){
        $module = DB::table('modules')->where('module_type', '=', 'Navigation')->get();
        
        return view('layouts.app', compact('module'));
    }
    public function index()
    {   
        $usertype = Auth::User()->user_type;
        $scorecardType = Auth::user()->scorecard_type;

        if($usertype == 3){

            $cond = 0;

            if($scorecardType == 2){
                $cond = 2;
                $logdist = Auth::user()->district_id;
                $getMid = mst_district::find($logdist);
                $mid = $getMid->municipality_id;
                $scorecard = DB::table('scorecard__headers as sh')
                ->leftJoin('template_headers as th', 'sh.template_id', '=', 'th.id')
                ->leftJoin('users as u', 'u.id', '=', 'sh.created_by')
                ->orderByRaw('sh.id DESC')
                ->where('sh.level', 1)
                ->where('th.template_type', $cond)
                ->where('sh.school_id', $mid)
                ->count();

                $evaluators = DB::table('scorecard_evaluators as se')
                ->leftJoin('scorecard__headers as sh', 'se.scorecard_header_id', '=', 'sh.id')
                ->leftJoin('template_headers as th', 'sh.template_id', '=', 'th.id')
                ->orderByRaw('sh.id DESC')
                ->where('sh.level', 1)
                ->where('th.template_type', $cond)
                ->where('sh.school_id', $mid)
                ->count();
            }else{
                $cond = 1;
                $districtid = Auth::user()->district_id;
                $scorecard = DB::table('scorecard__headers as sh')
                ->leftJoin('template_headers as th', 'sh.template_id', '=', 'th.id')
                ->leftJoin('users as u', 'u.id', '=', 'sh.created_by')
                ->leftJoin('mst_schools as ms', 'sh.school_id', '=', 'ms.id')
                ->orderByRaw('sh.id DESC')
                ->where('sh.level', 1)
                ->where('th.template_type', $cond)
                ->where('ms.district_id', $districtid)
                ->count();

                $evaluators = DB::table('scorecard_evaluators as se')
                ->leftJoin('scorecard__headers as sh', 'se.scorecard_header_id', '=', 'sh.id')
                ->leftJoin('mst_schools as ms', 'sh.school_id', '=', 'ms.id')
                ->leftJoin('template_headers as th', 'sh.template_id', '=', 'th.id')
                ->orderByRaw('sh.id DESC')
                ->where('sh.level', 1)
                ->where('th.template_type', $cond)
                ->where('ms.district_id', $districtid)
                ->count();
            }

        }else if($usertype == 2){
            $cond = 0;
            
            if($scorecardType == 2){
                $cond = 2;
            }else{
                $cond = 1;
            }

            $scorecard = DB::table('scorecard__headers as sh')
                ->leftJoin('template_headers as th', 'sh.template_id', '=', 'th.id')
                ->where('th.template_type', $cond)
                ->where('sh.level', 1)
                ->count();

            $evaluators = DB::table('scorecard_evaluators as se')
                ->leftJoin('scorecard__headers as sh', 'se.scorecard_header_id', '=', 'sh.id')
                ->leftJoin('template_headers as th', 'sh.template_id', '=', 'th.id')
                ->where('th.template_type', $cond)
                ->where('sh.level', 1)
                ->count();

        }else{
            $scorecard = Scorecard_Header::where('status', 1)->count();
            $evaluators = Scorecard_Evaluator::count();
            // $scorecard_data = DB::table('')->get();
            // $schools = mst_schools::paginate(10);
            // dd($schools);
        }

        return view('home', compact('scorecard', 'evaluators'));
    }

        public function getScorecardAnalytics(){
        // $municipality = mst_municipality::take(5)->get();
        $totalEval = 6;


        // dd($totalSC);

        $municipalities = mst_municipality::take(6)
        ->orderBy('municipality_name', 'DESC')
        ->get();

        $scores = array();
        $counter = 0;
        foreach ($municipalities as $mun) {

            $sccount = DB::table('scorecard__headers as sh')
            ->leftJoin('mst_schools as ms', 'sh.school_id', '=', 'ms.id')
            ->leftJoin('mst_municipalities as mm', 'ms.municipality_id', '=', 'mm.id')
            ->where('sh.level', 1)
            ->where('sh.scorecard_type', 1)
            ->where('mm.id', $mun->id)
            ->get();

            $totalSC = count($sccount) * $totalEval;

            $scorecard_ledger_header = DB::table('scorecard_ledger_headers as slh')
            ->leftJoin('scorecard__headers as sh', 'slh.scorecard_header_id', '=', 'sh.id')
            ->leftJoin('mst_schools as ms', 'sh.school_id', '=', 'ms.id')
            ->leftJoin('mst_municipalities as mm', 'ms.municipality_id', '=', 'mm.id')
            ->where('mm.id', $mun->id)
            ->get();

            $totalRated = count($scorecard_ledger_header);
            // dd($totalRated);
            if($totalRated > 0 && $totalRated != $totalSC){
                $minus = $totalSC - $totalRated;
                $mmd = $mun->municipality_name;
                $tr = $totalRated;
                // echo $mun->municipality_name.' = Progress: '.$totalRated.' = Not Started: '.$minus.'<br><br>';
                $scores[$counter]['mun_name'] = $mun->municipality_name;
                $scores[$counter]['totalRated'] = $totalRated;
                $scores[$counter]['left'] = $minus;
                $scores[$counter]['totalSC'] = $totalSC;
            }else if($totalRated <= 0){
                $minus = $totalSC - $totalRated;
                $mmd = $mun->municipality_name;
                $tr = $totalRated;
                // $minus = $totalSC - $totalRated;
                // echo $mun->municipality_name.' = Progress: '.$totalRated.' = Not Started: '.$minus.'<br><br>';
                $scores[$counter]['mun_name'] = $mun->municipality_name;
                $scores[$counter]['totalRated'] = $totalRated;
                $scores[$counter]['left'] = $minus;
                $scores[$counter]['totalSC'] = $totalSC;
            }

            $counter++;
        }

        return $scores;

        // return $object;
        // return $municipalities;
        // $scorecard_ledger_header = DB::table('scorecard_ledger_headers as slh')
        // ->leftJoin('scorecard__headers as sh', 'slh.scorecard_header_id', '=', 'sh.id')
        // ->leftJoin('mst_schools as ms', 'sh.school_id', '=', 'ms.id')
        // ->leftJoin('mst_municipalities as mm', 'ms.municipality_id', '=', 'mm.id')
        // ->where('mm.id', 44)
        // ->get();
        // dd(count($scorecard_ledger_header));

        // $totalRatedd = 0;
        // foreach ($sccount as $sh) {
        //     $scorecard_ledger_header = DB::table('scorecard_ledger_headers as slh')
        //     ->leftJoin('scorecard__headers as sh', 'slh.scorecard_header_id', '=', 'sh.id')
        //     ->leftJoin('mst_schools as ms', 'sh.school_id', '=', 'ms.id')
        //     ->leftJoin('mst_municipalities as mm', 'ms.municipality_id', '=', 'mm.id')
        //     ->where('mm.id', $sh->munid)
        //     ->get();
        //     $totalRated = count($scorecard_ledger_header);
        //     if($totalRated > 0 && $totalRated != $totalSC){
        //        $totalRatedd = $totalRated;
        //        $minus = $totalSC - $totalRated;
        //     }else if($totalRated == $totalSC){
        //        $totalSC = $totalSC;
        //     }
        // }

        // return [$scorecard__headers, $minus, $totalRatedd];
        // dd($totalRatedd);

        // return '<br>Total: '.$counts * 6;
        // $mst_schools = mst_schools::where('municipality_id', 44)->count();

        // $completedSC = $scorecard__headers * $totalEval;
        
        // $scorecard_ledger_header = Scorecard_Ledger_Header::where()->get();
        // $progressSC = DB::table('scorecard__headers as sh')
        // ->leftJoin('mst_schools as ms', 'sh.school_id', '=', 'ms.id')
        // ->leftJoin('mst_municipalities as mm', 'ms.municipality_id', '=', '');
        // $mst_municipality = DB::table('mst_municipalities as mm')
        // ->leftJoin('mst_schools as ms', 'mm.id', '=', 'ms.municipality_id')
        // ->count();
        // dd($completedSC);
        // return $municipality;
    }
    // public function getScorecardAnalytics(){
    //     // $municipality = mst_municipality::take(5)->get();
    //     $totalEval = 6;


    //     // dd($totalSC);

    //     $municipalities = mst_municipality::take(6)
    //     ->orderBy('municipality_name', 'DESC')
    //     ->get();

    //     $scores = array();
    //     $completed = array();
    //     $completedCounter = 0;
    //     $counter = 0;
    //     foreach ($municipalities as $mun) {

    //         $sccount = DB::table('scorecard__headers as sh')
    //         ->select('sh.id as sid')
    //         ->leftJoin('mst_schools as ms', 'sh.school_id', '=', 'ms.id')
    //         ->leftJoin('mst_municipalities as mm', 'ms.municipality_id', '=', 'mm.id')
    //         ->where('sh.level', 1)
    //         ->where('sh.scorecard_type', 1)
    //         ->where('mm.id', $mun->id)
    //         ->get();

    //         // $totalSC = count($sccount) * $totalEval;
    //         $totalSC = count($sccount);
    //         foreach ($sccount as $sc) {

    //            $scorecard_ledger_header = DB::table('scorecard_ledger_headers as slh')
    //             ->leftJoin('scorecard__headers as sh', 'slh.scorecard_header_id', '=', 'sh.id')
    //             ->leftJoin('mst_schools as ms', 'sh.school_id', '=', 'ms.id')
    //             ->leftJoin('mst_municipalities as mm', 'ms.municipality_id', '=', 'mm.id')
    //             ->where('slh.scorecard_header_id', $sc->sid)
    //             ->where('mm.id', $mun->id)
    //             ->count();

    //             // $scorecard_ledger_header = count($scorecard_ledger_header);
    //             // dd($scorecard_ledger_header);
    //             // echo $scorecard_ledger_header;

    //             $scorecard_evaluator = Scorecard_Evaluator::where('scorecard_header_id', $sc->sid)->count();

    //             if($scorecard_ledger_header == $scorecard_evaluator){
    //                 $sh = DB::table('scorecard_ledger_headers as slh')
    //                 ->select([
    //                     DB::raw('COUNT(slh.scorecard_header_id) as totalSlh')
    //                 ])
    //                 ->leftJoin('scorecard__headers as sh', 'slh.scorecard_header_id', '=', 'sh.id')
    //                 ->leftJoin('mst_schools as ms', 'sh.school_id', '=', 'ms.id')
    //                 ->leftJoin('mst_municipalities as mm', 'ms.municipality_id', '=', 'mm.id')
    //                 ->where('mm.id', $mun->id)
    //                 ->get();

    //                 dd($sh);
    //                 // $sh = Scorecard_Ledger_Header::distinct()
    //                 // ->where('scorecard_header_id', $sc->sid)
    //                 // ->get(['scorecard_header_id']);
    //                 $leftCount = $totalSC - count($sh);
    //                 $completed[$completedCounter]['CompletedScorecards'] = count($sh);
    //                 $completed[$completedCounter]['municipality'] = $mun->municipality_name;
    //                 // echo 'Full Scorecards '.count($sccount).' Scorecards plus id '.$sc->sid.' '.$scorecard_ledger_header.' '.$mun->municipality_name.' Completed '.count($sh).' Left'. $leftCount.'<br><br>';
    //             }else{
    //                 // if($scorecard_ledger_header > 0){
    //                 //     echo 
    //                 // }
    //                 // $cond = ($scorecard_ledger_header != 0 ? $scorecard_ledger_header : $totalSC);
    //                 // echo $cond;
    //                 // echo 'Full Scorecards '.count($sccount).' / Scorecards plus id '.$sc->sid.' '.$scorecard_ledger_header.' '.$mun->municipality_name.' / Total Evaluators: '.$scorecard_evaluator.'<br><br>';
    //             }

    //             $completedCounter++;
    //         }

    //         // echo $mun->municipality_name.' Completed: '. $completed;


    //         // $scorecard_ledger_header = DB::table('scorecard_ledger_headers as slh')
    //         // ->leftJoin('scorecard__headers as sh', 'slh.scorecard_header_id', '=', 'sh.id')
    //         // ->leftJoin('mst_schools as ms', 'sh.school_id', '=', 'ms.id')
    //         // ->leftJoin('mst_municipalities as mm', 'ms.municipality_id', '=', 'mm.id')
    //         // ->where('mm.id', $mun->id)
    //         // ->get();

    //         // $totalRated = count($scorecard_ledger_header);
            
    //         // echo $mun->municipality_name.' = Progress: '.$totalRated.'<br><br>';
    //         // if($totalRated > 0 && $totalRated != $totalSC){
    //         //     $minus = $totalSC - $totalRated;
    //         //     $mmd = $mun->municipality_name;
    //         //     $tr = $totalRated;
    //         //     // echo $mun->municipality_name.' = Progress: '.$totalRated.' = Not Started: '.$minus.'<br><br>';
    //         //     $scores[$counter]['mun_name'] = $mun->municipality_name;
    //         //     $scores[$counter]['totalRated'] = $totalRated;
    //         //     $scores[$counter]['left'] = $minus;
    //         //     $scores[$counter]['totalSC'] = $totalSC;
    //         // }else if($totalRated <= 0){
    //         //     $minus = $totalSC - $totalRated;
    //         //     $mmd = $mun->municipality_name;
    //         //     $tr = $totalRated;
    //         //     // $minus = $totalSC - $totalRated;
    //         //     // echo $mun->municipality_name.' = Progress: '.$totalRated.' = Not Started: '.$minus.'<br><br>';
    //         //     $scores[$counter]['mun_name'] = $mun->municipality_name;
    //         //     $scores[$counter]['totalRated'] = $totalRated;
    //         //     $scores[$counter]['left'] = $minus;
    //         //     $scores[$counter]['totalSC'] = $totalSC;
    //         // }

    //         $counter++;
    //     }
    //      dd($completed);
    //     // return $scores;

    //     // return $object;
    //     // return $municipalities;
    //     // $scorecard_ledger_header = DB::table('scorecard_ledger_headers as slh')
    //     // ->leftJoin('scorecard__headers as sh', 'slh.scorecard_header_id', '=', 'sh.id')
    //     // ->leftJoin('mst_schools as ms', 'sh.school_id', '=', 'ms.id')
    //     // ->leftJoin('mst_municipalities as mm', 'ms.municipality_id', '=', 'mm.id')
    //     // ->where('mm.id', 44)
    //     // ->get();
    //     // dd(count($scorecard_ledger_header));

    //     // $totalRatedd = 0;
    //     // foreach ($sccount as $sh) {
    //     //     $scorecard_ledger_header = DB::table('scorecard_ledger_headers as slh')
    //     //     ->leftJoin('scorecard__headers as sh', 'slh.scorecard_header_id', '=', 'sh.id')
    //     //     ->leftJoin('mst_schools as ms', 'sh.school_id', '=', 'ms.id')
    //     //     ->leftJoin('mst_municipalities as mm', 'ms.municipality_id', '=', 'mm.id')
    //     //     ->where('mm.id', $sh->munid)
    //     //     ->get();
    //     //     $totalRated = count($scorecard_ledger_header);
    //     //     if($totalRated > 0 && $totalRated != $totalSC){
    //     //        $totalRatedd = $totalRated;
    //     //        $minus = $totalSC - $totalRated;
    //     //     }else if($totalRated == $totalSC){
    //     //        $totalSC = $totalSC;
    //     //     }
    //     // }

    //     // return [$scorecard__headers, $minus, $totalRatedd];
    //     // dd($totalRatedd);

    //     // return '<br>Total: '.$counts * 6;
    //     // $mst_schools = mst_schools::where('municipality_id', 44)->count();

    //     // $completedSC = $scorecard__headers * $totalEval;
        
    //     // $scorecard_ledger_header = Scorecard_Ledger_Header::where()->get();
    //     // $progressSC = DB::table('scorecard__headers as sh')
    //     // ->leftJoin('mst_schools as ms', 'sh.school_id', '=', 'ms.id')
    //     // ->leftJoin('mst_municipalities as mm', 'ms.municipality_id', '=', '');
    //     // $mst_municipality = DB::table('mst_municipalities as mm')
    //     // ->leftJoin('mst_schools as ms', 'mm.id', '=', 'ms.municipality_id')
    //     // ->count();
    //     // dd($completedSC);
    //     // return $municipality;
    // }

    public function out(){
        session()->flush();

        return redirect('/login');
    }
    public function register(){
        return view('auth.register');
    }

    public function timeout(){
        return view('timeout.index');
    }
}
