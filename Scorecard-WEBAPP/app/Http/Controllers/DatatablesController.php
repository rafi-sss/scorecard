<?php

namespace App\Http\Controllers;

use App\User;

use Illuminate\Http\Request;
use DB;

class DatatablesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    //user management Datatables
    public function users(){
	 
	$users = DB::table('users as u')->select([
            'u.id as uid',
    		 DB::raw("CONCAT('fname','lname') AS name"),
    		'u.username',
    		'ut.type as utype',
    		])->leftJoin('user_type as ut', 'u.user_type', '=', 'ut.id')->get();

    return Datatables::of($users)
        ->setRowId(function ($c) {
            return $c->id;
        })
        ->addColumn('action', function ($c) {
            return '<a href="rawmaterial/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
            <a href="rawmaterial/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
            <a href="#" id="' . $c->id . '" class="btn btn-xs btn-danger dim delrawmat"><i class="fa fa-trash-o"  aria-hidden="true"></i> Delete</a>';
        })
        ->make(true);

    }
}
