<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use App\User;

use Illuminate\Foundation\Auth\AuthenticatesUsers;

class OtherController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';


    /**
     * Create a new controller instance.
     *
     * @return void
     */

    
    public function view(){
        return view('register');
    }

    public function create(){
        $user = new User();
        $user->name = Input::get('name');
        $user->email = Input::get('email');
        $user->password  =Input::get('password');
        $user->save();

        echo "User successfully created.";
    }
}
