<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Template_header;
use App\Template_KRA;
use App\Template_LOP;
use App\Template_Indicator;
use App\Template_type;
use App\scorecard_template_round_names;
use App\scorecard_template_setting;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Pagination\Paginator;
use Carbon; 
class TemplateController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
	
	public function index(){
		$templates = Template_header::leftjoin('template_types as types', 'types.id', '=', 'template_headers.template_type')->select('types.template_name as type_name', 'template_headers.template_name', 'template_headers.status', 'template_headers.id as id')
		->where('types.id', '<>', 7)
		->get();
		return view('template.index')->with('templates', $templates);
	}
	public function add(){
		$types = Template_type::all();
		$templates = Template_header::all();
		return view('template.add', compact('types', 'templates'));
	}
	public function create(Request $request){
		$this->validate($request, [
    		'template_name' => 'string|unique:template_headers',
    	]);
		$user = Auth::user()->id;
		$mytime = Carbon\Carbon::now();
		$code = $request->template_code;
		$name = $request->template_name;
		$type = $request->template_type;
		$evaluators = $request->number_evaluators;
		$rounds = $request->number_rounds;
		$components = $request->template_components;
		
		$template = New Template_header();
		if ($user == 1) {
			$default = 1;
		}else{
			$default = 0;
		}
		$template->template_name = $name;
		$template->template_code = $code; 
		$template->default = $default;
		$template->created_by = $user;
		$template->updated_by = $user;
		$template->created_at = $mytime;
		$template->updated_at = $mytime;
		$template->status = 'Published';
		$template->template_type = $type;
		$template->save();
		$param = $template->id;
		$scorecard = New scorecard_template_setting();
		$scorecard->template_header_id =  $param;
		$scorecard->num_of_rounds = $rounds;
		$scorecard->num_of_evaluators = $evaluators;
		$scorecard->save();
		if($components > 0){
			$kras = Template_KRA::where('template_header_id', $components)->get();
			foreach ($kras as $kra) {
				$kraName = $kra->kra_name;
				$headerId = $param;
				$weight = $kra->weight;
				$kraID = $kra->id;
				$kra = New Template_KRA();
				$kra->template_header_id = $headerId;
				$kra->kra_name = $kraName;
				$kra->weight = $weight;
				$kra->save();
				$kraid = $kra->id;
				$indis = Template_Indicator::where('kra_id', $kraID)->where('parent_id', 0)->get();
				foreach ($indis as $indi) {
					$indiName = $indi->detail_name;
					$indiWeight = $indi->weight;
					$parentIndi = $indi->parent_id;
					$mov = $indi->mov;
					$indiID = $indi->id;
					$hasChild = $indi->has_child;
					$mytime = Carbon\Carbon::now();
					$indi = New Template_Indicator();
					$indi->kra_id = $kraid;
					$indi->detail_name = $indiName;
					$indi->weight = $indiWeight;
					$indi->parent_id = $parentIndi;
					$indi->mov = $mov;
					$indi->created_at = $mytime;
					$indi->updated_at = $mytime;
					$indi->has_child = $hasChild;
					$indi->save();
					$inIndiID = $indi->id;
					if ($hasChild == 0) {
						$childLops = Template_LOP::where('indicator_id', $indiID)->get();
						foreach ($childLops as $childLop) {
							$childCode = $childLop->lop_code;
							$childLetter = $childLop->lop_letter;
							$childValue = $childLop->lop_value;
							$childDes = $childLop->lop_description;
							$mytime = Carbon\Carbon::now();
							$lop = New Template_LOP();
							$lop->indicator_id = $inIndiID;
							$lop->lop_code = $childCode;
							$lop->lop_letter = $childLetter;
							$lop->lop_value = $childValue;
							$lop->lop_description = $childDes;
							$lop->created_at = $mytime;
							$lop->updated_at = $mytime;
							$lop->save();
						}
					}else{
						$subIndis = Template_Indicator::where('kra_id', $kraID)->where('parent_id', $indiID)->get();
						foreach($subIndis as $subIndi){
							$indiName = $subIndi->detail_name;
							$indiWeight = $subIndi->weight;
							$mov = $subIndi->mov;
							$id = $subIndi->id;
							$mytime = Carbon\Carbon::now();
							$sub = New Template_Indicator();
							$sub->kra_id = $kraid;
							$sub->detail_name = $indiName;
							$sub->weight = $indiWeight;
							$sub->parent_id = $inIndiID;
							$sub->mov = $mov;
							$sub->created_at = $mytime;
							$sub->updated_at = $mytime;
							$sub->save();
							$indiForlop = $sub->id;
							$childLops = Template_LOP::where('indicator_id', $id)->get();
							foreach ($childLops as $childLop) {
								$childCode = $childLop->lop_code;
								$childLetter = $childLop->lop_letter;
								$childValue = $childLop->lop_value;
								$childDes = $childLop->lop_description;
								$mytime = Carbon\Carbon::now();
								$lop = New Template_LOP();
								$lop->indicator_id = $indiForlop;
								$lop->lop_code = $childCode;
								$lop->lop_letter = $childLetter;
								$lop->lop_value = $childValue;
								$lop->lop_description = $childDes;
								$lop->created_at = $mytime;
								$lop->updated_at = $mytime;
								$lop->save();
							}
						}
					}
					

				}
			}
		}
		return Redirect::route('template_edit', array('id' => $param));
	}
	
	public function edit($id){
		$template = Template_header::leftjoin('template_types', 'template_types.id', '=', 'template_headers.template_type')->leftjoin('scorecard_template_settings as settings', 'settings.template_header_id', '=', 'template_headers.id')->select('template_headers.template_name', 'template_headers.template_code', 'template_types.template_name as template_type_name', 'template_headers.id', 'template_headers.created_at', 'settings.num_of_evaluators', 'settings.num_of_rounds')->where('template_headers.id', $id)->first();
		// $kra = Template_KRA::all()->where('template_header_id', $id)->paginate(1);

		
		$lop = DB::table('template_lops as lop')->leftjoin('template_indicators as indi', 'lop.indicator_id', '=', 'indi.id')->leftjoin('template_kra as kra', 'indi.kra_id', '=', 'kra.id')->select('lop.lop_code', 'lop.lop_description', 'lop.lop_letter', 'lop.lop_value', 'indi.detail_name', 'kra.kra_name', 'lop.id')->where('kra.template_header_id', $id)->paginate(5, ['*'], 'lop_page');
		
		$kra =  DB::table('template_kra')->where('template_header_id', $id)->paginate(5, ['*'], 'kra_page');
		
		
		$indi = Template_Indicator::leftjoin('template_kra as kra', 'kra.id', '=', 'template_indicators.kra_id')->select('kra.kra_name', 'template_indicators.detail_name', 'template_indicators.weight', 'template_indicators.parent_id', 'template_indicators.id')->where('kra.template_header_id', $id)->paginate(5, ['*'], 'indi_page');
		$parent = Template_Indicator::leftjoin('template_kra as kra', 'kra.id', '=', 'template_indicators.kra_id')->select('kra.kra_name', 'template_indicators.detail_name', 'template_indicators.weight', 'template_indicators.parent_id', 'template_indicators.id')->where('kra.template_header_id', $id)->get();
		// dd(DB::table('template_indicators')->select('detail_name')->get());
		return view('template.edit', compact('template', 'kra', 'indi', 'lop', 'parent'));
	}

	public function roundnames(){

		$rounds = scorecard_template_round_names::paginate(10);
		return view('maintenance.roundnames', compact('rounds'));
		
	}

	public function addround(Request $request){
		$roundname = $request->roundname;
		$roundweight = $request->weight;
		$roundlevel = $request->level;

		$round = new scorecard_template_round_names();
		$round->round_name = $roundname;
		$round->weight = $roundweight;
		$round->round_type = $roundlevel;
		$round->save();

		return redirect('roundnames');
	}

	public function createKra(Request $request){
		$kraName = $request->kra_name;
		$headerId = $request->header_id;
		$weight = $request->weight;
		$kra = New Template_KRA();
		$kra->template_header_id = $headerId;
		$kra->kra_name = $kraName;
		$kra->weight = $weight;
		$kra->save();
		return 'New KRA has been added';
	}
	public function createLop(Request $request){
		$indicatorId = $request->lop_indi;
		$lopCode =  $request->lop_code;
		$lopLet = $request->lop_let;
		$lopVal = $request->lop_val;
		$lopDes = $request->lop_des;
		$mytime = Carbon\Carbon::now();
		$lop = New Template_LOP();
		$lop->indicator_id = $indicatorId;
		$lop->lop_code = $lopCode;
		$lop->lop_letter = $lopLet;
		$lop->lop_value = $lopVal;
		$lop->lop_description = $lopDes;
		$lop->created_at = $mytime;
		$lop->updated_at = $mytime;
		$lop->save();
		return "New Level of Performance Added";
	}
	public function fetchKra(Request $request){
		$headerId = $request->header_id;
		$fetch = DB::table('template_kra')->select('id', 'kra_name')->where('template_header_id', $headerId)->get();
		return $fetch;
	}
	public function fetchParentIndi(Request $request){
		$kra = $request->kra;
		$fetch = DB::table('template_indicators')->select('id', 'detail_name')->where('parent_id', 0)->where('kra_id', $kra)->get();
		return $fetch;
	}
	public function fetchLop(Request $request){
		$headerId = $request->header_id;
		$fetch = DB::table('template_lops')->select('id', 'lop_code')->where('template_header_id', $headerId)->get();
		return $fetch;
	}
	public function fetchIndi(Request $request){
		$headerID = $request->header_id;
		$fetch = DB::table('template_indicators as indi')->select('indi.id', 'indi.detail_name')->leftjoin('template_kra as kra', 'indi.kra_id', '=', 'kra.id')->where('kra.template_header_id', $headerID)->get();
		//->where('parent_id', '>', 0)
		return $fetch;
	}
	public function createIndi(Request $request){
		$indiName = $request->indi_name;
		$indiKraName = $request->indi_kra_name;
		$indiWeight = $request->indi_weight;
		$parentIndi = $request->parent_indi;
		$mov = $request->mov;
		$mytime = Carbon\Carbon::now();
		$indi = New Template_Indicator();
		$indi->kra_id = $indiKraName;
		$indi->detail_name = $indiName;
		$indi->weight = $indiWeight;
		$indi->parent_id = $parentIndi;
		$indi->mov = $mov;
		$indi->created_at = $mytime;
		$indi->updated_at = $mytime;
		$indi->save();
		$hasChild = 0;
		if ($parentIndi >= 1 ) {
			$hasChild = 1;
		}
		$parentIndis = Template_Indicator::find($parentIndi);
		$parentIndis->has_child = $hasChild;
		$parentIndis->save();
	

		return 'New Indicator successfully added';
	}
	public function componentKra(Request $request){
		$id = $request->id;
		$query = Template_KRA::find($id);
		return $query;
	}
	public function editKra(Request $request){
		$id = $request->id;
		$name = $request->kra_name;
		$weight = $request->weight;
		$query = Template_KRA::find($id);
		$query->kra_name = $name;
		$query->weight = $weight;
		$query->save();
		return 'success';
	}
	public function editIndi(Request $request){
		$id = $request->id;
		$indiName = $request->indi_name;
		$indiKraName = $request->indi_kra_name;
		$indiWeight = $request->indi_weight;
		$parentIndi = $request->parent_indi;
		$mov = $request->mov;
		$mytime = Carbon\Carbon::now();
		$indi = Template_Indicator::find($id);
		$indi->kra_id = $indiKraName;
		$indi->detail_name = $indiName;
		$indi->weight = $indiWeight;
		$indi->parent_id = $parentIndi;
		$indi->mov = $mov;
		$indi->updated_at = $mytime;
		$indi->save();
		$hasChild = 0;
		if ($parentIndi >= 1 ) {
			$hasChild = 1;
			$parentIndis = Template_Indicator::find($parentIndi);
			$parentIndis->has_child = $hasChild;
			$parentIndis->save();
		}
		

		return 'success';
	}
	public function componentIndi(Request $request){
		$id = $request->id;
		$headerId = $request->header_id;
		$query = DB::table('template_indicators as ti')
		->where('ti.id', $id)
		->select([
			'ti.id as tid',
			'ti.kra_id',
			'ti.detail_name',
			'ti.weight',
			'ti.parent_id',
			'ti.mov',
			'tk.kra_name'
		])
		->leftJoin('template_kra as tk', 'ti.kra_id', '=', 'tk.id')
		->get();
		$fetch = DB::table('template_kra')->select('id', 'kra_name')->where('template_header_id', $headerId)->get();
		$ti = DB::table('template_indicators as indi')->leftjoin('template_kra as kra', 'indi.kra_id', '=', 'kra.id')->select('indi.id as id', 'indi.detail_name as detail_name')->where('kra.template_header_id', $headerId)->get();
		return [$query, $fetch, $ti];
	}
	public function componentLop(Request $request){
		$id = $request->id;
		$query = DB::table('template_lops as lop')->leftjoin('template_indicators as indi', 'lop.indicator_id', '=', 'indi.id')->select('lop.lop_code', 'lop.lop_letter', 'lop.lop_value', 'lop.lop_description', 'lop.id as lopid', 'indi.detail_name', 'indi.id as indiid')->where('lop.id', $id)->get();
		return $query;
	}
	public function editLop(Request $request){
		$id =  $request->id;
		$indicatorId = $request->lop_indi;
		$lopCode =  $request->lop_code;
		$lopLet = $request->lop_let;
		$lopVal = $request->lop_val;
		$lopDes = $request->lop_des;
		$mytime = Carbon\Carbon::now();
		$lop = Template_LOP::find($id);
		$lop->indicator_id = $indicatorId;
		$lop->lop_code = $lopCode;
		$lop->lop_letter = $lopLet;
		$lop->lop_value = $lopVal;
		$lop->lop_description = $lopDes;
		$lop->updated_at = $mytime;
		$lop->save();
	}

	public function templateTypes(){
		$templateType = Template_type::paginate(10);
		return view('maintenance.templateType', compact('templateType'));
	}

	public function addTemp(Request $request){
		$templateTypes = new Template_type;
		$templateTypes->template_name = $request->ctemplatename;
		$templateTypes->save();
		
		return redirect('templateTypes');
	}

	public function updateTemp(Request $request){
	   $tempid = $request->tempid;
	   $newName = $request->templatename;

	   $templateTypes = Template_type::find($tempid);
	   $templateTypes->template_name = $newName;
	   $templateTypes->save();

	   return redirect('templateTypes');
	}
}