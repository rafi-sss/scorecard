<?php

namespace App\Http\Controllers;

use App\mst_schools;
use App\Scorecard_Header;
use App\Scorecard_Ledger_Header;
use App\Scorecard_Ledger_Detail;
use App\scorecard_template_setting;
use App\Template_Indicator;
use App\Template_KRA;
use App\recommend_school;
use App\Scorecard_Evaluator;
use App\Scorecard_Assessment_Rounds;
use App\mst_district;
use App\scorecard_template_round_names;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use DB;
use PDF;

class ReportsController extends Controller
{
    //
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    	$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function preindex(){
    	// dd('dfdf');
    	$usertype = Auth::user()->user_type;
    	$sctype = Auth::user()->scorecard_type;
    	//Condition for SDS / LGU users
    	if($usertype == 3){
    		//Condition for OCP Scorecard
    		if($sctype == 2){
    			$templates = DB::table('scorecard__headers as sh')
						->select('sh.project_name', 'sh.template_id')
						->leftJoin('template_headers as th', 'sh.template_id', '=', 'th.id')
						->where('sh.status', 1)
						->where('th.template_type', $sctype)
						->groupBy('sh.project_name', 'sh.template_id')
						->distinct()
						->get();
    		}
    		//Condition for SEED Scorecard
    		else{
    			$templates = DB::table('scorecard__headers as sh')
						->select('sh.project_name', 'sh.template_id')
						->leftJoin('template_headers as th', 'sh.template_id', '=', 'th.id')
						->where('sh.status', 1)
						->where('sh.level', 1)
						->where('th.template_type', $sctype)
						->groupBy('sh.project_name', 'sh.template_id')
						->distinct()
						->get();
    		}

			return view('reports.preindex', compact('templates', 'usertype'));
    	}
    	//Condition for Scorecard Admin users
    	else if($usertype == 2){
    		//Condition for OCP Scorecard
    		if($sctype == 2){
    			$templates = DB::table('scorecard__headers as sh')
						->select('sh.project_name', 'sh.template_id')
						->leftJoin('template_headers as th', 'sh.template_id', '=', 'th.id')
						->where('sh.status', 1)
						->where('th.template_type', $sctype)
						->groupBy('sh.project_name', 'sh.template_id')
						->distinct()
						->get();

	    		$templates2 = DB::table('scorecard__headers as sh')
							->select('sh.project_name', 'sh.template_id')
							->leftJoin('template_headers as th', 'sh.template_id', '=', 'th.id')
							->where('sh.status', 1)
							->where('th.template_type', $sctype)
							->groupBy('sh.project_name', 'sh.template_id')
							->distinct()
							->get();

				$districts = mst_district::all();

				$projects = DB::table('scorecard__headers as sh')
				->leftJoin('template_headers as th', 'sh.template_id', '=', 'th.id')
				->where('th.template_type', $sctype)
				->distinct()
				->get(['sh.project_name', 'sh.template_id']);

				return view('reports.preindex', compact('templates', 'templates2', 'usertype', 'districts', 'projects', 'sctype'));
    		}
    		//Condition for SEED Scorecard
    		else{
    			$sctype = 0;
    			$templates = DB::table('scorecard__headers')
						->select('project_name', 'template_id')
						->where('status', 1)
						->where('level', 2)
						->groupBy('project_name', 'template_id')
						->distinct()
						->get();

	    		$templates2 = DB::table('scorecard__headers')
							->select('project_name', 'template_id')
							->where('status', 1)
							->groupBy('project_name', 'template_id')
							->distinct()
							->get();

				$districts = mst_district::all();

				$projects = Scorecard_Header::distinct()
				->where('level', 1)
				->get(['project_name', 'template_id']);

				return view('reports.preindex', compact('templates', 'templates2', 'usertype', 'districts', 'projects', 'sctype'));
    		}
    	}
    	//Condition for Superadmin users
    	else{
    		$sctype = 0;
    		$templates = DB::table('scorecard__headers')
						->select('project_name', 'template_id')
						->where('status', 1)
						->where('level', 2)
						->groupBy('project_name', 'template_id')
						->distinct()
						->get();

    		$templates2 = DB::table('scorecard__headers')
						->select('project_name', 'template_id')
						->where('status', 1)
						->groupBy('project_name', 'template_id')
						->distinct()
						->get();

			$districts = mst_district::all();

			$projects = Scorecard_Header::distinct()
			->where('level', 1)
			->get(['project_name', 'template_id']);

				return view('reports.preindex', compact('templates', 'templates2', 'usertype', 'districts', 'projects', 'sctype'));
    	}

		
    }
    
	public function index(Request $request){	
		$re = $request->project;
		// dd($re);
		$usertype = Auth::user()->user_type;
		$districtID = Auth::user()->district_id;
		$schools = mst_schools::where('district_id', $districtID)->get();
		$getTemp = Scorecard_Header::where('project_name', $re)->first();
		// dd($districtID);
		if($getTemp){
			$templateID = $getTemp->template_id;
		}

		$eva = scorecard_template_setting::where('template_header_id', $templateID)->first();
		$totalEva = $eva->num_of_rounds * $eva->num_of_evaluators;
				
		$scorecards = DB::table('scorecard__headers as sc')
						->select('s.id', 
							's.school_code', 
							's.school_name', 
							'sc.id as scid', 
							'sc.template_id', 
							'sc.project_name',
							DB::raw('AVG(lh.total_score) as score'),
							DB::raw('COUNT(lh.scorecard_header_id) as count'))
						->leftJoin('scorecard_ledger_headers as lh', 'sc.id', '=', 'lh.scorecard_header_id')
						->leftJoin('mst_schools as s', 'sc.school_id', '=', 's.id')
						->where('s.district_id', $districtID)
						->where('sc.project_name', $re)
						->groupBy('s.id', 
							's.school_code', 
							's.school_name', 
							'sc.id', 
							'sc.template_id', 
							'sc.project_name')
						->orderBy('score', 'desc')
						->paginate(10);



			// dd($scorecards);


		$recommends = recommend_school::where('district_id', $districtID)
										->where('project_name', $re)
										->get();

		$allRecommends = DB::table('recommended_schools as rs')
							->select('rs.scorecard_id',
									 'rs.school_id',
									 'rs.district_id',
									 'rs.template_id',
									 'rs.updated_at',
									 'd.id',
									 'rs.project_name',
									 'u.lname',
									 'u.fname',
									 's.school_name',
									 's.school_code',
									 'd.district_name',
									 't.template_name',
									 DB::raw('AVG(lh.total_score) as score'))
							->leftJoin('mst_schools as s', 'rs.school_id', '=', 's.id')
							->leftJoin('scorecard_ledger_headers as lh', 'rs.scorecard_id', '=', 'lh.scorecard_header_id')
							->leftJoin('users as u', 'rs.recommend_by', '=', 'u.id')
							->leftJoin('mst_districts as d', 'rs.district_id', '=', 'd.id')
							->leftJoin('template_headers as t', 'rs.template_id', '=', 't.id')
							->where('project_name', $re)
							->groupBy('rs.scorecard_id',
									 'rs.school_id',
									 'rs.district_id',
									 'rs.template_id',
									 'rs.updated_at',
									 'd.id',
									 'rs.project_name',
									 'u.lname',
									 'u.fname',
									 's.school_name',
									 's.school_code',
									 'd.district_name',
									 't.template_name')
							->orderBy('d.id')
							->paginate(10);



		return view('reports.index', compact('usertype', 're', 'scorecards', 'districtID', 'recommends', 'allRecommends', 'totalEva', 'getTemp'));

	}

	public function indext(Request $request){
		$re = $request->project;
		// $re = 'SEED 2018';
		// dd($re);
		
		$districtID = Auth::user()->district_id;
		$usertype = Auth::user()->user_type;
		$schools = mst_schools::where('district_id', $districtID)->get();
		$getTemp = Scorecard_Header::where('project_name', $re)->first();
		if($getTemp){
			$templateID = $getTemp->template_id;
		}
		// dd($re);
		// dd(Auth::user()->scorecard_type);	
		if($usertype == 3){

			if(Auth::user()->scorecard_type == 2){
                $getMid = mst_district::find($districtID);
                $mid = $getMid->municipality_id;
				$scorecards = DB::table('scorecard__headers as sc')
						->select('s.id', 
							's.municipality_code as school_code', 
							's.municipality_name as school_name', 
							'sc.id as scid', 
							'sc.template_id', 
							'sc.project_name',
							DB::raw('AVG(lh.total_score) as score'),
							DB::raw('COUNT(lh.scorecard_header_id) as count'))
						->leftJoin('scorecard_ledger_headers as lh', 'sc.id', '=', 'lh.scorecard_header_id')
						->leftJoin('mst_municipalities as s', 'sc.school_id', '=', 's.id')
						->where('sc.school_id', $mid)
						->where('sc.project_name', $re)
						->groupBy('s.id', 
							's.municipality_code', 
							's.municipality_name', 
							'sc.id', 
							'sc.template_id', 
							'sc.project_name')
						->orderBy('score', 'desc')
						->orderBy('sc.id', 'desc')
						->paginate(10);

				$recommends = recommend_school::where('school_id', $mid)
							->where('project_name', $re)
							->get();
			}else{
				// dd('dfdf');
				$scorecards = DB::table('scorecard__headers as sc')
						->select('s.id', 
							's.school_code', 
							's.school_name', 
							'sc.id as scid', 
							'sc.template_id', 
							'sc.project_name',
							DB::raw('AVG(lh.total_score) as score'),
							DB::raw('COUNT(lh.scorecard_header_id) as count'))
						->leftJoin('scorecard_ledger_headers as lh', 'sc.id', '=', 'lh.scorecard_header_id')
						->leftJoin('mst_schools as s', 'sc.school_id', '=', 's.id')
						->where('district_id', $districtID)
						->where('sc.project_name', $re)
						->groupBy('s.id', 
							's.school_code', 
							's.school_name', 
							'sc.id', 
							'sc.template_id', 
							'sc.project_name')
						->orderBy('score', 'desc')
						->orderBy('sc.id', 'desc')
						->get();

				// dd($scorecards);
				$recommends = recommend_school::where('district_id', $districtID)
								->where('project_name', $re)
								->get();
			}

		}elseif($usertype < 3){
			// dd(Auth::user()->scorecard_type);
			if(Auth::user()->scorecard_type == 2){ 

                // dd($districtID);
				$scorecards = DB::table('scorecard__headers as sc')
						->select('s.id', 
							's.municipality_code as school_code', 
							's.municipality_name as school_name', 
							'sc.id as scid', 
							'sc.template_id', 
							'sc.project_name',
							DB::raw('AVG(lh.total_score) as score'),
							DB::raw('COUNT(CASE WHEN lh.evaluator_id <> 0 THEN lh.scorecard_header_id END) as count'))
						->leftJoin('scorecard_ledger_headers as lh', 'sc.id', '=', 'lh.scorecard_header_id')
						->leftJoin('mst_municipalities as s', 'sc.school_id', '=', 's.id')
						->where('sc.project_name', $re)
						->where('sc.scorecard_type', 2)
						->groupBy('s.id', 
							's.municipality_code', 
							's.municipality_name', 
							'sc.id', 
							'sc.template_id', 
							'sc.project_name')
						->orderBy('score', 'desc')
						->paginate(10);

				$recommends = recommend_school::where('project_name', $re)
								->get();
			}else{
				$scorecards = DB::table('scorecard__headers as sc')
						->select('s.id', 
							's.school_code', 
							's.school_name', 
							'sc.id as scid', 
							'sc.template_id', 
							'sc.project_name',
							DB::raw('AVG(lh.total_score) as score'),
							DB::raw('COUNT(CASE WHEN lh.evaluator_id <> 0 THEN lh.scorecard_header_id END) as count'))
						->leftJoin('scorecard_ledger_headers as lh', 'sc.id', '=', 'lh.scorecard_header_id')
						->leftJoin('mst_schools as s', 'sc.school_id', '=', 's.id')
						->where('level', 2)
						->where('sc.project_name', $re)
						->groupBy('s.id', 
							's.school_code', 
							's.school_name', 
							'sc.id', 
							'sc.template_id', 
							'sc.project_name')
						->orderBy('score', 'desc')
						->paginate(10);

				$recommends = recommend_school::where('project_name', $re)
								->get();
			}
		}

		$eva = scorecard_template_setting::where('template_header_id', $templateID)->first();
		if($getTemp->level == 1){
			$totalEva = $eva->num_of_rounds * $eva->num_of_evaluators;
		}else{
			$totalEva = $eva->num_of_rounds;
		}

		return view('reports.indext', compact('re', 'scorecards', 'districtID', 'recommends', 'totalEva', 'usertype', 'getTemp'));

	}

	public function top5(Request $request){
		$re = $request->project;
		$usertype = Auth::user()->user_type;

		if($usertype > 2)
		abort(401, 'Access denied'); 

		$scorecards = DB::table('scorecard__headers as sc')
						->select('s.id', 
							's.school_code', 
							's.school_name', 
							'sc.id as scid', 
							'sc.template_id', 
							'sc.project_name',
							DB::raw('AVG(lh.total_score) as score'),
							DB::raw('COUNT(CASE WHEN lh.evaluator_id <> 0 THEN lh.scorecard_header_id END) as count'))
						->leftJoin('scorecard_ledger_headers as lh', 'sc.id', '=', 'lh.scorecard_header_id')
						->leftJoin('mst_schools as s', 'sc.school_id', '=', 's.id')
						->leftJoin('recommended_schools as rs', 'sc.id', '=', 'rs.scorecard_id')
						->where('level', 2)
						->where('rs.tops', 5)
						->where('sc.project_name', $re)
						->groupBy('s.id', 
							's.school_code', 
							's.school_name', 
							'sc.id', 
							'sc.template_id', 
							'sc.project_name')
						->orderBy('score', 'desc')
						->get();

		$recommends = recommend_school::where('project_name', $re)
							->where('tops', 5)
							->get();

		return view('reports.tpf', compact('re', 'scorecards', 'recommends'));


	}

	public function recommend(Request $request){
		$userID = Auth::user()->id;
		$scid = $request->sc_id;
		$schoolID = $request->school_id;
		$project_name = $request->project_name;
		$templateID = $request->template_id;

		$school = mst_schools::find($schoolID);
		$districtID = $school->district_id;

		$recommend = new recommend_school;
		$recommend->scorecard_id = $scid;
		$recommend->school_id = $schoolID;
		$recommend->district_id = $districtID;
		$recommend->template_id = $templateID;
		$recommend->project_name = $project_name;
		$recommend->recommend_by = $userID;
		$recommend->save();

		return redirect('reports');
	}

	public function recommend5(Request $request){
		$userID = Auth::user()->id;
		$scid = $request->sc_id;
		$schoolID = $request->school_id;
		$project_name = $request->project_name;
		$templateID = $request->template_id;

		$school = mst_schools::find($schoolID);
		$districtID = $school->district_id;

		$recommend = new recommend_school;
		$recommend->scorecard_id = $scid;
		$recommend->school_id = $schoolID;
		$recommend->district_id = $districtID;
		$recommend->template_id = $templateID;
		$recommend->project_name = $project_name;
		$recommend->tops = 5;
		$recommend->recommend_by = $userID;
		$recommend->save();

		return $scid;
		// return redirect('reports');
	}

	public function scEvaluators($scid){
		$scorecards = Scorecard_Header::where('id', $scid)->first();
		$templateID = $scorecards->template_id;
		
		$weights = DB::table('scorecard_assessment_rounds as sar')
		->select([
			'sar.round_name as sarrn',
			'sar.round_num',
			'sar.weight',
			'strn.round_name'
		])
		->leftJoin('scorecard_template_round_names as strn', 'sar.round_name', '=', 'strn.id')
		->where('sar.scorecard_header_id', $scid)
		->get();

		// $weights = Scorecard_Assessment_Rounds::where('scorecard_header_id', $scid)
		// ->get();

		$evaluators = DB::table('scorecard_evaluators as e')
						->select('e.username',
								'e.fname',
								'e.lname',
								'e.mname',
								'e.mobile_no',
								'e.email',
								'ar.round_num',
								'lh.created_at',
								'lh.total_score as score')
						->leftJoin('scorecard_assessment_rounds as ar', 'e.assessment_round_id', 'ar.id')
						->leftJoin('scorecard_ledger_headers as lh', 'e.id', '=', 'lh.evaluator_id')
						->where('e.scorecard_header_id', $scid)
						->get();
		

		$result = '';
		foreach($weights as $weight){
			$result .='<div class="clearfix">
						<div>
							<h4>
							<span class="label label-primary">'.$weight->round_name.'</span>
							<span class="label label-danger">'.round($weight->weight, 0).'%</span>
							</h4>';

			$ff = $weight->sarrn;

			if($ff <= 2){
				$result .= '<table class="table table-stripped">
				<thead>
                    <th>Evaluator ID</th>
                    <th>Name</th>
                    <th>Score</th>
                    <th>Date Submitted</th>
                </thead><tbody>';

                foreach($evaluators as $evaluator){
					if($evaluator->round_num == $weight->round_num){
						if($evaluator->lname){
							$name = $evaluator->lname.', '.$evaluator->fname;
						}else{
							$name = "No information given.";
						}
						if($evaluator->created_at){
							$date = date('Y-m-d H:i:s', strtotime($evaluator->created_at));
						}else{
							$date = "No score submitted.";
						}
						if($evaluator->score){
							$score = $evaluator->score.'%';
						}else{
							$score = '---';
						}
						$result .='<tr>
										<td>'.$evaluator->username.'</td>
										<td>'.$name.'</td>
										<td>'.$score.'</td>
										<td>'.$date.'</td>
									</tr>';
					}
				}
			}else{

				$rn = $weight->round_num;
				$slh = Scorecard_Ledger_Header::where('assessment_round_id', $rn)
				->where('scorecard_header_id', $scid)
				->get();
				$countSlh = count($slh);

				$result .= '<table class="table table-stripped">
				<thead>
                    <th>Evaluator</th>
                    <th>Score</th>
                </thead><tbody>
                <tbody>';
                if($countSlh != 0){
                	foreach($slh as $sl){
            		$result .= '<tr>
	                	<td>RAFI Staff</td>
	                	<td>'.$sl->total_score.'</td>
	               	 </tr>
	                ';	
                	}
                }else{
                	$result .= '<tr>
	                	<td>RAFI Staff</td>
	                	<td>No score yet</td>
	               	 </tr>
	                ';
                }
                
                
			}		

			$result .='</tbody></table></div></div>';			
		}


		return $result;

	}

	public function getSchoolByDist($id){
		$distBySchool = mst_schools::where('district_id', $id)->get();
		return $distBySchool;
	}

	public function pdfArea(){
		$templateKRA = DB::table('template_indicators as i')
		    ->leftJoin('template_kra as k', 'i.kra_id', '=', 'k.id')
            ->where('template_header_id', 3)
            ->get();
            dd($templateKRA);
		return view('reports.pdfArea', compact('templateKRA'));
	}

	public function pdfView(){
		$templateKRA = DB::table('template_kra')
            ->where('template_header_id', 3)
            ->get();
        view()->share('templateKRA',$templateKRA);
		$pdf = PDF::loadView('reports.pdfview');
        return $pdf->download('reports.pdf');
	}

}
