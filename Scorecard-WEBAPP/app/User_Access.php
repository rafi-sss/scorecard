<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_Access extends Model
{
    //
    protected $table = 'user_accesses';
    protected $fillable = [
        'user_type_id', 'category', 'category_id', 'grant'
    ];
}
