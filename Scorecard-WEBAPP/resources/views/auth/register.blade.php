<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="icon" type="image/png" href="{{ asset('img/icon.png') }}">
</head>
<body class="h-100">
    <div class="page page-register h-100">
        <div class="container text-center h-100">
            <div class="v-align">
                <div class="d-table">
                    <div class="table-cell">
                        <div class="row">
                            
                            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-6 col-sm-offset-2 col-md-offset-2 col-lg-offset-3 float-none">
                                <!-- <img src="img/logo.png"> -->
                                
                                <div class="rel">
                                    <div class="bg-form"></div>
                                    <div class="form-user-cont form-register-cont">
                                        <h3 class="text-left">Register New Account</h4>
                                        <form class="form-user" method="POST" action="{{ route('register') }}">
                                            {{ csrf_field() }}
                                            <div class="row input-duo">
                                                <div class="col-xs-12 col-md-4">
                                                    <input id="fname" type="text" class="form-control" name="fname" placeholder="First Name" value="{{ old('fname') }}" required autofocus>

                                                    @if ($errors->has('fname'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('fname') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                                 <div class="col-xs-12 col-md-4">
                                                    <input type="text" name="mname" class="form-control" placeholder="Middle Name">
                                                </div>
                                                <div class="col-xs-12 col-md-4">
                                                    <input id="lname" type="text" class="form-control" name="lname" placeholder="Last Name" value="{{ old('lname') }}" required autofocus>

                                                    @if ($errors->has('lname'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('lname') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                                <div class="col-xs-12 col-md-6">
                                                    <select class="form-control" required name="user_type" id="user_type">
                                                        <option selected disabled>User Type</option>
                                                        <option value="2">Scorecard Admin</option>
                                                        <option value="3">School District Supervisor</option>
                                                    </select>
                                                </div>
                                                <div class="col-xs-12 col-md-6">
                                                    <select class="form-control d-none user-component" required name="template_type" id="template_type">
                                                        <option selected disabled>Template Type</option>
                                                        @foreach($templates as $template)
                                                          <option value="{{$template->id}}">{{$template->template_name}}</option>
                                                        @endforeach
                                                    </select>
                                                     <select class="form-control d-none user-component" required name="district" id="district">
                                                        <option selected disabled>District</option>
                                                        @foreach($districts as $district)
                                                          <option value="{{$district->id}}">{{$district->district_name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            
                                            <input id="username" type="text" class="form-control" name="username" placeholder="Username" value="{{ old('username') }}" required autofocus>

                                                    @if ($errors->has('username'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('username') }}</strong>
                                                        </span>
                                                    @endif
                                            <input type="text" name="mobile_number" class="form-control" placeholder="Mobile Number">
                                            <input id="email" type="email" class="form-control" name="email" placeholder="Email" value="{{ old('email') }}" required>
                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                            <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>

                                                    @if ($errors->has('password'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('password') }}</strong>
                                                        </span>
                                                    @endif
                                             <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required>
                                            <p class="text-left"><strong>Please Note:</strong> All account registration are subject for approval by the system administrator.</p>
                                            <div class="text-right">
                                                 <input type="submit" class="btn btn-proceed" value="Create Account">
                                                <a class="btn btn-cancel" href="#">Cancel Registration</a>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>





                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#user_type').change(function(){
            if($(this).val() == 2){
                $('#district').css('display', 'none');
                $('#template_type').css('display', 'block');
            }else if($(this).val() == 3){
                $('#template_type').css('display', 'none');
                $('#district').css('display', 'block');
            }
            });
        });
        
    </script>
</body>
</html>

<!-- <div class="col-md-8 col-md-offset-2">
                                <div class="panel panel-default">
                                    <div class="panel-heading">Register</div>

                                    <div class="panel-body">
                                        <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                                            {{ csrf_field() }}

                                            <div class="form-group{{ $errors->has('fname') ? ' has-error' : '' }}">
                                                <label for="fname" class="col-md-4 control-label">First name</label>

                                                <div class="col-md-6">
                                                    <input id="fname" type="text" class="form-control" name="fname" value="{{ old('fname') }}" required autofocus>

                                                    @if ($errors->has('fname'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('fname') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('lname') ? ' has-error' : '' }}">
                                                <label for="lname" class="col-md-4 control-label">Last name</label>

                                                <div class="col-md-6">
                                                    <input id="lname" type="text" class="form-control" name="lname" value="{{ old('lname') }}" required autofocus>

                                                    @if ($errors->has('lname'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('lname') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('mname') ? ' has-error' : '' }}">
                                                <label for="mname" class="col-md-4 control-label">Middle name</label>

                                                <div class="col-md-6">
                                                    <input id="mname" type="text" class="form-control" name="mname" value="{{ old('mname') }}" required autofocus>

                                                    @if ($errors->has('mname'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('mname') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                                <div class="col-md-6">
                                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                                    @if ($errors->has('email'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('email') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                                <label for="username" class="col-md-4 control-label">Username</label>

                                                <div class="col-md-6">
                                                    <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" required autofocus>

                                                    @if ($errors->has('username'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('username') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                                <label for="password" class="col-md-4 control-label">Password</label>

                                                <div class="col-md-6">
                                                    <input id="password" type="password" class="form-control" name="password" required>

                                                    @if ($errors->has('password'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('password') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                                                <div class="col-md-6">
                                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-6 col-md-offset-4">
                                                    <button type="submit" class="btn btn-primary">
                                                        Register
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>   
                            </div> -->