<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="icon" type="image/png" href="{{ asset('img/icon.png') }}">
</head>
<body class="h-100">
    <div class="page page-login h-100">
        <div class="container text-center h-100">
            <div class="v-align">
                <div class="d-table">
                    <div class="table-cell">
                        <div class="col-xs-12 col-sm-6 col-md-4 col-sm-offset-3 col-md-offset-4 float-none">
                            <img src="img/measureapp-header.png" style="width: 95%; border-radius: 20px;">
                            <h3>SCORECARD MANAGEMENT SYSTEM</h3>
                            <div class="rel">
                                <div class="bg-form"></div>
                                <div class="form-user-cont">
                                    
                                    <form class="form-user" method="POST" action="{{ route('login') }}">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <label >Username</label>
                                            <input type="text" name="username" class="form-control">
                                            @if ($errors->has('username'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('username') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label>Password</label>
                                             <input id="password" type="password" class="form-control" name="password" required>
                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        
                                        
                                        <input type="submit" value="Login" class="btn btn-proceed">
                                       <!--  <a class="btn btn-cancel" href="{{ URL::to('/register') }}">Sign Up</a> -->
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/jquery-3.2.1.min.js')}}"></script>
</body>
</html>
