@extends('layouts.app')
@section('content')
<!-- Add Modal -->
<div id="addarea" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content modal-md">
    <form method="POST" action="area/addarea">
    {{ csrf_field() }}
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Division</h4>
      </div>
      <div class="modal-body">
        <div class="access-controls">
        	<div class="firstbatch">
          <div class="clearfix">
              <div class="col-md-8">
              <label>Municipality </label>
                <select class="form-control" name="municipalityid">
                  @foreach($municipalities as $municipality)
                  <option value="{{ $municipality->id }}"> {{ $municipality->municipality_name }} </option>
                  @endforeach
                </select>
              </div>
            </div>
            <br />
            <div class="clearfix">
              <div class="col-md-8">
              <label>Division Code </label>
                <input type="text" name="areacode" id="areacode" placeholder="Division Code" class="form-control input-md" required>
              </div>
            </div>
            <br />
            <div class="clearfix">
              <div class="col-md-8">
              <label>Division Name </label>
                <input type="text" name="areaname" id="areaname" placeholder="Division Name" class="form-control input-md" required>
              </div>
            </div>
            <br />
        	</div>
        </div>
      </div>
      <div class="modal-footer">
      <button type="submit" class="btn btn-proceed" data-dismiss="modal" style="padding: 10px;" >CLOSE</button>
        <button type="submit" class="btn btn-cancel"  style="padding: 10px;" > ADD DIVISION </button>
      </div>
      </form>
    </div>

  </div>
</div>

<!--Update Modal -->
<div id="updatearea" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content modal-md">
    <form method="POST" action="area/editArea">
    {{ csrf_field() }}
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Division</h4>
      </div>
      <div class="modal-body">
        <div class="access-controls">
          <div class="firstbatch">
          <div class="clearfix">
              <div class="col-md-8">
              <label>Municipality: </label>
                <select class="form-control" name="umunicipalityid">
                  @foreach($municipalities as $municipality)
                  <option value="{{ $municipality->id }}"> {{ $municipality->municipality_name }} </option>
                  @endforeach
                </select>
              </div>
            </div>
            <br />
            <div class="clearfix">
              <div class="col-md-8">
              <label>Division Code: </label>
                <input type="text" name="uareacode" id="uareacode" placeholder="Division Code" class="form-control input-md" required>
              </div>
            </div>
            <br />
            <div class="clearfix">
              <div class="col-md-8">
              <label>Division name: </label>
                <input type="hidden" name="uareaid" id="uareaid">
                <input type="text" name="uareaname" id="uareaname" class="form-control input-md" required>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
      <button type="submit" class="btn btn-proceed" data-dismiss="modal" style="padding: 10px;" > Close </button>
        <button type="submit" class="btn btn-cancel"  style="padding: 10px;" > Update Division</button>
      </div>
      </form>
    </div>

  </div>
</div>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="page-right assesment-right">
	<h3 class="title icon-rafi">List of Divisions</h3>
  <div class="clearfix text-right">
    <button class="btn btn-create btn-md" data-toggle="modal" data-target="#addarea"><span class="fa fa-plus"></span> Add Division</button>
  </div>
  <div class="clearfix">
      <table class="table table-striped">
        <thead>
          <th>ID</th>
          <th>Code</th>
          <th>Division name</th>
          <th>Municipality</th>
          <th>Action</th>
        </thead>
        <tbody>
        @if(count($areas) > 0)
        @foreach($areas as $area)
	          <tr>
              <td>{{ $area->id }}</td>
	            <td>{{ $area->division_code }}</td>
              <td>{{ $area->division_name }}</td>
              @foreach($municipalities as $municipality)
              @if($area->municipality_id == $municipality->id)
              <td>{{ $municipality->municipality_name }}</td>
              @break
              @endif
              @endforeach
              <input type="hidden" name="mid" id="mid" value="{{ $area->municipality_id }}">
              <td><button class="btn btn-proceed ea" style="margin-right: 5px;" data-toggle="modal" data-target="#updatearea"><span class="fa fa-edit"></span> Edit </button></td>
	          </tr>
        @endforeach
        @else
	    	<tr>
	    		<td colspan="5" class="text-center"><strong> No division record. </strong></td>
	    	</tr>
        @endif
        </tbody>
      </table>
      {{ $areas->links() }}
  </div>
</div>
@endsection
@section('script')
 <script src="{{ URL::asset('js/usercontrol/area.js') }}"></script>
@endsection