@extends('layouts.app')
@section('content')

<div class="page-right assesment-right">
	<h3 class="title icon-rafi">List of Templates</h3>
  <div class="clearfix text-right">
    <button class="btn btn-create btn-md" data-toggle="modal" data-target="#addTemp"><span class="fa fa-plus"></span> New Type</button>
  </div>
  <div class="clearfix">
      <table class="table table-striped">
        <thead>
          <th>ID</th>
          <th>Template Name</th>
          <th>Actions</th>
        </thead>
        <tbody>
          @foreach($templateType as $tt)
            <tr>
              <td>{{$tt->id}}</td>
              <td>{{$tt->template_name}}</td>
              <td><button class="btn btn-proceed es" style="margin-right: 5px;" data-toggle="modal" data-target="#updateTemp"><span class="fa fa-edit"></span> Edit</button></td>
            </tr>
          @endforeach
        </tbody>
      </table>
      {{ $templateType->links() }}
  </div>
</div>

<!-- Modal -->
<div id="addTemp" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content modal-md">
    <form method="POST" action="templateTypes/addTemp">
    {{ csrf_field() }}
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">New Template Type</h4>
      </div>
      <div class="modal-body">
        <div class="access-controls">
          <div class="firstbatch">
            <div class="clearfix">
              <div class="col-md-8">
              <br>
              <label>Template Name </label>
                <input type="text" name="ctemplatename" id="ctemplatename" placeholder="Template Name" class="form-control input-md" required>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
      <button type="submit" class="btn btn-proceed" data-dismiss="modal" style="padding: 10px;" >CLOSE</button>
        <button type="submit" class="btn btn-cancel"  style="padding: 10px;" > ADD TEMPLATE </button>
      </div>
      </form>
    </div>

  </div>
</div>

<!-- UpdateTemp -->
<div id="updateTemp" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content modal-md">
    <form method="POST" action="templateTypes/updateTemp">
    {{ csrf_field() }}
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update Template Type</h4>
      </div>
      <div class="modal-body">
        <div class="access-controls">
          <div class="firstbatch">
            <div class="clearfix">
              <div class="col-md-8">
              <br>
              <input type="hidden" name="tempid" id="tempid">
              <label>Template Name </label>
                <input type="text" name="templatename" id="templatename" placeholder="Template Name" class="form-control input-md" required>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
      <button type="submit" class="btn btn-proceed" data-dismiss="modal" style="padding: 10px;" >CLOSE</button>
        <button type="submit" class="btn btn-cancel"  style="padding: 10px;" > ADD ROUND </button>
      </div>
      </form>
    </div>

  </div>
</div>
@endsection
@section('script')
 <script src="{{ URL::asset('js/usercontrol/templateType.js') }}"></script>
@endsection