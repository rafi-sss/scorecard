@extends('layouts.app')
@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
	<div class="page-right assesment-right">
		<h3 class="title icon-rafi">District List</h3>
		<div class="pull-left input-group" style="width: 40%">
		    <input type="text" class="form-control" placeholder="District Name" aria-describedby="basic-addon1" id="dname" name="dname">
		    <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
    	</div>
		<div>
			<button class="btn btn-success btn-md pull-right" data-toggle="modal" data-target="#adddistrict"><span class="fa fa-plus"></span> Add District</button>
		</div>
		<table class="table table-striped">
			<thead>
				<th> ID </th>
				<th> Code </th>
				<th> District Name </th>
				<th> Division </th>
				<th> Action </th>
			</thead>
			<tbody id="jeje">
			@if(count($district) > 0)
				@foreach($district as $dist)
				<tr>
					<td>{{$dist->id}}</td>
					<td>{{$dist->district_code}}</td>
					<td>{{$dist->district_name}}</td>
					@foreach($areas as $area)
					@if($dist->division_id == $area->id)
					<td>{{$area->division_name}}</td>
					@break
					@endif
					@endforeach
					<input type="hidden" name="did" id="did" value="{{ $dist->division_id }}">
					<td><button class="btn btn-proceed ed" style="margin-right: 5px;" data-toggle="modal" data-target="#updatedistrict"><span class="fa fa-edit"></span> Edit</button></td>
				</tr>
				@endforeach
			@else
	        <tr class="text-center">
	          <td colspan="5"> <strong>No district record.</strong></td>
	        </tr>
	        @endif
			</tbody>
		</table>
		<div class="lnk"> {{ $district->links() }} </div>
	</div>
	<!-- Modal -->
	<div id="adddistrict" class="modal fade" role="dialog">
	  <div class="modal-dialog">
	    <!-- Modal content-->
	    <div class="modal-content modal-md">
	    <form method="POST" action="district/adddistrict">
	    {{ csrf_field() }}
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Add District</h4>
	      </div>
	      <div class="modal-body">
	        <div class="access-controls">
	        	<div class="firstbatch">
	        	<div class="clearfix">
	              <div class="col-md-8">		              
	              <label>Division: </label>
	             	<select class="form-control" name="divisionid">
		                @foreach($areas as $area)
			                @foreach($dists as $ds)
			                	@if($ds->id == $area->municipality_id)
			                	<option value="{{ $area->id }}"> {{ $area->division_name }} - <span style="font-size: 10px;">({{$ds->municipality_name}})</span> </option>
			                	@endif
		             		@endforeach
		                @endforeach
		              </select>
	              </div>
	            </div>
	            <br />
	            <div class="clearfix">
	              <div class="col-md-8">		              
	              <label>District Code </label>
	             	<input type="text" name="districtcode" id="districtcode" placeholder="District Name" class="form-control input-md" required>
	              </div>
	            </div>
	            <br />

	            <div class="clearfix">
	              <div class="col-md-8">		              
	              <label>District Name </label>
	                <input type="text" name="districtname" id="districtname" placeholder="District Name" class="form-control input-md" required>
	              </div>
	            </div>
	            <br />
	        	</div>
	        </div>
	      </div>
	      <div class="modal-footer">
	      <button type="submit" class="btn btn-proceed" data-dismiss="modal" style="padding: 10px;" >Close</button>
	        <button type="submit" class="btn btn-cancel"  style="padding: 10px;" > Add District </button>
	      </div>
	      </form>
	    </div>

	  </div>
	</div>

	<!--Update Modal -->
<div id="updatedistrict" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content modal-md">
    <form method="POST" action="district/editDistrict">
    {{ csrf_field() }}
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit District</h4>
      </div>
      <div class="modal-body">
        <div class="access-controls">
          <div class="firstbatch">
          <div class="clearfix">
	              <div class="col-md-8">		              
	              <label>Division: </label>
	             	<select class="form-control" name="udivisionid">
		                @foreach($areas as $area)
		               @foreach($dists as $ds)
			                	@if($ds->id == $area->municipality_id)
			                	<option value="{{ $area->id }}"> {{ $area->division_name }} - <span style="font-size: 10px;">({{$ds->municipality_name}})</span> </option>
			                	@endif
		             		@endforeach
		                @endforeach
		              </select>
	              </div>
	            </div>
	            <br />
	            <div class="clearfix">
	              <div class="col-md-8">		              
	              <label>District Code: </label>
	             	<input type="text" name="udistrictcode" id="udistrictcode" placeholder="District Name" class="form-control input-md" required>
	              </div>
	            </div>
	            <br />

            <div class="clearfix">
              <div class="col-md-8">
              <label>District name: </label>
                <input type="hidden" name="udistrictid" id="udistrictid">
                <input type="text" name="udistrictname" id="udistrictname" class="form-control input-md" required>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
      <button type="submit" class="btn btn-proceed" data-dismiss="modal" style="padding: 10px;" > Close </button>
        <button type="submit" class="btn btn-cancel"  style="padding: 10px;" > Update District</button>
      </div>
      </form>
    </div>
  </div>
</div>

@endsection
@section('script')
 <script src="{{ URL::asset('js/usercontrol/district.js') }}"></script>
@endsection