@extends('layouts.app')
@section('content')
<!-- Modal -->
<div id="addSchool" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content modal-md">
    <form method="POST" action="schools/addschool">
    {{ csrf_field() }}
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add School</h4>
      </div>
      <div class="modal-body">
        <div class="access-controls">
        	<div class="firstbatch">
            <div class="clearfix">
              <div class="col-md-8">
              <label>District </label>
              <select class="form-control" name="districtid">
                @foreach($districts as $district)
                <option value="{{ $district->id }}"> {{ $district->district_name }} </option>
                @endforeach
              </select>
              <br>
              <label>School Code </label>
                <input type="text" name="schoolcode" id="schoolcode" placeholder="School Code" class="form-control input-md" required>
                              <br>
              <label>School Name </label>
                <input type="text" name="schoolname" id="schoolname" placeholder="School Name" class="form-control input-md" required>
                <br>
              <label>School Address </label>
                <textarea name="schooladdress" id="schooladdress" placeholder="School address" class="form-control input-md" required></textarea>
                
              </div>
            </div>
        	</div>
        </div>
      </div>
      <div class="modal-footer">
      <button type="submit" class="btn btn-proceed" data-dismiss="modal" style="padding: 10px;" >CLOSE</button>
        <button type="submit" class="btn btn-cancel"  style="padding: 10px;" > ADD SCHOOL </button>
      </div>
      </form>
    </div>

  </div>
</div>

  <!--Update Modal -->
<div id="updateschool" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content modal-md">
    <form method="POST" action="schools/editSchool">
    {{ csrf_field() }}
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit School</h4>
      </div>
      <div class="modal-body">
        <div class="access-controls">
          <div class="firstbatch">
            <div class="clearfix">
              <div class="col-md-8">
              <label>District: </label>
              <select class="form-control" name="udistrictid">
                @foreach($districts as $district)
                <option value="{{ $district->id }}"> {{ $district->district_name }} </option>
                @endforeach
              </select>
              <br>
              <label>School Code: </label>
                <input type="text" name="uschoolcode" id="uschoolcode" placeholder="School Name" class="form-control input-md" required>
                              <br>
              <label>School name: </label>
                <input type="hidden" name="uschoolid" id="uschoolid">
                <input type="text" name="uschoolname" id="uschoolname" class="form-control input-md" required>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
      <button type="submit" class="btn btn-proceed" data-dismiss="modal" style="padding: 10px;" > Close </button>
        <button type="submit" class="btn btn-cancel"  style="padding: 10px;" > Update School</button>
      </div>
      </form>
    </div>
  </div>
</div>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="page-right assesment-right">
	<h3 class="title icon-rafi">List of Schools</h3>
  <div class="clearfix text-right" style="margin-bottom: 20px">
    <div class="pull-left input-group" style="width: 40%">
        <input type="text" class="form-control" placeholder="District Name" aria-describedby="basic-addon1" id="dname" name="dname">
        <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
    </div>
    <div class="">
      <button class="btn btn-create btn-md" data-toggle="modal" data-target="#addSchool"><span class="fa fa-plus"></span> Add school</button>
    </div>
  </div>
  <div class="clearfix">
      <table class="table table-striped">
        <thead>
          <th>ID</th>
          <th>Code</th>
          <th>School Name</th>
          <th>District</th>
        </thead>
        <tbody id="jeje">
        @if(count($schools) > 0)
          @foreach($schools as $school)
            <tr>
              <td>{{ $school->id }}</td>
              <td>{{ $school->school_code }}</td>
              <td>{{ $school->school_name }}</td>
              @foreach($districts as $district)
              @if($school->district_id == $district->id)
              <td>{{ $district->district_name }}</td>
              <input type="hidden" name="disID" id="disID" value="{{ $school->district_id }}">
              @break
              @endif
              @endforeach
              <td><button class="btn btn-proceed es" style="margin-right: 5px;" data-toggle="modal" data-target="#updateschool"><span class="fa fa-edit"></span> Edit</button></td>
            </tr>
          @endforeach
        @else
        <tr class="text-center">
          <td colspan="5"> <strong>No school record.</strong></td>
        </tr>
        @endif
        </tbody>
      </table>
      <div class="lnk">{{ $schools->links() }}</div>
  </div>
</div>

@endsection
@section('script')
 <script src="{{ URL::asset('js/usercontrol/school.js') }}"></script>
@endsection