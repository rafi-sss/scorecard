@extends('layouts.app')
@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<!-- Add Modal -->
<div id="addprovince" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content modal-md">
    <form method="POST" action="province/addProvince">
    {{ csrf_field() }}
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add province</h4>
      </div>
      <div class="modal-body">
        <div class="access-controls">
        	<div class="firstbatch">
          <div class="clearfix">
              <div class="col-md-8">
              <label>Region </label>
                <select class="form-control" name="regionid">
                  @foreach($regions as $region)
                  <option value="{{ $region->id }}"> {{ $region->region_name }} </option>
                  @endforeach
                </select>
              </div>
            </div>
            <br />
            <div class="clearfix">
              <div class="col-md-8">
              <label>Province code </label>
                <input type="text" name="provincecode" id="provincecode" placeholder="province Code" class="form-control input-md" required>
              </div>
            </div>
            <br />
            <div class="clearfix">
              <div class="col-md-8">
              <label>Province name </label>
                <input type="text" name="provincename" id="provincename" placeholder="Area Name" class="form-control input-md" required>
              </div>
            </div>
            <br />
        	</div>
        </div>
      </div>
      <div class="modal-footer">
      <button type="submit" class="btn btn-proceed" data-dismiss="modal" style="padding: 10px;" >CLOSE</button>
        <button type="submit" class="btn btn-cancel"  style="padding: 10px;" > ADD PROVINCE </button>
      </div>
      </form>
    </div>

  </div>
</div>

<!--Update Modal -->
<div id="updateprovince" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content modal-md">
    <form method="POST" action="province/editProvince">
    {{ csrf_field() }}
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit province</h4>
      </div>
      <div class="modal-body">
        <div class="access-controls">
          <div class="firstbatch">
          <div class="clearfix">
              <div class="col-md-8">
              <label>Region: </label>
                <select class="form-control" name="uregionid">
                  @foreach($regions as $region)
                  <option value="{{ $region->id }}"> {{ $region->region_name }} </option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="clearfix">
              <div class="col-md-8">
              <label>Province code: </label>
                <input type="hidden" name="uprovinceid" id="uprovinceid">
                <input type="text" name="uprovincecode" id="uprovincecode" class="form-control input-md" required>
              </div>
            </div>
            <div class="clearfix">
              <div class="col-md-8">
              <label>Province name: </label>
                <input type="text" name="uprovincename" id="uprovincename" class="form-control input-md" required>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
      <button type="submit" class="btn btn-proceed" data-dismiss="modal" style="padding: 10px;" > Close </button>
        <button type="submit" class="btn btn-cancel"  style="padding: 10px;" > UPDATE PROVINCE</button>
      </div>
      </form>
    </div>

  </div>
</div>

<div class="page-right assesment-right">
	<h3 class="title icon-rafi">List of Provinces</h3>
  <div class="clearfix text-right">
    <button class="btn btn-create btn-md" data-toggle="modal" data-target="#addprovince"><span class="fa fa-plus"></span> Add Province</button>
  </div>
  <div class="clearfix">
      <table class="table table-striped">
        <thead>
          <th>ID</th>
          <th>Code</th>
          <th>Province Name</th>
          <th>Region</th>
          <th>Action</th>
        </thead>
        <tbody>
        @if(count($provinces) > 0)
        @foreach($provinces as $province)
	          <tr>
              <td>{{ $province->id }}</td>
              <td>{{ $province->province_code }}</td>
	            <td>{{ $province->province_name }}</td>
              @foreach($regions as $region)
              @if($province->region_id == $region->id)
              <td>{{ $region->region_name }}</td>
              @break
              @endif
              @endforeach
              <input type="hidden" name="pid" id="pid" value="{{ $province->region_id }}">
              <td><button class="btn btn-proceed ea" style="margin-right: 5px;" data-toggle="modal" data-target="#updateprovince"><span class="fa fa-edit"></span> Edit </button></td>
	          </tr>
        @endforeach
        @else
	    	<tr>
	    		<td colspan="5" class="text-center"><strong> No province record. </strong></td>
	    	</tr>
        @endif
        </tbody>
      </table>
      {{ $provinces->links() }}
  </div>
</div>
@endsection
@section('script')
 <script src="{{ URL::asset('js/usercontrol/province.js') }}"></script>
@endsection