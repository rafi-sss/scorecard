@extends('layouts.app')
@section('content')
	<div class="page-right assesment-right">
		<h3 class="title icon-rafi">Access Control</h3>
		<div class="clearfix text-right">
		<button class="btn btn-create btn-md" data-toggle="modal" data-target="#addtype"><span class="fa fa-plus"></span> Add User Type</button>
		</div>
							<table class="table table-striped">
								<thead>
									<th>User type</th>
									<th>Action</th>
								</thead>
								<tbody>
									@foreach($userTypesControl as $userType)
										<tr>
											<td>{{$userType->type}}</td>
											<td>
											@if($userType->type == 'Super Admin')
											<a id='{{ $userType->id }}' type="button" class="btn btn-proceed editBtn" data-toggle="modal" data-target="#"><span class="fa fa-edit"></span> View</a>
											@else
											<a id='{{ $userType->id }}' type="button" class="btn btn-proceed editBtn" data-toggle="modal" data-target="#myModal"><span class="fa fa-edit"></span> Edit</a>
											<a class="btn btn-cancel" style="margin-left: 5px"><span class="fa fa-trash"></span> Delete</a>
											@endif
											</td>	
										</tr>
									@endforeach
								</tbody>
							</table>
							<!-- Modal -->
							<div id="myModal" class="modal fade" role="dialog">
							  <div class="modal-dialog">

							    <!-- Modal content-->
							    <form action="" id="frmSubmit" method="POST">
							    {{ csrf_field() }}
								    <div class="modal-content">
								      <div class="modal-header">
								        <button type="button" class="close" data-dismiss="modal">&times;</button>
								        <h4 class="modal-title">Edit Access Control</h4>
								      </div>
								      <div class="modal-body">
								        <div class="access-controls">
								        	<div class="firstbatch">
								        	</div>
								        </div>
								      </div>
								      <div class="modal-footer">
								        <button type="button" id="btnSubmit" class="btn btn-cancel" style="padding: 10px;" >Change Access Controls</button>
								      </div>
								    </div>
								</form>
							  </div>
							</div>
							<!-- Add Modal -->
<div id="addtype" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content modal-md">
    <form method="POST" action="usercontrol/addType">
    {{ csrf_field() }}
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add User Type</h4>
      </div>
      <div class="modal-body">
        <div class="access-controls">
        	<div class="">
            <div class="clearfix">
              <div class="col-md-8">
              <label>User Type: </label>
                <input type="text" name="typename" id="typename" placeholder="User Type" class="form-control input-md" required>
              </div>
            </div>
        	</div>
        </div>
      </div>
      <div class="modal-footer">
      <button type="submit" class="btn btn-proceed" data-dismiss="modal" style="padding: 10px;" >CLOSE</button>
        <button type="submit" class="btn btn-cancel"  style="padding: 10px;" > ADD USER TYPE </button>
      </div>
      </form>
    </div>

  </div>
</div>
					</div>
@endsection
@section('script')
<script type="text/javascript" src="{{ asset('js/usercontrol/usercontrol.js')}}"></script>
@endsection