@extends('layouts.app')
@section('content')
<!-- Modal -->
<div id="adddccenter" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content modal-md">
    <form method="POST" action="schools/adddccenter">
    {{ csrf_field() }}
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Daycare Center</h4>
      </div>
      <div class="modal-body">
        <div class="access-controls">
        	<div class="firstbatch">
            <div class="clearfix">
              <div class="col-md-8">
              <label>Municipality </label>
              <select class="form-control" name="districtid">
                @foreach($municipalities as $mun)
                <option value="{{ $mun->id }}"> {{ $mun->municipality_name }} </option>
                @endforeach
              </select>
              <br>
              <label>School Code </label>
                <input type="text" name="schoolcode" id="schoolcode" placeholder="School Code" class="form-control input-md" required>
                              <br>
              <label>School Name </label>
                <input type="text" name="schoolname" id="schoolname" placeholder="School Name" class="form-control input-md" required>
                <br>
              <label>School Address </label>
                <textarea name="schooladdress" id="schooladdress" placeholder="School address" class="form-control input-md" required></textarea>
                
              </div>
            </div>
        	</div>
        </div>
      </div>
      <div class="modal-footer">
      <button type="submit" class="btn btn-proceed" data-dismiss="modal" style="padding: 10px;" >CLOSE</button>
        <button type="submit" class="btn btn-cancel"  style="padding: 10px;" > ADD SCHOOL </button>
      </div>
      </form>
    </div>

  </div>
</div>

  <!--Update Modal -->
<div id="updateschool" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content modal-md">
    <form method="POST" action="schools/editSchool">
    {{ csrf_field() }}
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit School</h4>
      </div>
      <div class="modal-body">
        <div class="access-controls">
          <div class="firstbatch">
            <div class="clearfix">
              <div class="col-md-8">
              <label>District: </label>
              <select class="form-control" name="udistrictid">
                @foreach($municipalities as $district)
                <option value="{{ $district->id }}"> {{ $district->district_name }} </option>
                @endforeach
              </select>
              <br>
              <label>School Code: </label>
                <input type="text" name="uschoolcode" id="uschoolcode" placeholder="School Name" class="form-control input-md" required>
                              <br>
              <label>School name: </label>
                <input type="hidden" name="uschoolid" id="uschoolid">
                <input type="text" name="uschoolname" id="uschoolname" class="form-control input-md" required>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
      <button type="submit" class="btn btn-proceed" data-dismiss="modal" style="padding: 10px;" > Close </button>
        <button type="submit" class="btn btn-cancel"  style="padding: 10px;" > Update School</button>
      </div>
      </form>
    </div>
  </div>
</div>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="page-right assesment-right">
	<h3 class="title icon-rafi">List of Daycare Centers</h3>
  <div class="clearfix text-right" style="margin-bottom: 20px">
    <div class="pull-left input-group" style="width: 40%">
        <input type="text" class="form-control" placeholder="Daycare Center Name" aria-describedby="basic-addon1" id="dname" name="dname">
        <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
    </div>
    <div class="">
      <button class="btn btn-create btn-md" data-toggle="modal" data-target="#adddccenter"><span class="fa fa-plus"></span> Add Daycare Center</button>
    </div>
  </div>
  <div class="clearfix">
      <table class="table table-striped">
        <thead>
          <th>Code</th>
          <th>Daycare Center Name</th>
          <th>Barangay</th>
        </thead>
        <tbody id="jeje">

        </tbody>
      </table>
  </div>
</div>

@endsection
@section('script')
 <script src="{{ URL::asset('js/usercontrol/school.js') }}"></script>
@endsection