@extends('layouts.app')
@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<!-- Add Modal -->
<div id="addmunicipality" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content modal-md">
    <form method="POST" action="municipality/addmunicipality">
    {{ csrf_field() }}
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Municipality</h4>
      </div>
      <div class="modal-body">
        <div class="access-controls">
        	<div class="firstbatch">
          <div class="clearfix">
              <div class="col-md-8">
              <label>Province </label>
                <select class="form-control" name="provinceid">
                  @foreach($provinces as $province)
                  <option value="{{ $province->id }}"> {{ $province->province_name }} </option>
                  @endforeach
                </select>
              </div>
            </div>
            <br />
            <div class="clearfix">
              <div class="col-md-8">
              <label>Municipality Code </label>
                <input type="text" name="municipalitycode" id="municipalitycode" placeholder="municipality Code" class="form-control input-md" required>
              </div>
            </div>
            <br />
            <div class="clearfix">
              <div class="col-md-8">
              <label>Municipality Name </label>
                <input type="text" name="municipalityname" id="municipalityname" placeholder="Area Name" class="form-control input-md" required>
              </div>
            </div>
            <br />
        	</div>
        </div>
      </div>
      <div class="modal-footer">
      <button type="submit" class="btn btn-proceed" data-dismiss="modal" style="padding: 10px;" >CLOSE</button>
        <button type="submit" class="btn btn-cancel"  style="padding: 10px;" > ADD MUNICIPALITY </button>
      </div>
      </form>
    </div>

  </div>
</div>

<!--Update Modal -->
<div id="updatemunicipality" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content modal-md">
    <form method="POST" action="municipality/editMunicipality">
    {{ csrf_field() }}
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Municipality</h4>
      </div>
      <div class="modal-body">
        <div class="access-controls">
          <div class="firstbatch">
           <div class="clearfix">
              <div class="col-md-4">
              <label>Province: </label>
                <select class="form-control" name="uprovinceid">
                  @foreach($provinces as $province)
                  <option value="{{ $province->id }}"> {{ $province->province_name }} </option>
                  @endforeach
                </select>
              </div>
            </div>
            <br />
            <div class="clearfix">
              <div class="col-md-8">
              <label>Municipality code: </label>
                <input type="hidden" name="umunicipalityid" id="umunicipalityid">
                <input type="text" name="umunicipalitycode" id="umunicipalitycode" class="form-control input-md" required>
              </div>
            </div>
            <br />
            <div class="clearfix">
              <div class="col-md-8">
              <label>Municipality name: </label>
                <input type="text" name="umunicipalityname" id="umunicipalityname" class="form-control input-md" required>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
      <button type="submit" class="btn btn-proceed" data-dismiss="modal" style="padding: 10px;" > Close </button>
        <button type="submit" class="btn btn-cancel"  style="padding: 10px;" > UPDATE MUNICIPALITY</button>
      </div>
      </form>
    </div>

  </div>
</div>

<div class="page-right assesment-right">
	<h3 class="title icon-rafi">List of Municipalities</h3>
  <div class="clearfix text-right">
    <button class="btn btn-create btn-md" data-toggle="modal" data-target="#addmunicipality"><span class="fa fa-plus"></span> Add Municipality</button>
  </div>
  <div class="clearfix">
      <table class="table table-striped">
        <thead>
          <th>ID</th>
          <th>Code</th>
          <th>Municipality Name</th>
          <th>Province</th>
          <th>Action</th>
        </thead>
        <tbody>
        @if(count($municipalities) > 0)
        @foreach($municipalities as $municipality)
	          <tr>
              <td>{{ $municipality->id }}</td>
              <td>{{ $municipality->municipality_code }}</td>
	            <td>{{ $municipality->municipality_name }}</td>
              @foreach($provinces as $province)
              @if($municipality->province_id == $province->id)
              <td>{{ $province->province_name }}</td>
              @break
              @endif
              @endforeach
              <input type="hidden" name="mid" id="mid" value="{{ $municipality->province_id }}">
              <td><button class="btn btn-proceed ea" style="margin-right: 5px;" data-toggle="modal" data-target="#updatemunicipality"><span class="fa fa-edit"></span> Edit </button></td>
	          </tr>
        @endforeach
        @else
	    	<tr>
	    		<td colspan="5" class="text-center"><strong> No municipality record. </strong></td>
	    	</tr>
        @endif
        </tbody>
      </table>
      <input type="hidden" name="project" value="SEED 2018">
      {{ $municipalities->links() }}
  </div>
</div>
@endsection
@section('script')
 <script src="{{ URL::asset('js/usercontrol/municipality.js') }}"></script>
@endsection