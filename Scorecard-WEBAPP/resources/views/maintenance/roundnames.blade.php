@extends('layouts.app')
@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="page-right assesment-right">
	<h3 class="title icon-rafi">List of Rounds</h3>
  <div class="clearfix text-right">
    <button class="btn btn-create btn-md" data-toggle="modal" data-target="#addRound"><span class="fa fa-plus"></span> New Round</button>
  </div>
  <div class="clearfix">
      <table class="table table-striped">
        <thead>
          <th>Round Name</th>
          <th>Weight</th>
          <th>Round Method</th>
          <th>Actions</th>
        </thead>
        <tbody>
          @foreach($rounds as $round)
            <tr>
              <td>{{$round->round_name}}</td>
              <td>{{$round->weight}}</td>
              @if($round->round_type == 1)
              <td>Scorecard Evaluation</td>
              @elseif($round->round_type ==2)
              <td>Panel Evaluation</td>
              @endif
              <td><button class="btn btn-proceed es" style="margin-right: 5px;" data-toggle="modal" data-target="#updateschool"><span class="fa fa-edit"></span> Edit</button><button class="btn btn-cancel"><span class="fa fa-trash"></span> Delete</button></td>
            </tr>
          @endforeach
        </tbody>
      </table>
      {{ $rounds->links() }}
  </div>
</div>

<!-- Modal -->
<div id="addRound" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content modal-md">
    <form method="POST" action="roundnames/addround">
    {{ csrf_field() }}
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">New Round Details</h4>
      </div>
      <div class="modal-body">
        <div class="access-controls">
          <div class="firstbatch">
            <div class="clearfix">
              <div class="col-md-8">
              <br>
              <label>Round Name </label>
                <input type="text" name="roundname" id="roundname" placeholder="Round Name" class="form-control input-md" required>
                              <br>
              <label>Weight </label>
                <input type="number" name="weight" id="weight" placeholder="Round Weight" class="form-control input-md" required> 
                <br>
              <label>Round Method </label>
                <select class="form-control" name="level">
                  <option value="1">Scorecard Evaluation</option>
                  <option value="2">Panel Evaluation</option>
                </select> 
                <br>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
      <button type="submit" class="btn btn-proceed" data-dismiss="modal" style="padding: 10px;" >CLOSE</button>
        <button type="submit" class="btn btn-cancel"  style="padding: 10px;" > ADD ROUND </button>
      </div>
      </form>
    </div>

  </div>
</div>

@endsection
@section('script')
 <script src="{{ URL::asset('js/usercontrol/rounds.js') }}"></script>
@endsection