@extends('layouts.app')
@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<!-- Add Modal -->
<div id="addregion" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content modal-md">
    <form method="POST" action="region/addRegion">
    {{ csrf_field() }}
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><span class="fa fa-plus"></span> Add Region</h4>
      </div>
      <div class="modal-body">
        <div class="access-controls">
        	<div class="firstbatch">
            <div class="clearfix">
              <div class="col-md-8">
              <label>Region code </label>
                <input type="text" name="regioncode" id="regioncode" placeholder="Region Code" class="form-control input-md" required>
              </div>
            </div>
            <br />
            <br />
            <div class="clearfix">
              <div class="col-md-8">
              <label>Region name </label>
                <input type="text" name="regionname" id="regionname" placeholder="Area Name" class="form-control input-md" required>
              </div>
            </div>
            <br />
        	</div>
        </div>
      </div>
      <div class="modal-footer">
      <button type="submit" class="btn btn-proceed" data-dismiss="modal" style="padding: 10px;" >CLOSE</button>
        <button type="submit" class="btn btn-cancel"  style="padding: 10px;" > ADD REGION </button>
      </div>
      </form>
    </div>

  </div>
</div>

<!--Update Modal -->
<div id="updateregion" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content modal-md">
    <form method="POST" action="region/editRegion">
    {{ csrf_field() }}
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Region</h4>
      </div>
      <div class="modal-body">
        <div class="access-controls">
          <div class="firstbatch">
            <div class="clearfix">
              <div class="col-md-8">
              <label>Region code: </label>
                <input type="hidden" name="uregionid" id="uregionid">
                <input type="text" name="uregioncode" id="uregioncode" class="form-control input-md" required>
              </div>
            </div>
            <div class="clearfix">
              <div class="col-md-8">
              <label>Region name: </label>
                <input type="text" name="uregionname" id="uregionname" class="form-control input-md" required>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
      <button type="submit" class="btn btn-proceed" data-dismiss="modal" style="padding: 10px;" > Close </button>
        <button type="submit" class="btn btn-cancel"  style="padding: 10px;" > Update Region</button>
      </div>
      </form>
    </div>

  </div>
</div>

<div class="page-right assesment-right">
	<h3 class="title icon-rafi">List of Regions</h3>
  <div class="clearfix text-right">
    <button class="btn btn-create btn-md" data-toggle="modal" data-target="#addregion">Add Region</button>
  </div>
  <div class="clearfix">
      <table class="table table-striped">
        <thead>
          <th>ID</th>
          <th>Code</th>
          <th>Region name</th>
          <th>Action</th>
        </thead>
        <tbody>
        @if(count($regions) > 0)
        @foreach($regions as $region)
	          <tr>
              <td>{{ $region->id }}</td>
              <td>{{ $region->region_code }}</td>
	            <td>{{ $region->region_name }}</td>
              <td><button class="btn btn-proceed ea" style="margin-right: 5px;" data-toggle="modal" data-target="#updateregion"><span class="fa fa-edit"></span> Edit </button></td>
	          </tr>
        @endforeach
        @else
	    	<tr>
	    		<td colspan="3" class="text-center"><strong> No region record. </strong></td>
	    	</tr>
        @endif
        </tbody>
      </table>
      {{ $regions->links() }}
  </div>
</div>
@endsection
@section('script')
 <script src="{{ URL::asset('js/usercontrol/region.js') }}"></script>
@endsection