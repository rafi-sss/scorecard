@extends('layouts.app')
@section('content')
<div class="page-right assesment-right">
  <h3 class="title icon-rafi">OCP Evaluators</h3>
  <div class="clearfix text-right">
    @if($usertype != 3)
    <button type="button" class="btn btn-success btn-adde" data-toggle="modal" data-target="#addEval"><span class="fa fa-plus"></span> Add Evaluator</button>
    @endif
  </div>
  <table class="table table-striped">
    <thead>
      <th>Evaluator Name</th>
      <th>Email Address</th>
      <th>Contact Number</th>
      <th>Municipality</th>
      <th>Alternate</th>
      @if($usertype != 3)
      <th>Actions</th>
      @endif
    </thead>
    <tbody>
      @foreach($ocp_evaluators as $oe)
      <tr>
        <td><span class="sfname">{{$oe->fname}}</span>, <span class="smname">{{$oe->mname}}</span>, <span class="slname">{{$oe->lname}}</span></td>
        <td>{{$oe->email_address}}</td>
        <td>{{$oe->contact_number}}</td>
        <td><span id="{{$oe->mid}}">{{$oe->municipality_name}}</span></td>
        <td><button type="button" data-id='{{$oe->id}}' class="btn btn-proceed va"><span class="fa fa-eye"></span> View</button></td>
        @if($usertype != 3)
        <td><button class="btn btn-danger" data-id='{{$oe->id}}' style="margin-right: 5px;" data-toggle="modal" data-target="#updatearea"><span class="fa fa-edit"></span> Edit </button></td>
        @endif
      </tr>
      @endforeach
    </tbody>
  </table>
</div>

<!-- Add Modal -->
<div id="addEval" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content modal-md">
    {{ csrf_field() }}
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Evaluator</h4>
      </div>
      <div class="modal-body">
      <form method="POST" action="evaluator/addOCPEval" id="addOCP">
        <label style="width:  100%;background:  gainsboro;padding:  5px;">Evaluators Details</label>
        <div class="mainEval">
          <input type="hidden" name="mainEvalVal" id="mainEvalVal" value="0">
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label>First Name</label>
                <input type="text" placeholder="First Name" name="fname" class="form-control">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Middle Name</label>
                <input type="text" placeholder="Middle Name" name="mname" class="form-control">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Last Name</label>
                <input type="text" placeholder="Last Name" name="lname" class="form-control">
              </div>
            </div>
            <div class="col-md-4">
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label>Municipality</label>
                <select class="form-control" name="municipality">
                  <option>-- Select Municipality --</option>
                  @foreach($municipalities as $municipality)
                  <option value="{{ $municipality->id }}"> {{ $municipality->municipality_name }} </option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Contact Number</label>
                <input type="number" placeholder="Contact Number" name="cnumber" class="form-control">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Email Address</label>
                <input type="email" placeholder="Email Address" name="eadd" class="form-control">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label>Position</label>
              <input type="text" class="form-control" placeholder="Position" name="position">
            </div>
          </div>
        </div>
        <hr style="border-color:#d8d8d8">
        <div class="form-group text-right mainevalControls">
          <button type="submit" class="btn btn-proceed" data-dismiss="modal" style="padding: 10px;" >CLOSE</button>
            <button type="submit" class="btn btn-cancel ae"  style="padding: 10px;" > ADD EVALUATOR </button>
          </div>
        </form>
        <form method="POST" action="evaluator/addOCPEval" id="altEvalOCP">
        <div class="altEval" style="display: none;">
        <label style="width:  100%;background:  gainsboro;padding:  5px;">Alternate Evaluator</label>
          <div class="row">
            <input type="hidden" name="mainEvalVal" id="mainEvalVal" value="1">
            <input type="hidden" name="altEvalVal" id="altEvalVal">
            <div class="col-md-4">
              <div class="form-group">
                <label>First Name</label>
                <input type="text" name="fname" class="form-control">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Middle Name</label>
                <input type="text" name="mname" class="form-control">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Last Name</label>
                <input type="text" name="lname" class="form-control">
              </div>
            </div>
            <div class="col-md-4">
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label>Municipality</label>
                <select class="form-control" name="municipality">
                  <option>-- Select Municipality --</option>
                  @foreach($municipalities as $municipality)
                  <option value="{{ $municipality->id }}"> {{ $municipality->municipality_name }} </option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Contact Number</label>
                <input type="number" name="cnumber" class="form-control">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Email Address</label>
                <input type="email" name="eadd" class="form-control">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label>Position</label>
              <input type="text" class="form-control" placeholder="Position" name="position">
            </div>
          </div>
        </div>
        <div class="form-group text-right altEvalCont" style="display: none;">
          <button type="submit" class="btn btn-proceed" data-dismiss="modal" style="padding: 10px;" >CLOSE</button>
            <button type="submit" class="btn btn-cancel ae"  style="padding: 10px;" > ADD ALTERNATE </button>
          </div>
      </form>
      </div>
    </div>

  </div>
</div>

<!--Alternatives-->
<div id="viewAlt" class="modal fade" role="dialog">
  <div class="viewAltWrapper">
  <div class="modal-dialog" style="display: table;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Alternate Evaluator for Evaluator <span class="mainEvalName"></span></h4>
      </div>
      <div class="modal-body">
          <table class="table table-striped table-responsive">
            <thead>
              <th>Evaluator Name</th>
              <th>Email Address</th>
              <th>Contact Number</th>
              <th>Municipality</th>
              <th>Alternate Action</th>
              @if($usertype != 3)
              <th>Actions</th>
              @endif
            </thead>
            <tbody id="response">
            </tbody>
          </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-proceed" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</div>

@endsection
@section('script')
 <script src="{{ URL::asset('js/usercontrol/evaluator.js') }}"></script>
@endsection