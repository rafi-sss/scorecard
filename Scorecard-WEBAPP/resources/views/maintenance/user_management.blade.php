@extends('layouts.app')
@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<!-- Add Modal -->
<div id="addum" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content modal-md">
    <form method="POST" action="usermanagement/addum">
    {{ csrf_field() }}
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add User</h4>
      </div>
      <div class="modal-body">
        <div class="access-controls">
          <div class="firstbatch">
          <div class="clearfix">
              <div class="col-md-6">
              <label>User Type: </label>
                <select class="form-control" id="utype" name="utype" required="">
                  @foreach($userTypes as $usertype)
                    <option value="{{ $usertype->id }}">{{ $usertype->type }}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-md-6" id="sctype" style="display: none;">
              <label>Scorecard Type: </label>
                <select class="form-control" name="scorecardtype" required="">
                  @foreach($tempTypes as $temp)
                    <option value="{{ $temp->id }}">{{ $temp->template_name }}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-md-6" id="userdistrict" style="display: none;">
              <label>District: </label>
                <select class="form-control" name="district" required="" id="district">
                  @foreach($mst_province as $prov)
                    <option disabled="" style="background: #cfd6de">{{ $prov->province_name }}</option>
                    @foreach($districts as $district)
                      @if($prov->id == $district->province_id)
                        <option value="{{ $district->id }}">{{ $district->district_name }}</option>
                      @endif
                    @endforeach
                  @endforeach
                </select>
              </div>
            </div>
            <br />
            <br />
            <div class="clearfix" style="background-color: #f2f2f2; margin-bottom: 10px;">
              <div class="col-md-6">
               <label>User Details</label>
              </div>
            </div>
            <div class="clearfix">
              <div class="col-md-6">
                <input type="text" name="fname" id="fname" placeholder="First name" class="form-control" required>
              </div>
              <div class="col-md-6">
                <input type="text" name="lname" id="lname" placeholder="Last name" class="form-control" required>
              </div>
            </div>
            <br />
            <div class="clearfix">
              <div class="col-md-6">
                <input type="text" name="mname" id="mname" placeholder="Middle name" class="form-control" required="">
              </div>
            </div>
            <br />
            <br />

            <div class="clearfix" style="background-color: #f2f2f2; margin-bottom: 10px;">
              <div class="col-md-6">
               <label>Login Details</label>
              </div>
            </div>
            <div class="clearfix">
              <div class="col-md-6">
                <input type="email" name="email" id="email" placeholder="Email address" class="form-control" required>
              </div>
            </div>
            <br />
            <div class="clearfix">
              <div class="col-md-6">
                <input type="text" name="username" id="username" placeholder="Username" class="form-control" required>
              </div>
            </div>
            <br />
            <div class="clearfix">
              <div class="col-md-6">
                <input type="password" name="password" id="password" placeholder="Password" class="form-control" required>
              </div>
              <div class="col-md-6">
                <input type="password" name="cpassword" id="cpassword" placeholder="Confirm password" class="form-control" required>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
      <button type="submit" class="btn btn-proceed" data-dismiss="modal" style="padding: 10px;" >CLOSE</button>
        <button type="submit" class="btn btn-cancel"  style="padding: 10px;" > ADD USER </button>
      </div>
      </form>
    </div>

  </div>
</div>

<!-- Update Modal -->
<div id="editUser" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content modal-md">
    <form method="POST" action="usermanagement/updateum">
    {{ csrf_field() }}
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">User Credentials</h4>
      </div>
      <div class="modal-body">
        <div class="access-controls">
          <div class="firstbatch">
          <div class="clearfix">
              <div class="col-md-6">
              <label>User Type: </label>
                <input type="text" name="uutype" id="uutype" placeholder="First name" class="form-control" readonly>
              </div>
              <div class="col-md-6" id="usctype" style="display: none;">
              <label>Scorecard Type: </label>
                <input type="text" name="uscorecardtype" id="uscorecardtype" placeholder="First name" class="form-control" readonly>
              </div>
              <div class="col-md-6" id="uuserdistrict" style="display: none;">
              <label>District: </label>
                <input type="text" name="udistrict" id="udistrict" placeholder="First name" class="form-control" readonly>
              </div>
            </div>
            <br />
            <br />
            <div class="clearfix" style="background-color: #f2f2f2; margin-bottom: 10px;">
              <div class="col-md-6">
               <label>User Details</label>
              </div>
            </div>
            <div class="clearfix">
              <div class="col-md-6">
                <input type="text" name="ufname" id="ufname" placeholder="First name" class="form-control" readonly>
              </div>
              <div class="col-md-6">
                <input type="text" name="ulname" id="ulname" placeholder="Last name" class="form-control" readonly>
              </div>
            </div>
            <br />
            <div class="clearfix">
              <div class="col-md-6">
                <input type="text" name="umname" id="umname" placeholder="Middle name" class="form-control" readonly>
              </div>
            </div>
            <br />
            <br />

            <div class="clearfix" style="background-color: #f2f2f2; margin-bottom: 10px;">
              <div class="col-md-6">
               <label>Login Details</label>
              </div>
            </div>
            <div class="clearfix">
              <div class="col-md-6">
                <input type="email" name="uemail" id="uemail" placeholder="Email address" class="form-control" readonly>
              </div>
            </div>
            <br />
            <div class="clearfix">
              <div class="col-md-6">
                <input type="text" name="uusername" id="uusername" placeholder="Username" class=" form-group form-control" readonly>
              </div>
              <div class="col-md-6">
                <input type="hidden" name="" id="uid">
                <input type="text" id="ustatus" class="form-control d-none" readonly>
                <select name="uuserstatus" id="uuserstatus" class="form-group form-control d-none">
                  <option value="pending">Pending</option>
                  <option value="approved">Activate</option>
                  <option value="deactivate">Deactivate</option>
                </select>
                <button type="button" class="btn btn-proceed d-none float-right" id="update-ustatus" style="margin-top: 10px;">Submit</button>
              </div>
            </div>
            <br />
            <div class="passthing">
            <div class="clearfix" style="background-color: #f2f2f2; margin-bottom: 10px;">
              <div class="col-md-6">
               <label>Password</label>
              </div>
            </div>
            <div class="clearfix">
              <input type="hidden" name="" id="uid1">
              <div class="col-md-6">
                <input type="password" name="uoldpass" id="uoldpass" placeholder="Old Password" class="form-control">
              </div>
              <div class="col-md-6">
                <input type="password" name="unewpass" id="unewpass" class="form-control" placeholder="New Password">
                <button type="button" class="btn btn-proceed float-right" id="changepass" style="margin-top: 10px;">Change Password</button>
              </div>
            </div>
            </div>
            <br />
          </div>
        </div>
      </div>
      <div class="modal-footer">
      <button type="submit" class="btn btn-cancel" data-dismiss="modal" style="padding: 10px;" >CLOSE</button>
      </div>
      </form>
    </div>

  </div>
</div>

<div class="page-right assesment-right">
	<h3 class="title icon-rafi">User Management</h3>
  <div class="clearfix text-right">
    <button class="btn btn-create btn-md" data-toggle="modal" data-target="#addum"><span class="fa fa-plus"></span> Add User</button>
  </div>
    <div class="box-content white-bg">
      <div class="table-responsive">
         <form action="" method="POST" class="form-scorecard">
          <table class="table table-striped">
            <thead>
              <th>ID</th>
              <th>Name</th>
              <th>User type</th>
              <th>Email</th>
              <th>Status</th>
              <th>Action</th>
            </thead>
            <tbody>
              @foreach($users as $user)
                <tr>
                  <td>{{ $user->id }}</td>
                  <td>{{ ucfirst($user->lname) }}, {{ ucfirst($user->fname) }}</td>
                  <td>{{ $user->type }}</td>
                  <td>{{ $user->email }}</td>
                  
                    @if($user->status == 'pending')
                      <td style="color: red; font-weight: bold;">Pending</td>
                    @elseif($user->status == 'approved')
                      <td style="color: green; font-weight: bold;">Activated</td>
                    @elseif($user->status == 'deactivate')
                      <td style="color: orange; font-weight: bold;">Deactivated</td>
                    
                    @endif
                 
                  <td><button type="button" class="btn btn-create upd" data-toggle="modal" data-target="#editUser" data-action="view"><span class="fa fa-eye"></span></button><button  style="margin-left: 8px" type="button" class="btn btn-proceed edit upd" data-action="edit" data-toggle="modal" data-target="#editUser"><span class="fa fa-edit"></span></button>
                  <button class="btn btn-cancel" type="button" style="margin-left: 5px" data-toggle="tooltip" title="Deletion request will be sent to administrator!" id="delete" data-delid="{{$user->id}}" data-namedelid="{{$user->fname}}"><span class="fa fa-trash"></span></button></td>
                  <td style="display: none">{{ $user->district_name }}</td>
                  <td style="display: none">{{ $user->template_name }}</td>
                  <td style="display: none">{{ $user->fname }}</td>
                  <td style="display: none">{{ $user->lname }}</td>
                  <td style="display: none">{{ $user->mname }}</td>
                  <td style="display: none">{{ $user->username }}</td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </form>
      </div>
    </div>
</div>
@endsection
@section('script')
 <script src="{{ asset('js/usercontrol/usermanagement.js') }}"></script>
@endsection