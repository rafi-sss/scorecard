@extends('layouts.app')

@section('content')
    <div class="page-dashboard">
        <div class="dashboard-cont">
            <div class="dashboard-cont-in">
                <div class="page-right">
                    
                    <div class="dashboard-right">
                        <h3 class="title">Dashboard</h3>
                        <div class="data">
                            <ul class="row">
                                <li class="col-xs-12 col-sm-4">
                                    <div class="col-in data-1">
                                        <div class="data-in-top">
                                            <h2 class="numbers">{{$scorecard}}</h2>
                                            <p>Published Scorecards</p>
                                            <div class="data-in-right">
                                                <img src="img/data1.jpg">
                                            </div>
                                        </div>
                                        <div class=data-in-btm>
                                            <a href="{{ URL::to('scorecard')}}">View Data <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </li>
                                <li class="col-xs-12 col-sm-4">
                                    <div class="col-in data-2">
                                        <div class="data-in-top">
                                            <h2 class="numbers">{{$evaluators}}</h2>
                                            <p>Evaluators</p>
                                            <div class="data-in-right">
                                                <img src="img/data2.jpg">
                                            </div>
                                        </div>
                                        <div class=data-in-btm>
                                            <a href="#" data-toggle="tooltip" title="Evaluators View Data is currently underdevelopment!">View Data <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </li>
                                <li class="col-xs-12 col-sm-4">
                                    <div class="col-in data-3">
                                        <div class="data-in-top">
                                            <h2 class="numbers">0</h2>
                                            <p>Completed Scorecards</p>
                                            <div class="data-in-right">
                                                <img src="img/data3.jpg">
                                            </div>
                                        </div>
                                        <div class=data-in-btm>
                                            <a href="#" data-toggle="tooltip" title="Completed Scorecards View Data is currently underdevelopment!">View Data <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="chart">
                            <div class="chart-container" style="position: relative;">
                                <canvas id="myBarChart"></canvas>
                            </div>
                            <div class="chart-container" style="position: relative;">
                                <canvas id="myChart"></canvas>
                            </div>
                        </div>
                        {{-- <div class="chart">
                        <h3 class="title">Analytics</h3>
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <div class="chart-container" style="position: relative;">
                                        <canvas id="myChart"></canvas>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <div class="chart-container" style="position: relative;">
                                        <canvas id="myBarChart"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div> --}}
                    </div>
                    <div class="dashboard-right">
                        <h3 class="title">Quick Links</h3>
                        <div class="quick-links">
                            <ul class="row">
                                <li class="col-sm-4 col-md-2 col-lg-2 quick-links-item">
                                    <p>
                                        <a class="btn" href="template">T</a>
                                        <span>Templates</span>
                                    </p>
                                </li>
                                <li class="col-sm-4 col-md-2 col-lg-2 quick-links-item">
                                    <p>
                                        <a class="btn create-card" href="{{route ('template_type')}}">S</a>
                                        <span>Create New Scorecard</span>
                                    </p>
                                </li>
                                <li class="col-sm-4 col-md-2 col-lg-2 quick-links-item">
                                    <p>
                                        <a class="btn create-account" href="#">A</a>
                                        <span>Create Accounts</span>
                                    </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@section('script')
<script type="text/javascript" src="{{ asset('js/analytics.js')}}"></script>
@endsection

@endsection