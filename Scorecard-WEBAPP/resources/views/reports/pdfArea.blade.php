<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <title>{{ config('app.name', 'Laravel') }}</title>
</head>
<body>
<!--Inline style-->
<style type="text/css" media="all">
    body{
        background: white;
        width: 90%;
        margin: 20px auto;
        font-family: monospace;
    }
    table{
        border: 5px solid black;
        width: 100%;
        max-width: 100%;
        border-collapse: collapse;
    }
    table, td, th {
        padding: 5px;
        border: 1px solid black;
    }
    img{
        width: 10%;
    }
    .column {
    float: left;
    width: 48.5%;
    margin-left: 5px;
    }
    .row:after {
        content: "";
        display: table;
        clear: both;
    }
    a.dpds {
    padding:  10px;
    background: slategray;
    border-radius:  5px;
    text-decoration:  none;
    font-family:  monospace;
    color:  white;
    font-size:  15px;
    }
    a.dpds:hover{
        background: #6b9ab3;
    }
    .dpdf {
    margin: 20px 0;
    }
    .header, .headtit {
    margin-left: 6px;
    }
</style>
<div class="header">
    <div class="">
        <div class="">
        <img src="{{asset('img/logo-seed.png')}}">
        </div>
        <div class="">
            <div class="text">
                <div class="textwrap">
                    <h3>DISTRICT ASSESSMENT REPORT</h3>
                    <hr>
                    <h5>District of San Remigio 1: <b>PUNTA ELEMENTARY SCHOOL</b></h5>
                </div>
            </div>
    </div>
</div>

<div class="headtit">
    <h5>RAFI SEED Site Validation Findings and Recommendations</h5>
</div>

<div class="row">
    <div class="column">
        <table>
            <tr>
                <td rowspan="2" class="rubric">RAFI SEED Award Rubric</td>
                <td>PUNTA ES</td>
                <td>REMARK</td>
            </tr>
  
            <tr>
                <td>{{$templateKRA[0]->kra_name}} <b>({{$templateKRA[0]->weight}}%)</b></td>
                <td>40</td>
                <td></td>
            </tr>
        </table>
    </div>

    <div class="column">
        <table>
            <tr>
                <td rowspan="2" class="rubric">RAFI SEED Award Rubric</td>
                <td>PUNTA ES</td>
                <td>REMARK</td>
            </tr>
            <tr>
                <td>SEED</td>
                <td></td>
            </tr>
                <tr>
                    <td>{{$templateKRA[1]->kra_name}} <b>({{$templateKRA[1]->weight}}%)</b></td>
                    <td>40</td>
                    <td></td>
                </tr>
        </table>
    </div>
</div>

<div class="dpdf">
<a href="{{ url('/reports/pdfArea/pdfView') }}" class="dpds">Download PDF</a>
</div>
</body>
</html>