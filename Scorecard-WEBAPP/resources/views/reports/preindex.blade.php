@extends('layouts.app')

@section('content')
    <div class="page-dashboard">
        <div class="dashboard-cont-in">
            <div class="page-right">
                <div class="dashboard-right">
                    <h3  class="title">Active Scorecards</h3>            
                    <form method="get" action="reports/indext">

                    <div class="clearfix">
                        <div class="col-sm-offset-2 col-sm-8">
                            <label>Project Name:</label>
                            <select name="project" class="form-control">
                                @foreach($templates as $template)
                                    <option value="{{$template->project_name}}">{{$template->project_name}}</option>
                                @endforeach
                            </select>
                            <br />
                            <button class="btn btn-block btn-lg btn-proceed">
                                <span class=""></span>
                            Proceed</button>
                            <br />
                            <br />
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    
         @if($usertype == 2 || $usertype == 1)
            @if($sctype != 2)
                <div class="dashboard-cont-in">
                    <div class="page-right">
                        <div class="dashboard-right">
                            <h3  class="title">Recommended Schools</h3>            
                            <form method="POST" action="reports/index">
                            {{ csrf_field() }}
                            <div class="clearfix">
                                <div class="col-sm-offset-2 col-sm-8">
                                    <label>Project Name:</label>
                                    <select name="project" class="form-control">
                                        @foreach($templates2 as $template)
                                            <option value="{{$template->project_name}}">{{$template->project_name}}</option>
                                        @endforeach
                                    </select>
                                    <br />
                                    <button class="btn btn-block btn-lg btn-proceed">
                                        <span class=""></span>
                                    Proceed</button>
                                    <br />
                                    <br />
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            @endif

        <div class="dashboard-cont-in">
            <div class="page-right">
                <div class="dashboard-right">
                    <h3  class="title">TOP 5 SCHOOLS</h3>            
                    <form method="POST" action="reports/top5">
                    {{ csrf_field() }}
                    <div class="clearfix">
                        <div class="col-sm-offset-2 col-sm-8">
                            <label>Project Name:</label>
                            <select name="project" class="form-control">
                                @foreach($templates as $template)
                                    <option value="{{$template->project_name}}">{{$template->project_name}}</option>
                                @endforeach
                            </select>
                            <br />
                            <button class="btn btn-block btn-lg btn-proceed">
                                <span class=""></span>
                            Proceed</button>
                            <br />
                            <br />
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="dashboard-cont-in">
            <div class="page-right">
                <div class="dashboard-right">
                    <h3  class="title">Generate Report</h3>            
                    <form method="POST" action="reports/index">
                    {{ csrf_field() }}
                    <div class="clearfix">
                        <div class="row" style="width: 50%;">
                            <div class="col-md-6">
                                <label>List of Projects</label>
                                <select class="form-control" id="proj">
                                    <option selected disabled="">Choose Project</option>
                                    @foreach($projects as $proj)
                                    <option value="{{$proj->template_id}}">{{$proj->project_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6">
                                <div class="ldist" style="display: none;">
                                    <label>List of Districts</label>
                                    <select class="form-control" id="dists">
                                        <option selected disabled="">Choose District</option>
                                        @foreach($districts as $dist)
                                        <option value="{{$dist->id}}">{{$dist->district_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div>
                            <table class="table table-striped">
                                <thead class="viewth">
                                    <th>School Name</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </thead>
                                <tbody id="tbodydata">
                                    <tr style="text-align: center;">
                                        <td colspan="3"><b style="color: red;">No District chosen!</b></td>
                                    </tr>
                                    <!-- <tr>
                                        <td>Punta Elementary School</td>
                                        <td>Complete</td>
                                        <td><input type="checkbox" name=""></td>
                                    </tr>
                                    <tr>
                                        <td>Smaple Elementary School</td>
                                        <td>Incomplete</td>
                                        <td><input type="checkbox" name="" disabled></td>
                                    </tr> -->
                                </tbody>
                            </table>
                        </div>
                        <div>
                            <button type="submit" class="btn btn-proceed btn-lg">Generate</button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        @endif
    </div>
@section('script')
<script type="text/javascript" src="{{ asset('js/reports/reports.js')}}"></script>
@endsection

@endsection