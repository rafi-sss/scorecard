@extends('layouts.app')

@section('content')
    <style type="text/css">
        tr > th{
            text-align: center;
        }
        td{
            text-align: center;
        }
        .p1info{
            color: #3a754e;
        }
    </style>
    <!-- Modal -->
<div id="evalModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content modal-md">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Evaluators</h4>
      </div>
      <div class="modal-body evalBody">
        
      </div>
      <div class="modal-footer">
      <button type="submit" class="btn btn-proceed" data-dismiss="modal" style="padding: 10px;" >CLOSE</button>
      </div>
    </div>

  </div>
</div>
    <div class="page-dashboard">
        <div class="dashboard-cont">
            <div class="container">
                <div class="row">
                    <div class="dashboard-cont-in">
                        <div class="col-xs-12 col-sm-9">
                            <div class="page-right">
                                <div class="dashboard-right">
                                    @if($usertype == 3)
                                    <h3 class="title" id="rank">School Ranking ({{$re}})</h3>
                                    @elseif($usertype == 2 || $usertype == 1)
                                    <h3 class="title" id="reco">Recommended Schools ({{$re}})</h3>
                                    <div class="clearfix text-right">
                                        <a href="{{ URL::to('scorecard/template_type/')}}">
                                            <button class="btn btn-create btn-md" data-toggle="modal" data-target="#addregion">Create Scorecard</button>
                                        </a>
                                    </div>    
                                    <br />
                                    @endif
                                    <div class="clearfix">
                                        <table class="table table-striped">
                                            @if($usertype == 3)
                                            <thead>
                                                <th>Rank</th>
                                                <th>School Name</th>
                                                <th>Evaluated</th>
                                                <th>Total Score</th>
                                                <th>Score Details</th>
                                                <th></th>
                                            </thead>
                                            @elseif($usertype == 2 || $usertype == 1)
                                            <thead>
                                                @if($getTemp->scorecard_type == 2)
                                                <th>Municipal name</th>
                                                @else
                                                <th>District</th>
                                                <th>School Name</th>
                                                @endif
                                                <th>Total Score</th>
                                                <th>Recommended by</th>
                                                <th>Date Recommended</th>
                                            </thead>
                                            @endif
                                            <tbody>
                                                @if($usertype ==  3)
                                                    <?php $counter = 1; ?>
                                                    @foreach($scorecards as $school)
                                                        <tr>
                                                        @if($counter == 1)
                                                        <td class="first-school"><span class="label label-success">{{ $counter }}</span>   </td>
                                                        <td class="first-school">{{ $school->school_name }}</td> 
                                                        <td>
<div class="progress p1" school-scid="{{$school->scid}}">
  <div class="progress-bar" role="progressbar" aria-valuenow="{{$school->count}}"
  aria-valuemin="0" aria-valuemax="{{$totalEva}}" style="width:100%">
    <span>{{$school->count}} of {{$totalEva}}</span>
  </div>
</div>
                                                        </td>
                                                        <td class="first-school p1" school-scid="{{$school->scid}}"><strong>{{ round($school->score, 2) }}% </strong> </td>
                                                        <td class="p1 p1info" school-scid="{{$school->scid}}"><span class="fa fa-info-circle"></span><br /></td>
                                                        <td class="first-school">
                                                        @if(count($recommends) < 1)
                                                        <button class="btn btn-success es">Recommend</button>
                                                        @else
                                                            @foreach($recommends as $reco)
                                                                @if($reco->school_id == $school->id && $reco->project_name == $re)
                                                                <span class="label label-warning">Recommended</span>
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                        </td>
                                                        
                                                        @else
                                                        <td class="first-school"><span class="label label-success">{{ $counter }}</span></td> 
                                                        <td>{{ $school->school_name }}</td> 
                                                        <td><div class="progress p1" school-scid="{{$school->scid}}">
  <div class="progress-bar" role="progressbar" aria-valuenow="{{$school->count}}"
  aria-valuemin="0" aria-valuemax="{{$totalEva}}" style="width:100%">
    <span>{{$school->count}} of {{$totalEva}}</span>
  </div>
</div>
                                                        </td>
                                                        <td class="p1" school-scid="{{$school->scid}}"><strong>{{ round($school->score, 2) }}%</strong></td>
                                                        <td class="p1 p1info" school-scid="{{$school->scid}}"><span class="fa fa-info-circle"></span><br /></td>
                                                        <td>
                                                        @if(count($recommends) < 1)
                                                        <button class="btn btn-success es">Recommend</button>
                                                        @else
                                                            @foreach($recommends as $reco)
                                                                @if($reco->school_id == $school->id && $reco->project_name == $re)
                                                                <span style="font-size: 14px;" class="label label-warning"> Recommended</span>
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                        </td>

                                                        @endif
                                                        <input type="hidden" name="sid" value="{{$school->id}}">
                                                        <input type="hidden" name="projectN" id="projectN" value="{{$re}}">
                                                        <input type="hidden" name="tempID" id="tempID" value="{{$school->template_id}}">
                                                        <input type="hidden" name="scid" id="scid" value="{{$school->scid}}">
                                                    </tr>
                                                    <?php $counter++; ?>
                                                    @endforeach    
                                                @elseif($usertype == 2 || $usertype == 1)
                                                    @foreach($allRecommends as $recos)
                                                        @if($re == $recos->project_name)
                                                            <tr>
                                                                @if($getTemp->scorecard_type == 2)
                                                                <td>{{$recos->school_name}}</td>
                                                                @else
                                                                <td>{{$recos->district_name}}</td>
                                                                <td>{{$recos->school_name}}</td>
                                                                @endif
                                                                <td>{{round($recos->score, 2)}}%</td>
                                                                <td>{{$recos->lname}}, {{$recos->fname}}</td>
                                                                <td>{{date('Y-m-d H:i:s', strtotime($recos->updated_at))}}</td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                        @if($usertype == 3)
                                         {{ $scorecards->links() }}
                                        @elseif($usertype == 2 || $usertype == 1)
                                        {{ $allRecommends->links() }}
                                        @endif
                                    </div>                       
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@section('script')
<script type="text/javascript" src="{{ asset('js/reports/reports.js')}}"></script>
@endsection

@endsection