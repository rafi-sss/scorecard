@extends('layouts.app')

@section('content')
    <style type="text/css">
        tr > th{
            text-align: center;
        }
        td{
            text-align: center;
        }
        .p1info{
            color: #3a754e;
        }
        .awardwrapper {
          background: #f46b45;  /* fallback for old browsers */
          background: -webkit-linear-gradient(to right, #eea849, #f46b45);  /* Chrome 10-25, Safari 5.1-6 */
          background: linear-gradient(to right, #eea849, #f46b45); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */

        }

        .texthere > h1 {
            text-align:  center;
            font-family:  raleway;
            color: #ff6531;
            font-weight:  600;
        }

        .texthere > h4 {
            text-align:  center;
            font-family:  raleway;
            font-size: 25px;
            color: #868686;
        }

        .texthere {
            padding: 15px;
        }

        .modal-content.modal-md {}

        .btncontrol {
            text-align: center;
            margin-top: 25px;
        }
        .btncontrol > button{
        margin-right: 5px;
        }

        button.btn.btn-primary.btnsds {
            border-radius:  0;
            background: #2cb0ff;
            padding: 20px;
            width: 40%;
            font-size:  15px;
            font-weight: 600;
            outline:  none;
            box-shadow: 0px 0px 10px  -1px gray;
        }
        .texthere > h5 {
          font-family: raleway;
            text-align:  center;
            font-size: 20px;
            color: #7b7b7b;
        }
        .animated {
        -webkit-animation-duration: 1s;
        animation-duration: 1s;
        -webkit-animation-fill-mode: both;
        animation-fill-mode: both;
      }

      .animated1 {
        -webkit-animation-duration: .5s;
        animation-duration: .5s;
        -webkit-animation-fill-mode: both;
        animation-fill-mode: both;
      }

      .animated.infinite {
        -webkit-animation-iteration-count: infinite;
        animation-iteration-count: infinite;
      }
      @-webkit-keyframes swing {
        20% {
          -webkit-transform: rotate3d(0, 0, 1, 15deg);
          transform: rotate3d(0, 0, 1, 15deg);
        }

        40% {
          -webkit-transform: rotate3d(0, 0, 1, -10deg);
          transform: rotate3d(0, 0, 1, -10deg);
        }

        60% {
          -webkit-transform: rotate3d(0, 0, 1, 5deg);
          transform: rotate3d(0, 0, 1, 5deg);
        }

        80% {
          -webkit-transform: rotate3d(0, 0, 1, -5deg);
          transform: rotate3d(0, 0, 1, -5deg);
        }

        to {
          -webkit-transform: rotate3d(0, 0, 1, 0deg);
          transform: rotate3d(0, 0, 1, 0deg);
        }
      }

      @keyframes swing {
        20% {
          -webkit-transform: rotate3d(0, 0, 1, 15deg);
          transform: rotate3d(0, 0, 1, 15deg);
        }

        40% {
          -webkit-transform: rotate3d(0, 0, 1, -10deg);
          transform: rotate3d(0, 0, 1, -10deg);
        }

        60% {
          -webkit-transform: rotate3d(0, 0, 1, 5deg);
          transform: rotate3d(0, 0, 1, 5deg);
        }

        80% {
          -webkit-transform: rotate3d(0, 0, 1, -5deg);
          transform: rotate3d(0, 0, 1, -5deg);
        }

        to {
          -webkit-transform: rotate3d(0, 0, 1, 0deg);
          transform: rotate3d(0, 0, 1, 0deg);
        }
      }

      .swing {
        -webkit-transform-origin: top center;
        transform-origin: top center;
        -webkit-animation-name: swing;
        animation-name: swing;
      }

      @-webkit-keyframes zoomIn {
      from {
        opacity: 0;
        -webkit-transform: scale3d(0.3, 0.3, 0.3);
        transform: scale3d(0.3, 0.3, 0.3);
      }

      50% {
        opacity: 1;
      }
    }

    @keyframes zoomIn {
      from {
        opacity: 0;
        -webkit-transform: scale3d(0.3, 0.3, 0.3);
        transform: scale3d(0.3, 0.3, 0.3);
      }

      50% {
        opacity: 1;
      }
    }

    .zoomIn {
      -webkit-animation-name: zoomIn;
      animation-name: zoomIn;
    }
    </style>
    <!-- Modal -->
<div id="evalModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content modal-md">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Evaluators</h4>
      </div>
      <div class="modal-body evalBody">
        
      </div>
      <div class="modal-footer">
      <button type="submit" class="btn btn-proceed" data-dismiss="modal" style="padding: 10px;" >CLOSE</button>
      </div>
    </div>

  </div>
</div>

<div id="recoSchool" class="modal animated1 zoomIn" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content modal-md" style="border: 0;outline: 0;border-radius:  0;">
      <div class="modal-body evalBody" style="padding: 0;">
        <div class="awardwrapper" style="padding: 25px;text-align:  center;">
          <img src="http://192.168.1.110:82/blog/public/img/award.png" style="width: 40%;" class="animated swing">
        </div>
        <div class="texthere">
            <h1>Congratulations!</h1>
            <h4 id="schoolh4">NAPO ELEMENTARY SCHOOL</h4>
            <h5>School of the Year!</h5>
            <div class="btncontrol">
              <button class="btn btn-primary btnsds" data-dismiss="modal">Notify SDS</button>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

    <div class="page-dashboard">
        <div class="dashboard-cont">
            <div class="container">
                <div class="row">
                    <div class="dashboard-cont-in">
                        <div class="col-xs-12 col-sm-9">
                            <div class="page-right">
                                <div class="dashboard-right">
                                    <h3 class="title" id="rank">TOP 5 SCHOOLS ({{$re}})</h3>
                                    <br />
                                    <br />
                                    <div class="clearfix">
                                        <table class="table table-striped">
                                            <thead>
                                                <th>Rank</th>
                                                <th>School Name</th>
                                                <th>Total Score</th>
                                                <th>Score Details</th>
                                                <th></th>
                                            </thead>
                                            <tbody>
                                                    <?php $counter = 1; ?>
                                                    @foreach($scorecards as $school)
                                                        <tr>
                                                        <td class="first-school"><span class="label label-success">{{ $counter }}</span></td> 
                                                        <td>{{ $school->school_name }}</td> 
                                                        <td class="p1" school-scid="{{$school->scid}}"><strong>{{ round($school->score, 2) }}%</strong></td>
                                                        <td class="p1 p1info" school-scid="{{$school->scid}}"><span class="fa fa-info-circle"></span><br />click me</td>
                                                        <td><button class="btn btn-success es">Recommend</button></td>
                                                        <input type="hidden" name="sid" value="{{$school->id}}">
                                                        <input type="hidden" name="projectN" id="projectN" value="{{$re}}">
                                                        <input type="hidden" name="tempID" id="tempID" value="{{$school->template_id}}">
                                                        <input type="hidden" name="scid" id="scid" value="{{$school->scid}}">
                                                        <input type="hidden" name="rank" value="{{$counter}}">
                                                    </tr>
                                                    <?php $counter++; ?>
                                                    @endforeach
                                            </tbody>
                                        </table>
                                    </div>                       
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@section('script')
<script type="text/javascript" src="{{ asset('js/reports/top5.js')}}"></script>
@endsection

@endsection