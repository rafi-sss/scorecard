<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="icon" type="image/png" href="{{ asset('img/icon.png') }}">
    
</head>
<body>
    <div id="app">
        
        <header class="site-header">
            <div class="header-cont">

                <div class="row">
                    <div class="header-left headstyle" style="z-index: 1">
                        <a href="{{route('home')}}"><img class="inline-block imgseed" src="{{asset('img/measureapp-raf.jpg')}}"></a>
                        <!-- <div class="sms-wrapper">
                            <div class="sms-subwrapper">
                                <h4 class="inline-block hidden-xs title" style="margin: 0;">SCORECARD MANAGEMENT SYSTEM</h4>
                            </div>
                        </div> -->
                    </div>
                    <a class="btn btn-cancel btn-mainmenu" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                          <i class="fa fa-bars" aria-hidden="true"></i>
                        </a>
                        <nav class="col-xs-12 col-sm-6 col-lg-7 col-sm-offset-6 col-lg-offset-5 collapse mainmenu-collapse header-right" id="collapseExample"> 
                            <ul class="row">

                                @foreach( $modules as $modul)
                                    <li class="menu-item col-xs-12 col-sm-3">
                                        @if( $modul->hasSubmodule > 0)
                                            <a href="#menu-{{$modul->id}}" data-toggle="collapse" >{{$modul->module_name}} <i class="fa fa-caret-down" aria-hidden="true"></i></a>
                                            <div class="submenu">
                                                <ul class="submenu-in collapse" id="menu-{{$modul->id}}">
                                                    @foreach($submodules as $submodule)
                                                        @if( $modul->id == $submodule->module_id)

                                                            <!-- start -->

                                                              @if( $submodule->hasChild > 0)
                                            <li class="hasChildHover"><a href="#menu-{{$submodule->id}}">{{$submodule->sub_module_name}} <i class="fa fa-caret-down" aria-hidden="true"></i></a>
                                            <!-- <div class="submenu"> -->
                                                <ul class="submenu-in childSubMenu" id="menu-{{$submodule->id}}">
                                                    @foreach($sbchild as $child)
                                                        @if( $submodule->id == $child->parent_id)
                                                            
                                                            <li><a href="{{ URL::to($child->route) }}">{{$child->sub_module_name}}</a></li>

                                                        @endif
                                                    @endforeach
                                                </ul>
                                            <!-- </div> -->
                                        </li>
                                        @else
                                         <li><a href="{{ URL::to($submodule->route) }}">{{$submodule->sub_module_name}}</a></li>

                                        @endif  


                                                            <!-- end -->
                                                           
                                                        @endif
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @else
                                            <a href="{{ URL::to($modul->route)}}">{{$modul->module_name}}</a>
                                        @endif
                                    </li>
                                @endforeach
                                 <li class="menu-item col-xs-12 col-sm-3">
                                    
                                     <a href="#acct" data-toggle="collapse" style="text-transform: capitalize;"><span class="fa fa-user fa-2x"></span> Hello, {{$getName->fname}}!</a>

                                    <div class="submenu">
                                        <ul class="submenu-in collapse" id="acct">
                                            @if($getName->user_type == 1)
                                            <li style="background: #ffc817;" id="delr" data-toggle="tooltip" title="Link underdevelopment!"><a href="#"><label class="label label-danger">5</label> Request</a></li>
                                            @endif
                                            <li style="background: #ffc817;"><a href=" {{  route('out') }} ">Logout</a></li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>

                        </nav>
                    
                    
                </div>
            </div>
        </header>
        <div class="page">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-3">
                        @include('layouts.sidebar')
                    </div>
                    <div class="col-xs-12 col-md-9">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    <footer>
        <h5 class="copyright">&copy; Powered by CSS </> </h5>
    </footer>
    <!-- Scripts -->
    <script src="{{ asset('js/jquery-3.2.1.min.js')}}"></script>
    <script type="text/javascript">
        
        var APP_URL = {!! json_encode(url('/')) !!};
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
    </script>
    <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/main.js')}}"></script>
    <script src="{{ asset('js/Chart.min.js')}}"></script>
    @yield('script')
</body>
</html>

