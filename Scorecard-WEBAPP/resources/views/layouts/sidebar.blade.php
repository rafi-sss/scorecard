    <!-- @if(Auth::user()->user_type == 1)
         <div class="sidebar-menu">
            <h5 class="title sidebar-title icon-rafi">
                <p class="s-title-in">
                    <span class="block">Account Activation</span>
                    <a href="#">View All</a>
                </p>

            </h5>
            <ul class="sidebar-list">
                <li>Sample User 1</li>
                <li>Sample User 1</li>
                <li>Sample User 1</li>
                <li>Sample User 1</li>
                <li>Sample User 1</li>
            </ul>
        </div>
    @endif -->
   <style type="text/css">
       .seed{
            float: left;
            width: 50px;
            margin-right: 10px;
            margin-top: -4px;
       }
   </style>
    <div class="text-center sidebar-menu">
        <img class="inline-block img-responsive imglog" src="{{asset('img/logo-seed.png')}}">
    </div>
    <div class="sidebar-menu menu-scorecard">
        <h5 class="title sidebar-title icon-rafi icon-card">
            <p class="s-title-in">
                <!-- <span class="fa fa-file-text-o fa-2x" style="float:  left;padding-right: 15px; color: gray"></span> --> <img src="{{asset('img/logo-seed-s.png')}}" class="seed"><span class="block">Active Scorecards</span>
                <a href="{{ route('viewall') }}">View All</a>
            </p>

        </h5>
        <ul class="sidebar-list">
            @if($count == 0)
            <hr>
            <div class="no-scorecard"><b style="color: red; text-align: center;">No active Scorecards!</b></div>
            @else
            @foreach($scorecards as $sc)
                <li><span class="fa fa-th-list fa-2x" style="float:  left;font-size:  20px;margin-right: 10px; color: gray"></span> <a href="{{route ('view', $sc->id)}}" style="color: black"><strong>{{$sc->project_name}} </strong><br><i>
                    @if($sc->template_type == 2)
                        @foreach($mst_municipality as $mm)
                            @if($mm->id == $sc->school_id)
                                ({{$mm->municipality_name}})
                            @endif
                        @endforeach
                    @else
                        @foreach($mst_schools as $sch)
                            @if($sch->id == $sc->school_id)
                                ({{$sch->school_name}})
                            @endif
                        @endforeach
                    @endif
                    </i></a></li>
            @endforeach
            @endif
        </ul>
    </div>
