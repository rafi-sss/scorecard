@extends('layouts.app')
@section('content')
	
	<div class="page-right assesment-right">
		<h3 class="title icon-rafi">Scorecards</h3>
		
		<div class="clear-fix pull-left">
			<div class="row">
				<div class="form-group col-md-6">
					<label>Project name:</label>
					<select id="prjname">
						<option selected="" disabled="">Project name</option>
						<option value="0">All</option>
						@foreach($projectlist as $pr)
						<option data-view="1" value="{{$pr->project_name}}" id="{{$pr->scorecard_type}}">{{$pr->project_name}}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group col-md-6">
					<div class="schoolsname" style="visibility: hidden;">
						<label class="livename">School Name:</label>
						<input type="text" name="livesearch" placeholder="School name" id="lives">
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix">
			<br>
			<a class="btn btn-create pull-right" href="{{route('template_type')}}" style="margin: 0; padding: 10px">Add New Scorecard</a>
		</div>
		<br />
		
	    <div class="box-content white-bg">
	      <div class="table-responsive">
	         <form action="" method="POST" class="form-scorecard">
	         	<div class="dash-talbe">
	          <table class="table table-striped">
	            <thead class="text-center">
		            <tr class="scorecard-tr">
		              <th>Project Name</th>
		              <th>Template</th>
		              <th>Status</th>
		              <th>Action</th>
		          	</tr>
	            </thead>
	            <tbody class="scorecard-alignment">
	              @if($count == 0)
	              	<tr>
	              		<td colspan="4" style="text-align: center; color: red; font-weight: bold;">No scorecards to show!</td>
	              	</tr>
	              @else
	              @foreach($scorecards as $scorecard)
	              	@if($scorecard->status == '1')
	              		<?php $pathTo = '#'; ?>
	              		<?php $disabled = 'disabled'; ?>
	              		<?php $textValue = '<span style="color:#1688b7; font-weight: bold;">Published</span>'; ?>
	              	@else
	              		<?php $pathTo = URL::to('/scorecard/edit/'.$scorecard->id);  ?>
	              		<?php $disabled = ''; ?>
	              		<?php $textValue = '<span style="color: green; font-weight: bold;">Draft</span>'; ?>
	              	@endif
	                <tr>
	                  <td><div class="clip" ><strong>{{ $scorecard->project_name }}</strong> <br><i style="font-size: 13px;">

	                  	@if($scorecard->template_type == 2)
	                  		@foreach($mst_municipality as $mm)
	                            @if($mm->id == $scorecard->school_id)
	                                ({{$mm->municipality_name}})
	                            @endif
                        	@endforeach
	                  	@else
	                  		@foreach($mst_schools as $sch)
	                            @if($sch->id == $scorecard->school_id)
	                                ({{$sch->school_name}})
	                            @endif
                        	@endforeach
	                  	@endif
	                  	
	                  </i></div></td>
	                  <td><div class="clip">{{ $scorecard->template_name }}</div></td>
	                  <td>{!! $textValue !!}</td>
	                  <td class="btn-area"><a  class="btn btn-create view" href="{{ URL::to('/scorecard/view/'.$scorecard->id) }}"><span class="fa fa-eye"></span></a><a class="btn btn-proceed edit" data-toggle="modal" data-target="#editUser" href="{{ $pathTo }}"><span class="fa fa-edit"></span></a>

	                    @if($scorecard->template_type == 2)
	                  		@foreach($mst_municipality as $mm)
	                            @if($mm->id == $scorecard->school_id)
	                                <button class="btn btn-cancel delete" type="button" data-toggle="tooltip" title="Deletion request will be sent to administrator!" id="delete" data-delid="{{$scorecard->id}}" data-namedelid="{{$mm->municipality_name}}"><span class="fa fa-trash"></span> </button>
	                            @endif
                        	@endforeach
	                  	@else
	                  		@foreach($mst_schools as $sch)
	                            @if($sch->id == $scorecard->school_id)
	                                <button class="btn btn-cancel delete" type="button" data-toggle="tooltip" title="Deletion request will be sent to administrator!" id="delete" data-delid="{{$scorecard->id}}" data-namedelid="{{$sch->school_name}}"><span class="fa fa-trash"></span> </button>
	                            @endif
                        	@endforeach
	                  	@endif

	                  	</td>
	                </tr>
	              @endforeach
	              @endif
	            </tbody>
	          </table>
	      </div>
	          <div class='pgs'>{{ $scorecards->links() }}</div>
	        </form>
	      </div>
	    </div>
	</div>
@endsection
@section('script')
<script type="text/javascript" src="{{ asset('js/scorecard/index.js')}}"></script>
@endsection