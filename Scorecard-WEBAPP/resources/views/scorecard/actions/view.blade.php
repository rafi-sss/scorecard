@extends('layouts.app')
@section('content')
	<div class="main-wrapper">
		<div class="page-right assesment-right">
				<h3 class="float-right badge score">{{$score}}</h3>
							<h3 class="title icon-rafi">{{ $scorecard->project_name }}</h3>
							<form action="add_scorecard" method="POST" class="form-scorecard">
								{{ csrf_field() }}
								<div class="row">
									<div class="col-xs-12 col-sm-2 col">
										<label>Project Name:</label>
									</div>
									
									<div class="col-xs-12 col-sm-10 col">
										<input type="text" name="project_name" value="{{ $scorecard->project_name }}" readonly>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-sm-2 col clearboth">
										<label>Template:</label>
									</div>
									<div class="col-xs-12 col-sm-10 col">
										<input type="hidden" name="template" value="{{ $templates->id }}">
										<input type="text" cla name="templatename" id="templatename" value="{{ $templates->template_name }}" readonly>
									</div>
								</div>
								<div class="row">
									@if($templates->template_type == 2)
										<div class="col-xs-12 col-sm-2 col clearboth">
										<label>Municipal Name:</label>
										</div>
										<div class="col-xs-12 col-sm-4 col">
											<input type="text" readonly="" value="{{$schools->municipality_name}}">
										</div>
									@else
										<div class="col-xs-12 col-sm-2 col clearboth">
											<label>School:</label>
										</div>
										<div class="col-xs-12 col-sm-4 col">
											<input type="text" readonly="" value="{{$schools->school_name}}">
										</div>
										<div class="col-xs-12 col-sm-2 col">
											<label>District:</label>
										</div>
										<div class="col-xs-12 col-sm-4 col">
											<input type="text" readonly="" value="{{$district->district_name}}">
										</div>
									@endif									
									<div class="col-xs-12 col-sm-2 clearboth col">
										<label>Start Date:</label>
									</div>
									<div class="col-xs-12 col-sm-4 col">
										<input type="date" name="start_date" value="{{date('Y-m-d', strtotime($scorecard->start_date))}}" style="">
									</div>
									<div class="col-xs-12 col-sm-2 col">
										<label>End Date:</label>
									</div>
									<div class="col-xs-12 col-sm-4 col">
										<input type="date" name="end_date" value="{{date('Y-m-d', strtotime($scorecard->end_date))}}" style="">
									</div>
									<div class="col-xs-12 col-sm-2  clearboth col">
										<label>No. of Assesment</label>
									</div>
									<div class="col-xs-12 col-sm-4 col">
										<input type="text" name="noa" value="{{ $scorecard->no_assessment }}" readonly>
									</div>
									<div class="col-xs-12 col-sm-2 col">
										<label>Status</label>
									</div>
									<div class="col-xs-12 col-sm-4 col">
											@if($scorecard->status == '1')
											<span class="statusViewP" style="color: #1688b7; font-weight: bold;">Published</span>
											@else
											<span class="statusViewD" style="color: green; font-weight: bold;">Draft</span>
											@endif
									</div>
								</div>

							</form>
						</div>

						<!--Assessment Round Details-->
						<div class="parent" id="accordion">
							@for($i=1;$i<=$scorecard->no_assessment;$i++)
								@foreach($scorecard_assessment_rounds as $sars)
								@if($sars->round_num == $i)
									@if($sars->round_name == 3 || $sars->round_name == 4)
									<div class="page-right assesment-right panel" >
									<h3 class="title icon-rafi" data-parent="#accordion" data-toggle="collapse" data-target="#collapse{{$i}}"> {{$sars->strname}} <span data-parent="#accordion" data-toggle="collapse" aria-expanded="false" class="pull-right fa fa-caret-up" id="toggleCollapse" href="#collapse{{$i}}"></span></h3>
									<div class="collapse in assesmentCollapse" id="collapse{{$i}}">
										<form action="#" id="rafiRate" method="POST" class="form-scorecard">
											{{ csrf_field() }}
											<div class="row">
												<div class="col-xs-12 col-sm-2 col">
													<label>Assessment Round:</label>
												</div>
												
												<div class="col-xs-12 col-sm-10 col">
													<input type="text" name="project_name" value="Round {{$i}}" readonly>
												</div>
											</div>
											<div class="row">
												<div class="col-xs-12 col-sm-2 clearboth col">
													<label>Start Date:</label>
												</div>
												<div class="col-xs-12 col-sm-4 col">
													<input type="date" name="start_date" value="{{date('Y-m-d', strtotime($sars->start_date))}}">
												</div>
												<div class="col-xs-12 col-sm-2 col">
													<label>End Date:</label>
												</div>
												<div class="col-xs-12 col-sm-4 col">
													<input type="date" name="end_date" value="{{date('Y-m-d', strtotime($sars->end_date))}}">
												</div>
												<div class="col-xs-12 col-sm-2  clearboth col">
													<label>Weight:</label>
												</div>
												<div class="col-xs-12 col-sm-4 col">
													<input type="text" class="w-initial" name="weight"value="{{$sars->weight}}" readonly="" style="width: 50%">
												</div>
												<div class="col-xs-12 col-sm-2 col">
													<label>Rate:</label>
												</div>
												<div class="col-xs-12 col-sm-4 col">
													<input type="number" name="rate" placeholder="Rate" data-weight="{{$sars->weight}}" data-scid="{{$scorecard->id}}" data-roundnum="{{$i}}" required="">
												</div>
												
											<div style="margin-right: 20px;">
												<input type="submit" value="Rate" class="btn btn-proceed pull-right" style="padding: 10px;width:  100px;">
											</div>
											</div>
										</form>
										
									</div>
								</div>
									@else
									<div class="page-right assesment-right panel" >
									<h3 class="title icon-rafi" data-parent="#accordion" data-toggle="collapse" data-target="#collapse{{$i}}"> {{$sars->strname}} <span data-parent="#accordion" data-toggle="collapse" aria-expanded="false" class="pull-right fa fa-caret-up" id="toggleCollapse" href="#collapse{{$i}}"></span></h3>
									<div class="collapse in assesmentCollapse" id="collapse{{$i}}">
										<form action="" method="POST" class="form-scorecard">
											{{ csrf_field() }}
											<div class="row">
												<div class="col-xs-12 col-sm-2 col">
													<label class="{{$sars->round_name}}">Assessment Round:</label>
												</div>
												
												<div class="col-xs-12 col-sm-10 col">
													<input type="text" name="project_name" value="Round {{$i}}" readonly>
												</div>
											</div>
											<div class="row">
												<div class="col-xs-12 col-sm-2 clearboth col">
													<label>Start Date:</label>
												</div>
												<div class="col-xs-12 col-sm-4 col">
													<input type="date" name="start_date" value="{{date('Y-m-d', strtotime($sars->start_date))}}">
												</div>
												<div class="col-xs-12 col-sm-2 col">
													<label>End Date:</label>
												</div>
												<div class="col-xs-12 col-sm-4 col">
													<input type="date" name="end_date" value="{{date('Y-m-d', strtotime($sars->end_date))}}">
												</div>
												<div class="col-xs-12 col-sm-2  clearboth col">
													<label>Weight:</label>
												</div>
												<div class="col-xs-12 col-sm-4 col">
													<select class="w-initial" name="weight">
														<option value="{{$sars->weight}}" selected="">{{$sars->weight}}</option>
													</select>
												</div>
												<div class="col-xs-12 col-sm-2 col">
													<label>No. of Evaluators:</label>
												</div>
												<div class="col-xs-12 col-sm-4 col">
													<select class="w-initial" name="no_of_evaluators">
														<option value="{{$sars->no_of_evaluators}}" selected="">{{$sars->no_of_evaluators}}</option>
													</select>
												</div>
											</div>
										</form>
										<div class="evaluatorsWrap">
										<h3 class="title icon-rafi" id="listOfEval">List of Evaluators</h3>
											<table class="table table-striped">
												<thead>
													<tr>
														<th>Evaluator Name</th>
														<th>Username</th>
														<th>Email</th>
														<th>Mobile No</th>
														@if($sars->strid == 1)
														<th>Weight</th>
														@endif
														<th>Expiry Date</th>
														<th>Action</th>
														<th></th>
													</tr>
												</thead>
												<tbody>
													@foreach($sar as $sa)
													@if($sa->round_num == $i)
														<tr>
															<td>{{$sa->fname}} {{$sa->mname}} {{$sa->lname}}</td>
															<td>{{$sa->username}}</td>
															<td>{{$sa->email}}</td>
															<td>{{$sa->mobile_no}}</td>
															@if($sa->round_name == 1)
															<td>{{$sa->evaluator_weight}}</td>
															@endif
															<td>{{date('Y-m-d', strtotime($sa->expiry_date))}}</td>
															<td><button class="btn btn-proceed" id="editDetails" data-evaluator-id="{{$sa->id}}" data-rid="{{$sars->strid}}"><span class="fa fa-edit"></span>Edit</button></td>
															<td>
																@foreach( $submit_evaluator as $eval)
																
																	@if($sa->id == $eval->evaluator_id)
																	<i class="fa stats fa-check-circle" aria-hidden="true"></i>
																	
																	@endif
																@endforeach
															</td>
														</tr>
													@endif
													@endforeach
												</tbody>
											</table>
										</div>
									</div>
								</div>
									@endif
								@endif
								@endforeach
							@endfor
						</div>
						@include('scorecard.actions.modal.editEval')
				</div>
				
@endsection
@section('script')
<script type="text/javascript" src="{{ asset('js/scorecard/scorecard.js')}}"></script>
@endsection