<style type="text/css">
  .margin > label{
    margin-left: 15px;
  }

  .header-bg{
    text-align:  center;
    background: linear-gradient(141deg, #0fb8ad 0%, #1fc8db 51%, #2cb5e8 75%);
  }

  span.fa.fa-user-circle {
    color: white;
    text-align: center;
    font-size: 100px;
    margin-top: 10px;
  }

  .evalUsername {
    font-size:  25px;
    color: white;
    font-weight:  bold;
  }

  .remove-border{
    border: 0;
    border-radius: 0;
  }

  .searchresults {
    min-width: 38%;
    z-index: 1;
    position: absolute;
    border: 1px solid #cdcdcd;
    box-shadow: 0px 6px 10px -8px;
    background: #ffffff;
    padding: 10px;
    margin: 0 0;
    border-top: 0.5px solid #cdcdcd;
    max-width: 40%;
  }

  .searchresults > ul > li
  {
    color: #6d6d6d;
    padding: 10px;
    border-bottom: 1px solid #dedede;
  }

  .searchresults > ul > li:last-child{
    border-bottom: 0;
  }

  input#evalname:focus {
    outline: 0;
    box-shadow: none;
    border: 1px solid #cdcdcd;
}
</style>
<div id="editEval" class="modal fade" role="dialog" data-backdrop="static">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content modal-md remove-border">
    <form id="evalForm" action="{{route('updateEvaluators')}}" method="POST">
    {{ csrf_field() }}
      <div class="modal-header header-bg">
        <span class="fa fa-user-circle"></span>
        <div class="evalUsername"></div>
      </div>
      <div class="modal-body">
          <input type="hidden" id="edit-evalid" name="evalId">
          <div class="evalWrapper">
              <div class="form-group">
                <label>Search Evaluator: </label>
                <div class="input-group" style="width: 40%;">
                    <span class="input-group-addon" id="basic-addon1" style="border-radius: 0;"><i class="fa fa-search"></i></span>
                    <input type="text" class="form-control" placeholder="Evaluator Name" aria-describedby="basic-addon1" name="evalname" id="evalname" style="border-radius: 0; border-bottom: 1px solid #cdcdcd" autocomplete="off">
                </div>
                <div class="searchresults" style="display: none;">
                  <ul class="searchresultsList">
                  </ul>
                </div>
              </div>
              <hr style="border-color: #cecece">
              <div class="form-group row">
                <div class="">
                <span class="margin"><label>Evaluator Name:</label></span>
                </div>
                <div class="col-sm-4 no-pad"><input type="text" class="form-control" placeholder="Last Name" name="lname" required="" autocomplete="off"></div>
                <div class="col-sm-4 no-pad"><input type="text" class="form-control" placeholder="Middle Name" name="mname" autocomplete="off"></div>
                <div class="col-sm-4 no-pad"><input type="text" class="form-control" placeholder="First Name" name="fname" required="" autocomplete="off"></div>
              </div>
              <div class="form-group row">
                <div class="">
                <span class="margin"><label>Email Address:</label></span>
                </div>
                <div class="col-sm-12"><input type="email" class="form-control" placeholder="Email Address" name="ea" required="" autocomplete="off"></div>
              </div>
              <div class="form-group row">
                <div class="col-sm-6">
                  <span class="margin">
                    <label style="margin-left: 0;">Mobile Number:</label>
                    <input type="number" class="form-control" placeholder="Mobile Number" name="mn" required="" autocomplete="off">
                  </span>
                </div>
                <div class="col-sm-6" id="addComp">
                    
                </div>
              </div>
          </div>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-proceed" style="padding: 10px;" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-cancel"  style="padding: 10px;" > Update Evaluator </button>
      </div>
      </form>
    </div>

  </div>
</div>