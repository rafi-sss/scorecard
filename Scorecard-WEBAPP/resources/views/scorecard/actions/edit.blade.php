@extends('layouts.app')
@section('content')
	<div class="dash-dis">
		<div class="page-right assesment-right">
							<h3 class="title icon-rafi">Edit Scorecard</h3>
							<form action="update/{{ $scorecard->id }}" method="POST" class="form-scorecard">
								{{ csrf_field() }}
								<div class="row">
									<div class="col-xs-12 col-sm-2 col">
										<label>Project Name:</label>
									</div>
									
									<div class="col-xs-12 col-sm-10 col">
										<input type="text" name="project_name" value="{{ $scorecard->project_name }}">
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-sm-2 col clearboth">
										<label>Template:</label>
									</div>
									<div class="col-xs-12 col-sm-10 col">
										<input type="hidden" name="template" value="{{ $templates->id }}">
										<input type="text" name="templatename" id="templatename" value="{{ $templates->template_name }}" readonly>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-sm-2 col clearboth">
										<label>Area:</label>
									</div>
									<div class="col-xs-12 col-sm-4 col">
										<select name="area">
											@foreach($areas as $allArea)
												@if($areaSelected == $allArea)
													<option value="{{$allArea->id}}" selected="">{{$allArea->area}}</option>
												@else
													<option value="{{$allArea->id}}">{{$allArea->area}}</option>
												@endif
											@endforeach
										</select>
									</div>
									<div class="col-xs-12 col-sm-2 col">
										<label>District:</label>
									</div>
									<div class="col-xs-12 col-sm-4 col">
										<select name="district">
											@foreach($district as $allDistrict)
												@if($districtSelected == $allDistrict)
													<option value="{{$allDistrict->id}}" selected="">{{$allDistrict->district}}</option>
												@else
													<option value="{{$allDistrict->id}}">{{$allDistrict->district}}</option>
												@endif
											@endforeach
										</select>
									</div>
									<div class="col-xs-12 col-sm-2 clearboth col">
										<label>Start Date:</label>
									</div>
									<div class="col-xs-12 col-sm-4 col">
										<input type="date" name="start_date" value="{{date('Y-m-d', strtotime($scorecard->start_date))}}" style="">
									</div>
									<div class="col-xs-12 col-sm-2 col">
										<label>End Date:</label>
									</div>
									<div class="col-xs-12 col-sm-4 col">
										<input type="date" name="end_date" value="{{date('Y-m-d', strtotime($scorecard->end_date))}}" style="">
									</div>
									<div class="col-xs-12 col-sm-2  clearboth col">
										<label>No. of Assesment</label>
									</div>
									<div class="col-xs-12 col-sm-4 col">
										<input type="text" name="noa" value="{{ $scorecard->no_assessment }}">
									</div>
									<div class="col-xs-12 col-sm-2 col">
										<label>Status</label>
									</div>
									<div class="col-xs-12 col-sm-4 col">
										<input type="text" value="D" name="status" style="font-weight: bold; border: none;outline: none; cursor: default; display: none;" readonly="">Draft
									</div>
								</div>
								<p class="text-right">
									<button class="btn btn-create" type="submit">Update Scorecard</button>
									<button class="btn btn-cancel">Cancel</button>
								</p>
							</form>
						</div>

						<!--Assessment Round Details-->
						<div class="parent" id="accordion">
							@for($i=1;$i<=$scorecard->no_assessment;$i++)
								<div class="page-right assesment-right panel">
									<h3 class="title icon-rafi">Assessment Round Detail - {{$i}} <span data-parent="#accordion" data-toggle="collapse" aria-expanded="false" class="pull-right fa fa-caret-down" id="toggleCollapse" href="#collapse{{$i}}"></span></h3>
									<div class="collapsed assesmentCollapse" id="collapse{{$i}}">
										<form action="" method="POST" class="form-scorecard">
											{{ csrf_field() }}
											<div class="row">
												<div class="col-xs-12 col-sm-2 col">
													<label>Assessment Round:</label>
												</div>
												
												<div class="col-xs-12 col-sm-10 col">
													<input type="text" name="project_name" value="Round {{$i}}">
												</div>
											</div>
											<div class="row">
												<div class="col-xs-12 col-sm-2 clearboth col">
													<label>Start Date:</label>
												</div>
												<div class="col-xs-12 col-sm-4 col">
													<input type="date" name="start_date" value="{{date('Y-m-d', strtotime($scorecard->start_date))}}">
												</div>
												<div class="col-xs-12 col-sm-2 col">
													<label>End Date:</label>
												</div>
												<div class="col-xs-12 col-sm-4 col">
													<input type="date" name="end_date" value="{{date('Y-m-d', strtotime($scorecard->end_date))}}">
												</div>
												<div class="col-xs-12 col-sm-2  clearboth col">
													<label>Weight:</label>
												</div>
												<div class="col-xs-12 col-sm-4 col">
													<select class="w-initial" name="no_of_assessment">
														<option value="100" selected="">100</option>
														<option value="50">50</option>
														<option value="30">30</option>
													</select>
													<span>%</span>
												</div>
												<div class="col-xs-12 col-sm-2 col">
													<label>No. of Evaluators:</label>
												</div>
												<div class="col-xs-12 col-sm-4 col">
													<select class="w-initial" name="no_of_assessment" id="noe">
														<option value="1" selected="">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
													</select>
												</div>
											</div>
											<p class="text-right">
												<a class="btn btn-create" id="getEval">Setup</a>
											</p>
										</form>
									</div>
								</div>
							@endfor
						</div>
						<div class="button-wrapper">
							<a class="btn btn-create btn-block">Publish</a>
							<a class="btn btn-cancel btn-block">Cancel</a>
							<a class="btn btn-proceed btn-block" href="{{route('main_scorecard')}}">Back to Scorecard</a>
						</div>
				</div>
				<!--Modal-->
				<div id="myModal" class="modal fade" role="dialog">
				  <div class="modal-dialog">
				    <!-- Modal content-->
				    <form action="" id="frmSubmit" method="POST">
				    {{ csrf_field() }}
					    <div class="modal-content">
					      <div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal">&times;</button>
					        <h4 class="modal-title">Assign Evaluators</h4>
					      </div>
					      <div class="modal-body">
					        <div class="evaluators-list">
					        	<table class="table table-striped">
					        		<thead>
					        			<tr>
					        				<th>Evaluators ID</th>
					        				<th>Action</th>
					        			</tr>
					        		</thead>
					        		<tbody id="evalBody">
					        			<!-- <tr>
					        				<td>Eval-009374123</td>
					        				<td>
					        					<div class="alignment">
					        						<input type="checkbox" />
					        						<label class="assignLabel">Assign</label>
					        					</div>
					        				</td>
					        			</tr>
					        			<tr>
					        				<td>Eval-45646344</td>
					        				<td>
					        					<div class="alignment">
					        						<input type="checkbox" />
					        						<label class="assignLabel">Assign</label>
					        					</div>
					        				</td>
					s        			</tr> -->
					        		</tbody>
					        	</table>
					        </div>
					      </div>
					      <div class="modal-footer">
					        <button type="button" id="btnSubmit" class="btn btn-proceed" style="padding: 10px;" ><span class="fa fa-hand-pointer-o"></span> Assign Evaluator(s)</button>
					      </div>
					    </div>
					</form>
				  </div>
				</div>
@endsection
@section('script')
<script type="text/javascript" src="{{ asset('js/scorecard/scorecard.js')}}"></script>
@endsection