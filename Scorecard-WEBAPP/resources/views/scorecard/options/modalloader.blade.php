<!--Modal-->
<div id="loader" class="modal fade in" role="dialog" data-backdrop="static" style="background: #C9D6FF;background: -webkit-linear-gradient(to right, #E2E2E2, #C9D6FF);background: linear-gradient(to right, #E2E2E2, #C9D6FF);">
	<div class="loader-wrapper">
	  <div class="modal-dialog loaderdia">

	    <!-- Modal content-->
	    <div class="modal-content loadercont">
	      
	      <div class="modal-body loaderbody">
	        <div class="loaderwrapper"><img src="{{asset('img/hourglass.gif')}}" class="loaderimg">
	        <p style="color: #424242;">Creating...</p>
			<p style="color: #505050;font-size: 25px;font-weight: 400;margin-top: 5px;font-family:  raleway;">Creating Scorecards, this may take 5 - 10 minutes.</p></div>
	      </div>
	    </div>

	  </div>
    </div>
</div>