<div class="row" style="margin: auto; width: 95%;">
  	<form action="{{route('createBulkScorecards')}}" method="POST" id="allschools">
		{{ csrf_field() }}
		<div class="row">
			<div class="col-xs-12 col-sm-2 col clearboth">
				<label>Project Name:</label>
			</div>
			<div class="col-xs-12 col-sm-10 col">
				<input type="text" placeholder="Project Name" name="proj_name" id="proj_name" required>
			</div>
		</div>
  		<div class="row">
  			<input type="hidden" name="proj_name_dump" value="{{ $templates->template_code }} 2018">
  			<input type="hidden" name="temp_id" value="{{$templates->id}}">
  			<input type="hidden" name="level" value="{{$level}}">
  			<input type="hidden" name="temptype" value="{{$templates->template_type}}">
			<div class="col-xs-12 col-sm-2 col clearboth">
				<label>Start Date:</label>
			</div>
			<div class="col-xs-12 col-sm-10 col">
				<input type="date" name="start_date" id="start_date" required>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-2 col clearboth">
				<label>End Date:</label>
			</div>
			<div class="col-xs-12 col-sm-10 col">
				<input type="date" name="end_date" id="end_date" required>
			</div>
		</div>		
		<div class="roundsAll">
		</div>
		<div class="pull-right">
			<button class="btn btn-proceed" type="submit">Create Scorecards</button>
			<button class="btn btn-cancel">Back to Scorecards</button>
		</div>
	</form>
</div>
