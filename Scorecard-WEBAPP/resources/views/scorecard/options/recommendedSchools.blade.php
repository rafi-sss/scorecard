<div class="row" style="margin: auto; width: 95%;">
  	<form action="{{route('chosenSchool2')}}" method="POST" id="chooseSchool">
  		{{ csrf_field() }}
  		<div class="">
  			<input type="hidden" name="level" value="{{$level}}">
  			<input type="hidden" name="proj_name_dump" value="{{ $templates->template_code }} 2018">
  			<input type="hidden" name="temp_id" value="{{$templates->id}}">
  			<table class="table table-striped">
		        <thead>
		          <th>Code</th>
		          <th>School Name</th>
		          <th>District</th>
		          <th>Choose</th>
		        </thead>
		        <tbody id="schools1">
		        </tbody>
		    </table>
  		</div>
  		<div class="controls">
  			<div class="row">
				<div class="col-xs-12 col-sm-2 col clearboth">
					<label>Base Project</label>
				</div>
				<div class="col-xs-12 col-sm-10">
					<select class="form-control input-lg" name="baseProj1" id="baseProj1">
						@foreach($projects as $project)
							<option value="{{$project->project_name}}">{{$project->project_name}}</option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-2 col clearboth">
					<label>Project Name</label>
				</div>
				<div class="col-xs-12 col-sm-10 col">
					<input type="text" name="proj_name1" id="proj_name1" required>
				</div>
			</div>
	  		<div class="row">
				<div class="col-xs-12 col-sm-2 col clearboth">
					<label>Start Date</label>
				</div>
				<div class="col-xs-12 col-sm-10 col">
					<input type="date" name="start_date" id="start_date" required="">
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-2 col clearboth">
					<label>End Date</label>
				</div>
				<div class="col-xs-12 col-sm-10 col">
					<input type="date" name="end_date" id="end_date" required="">
				</div>
			</div>
			<div class="roundsChoose">
			</div>
			<div class="pull-right">
				<button type="submit" class="btn btn-proceed" >Create Scorecards</button>
				<button class="btn btn-cancel">Back to Scorecards</button>
			</div>
		</div>
	</form>
</div>