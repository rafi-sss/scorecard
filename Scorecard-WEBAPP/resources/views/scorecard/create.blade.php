@extends('layouts.app')
@section('content')
	<div class="page-right assesment-right">
						@if($templates->template_type == 2)
							<h3 class="title icon-rafi">New Scorecard</h3>
							<h4><b>{{ $templates->template_name }}</b></h4>
							<div class="form-scorecard">
								<input type="hidden" id="temp_id" value="{{$templates->id}}">
								<div class="row">
									<!-- Centered Tabs -->
									<div class="col-xs-12 col-sm-12 clearboth col">
										<ul class="nav nav-tabs nav-justified">
										  <li class="active">
										  	<a data-toggle="tab" href="#all" id="alls">All Municipalities</a>
										  </li>
										  <li>
										  	<a data-toggle="tab" href="#choose" id="choos" data-temptype = "{{$templates->template_type}}">Choose Municipalities</a>
										  </li>
										</ul>
									</div>
									<div class="tab-content">
									  <div id="all" class="tab-pane fade in active">
									  	@include('scorecard.options.allschools')
									  </div>

									  <div id="choose" class="tab-pane fade">
									  	@include('scorecard.options.chooseschools')
									  </div>
									</div>
								</div>
							</div>
						@else
							<h3 class="title icon-rafi">New Scorecard (District Level)</h3>
							<h4><b>{{ $templates->template_name }}</b></h4>
							<div class="form-scorecard">
								<input type="hidden" id="temp_id" value="{{$templates->id}}">
								<div class="row">
									<!-- Centered Tabs -->
									<div class="col-xs-12 col-sm-12 clearboth col">
										<ul class="nav nav-tabs nav-justified">
										  <li class="active">
										  	<a data-toggle="tab" href="#all" id="alls">All Schools</a>
										  </li>
										  <li>
										  	<a data-toggle="tab" href="#choose" id="choos" data-temptype = "{{$templates->template_type}}">Choose Schools</a>
										  </li>
										</ul>
									</div>
									<div class="tab-content">
									  <div id="all" class="tab-pane fade in active">
									  	@include('scorecard.options.allschools')
									  </div>

									  <div id="choose" class="tab-pane fade">
									  	@include('scorecard.options.chooseschools')
									  </div>
									</div>
								</div>
							</div>
						@endif
						<!--modal-->
						@include('scorecard.successmessage.successmodal')
						@include('scorecard.successmessage.submittingLoader')
					</div>
					
@include('scorecard.options.modalloader')

@endsection
@section('script')
<script type="text/javascript" src="{{ asset('js/scorecard/scorecard.js')}}"></script>
@endsection