@extends('layouts.app')
@section('content')
	
	<div class="page-right assesment-right">
		<div class="success-wrapper">
			<div class="check_mark">
			  <div class="sa-icon sa-success animate">
			    <span class="sa-line sa-tip animateSuccessTip"></span>
			    <span class="sa-line sa-long animateSuccessLong"></span>
			    <div class="sa-placeholder"></div>
			    <div class="sa-fix"></div>
			  </div>
			</div>
			<!-- <div><span class="fa fa-check-circle" style="color: #26bf81; font-size: 15em"></span></div> -->
			<div class="message"><span class="no_of_sc">{{$schoolsCount}}</span> <span class="succ_mess">Scorecard(s) created Successfully!</span></div>
			<a href="" class="btn btn-proceed">Notify School District Supervisor(s)</a>
			<a href="{{route('main_scorecard')}}" class="btn btn-cancel">Back to Scorecard</a>
		</div>
	</div>
@endsection
@section('script')
<script type="text/javascript" src="{{ asset('js/scorecard/successMessage.js')}}"></script>
@endsection