<div id="submittingLoader" class="modal fade" role="dialog" data-backdrop="static">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content modal-md">
      <div class="modal-body">
        <center><img src="{{ asset('img/submitting.gif') }}"></center>
      </div>
    </div>

  </div>
</div>