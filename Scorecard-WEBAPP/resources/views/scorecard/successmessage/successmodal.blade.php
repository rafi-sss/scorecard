<style type="text/css">
  img.loader {
    width:  50%;
    margin:  auto;
  }
</style>
<div id="successModal" class="modal fade" role="dialog" data-backdrop="static">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content modal-md" style="background: white;border: 0;border-radius: 0;">
    <form method="POST" action="#">
    {{ csrf_field() }}
      <div class="modal-body" style="text-align:  center;">
        <img src="{{asset('img/happy.gif')}}" class="loader">
      </div>
      <div class="modal-footer" style="text-align: center;background: #36a3ff;">
        <span class="succ_mess" style="color: white;">Scorecard(s) created Successfully!</span>
        <div class="butgroup" style="margin-top: 10px;">
          <button type="button" class="btn btn-link" style="text-decoration:  underline;/* display:  block; */padding: 15px;box-shadow:  none;" id="btnCancel1">Cancel</button><button type="submit" class="btn btn-cancel" style="border-radius: 0;box-shadow:  none;padding: 15px;border: 1px solid #ffd7d3;"> Notify School District Supervisor </button>
        </div>
      </div>
      </form>
    </div>

  </div>
</div>