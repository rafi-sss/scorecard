@extends('layouts.app')
@section('content')
	<div class="page-right assesment-right">
		<h3 class="title icon-rafi">New Scorecard (RAFI Level)</h3>
		<div class="form-scorecard">
			<input type="hidden" id="temp_id" value="{{$templates->id}}">
			<div class="row">
				<!-- Centered Tabs -->
				<div class="col-xs-12 col-sm-12 clearboth col">
					<ul class="nav nav-tabs nav-justified">
					  <li class="active">
					  	<a data-toggle="tab" href="#all" id="alls2">All recommended schools <strong> ({{$countPrj}})</strong></a>
					  </li>
					  <li>
					  	<a data-toggle="tab" href="#choose" id="choos2">Choose schools</a>
					  </li>
					</ul>
				</div>
				<div class="tab-content">
				  <div id="all" class="tab-pane fade in active">
				  	@include('scorecard.options.allRecommendedSchools')
				  </div>
				  <div id="choose" class="tab-pane fade">
				  	@include('scorecard.options.recommendedSchools')
				  </div>
				</div>
			</div>
		</div>
		<!--modal-->
		@include('scorecard.successmessage.successmodal')
		@include('scorecard.successmessage.submittingLoader')
	</div>

@endsection
@section('script')
<script type="text/javascript" src="{{ asset('js/scorecard/scorecard2.js')}}"></script>
@endsection