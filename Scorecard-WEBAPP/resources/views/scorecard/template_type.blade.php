@extends('layouts.app')
@section('content')
	<div class="page-right assesment-right">
		<h3 class="title icon-rafi">Choose Template</h3>
		<table class="table table-striped">
			<thead>
				<th>Template</th>
				<th>Template Type</th>
				<th>Action</th>
			</thead>
			<tbody>
				@foreach($templateType as $tname)
					<tr>
						<td>{{$tname->thn}}</td>
						<td>{{$tname->ttn}}</td>
						<td>
							<button type="button" class="btn btn-proceed tempbut" data-tt="{{$tname->ttid}}" id="{{$tname->id}}">Proceed</button>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
@endsection
@section('script')
<script type="text/javascript" src="{{ asset('js/scorecard/level.js')}}"></script>
@endsection