@extends('layouts.app')
@section('content')
	<div class="page-right assesment-right">
		<h3 class="title icon-rafi">Choose Scorecard Level</h3>
		<div class="row btn-margin-bottom">
			<div class="col-md-6">
				<a href="{{ URL::to('scorecard/create/'.$templates->id.'/level/1')}}" class="btn btn-proceed btn-block">
					<!-- <span class="fa fa-user btn-icon"></span> -->
					<span class="input-lg">District Level</span>
					<!-- <div class="col-md-6">
						<div class="btn btn-lg btn-info form-control"> <span class="fa fa-user"></span> District Level </div>
					</div> -->
				</a>
			</div>
			<div class="col-md-6">
				<a href="{{ URL::to('scorecard/create/'.$templates->id.'/level/2')}}" class="btn btn-create btn-block">
					<!-- <span class="fa fa-user btn-icon"></span> -->
					<span class="input-lg">RAFI Level</span>
					<!-- <div class="col-md-6">
						<div class="btn btn-lg btn-info form-control"> <span class="fa fa-user"></span> District Level </div>
					</div> -->
				</a>
			</div>
		</div>
	</div>
@endsection