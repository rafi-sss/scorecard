@extends('layouts.app')
@section('content')

	<div class="page-right template-right">
	
		<p class="h4 page-title">Edit Template</p>
		<div class="row">
				
			<div class="temp-details">
				<div class="col-xs-12 col-md-2 col">
					<div class="form-group">
						<label>Template Name</label>
				    </div>
				</div>
				<div class="col-xs-12 col-md-8 col">
					<div class="form-group">
						<input type="text" class="form-control" value="{{$template->template_name}}" readonly>
					</div>
				</div>
				<div class="col-xs-12 col-md-6 col">
					<div class="form-group">
						<div class="row">
							<div class="col-xs-12 col-md-4">
								<label>Template Type</label>
							</div>
							<div class="col-xs-12 col-sm-4">
								<input type="text" class="form-control" value="{{$template->template_type_name}}" readonly>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-6 col">
					<div class="form-group">
						<div class="row">
							<div class="col-xs-12 col-md-4">
								<label>Template Code</label>
							</div>
							<div class="col-xs-12 col-md-4">
								<input type="text" class="form-control" value="{{$template->template_code}}" readonly>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-6 col">
					<div class="form-group">
						<div class="row">
							<div class="col-xs-12 col-md-4">
								<label>Number of Rounds</label>
							</div>
							<div class="col-xs-12 col-md-4">
								<input type="text" class="form-control" value="{{$template->num_of_rounds}}" readonly>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-6 col">
					<div class="form-group">
						<div class="row">
							<div class="col-xs-12 col-md-4">
								<label>Number of Evaluators</label>
							</div>
							<div class="col-xs-12 col-md-4">
								<input type="text" class="form-control" value="{{$template->num_of_evaluators}}" readonly>
							</div>
						</div>
					</div>
				</div>
				<input type="hidden" class="form-control" value="{{$template->created_at}}" readonly>
				<input type="hidden" class="form-control" value="{{$template->id}}" id="headerId">
		
				
			</div>
		</div>
		
	</div>
	<div class="page-right template-right">
		<div class="temp-comp">
			<p class="h4">Key Results Area</p>
			<p class="text-right">
				<button type="button" class="btn btn-create" data-toggle="modal" data-target="#myKra">
					Add
				</button>
			</p>
			<div class="dash-table">
				<table class="table table-striped" id="componentKra">
					<thead>
						<tr>
							<th>Name</th>
							<th>Weight</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($kra as $data)
							<tr>
								<td>{{$data->kra_name}}</td>
								<td>{{(int)$data->weight}}%</td>
								<td style="display:  grid;grid-template-columns: auto auto auto; grid-gap: 3px;">
									<button class="btn btn-create btn-action view" data-toggle="modal" data-target="#modactKra" data-component="{{$data->id}}" data-action="view">
										<span class="fa fa-eye"></span>
									</button>
									<button class="btn btn-proceed btn-action edit" data-toggle="modal" data-target="#modactKra" data-component="{{$data->id}}" data-action="edit">
										<span class="fa fa-edit"></span>
									</button>
									<button class="btn btn-cancel delete" type="button">
										<span class="fa fa-trash"></span> 
									</button>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
				
				{{$kra->appends(array_except(Request::query(), 'kra_page'))->links()}}    
			</div>
		</div>
	</div>
	<div class="page-right template-right">
		<div class="temp-comp">
			<p class="h4">Indicators</p>
			<p class="text-right">
				<button type="button" class="btn btn-create" data-toggle="modal" data-target="#myIndi" id="btnModIndi">
					Add
				</button>
			</p>
			<div class="dash-table">
				<table class="table table-striped" id="componentIndi">
					<thead>
						<tr>
							<th>Name</th>
							<th>Weight</th>
							<th>KRA</th>
							<th>Parent</th>
							<th>Action</th>
						</tr>
					</thead>	
					<tbody>
							
						@foreach($indi as $data)
							<tr>
								<td>{{$data->detail_name}}</td>
								<td>{{(int)$data->weight}}%</td>
								<td>{{$data->kra_name}}</td>
								<td>
									
									@foreach($parent as $sub)
										@if($data->parent_id == $sub->id)
										{{$sub->detail_name}}
										@break
										@elseif($data->parent_id == 0)
										PARENT
										@break
										@endif
									@endforeach
								
								</td>
								<td style="display:  grid;grid-template-columns: auto auto auto; grid-gap: 3px;">
									<button class="btn btn-create btn-action view" data-toggle="modal" data-target="#modActIndi" data-component="{{$data->id}}" data-action="view">
										<span class="fa fa-eye"></span>
									</button>
									<button class="btn btn-proceed btn-action edit" data-toggle="modal" data-target="#modActIndi" data-component="{{$data->id}}" data-action="edit">
										<span class="fa fa-edit"></span>
									</button>
									<button class="btn btn-cancel delete" type="button">
										<span class="fa fa-trash"></span> 
									</button>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
				
			
					{{$indi->appends(array_except(Request::query(), 'indi_page'))->links()}} 

			</div>
		</div>
	</div>
	<div class="page-right template-right">
		<div class="temp-comp">
			<div class="dash-table">
				<p class="h4">Level of Performance</p>
				<p class="text-right">
					<button type="button" class="btn btn-create" data-toggle="modal" data-target="#myLop" id="btnModLop">
						Add
					</button>
				</p>
				<table class="table table-striped" id="componentLop">
					<thead>
						<tr>
							<th>Indicator</th>
							<th>Name</th>
							<th>Value</th>
							<th>KRA</th>
							<th>Description</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($lop as $data)
							<tr>
								<td>{{$data->detail_name}}</td>
								<td>{{$data->lop_code}}</td>
								<td>{{(int)$data->lop_value}}</td>
								<td>{{$data->kra_name}}</td>
								<td>{{$data->lop_description}}</td>
								<td style="display:  grid;grid-template-columns: auto auto auto; grid-gap: 3px;">
									<button class="btn btn-create btn-action view" data-toggle="modal" data-target="#modActLop" data-component="{{$data->id}}" data-action="view">
										<span class="fa fa-eye"></span>
									</button>
									<button class="btn btn-proceed btn-action view" data-toggle="modal" data-target="#modActLop" data-component="{{$data->id}}" data-action="edit">
										<span class="fa fa-edit"></span>
									</button>
									<button class="btn btn-cancel delete" type="button">
										<span class="fa fa-trash"></span> 
									</button>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
				{{$lop->appends(array_except(Request::query(), 'lop_page'))->links()}}
			</div>
		</div>
		@include('template.ext.editModal')
	</div>
	
	
@endsection
@section('script')
<script type="text/javascript" src="{{ asset('js/template/template.js')}}"></script>
@endsection