@extends('layouts.app')
@section('content')
	<div class="page-right assesment-right">
		<p class="text-right">
			<a class="btn btn-create" href="{{route('template_add')}}">Add New</a>
		</p>
		<div class="dash-table">
			<table class="table table-striped">
			    <thead class="text-center">
			    	<tr class="scorecard-tr">
			    		<th>Template Type</th>
		                <th>Template Name</th>
		          		<th>Template Status</th>
		          		<th>Action</th>
		          	</tr>
			    </thead>
			    <tbody>
			    	@foreach($templates as $template)
			    		<tr>
				    		<td>{{$template->type_name}}</td>
							<td>{{ $template->template_name }}</td>
							<td>{{ $template->status}}</td>
							<td style="display:  grid;grid-template-columns: auto auto auto; grid-gap: 3px;">
								<a href="{{ route('template_edit', array('id' => $template->id)) }}" class="btn btn-create view">
									<span class="fa fa-eye"></span>
								</a>
								<a href="{{ route('template_edit', array('id' => $template->id)) }}" class="btn btn-proceed edit">
									<span class="fa fa-edit"></span>
								</a>
								<button class="btn btn-cancel delete" type="button">
									<span class="fa fa-trash"></span> 
								</button>
							</td>
						</tr>
					@endforeach
			    </tbody>
			</table>
		</div>    			
	</div>
@endsection