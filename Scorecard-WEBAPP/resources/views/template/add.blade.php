@extends('layouts.app')
@section('content')
	<div class="page-right assesment-right template-right">
						<h3 class="title icon-rafi">New Template</h3>
						<form action="{{route('template_create')}}" method="POST" class="form-scorecard">
							{{ csrf_field() }}
							<div class="row">
								<div class="col-xs-12 col-sm-2 col">
									<label>Template Name:</label>
								</div>
								<div class="col-xs-12 col-sm-10 col">
									<input type="text" name="template_name" required>
									@if ($errors->has('template_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('template_name') }}</strong>
                                        </span>
                                    @endif
								</div>
								<div class="col-xs-12 col-md-6 col">
									<div class="row">
										<div class="col-xs-12 col-sm-4">
											<label>Template Code:</label>
										</div>
										<div class="col-xs-12 col-sm-8">
											<input type="text" name="template_code" required>
										</div>
									</div>
								</div>
								<div class="col-xs-12 col-md-6 col">
									<div class="row">
										<div class="col-xs-12 col-sm-4 col">
											<label>Template Type:</label>
										</div>
										<div class="col-xs-12 col-sm-8 col">
											<select name="template_type" required>
												<option value="" disabled selected>Select Template Type</option>
												@foreach($types as $type)
													<option value="{{$type->id}}">{{$type->template_name}}</option>
												@endforeach
											</select>
										</div>
									</div>
								</div>
								<div class="col-xs-12 col-md-6">
									<div class="row">
										<div class="col-xs-12 col-sm-4 col">
											<label>Numbers of Rounds:</label>
										</div>
										<div class="col-xs-12 col-sm-8 col">
											<select name="number_rounds" required>
												<option value="1" selected>Select</option>
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="4">4</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-xs-12 col-md-6">
									<div class="row">
										<div class="col-xs-12 col-sm-4 col">
											<label>Number of evaluator:</label>
										</div>
										<div class="col-xs-12 col-sm-8 col">
											<select name="number_evaluators" required>
												<option value="1" selected>Select</option>
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="4">4</option>
												<option value="5">5</option>
												<option value="6">6</option>
												<option value="7">7</option>
												<option value="8">8</option>
												<option value="9">9</option>
												<option value="10">10</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-xs-12">
									<div class="row">
										<div class="col-xs-12 col-sm-2 col">
											<label>Template Components:</label>
										</div>
										<div class="col-xs-12 col-sm-8 col">
											<select name="template_components">
												<option value="" disabled selected>Select</option>
												@foreach($templates as $template)
													<option value="{{$template->id}}">{{$template->template_name}}</option>
												@endforeach
											</select>
										</div>
									</div>
								</div>
							</div>
							<p class="text-right">
								<button class="btn btn-create" type="submit">Create Template</button>
								<button class="btn btn-cancel">Cancel</button>
							</p>
						</form>
					</div>
@endsection