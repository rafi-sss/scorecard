<div class="modal fade modal-add" tabindex="-1" role="dialog" id="myKra" backdrop="static">
	<div class="response-ajax"></div>
	<div class="modal-dialog" role="document">
		<div class="modal-content res-message">
			<div class="modal-header">
				<p class="h4 modal-title inline-block">Key Results Area</p>
				<a class="float-right" data-dismiss="modal" id="btn-add"><i class="fa fa-times" aria-hidden="true"></i></a>
			</div>
			<form method="POST" id="formKra">
				<input type="text" class="form-control"  placeholder="Name" id="kraName" required>
				<input type="number" class="form-control" placeholder="Weight" id="weight" required>
				<p class="text-right">
					<button type="submit" class="btn btn-cancel" id="btnKra">
						Submit
					</button>
				</p>
			</form>
		</div>
	</div>
</div>
<div class="modal fade modal-add" tabindex="-1" role="dialog" id="myIndi" backdrop="static">
	<div class="response-ajax"></div>
	<div class="modal-dialog" role="document">
		<div class="modal-content res-message">
			<div class="modal-header">
				<p class="h4 modal-title inline-block">Indicators</p>
				<a class="float-right" data-dismiss="modal" id="btn-add"><i class="fa fa-times" aria-hidden="true"></i></a>
			</div>
			<form method="POST" id="formIndi">
				<input type="text" class="form-control"  placeholder="Detail Name" id="indiName" required>
				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<select class="form-control" id="indiKraName" required>
							<option value="0" default>Key Results Name</option>
						</select>
					</div>
					<div class="col-xs-12 col-sm-6">
						<input type="number" class="form-control"  placeholder="weight (Percentage)" id="indiWeight" required>
					</div>
				</div>
				<select class="form-control" id="parentIndi">
					<option value="0" default>Parent Indicator</option>
				</select> 
				
				<div class="row">
					<div class="sub-indi">
						<div class="col-xs-12 col-sm-6">
							<input type="text" class="form-control"  placeholder="Method of Verification" id="mov">
						</div>
					</div>
				</div>
				<p class="text-right">
					<button type="submit" class="btn btn-cancel" id="btnIndi">
						Submit
					</button>
				</p>
			</form>
		</div>
	</div>
</div>
<div class="modal fade modal-add" tabindex="-1" role="dialog" id="myLop" backdrop="static">
	<div class="response-ajax"></div>
	<div class="modal-dialog" role="document">
		<div class="modal-content res-message">
			<div class="modal-header">
				<p class="h4 modal-title inline-block">Level of Performance</p>
				<a class="float-right" data-dismiss="modal" id="btn-add"><i class="fa fa-times" aria-hidden="true"></i></a>
			</div>
			<form method="POST" id="formLop">
				<select class="form-control" id="lopIndi" required>
					<option value="" disabled selected>Indicator</option>
				</select>
				<input type="text" class="form-control"  placeholder="Lop Code" id="lopCode" required>
				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<input type="text" class="form-control" placeholder="Lop Letter" id="lopLet" required>
					</div>
					<div class="col-xs-12 col-sm-6">
						<input type="number" class="form-control" placeholder="Lop Value" id="lopVal" required>
					</div>
				</div>
				<textarea rows="5" columns="1" class="form-control" placeholder="Lop Description" id="lopDes" required></textarea>
				
				<p class="text-right">
					<button type="submit" class="btn btn-cancel" id="btnLop">
						Submit
					</button>
				</p>
			</form>
		</div>
	</div>
</div>
<div class="modal fade modal-action" tabindex="-1" role="dialog" backdrop="static" id="modactKra">
	<div class="modal-dialog" role="document">
		<div class="modal-content res-message">
			<div class="modal-header">
				<p class="h4 modal-title inline-block">Key Results Area</p>
				<a class="float-right" data-dismiss="modal" id="btn-add"><i class="fa fa-times" aria-hidden="true"></i></a>
			</div>
			<form class="action-form" id="viewKra">
				<input type="text" class="form-control"  placeholder="Name" id="kraName" readonly>
				<input type="number" class="form-control" placeholder="Weight" id="weight" readonly>
			</form>
			<form class="action-form" method="POST" id="editKra">
				{{ csrf_field() }}
				<input type="text" name="kra_name" class="form-control"  placeholder="Name" id="kraName">
				<input type="number" name="weight" class="form-control" placeholder="Weight" id="weight">
				<input type="hidden" name="id" id="componentId">
				<p class="text-right">
					<button type="submit" class="btn btn-cancel">
						Submit
					</button>
				</p>
			</form>
		</div>
	</div>
</div>
<div class="modal fade modal-action" tabindex="-1" role="dialog" id="modActIndi" backdrop="static">
	<div class="response-ajax"></div>
	<div class="modal-dialog" role="document">
		<div class="modal-content res-message">
			<div class="modal-header">
				<a class="float-right" data-dismiss="modal" id="btn-add"><i class="fa fa-times" aria-hidden="true"></i></a>
				<p class="h4 modal-title">Edit Indicators</p>
			</div>
			<form class="action-form" method="POST" id="editIndi">
				{{ csrf_field() }}
				<input type="text" class="form-control"  name="indi_name" placeholder="Detail Name" id="indiName">
				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<select class="form-control" name="indi_kra_name" id="indiKraName">
							<option value="0" default disabled>Key Results Name</option>
						</select>
					</div>
					<div class="col-xs-12 col-sm-6">
						<input type="number" name="weight" class="form-control"  placeholder="weight (Percentage)" id="indiWeight">
					</div>
				</div>
				<select class="form-control" name="parent_indi" id="parentIndi">
					
				</select> 
				
				<div class="row">
					<div class="sub-indi">
						<div class="col-xs-12 col-sm-6">
							<input type="text" name="mov" class="form-control"  placeholder="Method of Verification" id="mov">
						</div>
					</div>
				</div>
				<input type="hidden" id="componentIndiId">
				<p class="text-right">
					<button type="submit" class="btn btn-cancel">
						Submit
					</button>
				</p>
			</form>
			<form class="action-form" method="POST" id="viewIndi">
				<input type="text" class="form-control"  placeholder="Detail Name" id="indiName" readonly>
				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<input class="form-control" id="indiKraName" readonly>
					</div>
					<div class="col-xs-12 col-sm-6">
						<input type="number" class="form-control"  placeholder="weight (Percentage)" id="indiWeight" readonly>
					</div>
				</div>
				<input class="form-control" id="parentIndi" placeholder="Parent Indicator" readonly>
				
				<div class="row">
					<div class="sub-indi">
						<div class="col-xs-12 col-sm-6">
							<input type="text" class="form-control"  placeholder="Method of Verification" id="mov" readonly>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="modal fade modal-action" tabindex="-1" role="dialog" id="modActLop" backdrop="static">
	<div class="modal-dialog" role="document">
		<div class="modal-content res-message">
			<div class="modal-header">
				<p class="h4 modal-title">Level of Performance</p>
			</div>
			<form class="action-form" method="POST" id="viewLop">
				<label>Indicator</label>
				<input class="form-control" id="lopIndi" readonly>
				<label>LOP Code</label>
				<input type="text" class="form-control" id="lopCode" readonly>
				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<label>LOP Letter</label>
						<input type="text" class="form-control" id="lopLet" readonly>
					</div>
					<div class="col-xs-12 col-sm-6">
						<label>LOP Value</label>
						<input type="number" class="form-control" id="lopVal" readonly>
					</div>
				</div>
				<label>LOP Description</label>
				<textarea rows="5" columns="1" class="form-control" id="lopDes" readonly>	
				</textarea>
				
			</form>
			<form class="action-form" method="POST" id="editLop">
				<select class="form-control" id="lopIndi" name="lop_indi">
				</select>
				<input type="text" class="form-control"  name="lop_code" id="lopCode">
				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<input type="text" class="form-control" name="lop_letter" id="lopLet">
					</div>
					<div class="col-xs-12 col-sm-6">
						<input type="number" class="form-control" name="lop_value" id="lopVal">
					</div>
				</div>
				<textarea rows="5" columns="1" class="form-control" name="lop_description" id="lopDes">
					
				</textarea>
				<input type="hidden" name="id" id="id">
				<p class="text-right">
					<button type="submit" class="btn btn-cancel" id="btnLop">
						Submit
					</button>
				</p>
			</form>
		</div>
	</div>
</div>