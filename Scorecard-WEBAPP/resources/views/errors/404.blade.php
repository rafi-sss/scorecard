<!DOCTYPE html>
<html>
<head>
	<title>ERROR</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
</head>
<body style="background-color: #ffbf80">
	<div class="container">
		<div class="clearfix text-center" style="margin-top: 80px;">
			<p style="color: #404040; font-size: 200px;">404</p>
		</div>
		<div class="row text-center">
			<h2 style="color: #404040">Page not found.</h2>
			<p>The page you are trying to access is not found.</p>
		</div>
	</div>
	
</body>
</html>