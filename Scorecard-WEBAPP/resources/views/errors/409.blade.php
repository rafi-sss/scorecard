<!DOCTYPE html>
<html>
<head>
	<title>ERROR</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
</head>
<body style="background-color: #ffbf80">

	<div class="container">
		<div class="clearfix text-center" style="margin-top: 80px;">
			<p style="color: #404040; font-size: 200px;">409</p>
		</div>
		<div class="row text-center">
			<h2 style="color: #404040">{{ $exception->getMessage() }}</h2>
			<p>You don't have the permission to access this page.</p>
		</div>
	</div>
	
</body>
</html>