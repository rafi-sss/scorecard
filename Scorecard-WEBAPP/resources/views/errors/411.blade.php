<!DOCTYPE html>
<html>
<head>
	<title>ERROR</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css') }}">
	<link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body style="background-color: #ffbf80">
	<div class="container">
		<div class="clearfix text-center" style="margin-top: 200px; margin-bottom: -35px;">
			<p style="color: #404040; font-size: 80px;">{{ $exception->getMessage() }}</p>
		</div>
		<div class="row text-center">
			<h2 style="color: #404040"><span class="fa fa-warning fa-4x"></span></h2>
			<p><i>Please contact the Webmaster, for more information.</i></p>
		</div>
		<div class="clearfix text-center" style="margin-top: 50px;">
			<a href="{{  route('out') }}">Back to login page.</a>
		</div>
	</div>
	
</body>
</html>