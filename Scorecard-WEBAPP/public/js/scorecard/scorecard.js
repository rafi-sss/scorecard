var checkBoxCheckedCount = 0, strid = 0, aad = 0;

$(document).ready(function(){
$loader = $("#submittingLoader");
$("#accordion").on("shown.bs.collapse",function(){
		$.each($(this).find("span").get(), function(){
			var aria = $(this).attr("aria-expanded");
			
			if(aria == "true"){
				$(this).removeClass("fa fa-caret-down").addClass("fa fa-caret-up");
			}
		});
});

$("#accordion").on("hidden.bs.collapse",function(){
		$.each($(this).find("span").get(), function(){
			var aria = $(this).attr("aria-expanded");
			
			if(aria == "false"){
				$(this).removeClass("fa fa-caret-up").addClass("fa fa-caret-down");
			}
		});
});
var noe = 1	;

$(document).on("submit", "#eval",function(e){
	e.preventDefault();
	var url = $(this).attr("action");
	var method = $(this).attr("method");
	var data = $(this).serialize();
	
	$.ajax({
		type: method,
		url: APP_URL+"/scorecard"+url,
		data: data,
		success: function(response){
			$("#myModal").modal("show");
			$("#evalBody").html(response);
		}
	});
});

$(document).on("change", "#noe",function(){
	noe = $(this).val();
});

$(document).on("submit", "#frmSubmit", function(e){
	e.preventDefault();

	$.each($("input:checkbox:checked").get(), function(){
	var kraid = $(this).val();
	var evalid = $(this).attr("id");
	$.ajax({
		type: "POST",
		url: APP_URL+"/scorecard/assignEvaluator",
		data: {kraid: kraid, evalid: evalid},
		success: function(response){
			// alert(response);
			$("#myModal").modal("hide");
		}
	});
	});
});

$("#btn-publish").on("click", function(){
	var scorecard_header_id = $("#scorecard_header_id").val();
	$.ajax({
		type: "POST",
		url: APP_URL+"/scorecard/publishScorecard",
		data: {scorecard_header_id: scorecard_header_id},
		success: function(response){
			if(response == 1){
				window.location.replace(APP_URL+"/scorecard");
			}else{
				alert(response);
			}
		}
	});
});

function loadSchools(){
$.ajax({
	type: "GET",
	url: APP_URL+"/scorecard/chooseSchools",
	data: {aad:aad},
	success: function(data){
		$("#schools").html(data);
	}
});
}

function getRounds(className){
var tempid = $("#temp_id").val();
	$.ajax({
		type: "GET",
		url: APP_URL+"/scorecard/getRounds/"+tempid,
		success: function(data){
			var roundnamescount = data[0].length;
			var roundcount = data[1][0];
			$(className).empty();
			for(var i=1; i<=roundcount; i++){
				$(className).append('<div class="row"><div class="col-xs-12 col-sm-2 col clearboth"><label> <strong>Round '+i+' </strong></label></div><div class="col-xs-12 col-sm-4 col"><select style="margin:0" class="form-control input-lg selround" selc="'+i+'" data-roundname="rname" name="rname'+i+'" id="rname'+i+'"></select></div><div class="col-xs-12 col-sm-3 col"><input type="text" data-roundname="weight"  selc="'+i+'" name="weight'+i+'" id="weight'+i+'" placeholder="Round Weight" value="'+data[0][0].weight+'" readonly required class="sd'+i+'"></div></div>');
			}

			for(var a=0; a<roundnamescount; a++){
				$(".selround").append('<option value='+data[0][a].id+' rw="'+data[0][a].weight+'">'+data[0][a].round_name+'</option>');
			}

		}
	});
}

$("#alls").on("click", function(){
	var className = ".roundsAll";
	getRounds(className);
});

$("#choos").on("click", function(){
	aad = $(this).attr('data-temptype');
	loadSchools();
	var className = ".roundsChoose";
	getRounds(className);
});

$("#alls").trigger("click");

$(document).on("change",".selround", function(){
				var cou = $(this).attr('selc');
				var ss = $(this).find("option:selected").attr("rw");
				$("input.sd"+cou).val(ss);
			});


$("#chooseSchool").on("submit", function(e){
e.preventDefault();
checkBoxCheckedCount = $(this).find("input:checkbox:checked").length;
var countr = 0;
$("#loader").modal("show");

$.each($(this).find("input:checkbox:checked").get(), function(){
	var data = $("#chooseSchool").serialize() + "&schoolval="+$(this).val();
	var url = $("#chooseSchool").attr("action");
	var type = $("#chooseSchool").attr("method");
	$.ajax({
		type: type,
		url: url,
		data: data,
		success: function(data){
			// window.location.reload();
			$("#loader").modal("hide");
		$("#successModal").modal("show");
		},
		error: function(error){
			alert(error.responseTxt);
		}
	});
});
});

$('.no_of_sc').each(function () {
    $(this).prop('Counter',0).animate({
        Counter: $(this).text()
    }, {
        duration: 4000,
        easing: 'swing',
        step: function (now) {
            $(this).text(Math.ceil(now));
        }
    });
});

$("#allschools").on("submit", function(e){
e.preventDefault();

var data = $(this).serialize();
var url = $(this).attr("action");
var type = $(this).attr("method");

$.ajax({
	type: type,
	url: url,
	data: data,
	beforeSend: function(){
		$("#loader").modal("show");
	},
	success: function(data){
		$("#loader").modal("hide");
		$("#successModal").modal("show");
	}
});

});

$("#btnCancel, #btnCancel1").on("click",function(){
	$("#successModal").modal("hide");
location.reload();
});

$(document).on("click", "#editDetails", function(){
	var data = $(this).attr("data-evaluator-id");
	strid = $(this).attr('data-rid');
	$.ajax({
		type: "GET",
		url: APP_URL+"/scorecard/modifyEval/"+data,
		success: function(data){
			$(".evalUsername").html("");
			$("#edit-evalid").val("");
			$(".evalUsername").append(data.username);
			$("#edit-evalid").val(data.id);
			$("#editEval").modal("show");
			if(strid < 2){
				$('#addComp').html('<span class="margin"><label style="margin-left: 0;">Evaluator Weight:</label><input type="number" class="form-control"placeholder="Evaluator Weight" name="ew" required="" value="0" step=".01"></span>');
			}else{
				$('#addComp').empty();
			}
		}
	});
});

$("#evalForm").on("submit", function(e){
	e.preventDefault();
	var data;
	if(strid == 1){
		data = $(this).serialize() + "&strid="+strid;
	}else{
		data = $(this).serialize() + "&strid="+0;
	}
	// alert(data);
	// var data = $(this).serialize();
	var type = $(this).attr("method");
	var url = $(this).attr("action");
	// alert(data);
	$.ajax({
		type: type,
		url: url,
		data: data,
		success: function(response){
			alert(response);
			location.reload();
		}
	});
});
	var schoolIDs = [];
	$('#addSchoolIn').click(function(){
		var id = $('#addSchool').val();
		if(schoolIDs.indexOf(id) == -1){
			schoolIDs.push(id);
			var name = $('#addSchool option[value="'+id+'"]').attr('school_name');
			$('<li school_id="'+id+'">'+name+'</li>').appendTo('#addListSchool');
		}else{
			alert('School Already Choosed');
		}
		
		
	});

	$('#prjname').on('change',function(){
		alert($(this).find('option').attr('value'));
	});

	$(document).on('submit', '#rafiRate', function(e){
			e.preventDefault();
			var eval = 2, scid = $(this).find('input[type="number"]').attr('data-scid'), rn = $(this).find('input[type="number"]').attr('data-roundnum'), datas = $(this).serialize()+"&scid="+scid+"&rn="+rn+"&eval="+eval;
			alert(datas);
			$.ajax({
				type: 'POST',
				url: APP_URL+'/scorecard/rafiLevelScore',
				data: datas,
				success: function(response){
					alert(response);
				}
			});
	});

	$(document).on('keyup', 'input[name="rate"]', function(){
		var comparedWeight = Number($(this).attr('data-weight'));
		var currentWeight = Number($(this).val());
		// console.log(currentWeight + " = " +comparedWeight);
		if(currentWeight <= comparedWeight){
			console.log(currentWeight);
		}else{
			alert('Must be lesser than (<) or equal to (=) the specified weight!');
			$(this).val('');
		}
	});

	$('#evalname').on('keyup', function(){
		var name = $(this).val();
		if($(this).val() == ""){
			$('#basic-addon1').css('border', '1px solid rgb(205, 205, 205)');
			$('#evalname').css('border', '1px solid rgb(205, 205, 205)');
			$('.searchresults').hide();
			$('.searchresultsList').empty();
		}else{
			$.ajax({
				type: 'GET',
				url: APP_URL+'/scorecard/filterEvaluator',
				data: {name: name},
				success: function(response){
					$('#basic-addon1').css('border-bottom', '0');
					$('#evalname').css('border-bottom', '0');
					$('.searchresults').show();
					$('.searchresultsList').html(response);
					// $('.searchresultsList').append('<a href="#" class="resultLink"><li>'+response+'</li><a>');
				}
			});
			
		}
	});

	$(document).on('click', '.resultLink', function(){
		var fname = $(this).find('li > span.fname').text();
		var mname = $(this).find('li > span.mname').text();
		var lname = $(this).find('li > span.lname').text();
		var eAdd = $(this).find('li > input.eAdd').val();
		var cnumber = $(this).find('li > input.mnum').val();

		$('input[name="fname"]').val(fname);
		$('input[name="mname"]').val(mname);
		$('input[name="lname"]').val(lname);
		$('input[name="ea"]').val(eAdd);
		$('input[name="mn"]').val(cnumber);
		$('#basic-addon1').css('border', '1px solid rgb(205, 205, 205)');
		$('#evalname').css('border', '1px solid rgb(205, 205, 205)');
		$('.searchresults').hide();
		$('.searchresultsList').empty();
	});
});