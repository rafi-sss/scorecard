$(document).ready(function(){
$loader = $("#submittingLoader");
$("#accordion").on("shown.bs.collapse",function(){
		$.each($(this).find("span").get(), function(){
			var aria = $(this).attr("aria-expanded");
			
			if(aria == "true"){
				$(this).removeClass("fa fa-caret-down").addClass("fa fa-caret-up");
			}
		});
});

$("#accordion").on("hidden.bs.collapse",function(){
		$.each($(this).find("span").get(), function(){
			var aria = $(this).attr("aria-expanded");
			
			if(aria == "false"){
				$(this).removeClass("fa fa-caret-up").addClass("fa fa-caret-down");
			}
		});
});
var noe = 1	;

$("#proj_name").val($("#baseProj").val()+' - RAFI Level');
$("#proj_name1").val($("#baseProj1").val()+' - RAFI Level');

$("#baseProj").change(function(){
	$("#proj_name").val($(this).val()+' - RAFI Level');
});

$("#baseProj1").change(function(){
	$("#proj_name1").val($(this).val()+' - RAFI Level');
});





$(document).on("submit", "#eval",function(e){
	e.preventDefault();
	var url = $(this).attr("action");
	var method = $(this).attr("method");
	var data = $(this).serialize();
	
	$.ajax({
		type: method,
		url: APP_URL+"/scorecard"+url,
		data: data,
		success: function(response){
			$("#myModal").modal("show");
			$("#evalBody").html(response);
		}
	});
});

$(document).on("change", "#noe",function(){
	noe = $(this).val();
});

$(document).on("submit", "#frmSubmit", function(e){
	e.preventDefault();

	$.each($("input:checkbox:checked").get(), function(){
	var kraid = $(this).val();
	var evalid = $(this).attr("id");
	$.ajax({
		type: "POST",
		url: APP_URL+"/scorecard/assignEvaluator",
		data: {kraid: kraid, evalid: evalid},
		success: function(response){
			// alert(response);
			$("#myModal").modal("hide");
		}
	});
	});
});

$("#btn-publish").on("click", function(){
	var scorecard_header_id = $("#scorecard_header_id").val();
	$.ajax({
		type: "POST",
		url: APP_URL+"/scorecard/publishScorecard",
		data: {scorecard_header_id: scorecard_header_id},
		success: function(response){
			if(response == 1){
				window.location.replace(APP_URL+"/scorecard");
			}else{
				alert(response);
			}
		}
	});
});

function loadSchools(proj){
$.ajax({
	type: "GET",
	url: APP_URL+"/scorecard/recoSchools/"+proj,
	success: function(data){
		$("#schools1").html(data);
	}
});
}
var projname = $("#baseProj1").val();
loadSchools(projname);

$("#baseProj1").change(function(){
	var pname = $(this).val();
	loadSchools(pname);
});

function getRounds(className){
var tempid = $("#temp_id").val();
	$.ajax({
		type: "GET",
		url: APP_URL+"/scorecard/getRounds/"+tempid,
		success: function(data){
			var roundnamescount = data[0].length;
			var roundcount = data[1][0];
			$(className).empty();
			for(var i=1; i<=roundcount; i++){
				$(className).append('<div class="row"><div class="col-xs-12 col-sm-2 col clearboth"><label> <strong>Round '+i+' </strong></label></div><div class="col-xs-12 col-sm-4 col"><select style="margin:0" class="form-control input-lg selround" selc="'+i+'" data-roundname="rname" name="rname'+i+'" id="rname'+i+'"></select></div><div class="col-xs-12 col-sm-3 col"><input type="text" data-roundname="weight"  selc="'+i+'" name="weight'+i+'" id="weight'+i+'" placeholder="Round Weight" value="'+data[0][0].weight+'" readonly required class="sd'+i+'"></div></div>');
			}

			for(var a=0; a<roundnamescount; a++){
				$(".selround").append('<option value='+data[0][a].id+' rw="'+data[0][a].weight+'">'+data[0][a].round_name+'</option>');
			}

		}
	});
}

$("#alls2").on("click", function(){
	var className = ".roundsAll";
	getRounds(className);
});

$("#choos2").on("click", function(){
	var className = ".roundsChoose";
	getRounds(className);
});

$("#alls2").trigger("click");

$(document).on("change",".selround", function(){
	var cou = $(this).attr('selc');
	var ss = $(this).find("option:selected").attr("rw");
	$("input.sd"+cou).val(ss);
});

$("#chooseSchool").on("submit", function(e){
e.preventDefault();
var checkBoxCheckedCount;
$.each($(this).find("input:checkbox:checked").get(), function(){
	// checkBoxCheckedCount = $(this).find("input:checkbox:checked").length;
	var data = $("#chooseSchool").serialize() + "&schoolval="+$(this).val();
	var url = $("#chooseSchool").attr("action");
	var type = $("#chooseSchool").attr("method");
	alert(data);
	// $.ajax({
	// 	type: type,
	// 	url: url,
	// 	data: data,
	// 	success: function(data){
	// 		$("#successModal").modal("show");
	// 	},
	// 	error: function(error){
	// 		alert(error.responseTxt);
	// 	}
	// });
});
});

$('.no_of_sc').each(function () {
    $(this).prop('Counter',0).animate({
        Counter: $(this).text()
    }, {
        duration: 4000,
        easing: 'swing',
        step: function (now) {
            $(this).text(Math.ceil(now));
        }
    });
});

$("#allschools").on("submit", function(e){
e.preventDefault();
// $loader.modal("show");
var data = $(this).serialize();
var url = $(this).attr("action");
var type = $(this).attr("method");
// alert(data);
$.ajax({
	type: type,
	url: url,
	data: data,
	beforeSend: function(){
		$("#loader").modal("show");
	},
	success: function(data){
		$("#loader").modal("hide");
		// $loader.modal("hide");
		$("#successModal").modal("show");
	}
});

});

$("#btnCancel, #btnCancel1").on("click",function(){
	$("#successModal").modal("hide");
location.reload();
});

$(document).on("click", "#editDetails", function(){
	var data = $(this).attr("data-evaluator-id");
	$.ajax({
		type: "GET",
		url: APP_URL+"/scorecard/modifyEval/"+data,
		success: function(data){
			$(".evalUsername").html("");
			$("#edit-evalid").val("");
			$(".evalUsername").append(data.username);
			$("#edit-evalid").val(data.id);
			$("#editEval").modal("show");
		}
	});
});

$("#evalForm").on("submit", function(e){
	e.preventDefault();
	var data = $(this).serialize();
	var type = $(this).attr("method");
	var url = $(this).attr("action");
	// alert(data);
	$.ajax({
		type: type,
		url: url,
		data: data,
		success: function(response){
			alert(response);
			location.reload();
		}
	});
});
});