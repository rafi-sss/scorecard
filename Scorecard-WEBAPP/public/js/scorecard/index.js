$prjname = $('#prjname');
$schoolsName = $('.schoolsname');
$lives = $('#lives');
var chosenProj;

window.onload = function(e){
	$prjname.prop('selectedIndex', 0);
	$lives.val("");
}

$prjname.change(function(){
	var sctype = $(this).find('option:selected').attr('id'),
	dview = $(this).find('option:selected').attr('data-view');
	if($(this).find('option:selected').attr('value') == 0){
		window.location.reload();
	}else{
		if(sctype == 2){
			$('.livename').html('Municipality Name');	
			$('[name="livesearch"]').attr('placeholder', 'Municipality Name');
		}
		$lives.attr('data-sctype', sctype);
		$lives.attr('data-iview', dview);
		$schoolsName.css('visibility', 'visible');
		chosenProj = $(this).find('option:selected').attr('value');
		console.log(chosenProj);
	}
});

$lives.keyup(function(){
	var keyword = 'keyword='+$(this).val()+'&chosenProj='+chosenProj+"&sctype="+$(this).attr('data-sctype')+"&view="+$(this).attr('data-iview');

	$.ajax({
		type: 'GET',
		url: APP_URL+'/scorecard/liveSearch',
		data: keyword,
		success: function(response){
			console.log(response[1])
			$('.scorecard-alignment').empty();
			$('.scorecard-alignment').append(response[0]);
			$('.pgs').empty();
			// $('.pgs').append(response[1]);
		}
	});
});

$(document).on('click','#delete',function(){
	var schlname = $(this).attr('data-namedelid');
	if(confirm('Deletion request of "'+schlname+'" scorecard will be sent to admin, proceed?')){
		alert('Request Sent!');
	}
});