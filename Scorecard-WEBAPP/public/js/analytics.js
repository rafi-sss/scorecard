
$(document).ready(function(){

	function loadMunicipalities(){
		$.ajax({
			type: "GET",
			url: APP_URL+'/getSCStat',
			success: function(response){
				var mun_name = [], totalRated = [], left = [], totalSC = [];
				// alert(response.length);
				// for(var i=0; i<){

				// }
				// $tr = true;
				// $tr1 = false;

				// var createT = (10 > 91) 
				// ? 
				// $tr : $tr1;
				var variable;
				// alert(createT);
				for(var i in response){
					mun_name.push(response[i].mun_name);
					left.push(response[i].left);
					variable = (response[i].totalRated == response[i].totalSC) ? totalSC.push(response[i].totalSC) : totalRated.push(response[i].totalRated);
					// if(response[i].totalRated == response[i].totalSC){

					// }else{

					// }
					// totalRated.push(response[i].totalRated);
					// totalSC.push(response[i].totalSC);
				}
				// alert(mun_name);
				var data = {
					  labels: mun_name,
					  datasets: [
					  {
					    label: "Progress",
					    backgroundColor: "#2d7ac5",
					    borderColor: "#2d7ac5",
					    borderWidth: 2,
					    hoverBackgroundColor: "rgba(0, 128, 198, 0.6)",
					    hoverBorderColor: "rgba(0, 128, 198, 0.6)",
					    data: totalRated,
					  },
					  {
					    label: "Complete",
					    backgroundColor: "#30d94e",
					    borderColor: "#30d94e",
					    borderWidth: 2,
					    hoverBackgroundColor: "rgba(57, 210, 83, 0.6)",
					    hoverBorderColor: "rgba(57, 210, 83, 0.6)",
					    data: totalSC,
					  },
					  {
					    label: "Not started",
					    backgroundColor: "#f89b12",
					    borderColor: "#f89b12",
					    borderWidth: 2,
					    hoverBackgroundColor: "rgba(248, 155, 18, 0.58)",
					    hoverBorderColor: "rgba(248, 155, 18, 0.58)",
					    data: left,
					  }
					  ]
					};

				var options = {
				  scales: {
				    yAxes: [{
				    	ticks: {
				     		beginAtZero: true,
				    	},	
				      gridLines: {
				        display: true,
				        color: "rgba(255,99,132,0.2)"
				      }
				    }],
				    xAxes: [{
				      gridLines: {
				        display: false
				      }
				    }]
				  }
				};

				Chart.Bar('myBarChart', {
				  options: options,
				  data: data
				});
			}
		});
	}

	loadMunicipalities();


});

var data = {
	labels: ['2010', '2011', '2012'],
	datasets: [
		{
			label: 'CTU',
			fill: true,
			data: [32, 25,44],
			backgroundColor: [
                '#4472c4','#ffc000', '#23e148'
            ],
            borderColor: [
            ],
            borderWidth: 2

		}
	]
}

var options = {
	responsive: true,
	title: {
				display: true,
				text: 'Scorecard Completion'
			},
	tooltips: {
				mode: 'index',
				intersect: false,
			},
			hover: {
				mode: 'nearest',
				intersect: true
			}
}

var context = document.querySelector("#myChart").getContext("2d");

 new Chart(context , {
	type: "pie",
	data: data, 
	options: options,
	 	title: {
		display: true,
		text: 'Scorecards'
	}
});

 //dfdfd

//  var datas = {
// 	labels: ['Cebu City', 'Mandaue City', 'Carcar City', 'Cebu Province'],
// 	datasets: [
// 		{
// 			label: 'Progress',
// 			fill: false,
// 			data: [50, 40, 50, 25,0],
// 			backgroundColor: [
//                 '#4472c4',
//                 '#4472c4',
//                 '#4472c4',
//                 '#4472c4'
//             ],
//             borderColor: [
//             ],
//             borderWidth: 1

// 		},
// 		{
// 			label: 'Not started',
// 			fill: false,
// 			data: [35, 25, 70, 35,0],
// 			backgroundColor: [
//                 '#ffc000',
//                 '#ffc000',
//                 '#ffc000',
//                 '#ffc000'
//             ],
//             borderColor: [
//             ],
//             borderWidth: 1

// 		},
// 		{
// 			label: 'Completed',
// 			fill: false,
// 			data: [70, 90, 60, 85,0],
// 			backgroundColor: [
//                 '#23e148',
//                 '#23e148',
//                 '#23e148',
//                 '#23e148'
//             ],
//             borderColor: [
//             ],
//             borderWidth: 1

// 		},

		// ]
// }

// var optionss = {
// 	responsive: true,
// 	title: {
// 				display: true,
// 				text: 'School Scorecard'
// 			},
// 	tooltips: {
// 				mode: 'index',
// 				intersect: false,
// 			},
// 	 hover: {
// 				mode: 'nearest',
// 				intersect: true
// 			},


// }

// var contexts = document.querySelector("#myBarChart").getContext("2d");

//  new Chart(contexts , {
// 	type: "bar",
// 	data: datas, 
// 	options: optionss,
// 	 	title: {
// 		display: true,
// 		text: 'Scorecards'
// 	}
// });

 	$('.numbers').each(function () {
    $(this).prop('Counter',0).animate({
        Counter: $(this).text()
    }, {
        duration: 1000,
        easing: 'swing',
        step: function (now) {
            $(this).text(Math.ceil(now));
        }
    });
});