$(document).ready(function(){
	var par, schoolID, conf, note, pn, ti, scid;
	$(".es").click(function(){
		par = $(this).closest('tr');
		note = 'You are about to recommend '+par.children().eq(1).html();
		conf = confirm(note);

		schoolID = par.children().eq(6).val();
		pn = par.children().eq(7).val();
		ti = par.children().eq(8).val();
		scid = par.children().eq(9).val();

		if(conf){
			$("#schoolh4").html(par.children().eq(1).html());
			$("#recoSchool").modal('toggle');
			// $.ajax({
			//     type:'POST',
			//     data: {sc_id: scid, school_id: schoolID, project_name: pn, template_id: ti},
			//     url: 'recommend',
			//     success: function(data){
			//     		window.location.reload();
			//     	//responseAjax(data);
			//     	// document.getElementById('formIndi').reset();
			//     },
			//     error: function(data){
			//     	console.log(data);
			//     }
			// }); 
		}else{
			alert('Cancelled!');
		}

	});

	$(".p1").click(function(){
		var schoolscid = $(this).attr('school-scid');

		$.ajax({
			type: "GET",
			url: APP_URL+'/reports/scEvals/'+schoolscid,
			success:function(data){
				$(".evalBody").html(data);
				$("#evalModal").modal('toggle');
			},
			error: function(){
				alert('Something went wrong during process.');
			}
		});

	});

});