$(document).ready(function(){
	var idHidden;
	var counter = 0;
	$("#main-ul").hide();
	$(document).on("click", "#button", function(){
		$(this).next("#main-ul").slideToggle(function(){
			$("#button").toggleClass("fa fa-caret-down fa fa-caret-up");
		});
	});

	$(".editBtn").click(function(){
		var id = $(this).attr('id');
		$.ajax({
			type: 'GET',
			url: APP_URL+'/usercontrol/getAccess/'+id,
			success: function(data){
				idHidden = id;
				$(".firstbatch").html(data);
			}
		});
	});

	$("#btnSubmit").click(function(e){
		$.each($("input:checkbox").get(), function(){
		var inputData = $(this).val();
		var cls = $(this).attr("class");
		var checked = $(this).prop("checked");
		var data ="cls="+cls+"&inputData="+inputData+"&checked="+checked;
		var id = idHidden;
		$.ajax({
			type: "get",
			url: APP_URL+'/usercontrol/updateUserAccess/'+id,
			data: {cls:cls, inputData: inputData, checked: checked},
			success: function(data){
				window.location.reload();
			}
		});
		});	
	});

});