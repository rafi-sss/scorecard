$(document).ready(function(){
	$('#addOCP').on('submit',function(e){
		e.preventDefault();
		var url = $(this).attr('action');
		var method = $(this).attr('method');
		var data = $(this).serialize();
		$.ajax({
			type: method,
			url: url,
			data: data,
			success: function(response){
				$('.mainevalControls').hide();
				$('.altEval').fadeIn();
				$('.altEvalCont').fadeIn();
				$('#altEvalVal').val(response);
			}
		});
	});

	$('#altEvalOCP').on('submit', function(e){
		e.preventDefault();
		var url = $(this).attr('action');
		var method = $(this).attr('method');
		var data = $(this).serialize();

		$.ajax({
			type: method,
			url: url,
			data: data,
			success: function(response){
				window.location.reload();
			}
		});
	});

	$('.va').on('click', function(){
		var calltable = $(this).closest('tr');
		var evalName = calltable.children().eq(0).find('span.sfname').text();
		var id = calltable.children().eq(3).find('span').attr('id');
		$.ajax({
			type: 'GET',
			url: APP_URL+'/evaluator/viewOCPAltEval/'+id,
			success: function(response){
				$('#response').html(response);
				$('#viewAlt').modal('show');
				$('.mainEvalName').html('<b>'+evalName+'</b>');
			}
		});
	});
	
});