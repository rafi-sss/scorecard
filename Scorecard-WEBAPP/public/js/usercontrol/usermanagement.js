$(document).ready(function(){
	var par;
	$(".upd").click(function(){
		par = $(this).closest('tr');
		$("#uid").val(par.children().eq(0).html());
		$("#uid1").val(par.children().eq(0).html());
		$("#uutype").val(par.children().eq(2).html());
		$("#uscorecardtype").val(par.children().eq(7).html());
		$("#udistrict").val(par.children().eq(6).html());
		$("#ufname").val(par.children().eq(8).html());
		$("#ulname").val(par.children().eq(9).html());
		$("#umname").val(par.children().eq(10).html());
		$("#uemail").val(par.children().eq(3).html());
		$("#uusername").val(par.children().eq(11).html());
		var status = par.children().eq(4).html();
		var action = $(this).attr('data-action');
		if (action == 'edit') {
			$('.passthing').show();
			$('#ustatus').css('display', 'none');
			$('#uuserstatus, #update-ustatus').css('display', 'block');
			if(status == 'Pending'){
			$('option[value=pending]').attr('selected', 'selected');
			}else if(status == 'Activated'){
				$('option[value=approved]').attr('selected', 'selected');
			}else if(status == 'Deactivated'){
				$('option[value=deactivate]').attr('selected', 'selected');
			}
		}else{
			$('.passthing').hide();
			$('#uuserstatus').css('display', 'none');
			$('#ustatus').val(status);
			$('#ustatus').css('display', 'block');
		}
		
		// $("uuserstatus").append('<option value="'+status+'" selected>'+status+'</option><option></option>');
		if(par.children().eq(2).html() == 'Scorecard Admin'){
			$("#uuserdistrict").fadeOut("fast");
			$("#usctype").fadeIn("fast");
		}else if(par.children().eq(2).html() == 'SDS'){
			$("#usctype").fadeOut("fast");
			$("#uuserdistrict").fadeIn("fast");
		}else{
			$("#usctype").fadeOut("fast");
			$("#uuserdistrict").fadeOut("fast");
		}
	});

	$("#utype").change(function(){
		if($("#utype").find(":selected").val() == 2){
			$("#userdistrict").fadeOut("fast");
			$("#sctype").fadeIn("fast");
		}else if($("#utype").find(":selected").val() == 3){
			$("#sctype").fadeOut("fast");
			$("#userdistrict").fadeIn("fast");
		}else{
			$("#sctype").fadeOut("fast");
			$("#userdistrict").fadeIn("fast");
		}
	});
	$('#update-ustatus').click(function(){
		var id = $('#uid').val();
		var stat = $('#uuserstatus').val();
		$.ajax({
			type: 'POST',
			data: 'id='+id+'&status='+stat,
			url: APP_URL+'/usermanagement/update/userstatus',
			success: function(data){
				location.reload();
			},
			error: function(data){
				alert(data);
			}
		});
	});


	$(document).on('click','#delete',function(){
	var schlname = $(this).attr('data-namedelid');
	if(confirm('Deletion request of "'+schlname+'" account will be sent to admin, proceed?')){
		alert('Request Sent!');
	}
	});

	$("#changepass").click(function(){
		var newpass = $("#unewpass").val();
		var oldpass = $("#uoldpass").val();
		var id = $('#uid1').val();
		$.ajax({
			type: "POST",
			url: APP_URL+'/usermanagement/update/changePass',
			data: "id="+id+"&oldpass="+oldpass+"&newpass="+newpass,
			success: function(response){
				if(response == 1){
					alert('Password successfuly changed!');
					window.location.reload();
				}else{
					alert('Password is Incorrect!');
				}
			}
		});
	});

	$('#district').on('change', function(){
		$('#sctype').css('margin-bottom','10px').show();
	});
});
