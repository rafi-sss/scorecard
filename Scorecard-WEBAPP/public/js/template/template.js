$(document).ready(function(){
	$('#btnKra').click(function(event){
		if ($('#formKra')[0].checkValidity()) {
			event.preventDefault();
			var headerId = $('#headerId').val();
			var kraName = $('#kraName').val();
			var weight = $('#weight').val();
			$.ajax({
			    type:'POST',
			    data: 'header_id='+headerId+'&kra_name='+kraName+'&weight='+weight,
			    url: APP_URL+'/template/create/kra',
			    success: function(data){
			    	responseAjax(data);
			    	document.getElementById('formKra').reset();
			    },
			    error: function(data){
			    	console.log(data);
			    }
			});   
		}
	});
	$('#btnLop').click(function(event){
		if ($('#formLop')[0].checkValidity()) {
			event.preventDefault();
			var lopIndi = $('#lopIndi').val();
			var lopCode = $('#lopCode').val();
			var lopLet = $('#lopLet').val();
			var lopVal = $('#lopVal').val();
			var lopDes = $('#lopDes').val();
			$.ajax({
			    type:'POST',
			    data: 'lop_indi='+lopIndi+'&lop_code='+lopCode+'&lop_let='+lopLet+'&lop_val='+lopVal+'&lop_des='+lopDes,
			    url: APP_URL+'/template/create/lop',
			    success: function(data){
			    	responseAjax(data);
			    	// document.getElementById('formLop').reset();
			    },
			    error: function(data){
			    	console.log(data);
			    }
			});   
		}
	});
	$('#btnIndi').click(function(event){
		if ($('#formIndi')[0].checkValidity()){
			event.preventDefault();
			var indiName = $('#indiName').val();
			var indiKraName = $('#indiKraName').val();
			var indiWeight = $('#indiWeight').val();
			var parentIndi = $('#parentIndi').val();
			var indiLopCode = $('#indiLopCode').val();
			var mov = $('#mov').val();
			$.ajax({
			    type:'POST',
			    data: 'indi_name='+indiName+'&indi_kra_name='+indiKraName+'&indi_weight='+indiWeight+'&parent_indi='+parentIndi+'&mov='+mov,
			    url: APP_URL+'/template/create/indi',
			    success: function(data){
			    	responseAjax(data);
			    	document.getElementById('formIndi').reset();
			    },
			    error: function(data){
			    	console.log(data);
			    }
			});   
		};
	});

	$('#btnModIndi').click(function(){
		headerId = $('#headerId').val();
		$.ajax({
			type: 'GET',
			data: 'header_id='+headerId,
			url: APP_URL+'/template/fetch/kra',
			success: function(data){
				var count = data.length;
				$('#indiKraName .appe-opt').remove();
				for(var i = 0; i < count; i++){
					$('#indiKraName').append('<option class="appe-opt" value="'+data[i].id+'">'+data[i].kra_name+'</option>');
				}
				
			}
		});
	});
	$('#indiKraName').change(function(){
		if ($(this).val() > 0) {
			var kra = $('#indiKraName').val();
			$.ajax({
				type: 'GET',
				data: 'kra='+kra,
				url: APP_URL+'/template/fetch/parent-indi',
				success: function(data){
					var indiCount = data.length;
					$('#parentIndi .appe-opt').remove();
					for(var i = 0; i < indiCount; i++){
						$('#parentIndi').append('<option class="appe-opt" value="'+data[i].id+'">'+data[i].detail_name+'</option>');
					}
					
				}
			});
		}else{
			$('#parentIndi .appe-opt').remove();
		}
	});
	$('#parentIndi').change(function(){
		if ($(this).val() > 0) {
			$('#indiLopCode').attr('required', 'required');
			$('.sub-indi').css('display', 'block');
			var headerId = $('#headerId').val();
			$.ajax({
				type: 'GET',
				data: 'header_id='+headerId,
				url: APP_URL+'/template/fetch/lop',
				success: function(data){
					var indiCount = data.length;
					for(var i = 0; i < indiCount; i++){
						$('#indiLopCode').append('<option value="'+data[i].id+'">'+data[i].lop_code+'</option>');
					}
				}
			});
		}else{
			$('#indiLopCode').removeAttr('required');
			$('.sub-indi').css('display', 'none');
		}

	});
	$('#btnModLop').click(function(){
		var headerId = $('#headerId').val();
		$.ajax({
			type: 'GET',
			url: APP_URL+'/template/fetch/indi',
			data: 'header_id='+headerId,
			success: function(data){
				var indiCount = data.length;
				$('#lopIndi .appe-opt').remove();
				for(var i = 0; i < indiCount; i++){
					$('#lopIndi').append('<option class="appe-opt" value="'+data[i].id+'">'+data[i].detail_name+'</option>');
				}
			}
		});
	});
	$('.modal-add').on('hidden.bs.modal', function () {
    	location.reload();
	})
	$('#componentKra .btn-action').click(function(){
		var id = $(this).attr('data-component');
		var action = $(this).attr('data-action');
		$.ajax({
			type: 'GET',
			url: APP_URL+'/template/component/kra',
			data: 'id='+id,
			success: function(data){
				if( action == 'view'){
					$('#editKra').css('display', 'none');
					$('#viewKra').css('display', 'block');
				}else{
					$('#viewKra').css('display', 'none');
					$('#editKra').css('display', 'block');
					$('#componentId').val(data.id);
				}
				$('.action-form #kraName').val(data.kra_name);
				// var weight = parseInt(data.weight);
				$('.action-form #weight').val(parseInt(data.weight));
				
			}
		});
	});
	$('#componentIndi .btn-action').click(function(){
		var id = $(this).attr('data-component');
		var action = $(this).attr('data-action');
		var template_header_id = $('#headerId').val();
		$('#componentIndiId').val(id);
		$.ajax({
			type: 'GET',
			url: APP_URL+'/template/component/indi',
			data: 'id='+id+'&header_id='+template_header_id,
			success: function(data){
				if( action == 'view'){
					$('#editIndi').css('display', 'none');
					$('#viewIndi').css('display', 'block');
					$('#viewIndi #indiKraName').val(data[0][0].kra_name);
					$("#viewIndi #parentIndi").val(data[0][0].detail_name);
				}else{
					$('#viewIndi').css('display', 'none');
					$('#editIndi').css('display', 'block');
					$(".action-form #indiKraName").html('');
					for(var i=0; i<data[1].length; i++){
						if(data[0][0].kra_id == data[1][i].id){
						// $('.action-form #indiKraName').append("<option value='+data[1][i].id+'selected>'+data[1][i].kra_name+'</option>");
							$(".action-form #indiKraName").append("<option value="+data[1][i].id+" selected>"+data[1][i].kra_name+"</option>");
						}else{
							$('.action-form #indiKraName').append("<option value="+data[1][i].id+">"+data[1][i].kra_name+"</option>");
						}
						// $('.action-form #indiKraName').append("<option value="+data[1][i].id+">"+data[1][i].kra_name+"</option>");
					}
					$(".action-form #parentIndi").html('');
					var count = 0;
					for(var a=0; a<data[2].length; a++){
						if(data[0][0].parent_id == data[2][a].id){
							selected = "selected";
						}else{
							selected = "";
						}
						if (count == 0) {
							$('.action-form #parentIndi').append('<option value="0" default>Parent Indicator</option>');
						}
						count++;
						$(".action-form #parentIndi").append("<option value="+data[2][a].id+" "+selected+">"+data[2][a].detail_name+"</option>");
						// if(data[0][0].parent_id == 0 && data[2][a].id == data[0][0].tid){
						// 	$()
						// }

					}
				}
				if (data[0][0].parent_id != 0 ) {
					$('#editIndi .sub-indi').css('display', 'block');
					$('#editIndi #mov').val(data[0][0].mov);
				}
				$('.action-form #indiName').val(data[0][0].detail_name);
				$('.action-form #indiWeight').val(parseInt(data[0][0].weight));
				
			}
		});
	});

	$("#editIndi").on("submit", function(e){
		e.preventDefault();
		var id = $('#componentIndiId').val();
		var indiName = $('#editIndi #indiName').val();
		var indiKraName = $('#editIndi #indiKraName').val();
		var indiWeight = $('#editIndi #indiWeight').val();
		var parentIndi = $('#editIndi #parentIndi').val();
		var indiLopCode = $('#editIndi #indiLopCode').val();
		var mov = $('#mov').val();
		$.ajax({
		    type:'POST',
		    url: APP_URL+'/template/edit/indi',
		    data: 'id='+id+'&indi_name='+indiName+'&indi_kra_name='+indiKraName+'&indi_weight='+indiWeight+'&parent_indi='+parentIndi+'&mov='+mov,
		    success: function(data){
		    	location.reload();
		    }
		});   
	});
	$("#editKra").on("submit", function(e){
		e.preventDefault();
		var kra =  $('#editKra #kraName').val();
		var weight = $('#editKra #weight').val();
		var id = $('#editKra #componentId').val();
		$.ajax({
			type: 'POST',
			url: APP_URL+'/template/edit/kra',
			data: 'id='+id+'&kra_name='+kra+'&weight='+weight,
		success: function(data){
			location.reload();
		}
		});
	});
	$('#editLop').on('submit', function(e){
		e.preventDefault();
		var id = $('#editLop #id').val();
		var lopIndi = $('#editLop #lopIndi').val();
		var lopCode = $('#editLop #lopCode').val();
		var lopLet = $('#editLop #lopLet').val();
		var lopVal = $('#editLop #lopVal').val();
		var lopDes = $('#editLop #lopDes').val();
		$.ajax({
		    type:'POST',
		    data: 'id='+id+'&lop_indi='+lopIndi+'&lop_code='+lopCode+'&lop_let='+lopLet+'&lop_val='+lopVal+'&lop_des='+lopDes,
		    url: APP_URL+'/template/edit/lop',
		    success: function(data){
		    	location.reload();
		    },
		});
	});
	$('#componentLop .btn-action').click(function(){
		var id = $(this).attr('data-component');
		var action = $(this).attr('data-action');
		$.ajax({
			type: 'GET',
			url: APP_URL+'/template/component/lop',
			data: 'id='+id,
			success: function(data){
				var indiid = data[0].indiid;
				$('.action-form #lopIndi').val(data[0].detail_name);
				$('.action-form #lopCode').val(data[0].lop_code);
				$('.action-form #lopLet').val(data[0].lop_letter);
				$('.action-form #lopVal').val(data[0].lop_value);
				$('.action-form #lopDes').val(data[0].lop_description);
				if( action == 'view'){
					$('#editLop').css('display', 'none');
					$('#viewLop').css('display', 'block');
				}else{
					$('#viewLop').css('display', 'none');
					$('#editLop').css('display', 'block');
					$('#id').val(data[0].lopid);
					var headerId = $('#headerId').val();
					$.ajax({
						type: 'GET',
						url: APP_URL+'/template/fetch/indi',
						data: 'header_id='+headerId,
						success: function(data){
							for (var i = 0; i < data.length; i++) {
								if (data[i].id == indiid) {
									$('.action-form #lopIndi').append('<option selected class="appe-opt" value="'+data[i].id+'">'+data[i].detail_name+'</option>');
								}else{
									$('.action-form #lopIndi').append('<option class="appe-opt" value="'+data[i].id+'">'+data[i].detail_name+'</option>');
								}
								
							}
						}
					});
				}
			}
		});
	});


});