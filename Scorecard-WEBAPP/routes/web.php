<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Authentication*/
Auth::routes();

Route::get('/dashboard', 'HomeController@index')->name("home")->middleware('prereg');
Route::get('/out', 'HomeController@out')->name('out');
Route::get('/timeout', 'HomeController@timeout');
Route::get('/getSCStat', 'HomeController@getScorecardAnalytics');

/*Middleware start*/
Route::group(['middleware' => ['access', 'prereg']], function () {
/*Scorecard Routes*/
Route::get('/scorecard', 'ScorecardController@index')->name('main_scorecard');
Route::get('/scorecard/create/{id}/level/{level}', 'ScorecardController@add')->name('scorecard_view');
Route::get('/scorecard/create/{id}/level', 'ScorecardController@level');
Route::post('/scorecard/edit/update/{id}', 'ScorecardController@updateScorecard');
Route::post('/scorecard/create/add_scorecard', 'ScorecardController@add_scorecard');
Route::get('/scorecard/template_type', 'ScorecardController@tempType')->name('template_type');
Route::get('/scorecard/getScorecard', 'ScorecardController@getsc');
Route::get('/scorecard/chooseSchools', 'ScorecardController@chooseSchools');
Route::get('/scorecard/recoSchools/{pname}', 'ScorecardController@recoSchools');
Route::post('/scorecard/createBulkScorecard', 'ScorecardController@createBulkScorecard')->name('createBulkScorecards');
Route::post('/scorecard/createChosenScorecard', 'ScorecardController@createChosenScorecard')->name('chosenSchool');
// -- rafi-level scorecard
Route::post('/scorecard/createBulkScorecard2', 'ScorecardController@createBulkScorecard2')->name('createBulkScorecards2');
Route::post('/scorecard/createChosenScorecard2', 'ScorecardController@createChosenScorecard2')->name('chosenSchool2');
// -- end
Route::post('/scorecard/rafiLevelScore', 'ScorecardController@rafiLevelScore');
//live-search
Route::get('/scorecard/liveSearch', 'ScorecardController@liveSearch');
Route::get('/scorecard/paginateResult', 'ScorecardController@paginateResult');
//end live-search
Route::get('/scorecard/getRounds/{id}', 'ScorecardController@getRounds');
Route::get('/scorecard/modifyEval/{id}', 'ScorecardController@modifyEval');
Route::post('/scorecard/updateEvaluators', 'ScorecardController@updateEvaluators')->name('updateEvaluators');

/*Scorecard Actions*/
Route::get('/scorecard/view/{id}', 'ScorecardController@view')->name("view");
Route::get('/scorecard/edit/{id}', 'ScorecardController@edit');
Route::get('/scorecard/delete/{id}', 'ScorecardController@delete');
Route::post('/scorecard/assignEvaluator', 'ScorecardController@assignEvaluator');
Route::post('/scorecard/getEvaluators', 'ScorecardController@getEvaluators');
Route::post('/scorecard/publishScorecard', 'ScorecardController@publishScorecard');
Route::get('/scorecard/viewAllPublish', 'ScorecardController@viewAllPublish')->name("viewall");
Route::get('/scorecard/filterEvaluator', 'ScorecardController@filterEvaluator');

/*User Management Routes*/
Route::post('/usermanagement/update/changePass', 'MaintenanceController@changePass');
Route::get('/usercontrol', 'MaintenanceController@index')->name('usercontrol_view');
Route::get('/usermanagement', 'MaintenanceController@users')->name('usermanagement');
Route::post('/usermanagement/updateum', 'MaintenanceController@updateum');
Route::post('/usermanagement/addum', 'MaintenanceController@addum');
Route::get('/usercontrol/getmodules' , 'MaintenanceController@getModules');
Route::get('/usercontrol/getAccess/{id}', 'MaintenanceController@getAccess');
Route::get('/usercontrol/updateUserAccess/{id}', 'MaintenanceController@updateUserAccess');
Route::post('/usercontrol/addType', 'MaintenanceController@addType');
Route::post('/usermanagement/update/userstatus', 'MaintenanceController@updateumstatus');

/*Template Routes*/
Route::get('/template', 'TemplateController@index');
Route::get('/template/create', 'TemplateController@add')->name('template_add');
Route::post('/template/create', 'TemplateController@create')->name('template_create');
Route::get('/template/{id}/edit', 'TemplateController@edit')->name('template_edit');
Route::post('/template/create/kra', 'TemplateController@createKra')->name('kra_create');
Route::post('/template/create/lop', 'TemplateController@createLop');
Route::post('/template/create/indi', 'TemplateController@createIndi');
Route::get('/template/fetch/kra', 'TemplateController@fetchKra');
Route::get('/template/fetch/parent-indi', 'TemplateController@fetchParentIndi');
Route::get('/template/fetch/lop', 'TemplateController@fetchLop');
Route::get('/template/fetch/indi', 'TemplateController@fetchIndi');
Route::get('/template/component/kra', 'TemplateController@componentKra');
Route::post('/template/edit/kra', 'TemplateController@editKra')->name('kra_edit');
Route::post('/template/edit/indi', 'TemplateController@editIndi');
Route::get('/template/component/indi', 'TemplateController@componentIndi');
Route::get('/template/component/lop', 'TemplateController@componentLop');
Route::post('/template/edit/lop', 'TemplateController@editLop');
Route::get('/roundnames', 'TemplateController@roundnames');
Route::post('/roundnames/addround', 'TemplateController@addround');
Route::get('/templateTypes', 'TemplateController@templateTypes');
Route::post('/templateTypes/addTemp', 'TemplateController@addTemp');
Route::post('/templateTypes/updateTemp', 'TemplateController@updateTemp');

/*Reports*/
Route::get('/reports', 'ReportsController@preindex');
Route::get('/reports/index', 'ReportsController@index');
Route::get('/reports/indext', 'ReportsController@indext');
Route::post('/reports/recommend', 'ReportsController@recommend');
Route::post('/reports/recommend5', 'ReportsController@recommend5');
Route::post('/reports/top5', 'ReportsController@top5');
Route::get('/reports/scEvals/{scid}', 'ReportsController@scEvaluators');
Route::get('/reports/getSchoolByDist/{id}', 'ReportsController@getSchoolByDist');
Route::get('/reports/pdfArea', 'ReportsController@pdfArea');
//PDF Route
Route::get('/reports/pdfArea/pdfView', 'ReportsController@pdfView');
// Route::get('/reports/pdfArea/pdfview','pdfController@pdfview')->name('pdfview');
/*Other Maintenance*/
// area as Division
Route::get('/district/liveSearchDistrict', 'OtherMaintenanceController@liveSearchDistrict');
Route::get('/schools/liveSearchSchool', 'OtherMaintenanceController@liveSearchSchool');
Route::get('/schools', 'OtherMaintenanceController@schools');
Route::post('/schools/addschool', 'OtherMaintenanceController@addschool');
Route::post('/schools/editSchool', 'OtherMaintenanceController@editSchool');
Route::get('/region', 'OtherMaintenanceController@region');
Route::post('/region/addRegion', 'OtherMaintenanceController@addRegion');
Route::post('/region/editRegion', 'OtherMaintenanceController@editRegion');
Route::get('/province', 'OtherMaintenanceController@province');
Route::post('/province/addProvince', 'OtherMaintenanceController@addProvince');
Route::post('/province/editProvince', 'OtherMaintenanceController@editProvince');
Route::get('/municipality', 'OtherMaintenanceController@municipality');
Route::post('/municipality/addmunicipality', 'OtherMaintenanceController@addMunicipality');
Route::post('/municipality/editMunicipality', 'OtherMaintenanceController@editmunicipality');
Route::get('/area', 'OtherMaintenanceController@areas');
Route::post('/area/addarea', 'OtherMaintenanceController@addarea');
Route::post('/area/editArea', 'OtherMaintenanceController@editArea');
Route::get('/district', 'OtherMaintenanceController@district');
Route::post('/district/adddistrict', 'OtherMaintenanceController@addDistrict');
Route::post('/district/editDistrict', 'OtherMaintenanceController@editDistrict');
Route::get('/template/frans', 'TemplateController@componentIndi');

//Evaluator
Route::get('/evaluator', 'OtherMaintenanceController@evaluatorMaintenance');
Route::post('/evaluator/addOCPEval', 'OtherMaintenanceController@addOCPEval');
Route::get('/evaluator/viewOCPAltEval/{id}', 'OtherMaintenanceController@viewOCPAltEval');
/*Datatables*/

/*dccenter*/
Route::get('/dccenter', 'OtherMaintenanceController@dccenter');
Route::post('/dccenter/adddccenter', 'OtherMaintenanceController@addcenter');
Route::get('/barangay', 'OtherMaintenanceController@barangay');
Route::post('/dccenter/addbarangay', 'OtherMaintenanceController@addbarangay');

});